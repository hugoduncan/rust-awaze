//! A crate for task automation
#![feature(collections, custom_attribute, custom_derive, plugin)]

#![cfg_attr(test, deny(warnings))]
#![deny(missing_docs)]
#![plugin(serde_macros)]
#![plugin(regex_macros)]

extern crate handlebars;
extern crate rustc_serialize;
#[macro_use] extern crate log;

extern crate serde;
extern crate regex;

pub use ::generate::generate;

mod utils;
mod error;
mod ast;
mod descriptor;
mod generate;
