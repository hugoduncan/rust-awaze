//! An "AST" for codegen
use std::collections::BTreeMap;
use std::collections::HashSet;
use rustc_serialize::json;

#[derive(Debug, RustcEncodable)]
pub struct Mod {
    pub doc: String,
    pub name: String,
    pub structs: Vec<Struct>,
    pub lists: Vec<List>,
    pub enums: Vec<Enum>,
    pub types: Vec<Type>,
}

#[derive(Debug, RustcEncodable)]
pub struct ModInfo {
    pub module: String,
    pub uses: HashSet<String>,
}

impl ModInfo {
    pub fn new(mod_name: String) -> ModInfo {
        ModInfo {
            module: mod_name,
            uses: HashSet::new()
        }
    }
}

#[derive(Debug, RustcEncodable)]
pub struct Struct {
    pub doc: String,
    pub name: String,
    pub fields: Vec<Field>,
    pub mod_info: ModInfo,
    pub struct_impl: Impl,
    pub derives: Vec<String>,
}

#[derive(Debug, RustcEncodable)]
pub struct Field {
    pub doc: String,
    pub name: String,
    pub ext_ser_name: Option<String>,
    pub ext_de_name: Option<String>,
    pub type_string: String,
}

#[derive(Debug, RustcEncodable)]
pub struct Impl {
    pub methods: Vec<Method>,
}


#[derive(Debug, RustcEncodable)]
pub struct Method {
    pub doc: String,
    pub name: String,
    pub args: Vec<Arg>,
    pub generics: Option<String>,
    pub method_type: MethodType,
    pub type_string: String,
    pub body: String,
}

#[derive(Debug, RustcEncodable)]
pub enum MethodType {
    // ImmutSelf,
    MutSelf,
    // Static
}

#[derive(Debug, RustcEncodable)]
pub struct List {
    pub doc: String,
    pub name: String,
    pub mod_info: ModInfo,
    pub type_string: String,
}

#[derive(Debug, RustcEncodable)]
pub struct Type {
    pub doc: String,
    pub name: String,
    pub mod_info: ModInfo,
    pub type_string: String,
}

#[derive(Debug, RustcEncodable)]
pub struct Enum {
    pub doc: String,
    pub name: String,
    pub mod_info: ModInfo,
    pub variants: Vec<String>,
    pub values: Vec<String>,
    pub derives: Vec<String>,
}

// #[derive(Debug, RustcEncodable)]
// pub enum HttpMethod {
//     Get,
//     Post,
//     // Put,
//     // Delete,
// }

#[derive(Debug, RustcEncodable)]
pub struct Arg {
    pub name: String,
    pub type_string: String,
}

impl json::ToJson for Mod {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("structs".to_string(), self.structs.to_json());
        m.insert("lists".to_string(), self.lists.to_json());
        m.insert("enums".to_string(), self.enums.to_json());
        m.insert("types".to_string(), self.types.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Struct {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("fields".to_string(), self.fields.to_json());
        m.insert("mod_info".to_string(), self.mod_info.to_json());
        m.insert("struct_impl".to_string(), self.struct_impl.to_json());
        m.insert("derives".to_string(), self.derives.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Field {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("ext_ser_name".to_string(), self.ext_ser_name.to_json());
        m.insert("ext_de_name".to_string(), self.ext_de_name.to_json());
        m.insert("type_string".to_string(), self.type_string.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Impl {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("methods".to_string(), self.methods.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Method {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("args".to_string(), self.args.to_json());
        m.insert("type_string".to_string(), self.type_string.to_json());
        m.insert("generics".to_string(), self.generics.to_json());
        m.insert("body".to_string(), self.body.to_json());
        m.insert("method_type".to_string(), self.method_type.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for MethodType {
    fn to_json(&self) -> json::Json {
        let s = match *self {
            // MethodType::ImmutSelf => "&self,",
            MethodType::MutSelf => "&mut self,",
            // MethodType::Static => "",
        };
        s.to_json()
    }
}

impl json::ToJson for Arg {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("name".to_string(), self.name.to_json());
        m.insert("type_string".to_string(), self.type_string.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for List {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("type_string".to_string(), self.type_string.to_json());
        m.insert("mod_info".to_string(), self.mod_info.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Type {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("type_string".to_string(), self.type_string.to_json());
        m.insert("mod_info".to_string(), self.mod_info.to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for Enum {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("doc".to_string(), self.doc.to_json());
        m.insert("name".to_string(), self.name.to_json());
        m.insert("variants".to_string(), self.variants.to_json());
        m.insert("derives".to_string(), self.derives.to_json());
        m.insert("mod_info".to_string(), self.mod_info.to_json());
        let n = self.variants.len();
        m.insert("pairs".to_string(),
                 self.variants.iter()
                 .map(|v| format!("{}::{}", self.name, v))
                 .zip(self.values.iter())
                 .enumerate()
                 .map(|(i,(var,val))| {
                     let mut m = BTreeMap::new();
                     if i!=n-1 {
                         m.insert("comma".into(), true.to_json());
                     }
                     m.insert("variant".into(), var.to_json());
                     m.insert("value".into(), val.to_json());
                     json::Json::Object(m)
                 })
                 .collect::<Vec<_>>()
                 .to_json());
        json::Json::Object(m)
    }
}

impl json::ToJson for ModInfo {
    fn to_json(&self) -> json::Json {
        let mut m = BTreeMap::new();
        m.insert("module".to_string(), self.module.to_json());
        // HashSet doesn't implement ToJson
        let mut v = self.uses.iter().map(|x|x.clone()).collect::<Vec<_>>();
        v.sort();
        m.insert("uses".to_string(), v.to_json());
        json::Json::Object(m)
    }
}

// impl json::ToJson for HttpMethod {
//     fn to_json(&self) -> json::Json {
//         let m = match *self {
//             HttpMethod::Get => "Method::Get",
//             HttpMethod::Post => "Method::Post",
//             // HttpMethod::Put => "Method::Put",
//             // HttpMethod::Delete => "Method::Delete",
//         };
//         m.to_json()
//     }
// }
