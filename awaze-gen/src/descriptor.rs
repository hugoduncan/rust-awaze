//! Service descriptor
use std::borrow::ToOwned;
use std::collections::HashMap;
use std::collections::BTreeMap;

use serde::{Deserialize, Deserializer};

use ::utils::*;

//----------------------------------------------------------------------

/// Api descriptor
#[derive(Debug,Deserialize)]
pub struct Api {
    /// API version
    pub version: String,
    pub metadata: ServiceMetadata,
    pub operations: BTreeMap<String, Operation>,
    pub shapes: BTreeMap<String, Shape>
}


/// Service Metadata
#[derive(Debug,Deserialize)]
pub struct ServiceMetadata {
    #[serde(rename="apiVersion")]
    pub api_version: String,
    #[serde(rename="endpointPrefix")]
    pub endpoint_prefix: String,
    #[serde(rename="serviceAbbreviation")]
    pub service_abbreviation: String,
    #[serde(rename="serviceFullName")]
    pub service_full_name: String,
    #[serde(rename="signatureVersion")]
    pub signature_version: String,
    #[serde(rename="xmlNamespace")]
    pub xml_namespace: String,
    pub protocol: String
}


/// An Operation
#[derive(Debug,Deserialize)]
pub struct Operation {
    pub name: String,
    pub http: Http,
    pub input: ShapeRef,
    #[serde(default)]
    pub output: Option<ShapeRef>,
}


/// An Operation Http Type
#[derive(Debug,Deserialize)]
pub struct Http {
    pub method: String,
    #[serde(rename="requestUri")]
    pub request_uri: String,
}


/// A Shape reference
#[derive(Clone, Debug, Deserialize)]
pub struct ShapeRef {
    pub shape: String,
    #[serde(default,rename="locationName")]
    pub location_name: Option<String>,
    #[serde(default,rename="queryName")]
    pub query_name: Option<String>,
}

impl ShapeRef {
    /// Name used when parsing responses
    pub fn location_name(&self, default: &str) -> String {
        self.location_name.as_ref()
            .map(|x| x.clone())
            .unwrap_or(default.to_owned())
    }

    /// Name used when constructing query parameters for requests
    pub fn query_name(&self, default: &str) -> String {
        self.query_name.as_ref()
            .map(|x| x.clone())
            .unwrap_or(
                self.location_name.as_ref()
                    .map(|x| capitalise(x))
                    .unwrap_or(default.to_owned()))
    }
}

/// A Shape
#[derive(Debug,Deserialize)]
pub struct Shape
{
    #[serde(rename="type")]
    pub shape_type: ShapeType, // "structure"
    #[serde(default)]
    pub required: Vec<String>,
    #[serde(default)]
    pub members: Option<BTreeMap<String, ShapeRef>>,
    #[serde(default)]
    pub member: Option<ShapeRef>,
    #[serde(default,rename="enum")]
    pub enum_vals: Option<Vec<String>>,
}

/// An Attribute type
#[derive(Debug)]
pub enum ShapeType {
    Structure,
    List,
    String,
    Blob,
    Boolean,
    Timestamp,
    Double,
    Float,
    Integer,
    Long,
}

struct ShapeTypeVisitor;

impl ShapeTypeVisitor {
    fn from(s: &str) -> ShapeType {
        match s {
            "structure" => ShapeType::Structure,
            "list" => ShapeType::List,
            "string" => ShapeType::String,
            "blob" => ShapeType::Blob,
            "boolean" => ShapeType::Boolean,
            "timestamp" => ShapeType::Timestamp,
            "double" => ShapeType::Double,
            "float" => ShapeType::Float,
            "integer" => ShapeType::Integer,
            "long" => ShapeType::Long,
            _ => panic!("Unknown type"),
       }
    }
}

impl ::serde::de::Visitor for ShapeTypeVisitor {
    type Value = ShapeType;

    fn visit_str<E>(&mut self, v: &str) -> Result<ShapeType, E>
        where E: ::serde::de::Error,
    {
        Ok(ShapeTypeVisitor::from(&v.to_string()))
    }

    fn visit_string<E>(&mut self, v: String) -> Result<ShapeType, E>
        where E: ::serde::de::Error,
    {
        Ok(ShapeTypeVisitor::from(&v))
    }
}

impl Deserialize for ShapeType {
    fn deserialize<D>(deserializer: &mut D) -> Result<ShapeType, D::Error>
        where D: Deserializer,
    {
        deserializer.visit(ShapeTypeVisitor)
    }

}

//----------------------------------------------------------------------


/// A type for docs
#[derive(Debug,Deserialize)]
pub struct Docs {
    pub version: String,
    pub operations: HashMap<String, String>,
    pub service: String,
    pub shapes: HashMap<String, ShapeDoc>
}

impl Docs {
    pub fn for_shape(&self, shape: &str) -> String {
        self.shapes
            .get(&shape.to_owned())
            .map(|sdoc|
                 sdoc.base.as_ref()
                 .map(|s| s.clone())
                 .unwrap_or("".into()))
            .unwrap_or("".into())
    }
    pub fn for_operation(&self, op: &str) -> String {
        self.operations
            .get(op)
            .map(|s| &s[..])
            .unwrap_or("")
            .to_owned()
    }
}

/// A type for shape docs
#[derive(Debug,Deserialize)]
pub struct ShapeDoc {
    pub base: Option<String>,
    pub refs: HashMap<String, Option<String>>
}

//----------------------------------------------------------------------

// TODO: paginators, resources, waiters

//----------------------------------------------------------------------


/// Service Endpoints
#[derive(Debug,Deserialize)]
pub struct Endpoints {
    pub version: String,
    pub endpoints: HashMap<String, Endpoint>
}

/// Service Endpoints
#[derive(Debug,Deserialize)]
pub struct Endpoint {
    pub endpoint: String,
    #[serde(rename="signatureVersion")]
    pub signature_version: String
}

//----------------------------------------------------------------------

/// Overrides
#[derive(Debug,Deserialize)]
pub struct Overrides {
    pub shapes: HashMap<String, ShapeOverride>,
}

/// ShapeOverride
#[derive(Debug,Deserialize)]
pub struct ShapeOverride {
    #[serde(default)]
    pub rename: HashMap<String, String>,
    #[serde(default)]
    pub locations: HashMap<String, String>,
    #[serde(default)]
    pub variants: Option<Vec<String>>,
    #[serde(default)]
    pub required: Option<Vec<String>>,
}

impl ShapeOverride {
    pub fn field(&self, name: &str) -> String {
        self.rename.get(name)
            .map(|x| x.to_owned())
            .unwrap_or(name.to_owned())
    }

    pub fn location(&self, name: &str) -> Option<String> {
        self.locations.get(name)
            .map(|x| x.to_owned())
    }
}
