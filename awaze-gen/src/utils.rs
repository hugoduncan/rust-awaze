//! Utils used across other modules.

pub fn capitalise(arg: &str) -> String {
    let mut u = arg.chars().nth(0).map(|c| c.to_uppercase()).map(|cs| cs.collect()).unwrap_or(String::new());
    let r = arg.chars().skip(1).collect::<String>();
    u.push_str(&r);
    u
}

pub fn underscore(arg: &str) -> String {
    let re1 = regex!(r"(.)([A-Z][a-z]+)");
    let re2 = regex!(r"([a-z0-9])([A-Z])");
    // "$1_$2" Seems to trigger a bug
    let s1 = re1.replace_all(arg, "$1-$2");
    let s2 = re2.replace_all(&s1, "$1-$2");
    s2.to_lowercase().replace("-","_")
}

pub fn camel_case(arg: &str) -> String {
    arg.split(|c| c=='-' || c=='_' || c=='.' || c==' ')
        .map(capitalise)
        .fold(String::new(),
              |mut r, s| { r.push_str(&s); r } )
}
