//! Generate APIs

use std::borrow::ToOwned;
use std::collections::BTreeMap;
use std::convert::AsRef;
use std::fs;
use std::io::Write;
use std::path::Path;

use handlebars::Handlebars;
use serde::json;

use ::error::*;
use ::ast::*;
use ::descriptor::*;
use ::utils::*;

/// Generate an API
pub fn generate<P, Q, R>(out_path: P, model_path: Q,
                         override_path: R) -> Result<(), Error>
    where P: AsRef<Path>, Q: AsRef<Path>, R: AsRef<Path>
{
    let model = try!(load_model(&model_path));
    println!("Read model");
    let docs = try!(load_docs(&model_path));
    println!("Read docs");
    let overrides = try!(load_overrides(&override_path,
                                        &model.metadata.endpoint_prefix));
    println!("Read overrides");

    let module = try!(transform(model, docs, overrides));
    println!("Generated module data");
    let ctxt = try!(Context::new());
    try!(output(ctxt, out_path.as_ref(), module));
    println!("Module output");
    Ok(())
}

fn load_model<P>(model_path: P) -> Result<Api, Error>
    where P: AsRef<Path>
{
    for e in try!(fs::read_dir(model_path)) {
        let entry = try!(e);
        debug!("Trying to load model from {:?}", entry.path());
        let model = json::de::from_reader(try!(fs::File::open(entry.path())));
        trace!("model {:?}", model);
        match model {
            Ok(model) => return Ok(model),
            Err(err) => {
                warn!("Could not load model from {:?}: {}", entry.path(), err);
            }
        }
    }
    Err(Error::NoModelFound)
}

fn load_docs<P>(model_path: P) -> Result<Docs, Error>
    where P: AsRef<Path>
{
    for e in try!(fs::read_dir(model_path)) {
        let entry = try!(e);
        debug!("Trying to load description from {:?}", entry.path());
        let docs = json::de::from_reader(try!(fs::File::open(entry.path())));
        trace!("model {:?}", docs);
        match docs {
            Ok(docs) => return Ok(docs),
            Err(err) => {
                warn!("Could not load docs from {:?}: {}", entry.path(), err);
            }
        }
    }
    Err(Error::NoModelFound)
}

fn load_overrides<P>(override_path: P, name: &str) -> Result<Overrides, Error>
    where P: AsRef<Path>
{
    let path = override_path.as_ref().join(format!("{}.json", name));
    debug!("Trying to load overrides from {:?}", path);
    let overrides = json::de::from_reader(try!(fs::File::open(&path)));
    trace!("overrides {:?}", overrides);
    match overrides {
        Ok(overrides) => return Ok(overrides),
        Err(err) => {
            warn!("Could not load overrides from {:?}: {}", path, err);
            Err(Error::CouldNotReadOverrides(Box::new(err)))
            // Ok(Overrides { shapes: HashMap::new() })
        }
    }
}


static MOD: &'static str = "
//! {{doc}}
#![feature(custom_attribute, custom_derive, plugin)]

#![plugin(serde_macros)]

#[macro_use] extern crate log;
#[macro_use] extern crate awaze_core;
extern crate hyper;
extern crate serde;

{{#each structs}}pub use {{mod_info.module}}::*;
{{/each}}
{{#each lists}}pub use {{mod_info.module}}::*;
{{/each}}
{{#each enums}}pub use {{mod_info.module}}::*;
{{/each}}
{{#each types}}pub use {{mod_info.module}}::*;
{{/each}}

{{#each structs}}mod {{mod_info.module}};
{{/each}}
{{#each lists}}mod {{mod_info.module}};
{{/each}}
{{#each enums}}mod {{mod_info.module}};
{{/each}}
{{#each types}}mod {{mod_info.module}};
{{/each}}
";

static IMPL: &'static str = "
impl {{name}} {
  {{#with struct_impl}}{{#each methods}}{{> method}}{{/each}}{{/with}}
}";

static METHOD: &'static str = "
{{#if doc}}/// {{{doc}}}{{/if}}
pub fn {{name}}{{#if generics}}<{{generics}}>{{/if}}({{{method_type}}}{{#each args}}{{> arg}},{{/each}}) -> Result<{{type_string}}, Error> {
    {{{body}}}
}";

static ARG: &'static str = "{{name}}: {{{type_string}}}";

static STRUCT: &'static str = "
{{#each mod_info.uses}}use {{this}};
{{/each}}

{{#if doc}}/// {{{doc}}}{{/if}}
#[derive(Debug{{#each derives}}, {{this}}{{/each}})]
pub struct {{name}} {
{{#each fields}}{{> field}}{{/each}}
}

{{#if struct_impl.methods}}{{> impl}}{{/if}}
";

static FIELD: &'static str =
    "{{#if doc}}    /// {{{doc}}}{{/if}}
{{#if ext_de_name}}    #[serde(rename_deserialize=\"{{{ext_de_name}}}\", rename_serialize=\"{{{ext_ser_name}}}\")]{{/if}}
    pub {{{name}}}: {{{type_string}}},";

static LIST: &'static str = "
use awaze_core::ItemVec;
{{#each mod_info.uses}}use {{this}};
{{/each}}

{{#if doc}}/// {{{doc}}}{{/if}}
pub type {{name}} = ItemVec<{{type_string}}>;
";

static TYPE: &'static str = "
{{#each mod_info.uses}}use {{this}};
{{/each}}

{{#if doc}}/// {{{doc}}}{{/if}}
pub type {{name}} = {{type_string}};
";

static ENUM: &'static str = "
{{#each mod_info.uses}}use {{this}};
{{/each}}

{{#if doc}}/// {{{doc}}}{{/if}}
#[derive(Debug{{#each derives}}, {{this}}{{/each}})]
pub enum {{name}} {
   {{#each variants}}{{this}},{{/each}}
}

string_enum!{ {{name}},
{{#each pairs}}    {{variant}} => \"{{value}}\" => {{variant}}{{#if comma}},{{/if}}
{{/each}}
}";


struct Context {
    handlebars: Handlebars,
}

impl Context {
    fn new() -> Result<Context, Error> {
        let mut hbars = Handlebars::new();
        try!(hbars.register_template_string("mod", MOD.into()));
        try!(hbars.register_template_string("impl", IMPL.into()));
        try!(hbars.register_template_string("method", METHOD.into()));
        try!(hbars.register_template_string("arg", ARG.into()));
        try!(hbars.register_template_string("struct", STRUCT.into()));
        try!(hbars.register_template_string("field", FIELD.into()));
        try!(hbars.register_template_string("list", LIST.into()));
        try!(hbars.register_template_string("enum", ENUM.into()));
        try!(hbars.register_template_string("type", TYPE.into()));
        Ok(Context {
            handlebars: hbars
        })
    }
}

fn transform(mut model: Api, docs: Docs, overrides: Overrides)
             -> Result<Mod, Error>
{
    let mut module = Mod {
        name: model.metadata.endpoint_prefix.to_owned(),
        doc: docs.service.clone(),
        structs: vec![],
        lists: vec![],
        enums: vec![],
        types: vec![],
    };

    let service_struct = service_struct(&mut model, &docs);
    module.structs.push(service_struct);

    for (name, shape) in model.shapes.iter() {
        let oride = overrides.shapes.get(name);
        let doc = docs.for_shape(&name);
        let name = result_to_response(name);
        trace!("Override {:?} {:?}", name, oride);
        match shape.shape_type {
            ShapeType::Structure => {
                let mut s = Struct {
                    doc: doc,
                    name: name.to_owned(),
                    mod_info: ModInfo::new(underscore(&name)),
                    fields: vec![],
                    struct_impl: Impl {
                        methods: vec![]
                    },
                    derives: vec!["Serialize".into(),
                                  "Deserialize".into(),
                                  // "Default".into()
                                  ]
                };

                let required = &shape.required;
                let required = oride
                    .map(|o| o.required.as_ref().unwrap_or(required))
                    .unwrap_or(required);

                shape.members.as_ref().map(|ms| {
                    ms.iter().fold((), |_, (m, sr)| {
                        let name = (&*m).to_owned();
                        let name = oride
                            .map(|o|
                                 o.rename.get(&name)
                                 .map(|x| x.to_owned())
                                 .unwrap_or(name.to_owned()))
                            .unwrap_or(name.to_owned());

                        let ser_name =
                            oride.map(|o| o.location(&name)).unwrap_or(None)
                            .unwrap_or(sr.query_name(&name));

                        let de_name =
                            oride.map(|o| o.location(&name)).unwrap_or(None)
                            .unwrap_or(sr.location_name(&name));

                        let field_type = type_name_for_shape(&sr.shape);
                        s.fields.push( Field {
                            doc: "".into(), // TODO: lookup doc
                            name: underscore(&name),
                            ext_ser_name: Some(ser_name.to_owned()),
                            ext_de_name: Some(de_name.to_owned()),
                            type_string: if required.iter().any(|x| *x == name)
                            {
                                field_type.to_owned()
                            } else {
                                format!("Option<{}>", field_type)
                            },
                        });
                        if !is_primitive(&field_type) {
                            s.mod_info.uses.insert(
                                format!("::{}::*", underscore(&field_type)));
                        };
                    })
                });
                // Add a requestId field into result structures.
                // This seems a bit of a hack.
                if name.ends_with("Response") {
                    s.fields.push( Field {
                        doc: "".into(), // TODO: lookup doc
                        name: "request_id".into(),
                        ext_ser_name: Some("requestId".into()),
                        ext_de_name: Some("requestId".into()),
                        type_string: "String".into(),
                    })
                }
                module.structs.push(s);
            },
            ShapeType::List => {
                let field_type = type_name_for_shape(
                    &shape.member.as_ref().unwrap().shape);
                let mut l = List {
                    doc: docs.for_shape(&name),
                    name: name.to_owned(),
                    mod_info: ModInfo::new(underscore(&name)),
                    type_string: field_type.to_owned(),
                };
                if !is_primitive(&field_type) {
                    l.mod_info.uses.insert(
                        format!("::{}::*", underscore(&field_type)));
                };
                module.lists.push(l);
            },
            ShapeType::Blob => {
                let l = List {
                    doc: docs.for_shape(&name),
                    name: name.to_owned(),
                    mod_info: ModInfo::new(underscore(&name)),
                    type_string: "u8".into(),
                };
                module.lists.push(l);
            },
            ShapeType::String => {
                debug!("String {:?} {:?}", name, shape);
                if name!="String" {
                    let vals = shape.enum_vals.as_ref().unwrap().to_owned();
                    let variants = vals.iter()
                        .map(|x| camel_case(&x))
                        .collect();
                    let e = Enum {
                        doc: docs.for_shape(&name),
                        name: name.to_owned(),
                        mod_info: ModInfo::new(underscore(&name)),
                        variants: match oride {
                            None => variants,
                            Some(ref oride) =>
                                oride.variants.as_ref()
                                .map(|s| s.to_owned())
                                .unwrap_or(variants)
                        },
                        values: vals,
                        derives: vec!["Clone".into()]
                    };
                    module.enums.push(e);
                }
            },
            ShapeType::Double => {
                warn!("Ignoring shape type {:?}", shape);
            },
            ShapeType::Float => {
                warn!("Ignoring shape type {:?}", shape);
            },
            ShapeType::Integer => {
                warn!("Ignoring shape type {:?}", shape);
            },
            ShapeType::Long => {
                warn!("Ignoring shape type {:?}", shape);
            },
            ShapeType::Timestamp => {
                let l = Type {
                    doc: docs.for_shape(&name),
                    name: name.to_owned(),
                    mod_info: ModInfo::new(underscore(&name)),
                    type_string: "u64".into(),
                };
                module.types.push(l);
            },
            ShapeType::Boolean => {
                warn!("Ignoring shape type {:?}", shape);
            },
           // _ => {
           //      debug!("Unimplemented shape type {:?}", shape);
           //      panic!("Unimplemented shape type");
           //  }
        }
    }

    Ok(module)
}

fn service_struct(model: &mut Api, docs: &Docs) -> Struct {
        let mut service_struct = Struct {
        doc: docs.service.clone(),
        name: capitalise(&model.metadata.endpoint_prefix),
        mod_info: ModInfo::new(underscore(&model.metadata.endpoint_prefix)),
        fields: vec![Field {
            doc: "AWS connection".into(),
            name: "conn".into(),
            type_string: "Aws".into(),
            ext_ser_name: None,
            ext_de_name: None,
        }],
        struct_impl: Impl {
            methods: vec![Method{
                doc: "Make a request for the service".into(),
                name: "request".into(),
                generics: Some("I: Serialize + ::std::fmt::Debug".into()),
                method_type: MethodType::MutSelf,
                args: vec![
                    Arg {
                        name: "method".into(),
                        type_string: "Method".into(),
                    },
                    Arg {
                        name: "uri".into(),
                        type_string: "&str".into(),
                    },
                    Arg {
                        name: "action".into(),
                        type_string: "&str".into(),
                    },
                    Arg {
                        name: "arg".into(),
                        type_string: "&I".into(),
                    }],
                type_string: "String".into(),
                body: format!(
                    "self.conn.request(method, \"{}\", \"{}\", uri, action, arg)",
                    model.metadata.endpoint_prefix,
                    model.metadata.api_version)
            }]
        },
        derives: vec![]
    };
    service_struct.mod_info.uses.insert("awaze_core::*".into());
    service_struct.mod_info.uses.insert("hyper::method::Method".into());
    service_struct.mod_info.uses.insert("serde::xml::de".into());
    service_struct.mod_info.uses.insert("serde::Serialize".into());

    for (_, op) in model.operations.iter() {
        let in_type = result_to_response(&op.input.shape);

        let mut output = op.output.clone();
        if output.is_none() {
            let shape_name = in_type.replace("Request", "Result");
            output = Some(
                ShapeRef {
                    shape: shape_name.clone(),
                    location_name: None,
                    query_name: None,
                });
            let mut m = BTreeMap::new();
            m.insert("result".into(),
                     ShapeRef {
                         shape: "Boolean".into(),
                         location_name: Some("return".into()),
                         query_name: None,
                     });
            model.shapes.insert(
                shape_name,
                Shape {
                    shape_type: ShapeType::Structure,
                    required: vec!["result".into()],
                    members: Some(m),
                    member: None,
                    enum_vals: None,
            });
        }

        let out_type = result_to_response(
            &output.as_ref()
                .map(|o| o.shape.clone())
                .expect("Internal error for result type"));

        trace!("{} {} -> {}", op.name, in_type, out_type);
        let method = Method {
            doc: docs.for_operation(&op.name),
            name: underscore(&op.name),
            args: vec![Arg{
                name: "arg".into(),
                type_string: format!("&{}", in_type),
            },],
            type_string: out_type.to_owned(),
            method_type: MethodType::MutSelf,
            generics: None,
            body: {
                let method = match &op.http.method[..] {
                    "POST" => "Method::Post",
                    "GET" => "Method::Get",
                    _ => {
                        println!("Unsupported method {}.", op.http.method);
                        panic!("Unsupported http method");
                    }
                };
                let uri = op.http.request_uri.clone();
                format!("
 let resp = try!(self.request({}, \"{}\", \"{}\", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)",
                        method, uri, op.name)
            }
        };
        service_struct.mod_info.uses.insert(
            format!("::{}::*", underscore(&in_type)));
        if "()" != &out_type[..] {
            service_struct.mod_info.uses.insert(
                format!("::{}::*", underscore(&out_type)));
        }
        service_struct.struct_impl.methods.push(method);
    }
    service_struct
}

fn is_primitive(f: &str) -> bool {
    match &underscore(f)[..] {
        "bool"|"i32"|"i64"|"f32"|"f64"|"string" => true,
        _ => false
    }
}

fn output(ctxt: Context, path: &Path, module: Mod) -> Result<(), Error> {
    // let path = path.to_path_buf();
    let mod_path = path.join(&module.name);
    let src_path = mod_path.join("src");
    try!(fs::create_dir_all(&src_path));
    debug!("Output module");
    try!(output_mod(&ctxt, &src_path, &module));
    debug!("Output structs");
    for s in module.structs.iter() {
        try!(output_struct(&ctxt, &src_path, s));
    }
    debug!("Output lists");
    for list in module.lists.iter() {
        try!(output_list(&ctxt, &src_path, list));
    }
    debug!("Output type aliases");
    for typ in module.types.iter() {
        try!(output_type(&ctxt, &src_path, typ));
    }
    debug!("Output enums");
    for enm in module.enums.iter() {
        try!(output_enum(&ctxt, &src_path, enm));
    }
    debug!("Output done");
    Ok(())
}

fn output_mod(ctxt: &Context, path: &Path, module: &Mod) -> Result<(), Error> {
    trace!("Ouptut module: {:?}", module);
    let mut f = try!(fs::File::create(path.join(format!("lib.rs"))));
    let s = try!(ctxt.handlebars.render("mod", module));
    try!(f.write_fmt(format_args!("{}", s)));
    Ok(())
}

fn output_struct(ctxt: &Context, path: &Path, s: &Struct) -> Result<(), Error> {
    trace!("Ouptut struct: {:?}", s);
    let p = path.join(format!("{}.rs", s.mod_info.module));
    debug!("Ouptut struct to: {:?}", p);
    let mut f = try!(fs::File::create(p));
    let s = try!(ctxt.handlebars.render("struct", s));
    try!(f.write_fmt(format_args!("{}", s)));
    Ok(())
}

fn output_list(ctxt: &Context, path: &Path, list: &List) -> Result<(), Error> {
    trace!("Ouptut list: {:?}", list);
    let mut f = try!(fs::File::create(
        path.join(format!("{}.rs", list.mod_info.module))));
    let s = try!(ctxt.handlebars.render("list", list));
    try!(f.write_fmt(format_args!("{}", s)));
    Ok(())
}

fn output_type(ctxt: &Context, path: &Path, typ: &Type) -> Result<(), Error> {
    trace!("Ouptut type alias: {:?}", typ);
    let mut f = try!(fs::File::create(
        path.join(format!("{}.rs", typ.mod_info.module))));
    let s = try!(ctxt.handlebars.render("type", typ));
    try!(f.write_fmt(format_args!("{}", s)));
    Ok(())
}

fn output_enum(ctxt: &Context, path: &Path, enm: &Enum) -> Result<(), Error> {
    trace!("Ouptut enum: {:?}", enm);
    let mut f = try!(fs::File::create(
        path.join(format!("{}.rs", enm.mod_info.module))));
    let s = try!(ctxt.handlebars.render("enum", enm));
    try!(f.write_fmt(format_args!("{}", s)));
    Ok(())
}

/// Rename ...Result to ...Response
///
/// The XML returned seems to contain ...Response names.
fn result_to_response(name: &str) -> String {
    let re=regex!("Result$");
    re.replace(name, "Response")
}


fn type_name_for_shape(shape: &str) -> String {
    match shape {
        "Integer" => "i32".into(),
        "Long" => "i64".into(),
        "Boolean" => "bool".into(),
        "Float" => "f32".into(),
        "Double" => "f64".into(),
        "Timestamp" => "u64".into(),
        _ => result_to_response(shape)
    }
}


#[cfg(test)]
mod test {
    use ::super::{camel_case, underscore};

    #[test]
    fn test_underscore() {
        assert_eq!("camel_case",&underscore("CamelCase"));
        assert_eq!("camel_camel_case",&underscore("CamelCamelCase"));
        assert_eq!("camel2_camel2_case",&underscore("Camel2Camel2Case"));
        assert_eq!("get_http_response_code",&underscore("getHTTPResponseCode"));
        assert_eq!("get2_http_response_code",
                   &underscore("get2HTTPResponseCode"));
        assert_eq!("http_response_code",&underscore("HTTPResponseCode"));
        assert_eq!("http_response_code_xyz",&underscore("HTTPResponseCodeXYZ"));
    }

    #[test]
    fn test_camel_ae() {
        assert_eq!("I386",&camel_case("i386"));
        assert_eq!("X8664",&camel_case("x86_64"));
    }
}
