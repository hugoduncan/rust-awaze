//! Generate an AWS module from botocore json service descriptors

extern crate docopt;
extern crate env_logger;
extern crate rustc_serialize;

extern crate awaze_gen;

use std::process;

use docopt::Docopt;
use awaze_gen::*;

static USAGE : &'static str = "
Usage:  gen <out-path> <model-path> <override-path>
";

#[derive(RustcDecodable, Debug)]
struct Args {
    arg_out_path: String,
    arg_model_path: String,
    arg_override_path: String,
}

fn main() {
    env_logger::init().unwrap();
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|e| e.exit());

    match generate(args.arg_out_path,
                   args.arg_model_path,
                   args.arg_override_path) {
        Ok(()) => (),
        Err(e) => {
            println!("Failed: {}", e);
            process::exit(1)
        }
    }
}
