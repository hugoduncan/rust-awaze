//! An Error type

use std::error;
use std::fmt;
use std::io;

use handlebars::{RenderError, TemplateError};
use serde::json;

/// An error type for API generation
#[derive(Debug)]
pub enum Error {
    /// An IO error
    IoError(io::Error),
    /// A json error
    JsonError(json::Error),
    /// A handlebars render error
    RenderError(RenderError),
    /// A handlebars render error
    TemplateError(TemplateError),
    /// No Model found
    NoModelFound,
    /// Could not read overrides
    CouldNotReadOverrides(Box<error::Error+Send>),

}


impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IoError(err)
    }
}

impl From<json::Error> for Error {
    fn from(err: json::Error) -> Error {
        Error::JsonError(err)
    }
}

impl From<RenderError> for Error {
    fn from(err: RenderError) -> Error {
        Error::RenderError(err)
    }
}

impl From<TemplateError> for Error {
    fn from(err: TemplateError) -> Error {
        Error::TemplateError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::IoError(ref e) => write!(f,"{}",e),
            Error::JsonError(ref e) => write!(f,"{}",e),
            Error::RenderError(ref e) => write!(f,"{}",e.desc),
            Error::TemplateError(ref e) => write!(f,"{}",e),
            Error::NoModelFound => write!(f,"NoModelFound"),
            Error::CouldNotReadOverrides(ref e) =>
                write!(f,"CouldNotReadOverrides: {}",e),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::IoError(ref e) => error::Error::description(e),
            Error::JsonError(ref e) => e.description(),
            Error::RenderError(ref e) => e.desc,
            Error::TemplateError(ref e) => e.description(),
            Error::NoModelFound => "No model found",
            Error::CouldNotReadOverrides(_) => "Could not read overrides",
        }

    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::IoError(ref e) => Some(e),
            Error::JsonError(ref e) => Some(e),
            Error::RenderError(_) => None,
            Error::TemplateError(_) => None,
            Error::NoModelFound => None,
            Error::CouldNotReadOverrides(ref e) => Some(&**e),
        }
    }
}
