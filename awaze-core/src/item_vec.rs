use std::ops::{Deref, DerefMut};
use serde::{Deserialize, Serialize};
use serde::de::Deserializer;
use serde::ser::Serializer;

#[derive(PartialEq, Debug)]
pub struct ItemVec<T>(pub Vec<T>);

impl<T: Deserialize> Deserialize for ItemVec<T> {
    fn deserialize<D>(deserializer: &mut D) -> Result<ItemVec<T>, D::Error>
        where D: Deserializer,
    {
        #[derive(PartialEq, Debug, Serialize, Deserialize)]
        struct Helper<U> {
            item: Vec<U>,
        }
        let h: Helper<_> = try!(Deserialize::deserialize(deserializer));
        Ok(ItemVec(h.item))
    }
}

impl<T: Serialize + Deserialize> Serialize for ItemVec<T> {
    fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
        where S: Serializer,
    {
        self.0.serialize(serializer)
    }
}

impl<T> Deref for ItemVec<T> {
    type Target = Vec<T>;

    fn deref<'a>(&'a self) -> &'a Self::Target {
        &self.0
    }
}

impl<T> DerefMut for ItemVec<T> {
    fn deref_mut<'a>(&'a mut self) -> &'a mut Vec<T> {
        &mut self.0
    }
}
