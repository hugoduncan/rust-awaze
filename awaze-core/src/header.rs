//! Amazon specific headers

// use hyper::header::{Header, HeaderFormat};
use ::amz_date::*;

header! {
    #[doc="`x-amz-date` header"]
    #[doc=""]
    #[doc="The `x-amz-date` header field represents the date and time at which"]
    #[doc="the message was originated."]
    #[doc=""]
    #[doc="# ABNF"]
    #[doc="```plain"]
    #[doc="Date = HTTP-date"]
    #[doc="```"]
    (XAmzDate, "x-amz-date") => [AmzDate]
}

impl XAmzDate {
    pub fn now() -> XAmzDate {
        XAmzDate(AmzDate::now())
    }
}
