//! Encode data structures for aws queries
//!
//! Converts arrays to keys with the index in them.
//! Serialisation support for url encoded structs

use std::borrow::ToOwned;
use std::collections::BTreeMap;
use std::io::Write;
use std::fmt;

use serde::{Deserializer, Serialize};
use serde::ser;
use serde::json;

use ::error::*;

struct Serializer {
    key: Option<String>,
    map: BTreeMap<String,String>,
    leaf: Option<String>,
}

impl fmt::Debug for Serializer {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        try!(write!(f, "Serializer {{ ",));
        try!(write!(f, "key: {:?}, ", self.key));
        try!(write!(f, "map: {:?}, ", self.map));
        try!(write!(f, "leaf: {:?}, ", self.leaf));
        write!(f, "}}")
    }
}

impl Serializer {
    fn new() -> Serializer {
        trace!("Serializer::new");
        Serializer {
            key: None,
            map: BTreeMap::new(),
            leaf: None
        }
    }

    fn to_json<T: Serialize>(&mut self, value: &T) -> Result<(), Error> {
        let v = try!(json::to_string(value));
        self.leaf = Some(v.replace("\"",""));
        Ok(())
    }

    fn push_name(&mut self, name: &str) {
        self.key = match self.key {
            None => Some(name.to_owned()),
            Some(ref v) => Some(format!("{}.{}", v, name))
        };
    }
}




impl ser::Serializer for Serializer {
    type Error = Error;

    #[inline]
    fn visit_unit(&mut self) -> Result<(), Error> {
        self.to_json(&())
    }

    #[inline]
    fn visit_bool(&mut self, value: bool) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_isize(&mut self, value: isize) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_i8(&mut self, value: i8) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_i16(&mut self, value: i16) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_i32(&mut self, value: i32) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_i64(&mut self, value: i64) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_usize(&mut self, value: usize) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_u8(&mut self, value: u8) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_u16(&mut self, value: u16) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_u32(&mut self, value: u32) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_u64(&mut self, value: u64) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_f64(&mut self, value: f64) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_char(&mut self, value: char) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_str(&mut self, value: &str) -> Result<(), Error> {
        self.to_json(&value)
    }

    #[inline]
    fn visit_none(&mut self) -> Result<(), Error> {
        self.leaf = Some("".into());
        Ok(())
    }

    #[inline]
    fn visit_some<V>(&mut self, value: V) -> Result<(), Error>
        where V: Serialize
    {
        value.serialize(self)
    }

    #[inline]
    fn visit_named_seq<V>(&mut self, name: &str, visitor: V)
                          -> Result<(), Error>
        where V: ser::SeqVisitor,
    {
        trace!("visit_named_seq {:?} {}", self, name);
        self.push_name(name);
        self.visit_seq(visitor)
    }

    #[inline]
    fn visit_seq<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::SeqVisitor,
    {
        trace!("visit_seq {:?}", self);
        let prefix = self.key.clone();
        let mut i = 1;

        self.key = Some(match prefix {
            None => format!("{}", i),
            Some(ref p) => format!("{}.{}", p, i)
        });

        while let Some(()) = try!(visitor.visit(self)) {
            if let Some(ref mut leaf) = self.leaf {
                if &leaf[..] != "" {
                    let k = self.key.as_ref().unwrap().to_owned();
                    let mut s = String::new();
                    ::std::mem::swap(leaf, &mut s);
                    self.map.insert(k, s);
                }
            }
            self.leaf = None;
            self.key = prefix.clone();
            i += 1;
            self.key = Some(match prefix {
                None => format!("{}", i),
                Some(ref p) => format!("{}.{}", p, i)
            });
        }
        Ok(())
    }

            #[inline]
    fn visit_seq_elt<T>(&mut self, value: T) -> Result<(), Error>
        where T: Serialize,
    {
        value.serialize(self)
    }

    #[inline]
    fn visit_named_map<V>(&mut self, name: &str, visitor: V)
                          -> Result<(), Error>
        where V: ser::MapVisitor,
    {
        trace!("visit_named_map {:?} {}", self, name);
        // self.push_name(name);
        self.visit_map(visitor)
    }

    #[inline]
    fn visit_map<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::MapVisitor,
    {
        trace!("visit_map {:?}", self);
        while let Some(()) = try!(visitor.visit(self)) { }
        Ok(())
    }

    #[inline]
    fn visit_map_elt<K, V>(&mut self, key: K, value: V) -> Result<(), Error>
        where K: Serialize, V: Serialize,
    {
        trace!("visit_map_elt {:?}", self);
        let k = try!(json::to_string(&key)).replace("\"","");
        let prefix = self.key.clone();

        let k = match prefix {
            None => format!("{}", k),
            Some(ref p) => format!("{}.{}", p, k)
        };

        self.key = Some(k.clone());

        try!(value.serialize(self));

        if let Some(ref mut leaf) = self.leaf {
            if &leaf[..] != "" {
                let mut s = String::new();
                ::std::mem::swap(leaf, &mut s);
                self.map.insert(k, s);
            }
        }
        self.leaf = None;
        self.key = prefix;
        Ok(())
    }
}

#[inline]
pub fn to_map<T>(value: &T) -> Result<BTreeMap<String, String>, Error>
    where T: Serialize,
{
    let mut ser = Serializer::new();
    try!(value.serialize(&mut ser));
    Ok(ser.map)
}



#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::BTreeMap;
    use env_logger;
    use ::item_vec::*;

    #[derive(Debug, Serialize, Deserialize)]
    struct Tag {
        #[serde(rename="key")]
        pub key: Option<String>,
        #[serde(rename="value")]
        pub value: Option<String>,
    }

    #[derive(Debug, Serialize, Deserialize)]
    struct S {
        tags: Vec<Tag>,
        ids: ItemVec<String>
    }

    #[test]
    fn test_map() {
        let _log = env_logger::init();
        let s = S {
            tags: vec![
                Tag {
                    key: Some("k1".into()),
                    value: Some("v1".into())
                },
                Tag {
                    key: Some("k2".into()),
                    value: Some("v2".into())
                },
                ],
            ids: ItemVec(vec!["a".into(), "b".into()])
        };

        let m = to_map(&s);
        let mut b = BTreeMap::new();
        b.insert("tags.1.key".into(), "k1".into());
        b.insert("tags.1.value".into(), "v1".into());
        b.insert("tags.2.key".into(), "k2".into());
        b.insert("tags.2.value".into(), "v2".into());
        b.insert("ids.1".into(), "a".into());
        b.insert("ids.2".into(), "b".into());
        assert_eq!(b, m.unwrap());
    }
}
