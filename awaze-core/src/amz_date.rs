use std::fmt;
use std::str::FromStr;

use time;
use error::*;

/// A `time::Time` with formatting and parsing for amazon headers
///  e.g. 20110909T233600Z
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct AmzDate(pub time::Tm);

impl AmzDate {
    pub fn now() -> AmzDate {
        AmzDate(time::now())
    }

    /// Return a date string for the date, as YYYYMMDD
    pub fn date_string(&self) -> Result<String, Error> {
        let tm = self.0;
        let tm = match tm.tm_utcoff {
            0 => tm,
            _ => tm.to_utc(),
        };
        let r = try!(tm.strftime("%Y%m%d"));
        Ok(format!("{}",r))
    }

    /// Return a date string for the date, as YYYYMMDD
    pub fn datetime_string(&self) -> Result<String, Error> {
        let tm = self.0;
        let tm = match tm.tm_utcoff {
            0 => tm,
            _ => tm.to_utc(),
        };
        let r = try!(tm.strftime("%Y%m%dT%H%M%SZ"));
        Ok(format!("{}",r))
    }
}

impl FromStr for AmzDate {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, ()> {
        match time::strptime(s, "%Y-%m-%dT%H:%M:%S%Z") {
            Ok(t) => Ok(AmzDate(t)),
            Err(_) => Err(()),
        }
    }
}

impl fmt::Display for AmzDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tm = self.0;
        let tm = match tm.tm_utcoff {
            0 => tm,
            _ => tm.to_utc(),
        };
        match tm.strftime("%Y-%m-%dT%H:%M:%SZ") {
            Ok(s) => fmt::Display::fmt(&s, f),
            Err(e) => {
                error!("Problem formatting AmzDate: {}", e);
                Err(fmt::Error)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use time::Tm;
    use super::AmzDate;

    const NOV_07: AmzDate = AmzDate(Tm {
        tm_nsec: 0,
        tm_sec: 37,
        tm_min: 48,
        tm_hour: 8,
        tm_mday: 7,
        tm_mon: 10,
        tm_year: 94,
        tm_wday: 0,
        tm_isdst: 0,
        tm_yday: 0,
        tm_utcoff: 0,
    });

    #[test]
    fn test_parse() {
        assert_eq!("19941107T084837Z".parse(), Ok(NOV_07));
    }

    #[test]
    fn test_format() {
        assert_eq!("19941107T084837Z", format!("{}", NOV_07));
    }
}
