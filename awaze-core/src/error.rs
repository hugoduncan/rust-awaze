//! Base AWS Error type
use std::env;
use std::error;
use std::fmt;
use std::io;
use std::string::FromUtf8Error;

use hyper::error::HttpError;
use serde::json;
use serde::xml;

/// Base AWS Error type
#[derive(Debug)]
pub enum Error {
    /// An IO error
    IoError(io::Error),
    /// A json error
    JsonError(json::Error),
    /// A xml error
    XmlError(xml::Error),
    /// A utf error
    FromUtf8Error(FromUtf8Error),
    /// A utf error
    HttpError(HttpError),
    /// A time parsing error
    TimeParseError(::time::ParseError),
    /// Env var error
    VarError(env::VarError),
    /// AWS Error
    AwsError(String),
    /// Url encoding error
    UrlencodedError(::urlencoded::Error)
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IoError(err)
    }
}

impl From<json::Error> for Error {
    fn from(err: json::Error) -> Error {
        Error::JsonError(err)
    }
}

impl From<xml::Error> for Error {
    fn from(err: xml::Error) -> Error {
        Error::XmlError(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Error {
        Error::FromUtf8Error(err)
    }
}

impl From<HttpError> for Error {
    fn from(err: HttpError) -> Error {
        Error::HttpError(err)
    }
}

impl From<::time::ParseError> for Error {
    fn from(err: ::time::ParseError) -> Error {
        Error::TimeParseError(err)
    }
}

impl From<env::VarError> for Error {
    fn from(err: env::VarError) -> Error {
        Error::VarError(err)
    }
}

impl From<::urlencoded::Error> for Error {
    fn from(err: ::urlencoded::Error) -> Error {
        Error::UrlencodedError(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::IoError(ref e) => write!(f,"{}",e),
            Error::JsonError(ref e) => write!(f,"{}",e),
            Error::XmlError(ref e) => write!(f,"{}",e),
            Error::FromUtf8Error(ref e) => write!(f, "{}", e),
            Error::HttpError(ref e) => write!(f, "{}", e),
            Error::TimeParseError(ref e) => write!(f, "{}", e),
            Error::VarError(ref e) => write!(f, "{}", e),
            Error::AwsError(ref e) => write!(f, "{}", e),
            Error::UrlencodedError(ref e) => write!(f, "{}", e),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::IoError(ref e) => error::Error::description(e),
            Error::JsonError(ref e) => e.description(),
            Error::XmlError(ref e) => e.description(),
            Error::FromUtf8Error(ref e) => e.description(),
            Error::HttpError(ref e) => e.description(),
            Error::TimeParseError(_) => "Time parse error",
            Error::AwsError(_) => "AWS error",
            Error::VarError(ref e) => e.description(),
            Error::UrlencodedError(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::IoError(ref e) => Some(e),
            Error::JsonError(ref e) => Some(e),
            Error::XmlError(ref e) => Some(e),
            Error::FromUtf8Error(ref e) => Some(e),
            Error::HttpError(ref e) => Some(e),
            Error::TimeParseError(_) => None,
            Error::AwsError(_) => None,
            Error::VarError(ref e) => Some(e),
            Error::UrlencodedError(ref e) => Some(e),
        }
    }
}
