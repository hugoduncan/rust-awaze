//! Core networking used across all awaze modules
//!
//! For signing, see http://docs.aws.amazon.com/general/latest/gr/signature-version-4.html
use std::borrow::ToOwned;
use std::collections::HashMap;
use std::env;
use std::fmt;
use std::io::Read;

use crypto::digest::Digest;
use crypto::hmac::Hmac;
use crypto::mac::Mac;
use crypto::sha2::Sha256;
use hyper::client;
use hyper::method::Method;
use hyper::header;
use mime::{Attr, Mime, TopLevel, SubLevel, Value};

use serde::Serialize;

use urlencoded;

use ::error::*;
use ::header::*;
use ::awsencoded::*;
use ::amz_date::*;


/// struct for urlencoding args
#[derive(Debug, Serialize)]
struct Request {
    #[serde(rename="Action")]
    action: String,
    // #[serde(rename="AWSAccessKeyId")]
    // access_key: String,
    #[serde(rename="Version")]
    version: String,
    #[serde(rename="SignatureVersion")]
    signature_version: u32,
    #[serde(rename="SignatureMethod")]
    signature_method: String,
    #[serde(rename="Timestamp")]
    timestamp: String,
}


/// Environment
#[derive(Clone, Debug)]
pub struct Env {
    region: String,
    auth: Auth
}

impl Env {
    /// Return an endpoint url
    pub fn endpoint(&self, service: &str) -> String {
        let s1=format!("{}/{}", self.region, service);
        let s2=format!("*/{}", service);
        let s3=format!("{}/*", self.region);
        let s4="*/*";
        let e = ENDPOINTS.get(&s1[..])
            .or_else(|| ENDPOINTS.get(&s2[..]))
            .or_else(|| ENDPOINTS.get(&s3[..]))
            .or_else(|| ENDPOINTS.get(s4))
            .unwrap();
        e.replace("{service}", service)
            .replace("{region}", &self.region)
    }
}

/// Auth credentials
#[derive(Clone, Debug)]
pub struct Auth {
    access_key: String,
    secret_key: String,
}

/// AWS connection object
pub struct Aws {
    env: Env,
    client: client::Client,
}

impl Aws {
    pub fn new() -> Result<Aws,Error> {
        Ok(Aws {
            env: Env {
                region: "us-east-1".into(),
                auth: Auth {
                    access_key: try!(env::var("AWS_ACCESS_KEY")),
                    secret_key: try!(env::var("AWS_SECRET_KEY")),
                },
            },
            client: client::Client::new(),
        })
    }

    pub fn request<I>(
        &mut self,
        method: Method,
        service: &str,
        version: &str,
        uri: &str,
        action: &str,
        arg: &I
            ) -> Result<String, Error>
        where I: Serialize + fmt::Debug
    {
        debug!("request {:?} {} {} {:?}", method, service, uri, arg);
        let xdate = XAmzDate::now();

        let qargs = Request {
            action: action.to_owned(),
            version: version.to_owned(),
            signature_version: 4,
            signature_method: "HmacSHA256".into(),
            timestamp: format!("{}", xdate.0),
        };
        let qs = try!(urlencoded::to_string(&qargs));


        let arg = try!(to_map(arg));
        let body = format!("{}&{}",
                           qs,
                           try!(urlencoded::to_string(&arg)));
        debug!("request body {:?}", body);
        let mut headers = header::Headers::new();

        let host = self.env.endpoint(service);

        headers.set(header::Host{ hostname: host.to_owned(), port: None });
        headers.set(header::Accept(
            vec![header::qitem(
                Mime(TopLevel::Application, SubLevel::Json, vec![]))]));
        headers.set(
            header::ContentType(
                Mime(TopLevel::Application,
                     SubLevel::WwwFormUrlEncoded,
                     vec![(Attr::Charset, Value::Utf8)])));

        let date = try!(xdate.date_string());

        let now = xdate.0;

        headers.set(xdate.to_owned());

        debug!("request headers {:?}", headers);

        let auth_header = try!(self.auth_header(
            &method, service, uri, &now, &date, &headers, &body));

        let url = format!("https://{}{}", host, uri);

        let builder = match method {
            Method::Get => self.client.get(&url[..]),
            Method::Post => self.client.post(&url[..]),
            _ => panic!("Unsupported http method in request")
        };

        let resp = builder
            .headers(headers)
            .header(header::Authorization(auth_header))
            .body(&body[..])
            .send();

        let mut resp = try!(resp);
        debug!("Response status {:?}", resp.status);
        let mut s = String::new();
        try!(resp.read_to_string(&mut s));
        debug!("Response {:?}", s);
        if resp.status.is_success() {
            let s = cleanup_xml(&s);
            debug!("Response {:?}", s);
            Ok(s)
        } else {
            Err(Error::AwsError(s))
        }
    }


    fn auth_header(
        &self,
        method: &Method,
        service: &str,
        uri: &str,
        now: &AmzDate,
        date: &str,
        headers: &header::Headers,
        body: &str,
            ) -> Result<String, Error>
    {
        let (cr_hash, cr, hdrs) = Aws::canonical_request(
            &method, uri, "", &headers, &body);
        trace!("Canonical request\n{}", cr);

        let s = Aws::string_to_sign(
            &now, &date, &self.env.region, service, &cr_hash);
        trace!("String to sign\n{}", s);

        let key = self.signing_key(&date, service);
        trace!("Signing key\n{}", hex_string(&key));

        let signature = sign(&key, &s);

        let auth_header = format!(
                "AWS4-HMAC-SHA256 Credential={}/{}/{}/{}/aws4_request, SignedHeaders={}, Signature={}",
                self.env.auth.access_key, try!(now.date_string()),
                self.env.region, service,
            hdrs, signature);
        trace!("Auth header \n{}", auth_header);
        Ok(auth_header)
    }

    /// AWS V4 Canonical request
    fn canonical_request(
        method: &Method,
        uri: &str,
        query_string: &str,
        headers: &header::Headers,
        body: &str)
        -> (String, String, String)
    {
        let s = format!("{}\n{}\n{}\n{}\n{}\n{}",
                        method, uri, query_string,
                        canonical_headers(headers),
                        signed_headers(headers),
                        hash(body)
                        );
        (hash(&s), s, signed_headers(headers))
    }

    /// AWS V4 String to sign
    fn string_to_sign(now: &AmzDate, date: &str, region: &str, service: &str, hash: &str)
                      -> String
    {
        format!("AWS4-HMAC-SHA256\n{}\n{}/{}/{}/aws4_request\n{}",
                now.datetime_string().unwrap(), date, region, service, hash)
    }

    /// AWS V4 signing key
    fn signing_key(&self, date: &str, service: &str) -> Vec<u8> {
        let key = format!("AWS4{}", self.env.auth.secret_key);

        let v = hmac(date, key.as_bytes());
        let v = hmac(&self.env.region, &v);
        let v = hmac(service, &v);
        let v = hmac("aws4_request", &v);

        debug!("Signing key from {} {} {} {}",
               key, date, self.env.region, service);
        v
    }
}

/// AWS V4 sign
fn sign(signing_key: &Vec<u8>, s: &str) -> String {
    let res = hmac(s, &signing_key);
    hex_string(&res)
}

fn hmac(value: &str, key: &[u8]) -> Vec<u8> {
    let hasher = Sha256::new();
    let mut hmac = Hmac::new(hasher, key);
    hmac.input(value.as_bytes());
    hmac_out(&mut hmac)
}

fn hmac_out(hmac: &mut Hmac<Sha256>) -> Vec<u8> {
    let n = hmac.output_bytes();
    let mut res : Vec<u8> = Vec::with_capacity(n);
    unsafe { res.set_len(n); }
    hmac.raw_result(&mut res);
    res
}

fn hex_string(bytes: &Vec<u8>) -> String {
    bytes.iter().fold(String::new(),
                   |r,s|
                   format!("{}{:02x}", r, s))
}

fn canonical_headers(headers: &header::Headers) -> String {
    let mut hdrs : Vec<_> = headers.iter()
        .map(|hv| (hv.name().to_lowercase(),hv.value_string()))
        .collect();
    hdrs.sort_by(|x,y| x.0.cmp(&y.0));
    hdrs.iter()
        .fold(String::new(),
              |mut r, &(ref n, ref v)| {
                  r.push_str(n);
                  r.push_str(":");
                  r.push_str(v.trim());
                  r.push_str("\n");
                  r})
}

fn signed_headers(headers: &header::Headers) -> String {
    let mut names : Vec<_> = headers.iter()
        .map(|hv| hv.name().to_lowercase())
        .collect();
    names.sort();
    names.iter()
        .fold(String::new(),
              |mut r,s| {
                  if &r[..]!="" {
                      r.push_str(";");
                  }
                  r.push_str(&s);
                  r})
}

fn hash(s: &str) -> String {
    sha256_digest(s)
}

fn sha256_digest(s: &str) -> String {
    let mut hasher = Sha256::new();
    hasher.reset();
    hasher.input(s.as_bytes());
    hasher.result_str()
}

impl fmt::Debug for Aws {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "Aws {{ env: {:?} }}", self.env)
    }
}

lazy_static! {
    static ref ENDPOINTS: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        m.insert("*/*", "{service}.{region}.amazonaws.com");
        m.insert("cn-north-1/*", "{service}.{region}.amazonaws.com.cn");
        m.insert("us-gov-west-1/iam", "iam.us-gov.amazonaws.com");
        m.insert("us-gov-west-1/s3", "s3-{region}.amazonaws.com");
        m.insert("*/cloudfront", "cloudfront.amazonaws.com");
        m.insert("*/iam", "iam.amazonaws.com");
        m.insert("us-gov-west-1/sts", "sts.us-gov-west-1.amazonaws.com");
        m.insert("*/importexport", "importexport.amazonaws.com");
        m.insert("*/route53", "route53.amazonaws.com");
        m.insert("*/sts", "sts.amazonaws.com");
        m.insert("us-east-1/sdb", "sdb.amazonaws.com");
        m.insert("us-east-1/s3", "s3.amazonaws.com");
        m.insert("us-west-1/s3", "s3-{region}.amazonaws.com");
        m.insert("us-west-2/s3", "s3-{region}.amazonaws.com");
        m.insert("eu-west-1/s3", "s3-{region}.amazonaws.com");
        m.insert("ap-southeast-1/s3", "s3-{region}.amazonaws.com");
        m.insert("ap-southeast-2/s3", "s3-{region}.amazonaws.com");
        m.insert("ap-northeast-1/s3", "s3-{region}.amazonaws.com");
        m.insert("sa-east-1/s3", "s3-{region}.amazonaws.com");
        m
    };
}


fn cleanup_xml(xml: &str) -> String {
    let s = xml.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n",
                        "");
    let re = regex!(" xmlns=\"[^\"]+\"");
    let s = re.replace_all(&s, "");
    let re2 = regex!("\n *");
    let s = re2.replace_all(&s, "");
    s
}

#[cfg(test)]
mod test {
    use super::*;
    use super::cleanup_xml;
    use hyper::method::Method;
    use env_logger;
    use time::Tm;
    use hyper::header;
    use hyper::header::HttpDate;
    use mime::{Mime, TopLevel, SubLevel, Attr, Value};

    use ::amz_date::*;

    #[test]
    fn test_endpoint() {
        let _log = env_logger::init();
        let auth = Auth {
            access_key: "a".into(),
            secret_key: "s".into()
        };

        assert_eq!("ec2.us-east-1.amazonaws.com".to_string(),
                   Env {
                       region: "us-east-1".into(),
                       auth: auth.clone(),
                   }.endpoint("ec2"));

        assert_eq!("route53.amazonaws.com".to_string(),
                   Env {
                       region: "us-east-1".into(),
                       auth: auth.clone(),
                   }.endpoint("route53"));
        assert_eq!("ec2.cn-north-1.amazonaws.com.cn".to_string(),
                   Env {
                       region: "cn-north-1".into(),
                       auth: auth.clone(),
                   }.endpoint("ec2"));
        assert_eq!("sdb.amazonaws.com".to_string(),
                   Env {
                       region: "us-east-1".into(),
                       auth: auth.clone(),
                   }.endpoint("sdb"));
        assert_eq!("s3-us-west-1.amazonaws.com".to_string(),
                   Env {
                       region: "us-west-1".into(),
                       auth: auth.clone(),
                   }.endpoint("s3"));
    }

    #[derive(Debug, Serialize)]
    struct Test {
        #[serde(rename="DryRun")]
        dry_run: bool
    }

    #[test]
    fn test_describe_instances() {
        let _log = env_logger::init();
        let mut aws = Aws::new().unwrap();
        //let req = "{\"Filter\":[],\"nextToken\":\"\",\"InstanceId\":[],\"maxResults\":100,\"dryRun\":false}".to_string();
        let req = Test { dry_run: false };
        let res = aws.request(
            Method::Post,
            "ec2",
            "2014-10-01",
            "/",
            "DescribeInstances",
            &req);
        println!("Res {:?}", res);
        assert!(res.is_ok());
        assert!(false);
    }

    #[test]
    fn test_auth_header() {
        // aws4_testsuite post-x-www-form-urlencoded-parameters
        let _log = env_logger::init();
        let mut aws = Aws::new().unwrap();
        aws.env.auth.access_key = "AKIDEXAMPLE".into();
        aws.env.auth.secret_key =
            "wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY".into();

        const SEP_09: AmzDate = AmzDate(Tm {
            tm_nsec: 0,
            tm_sec: 00,
            tm_min: 36,
            tm_hour: 23,
            tm_mday: 9,
            tm_mon: 8,
            tm_year: 111,
            tm_wday: 1,
            tm_isdst: 0,
            tm_yday: 0,
            tm_utcoff: 0,
        });
        println!("Date {:?}", SEP_09);
        let date = "20110909".to_string();
        let body = "foo=bar";

        let mut headers = header::Headers::new();
        headers.set(header::Host{
            hostname: "host.foo.com".into(), port: None
        });
        headers.set(header::Date(HttpDate(SEP_09.0)));
        headers.set(
            header::ContentType(
                Mime(TopLevel::Application,
                     SubLevel::WwwFormUrlEncoded,
                     vec![(Attr::Charset, Value::Ext("utf8".into()))])));

        let res = aws.auth_header(
            &Method::Post,
            "host",
            "/",
            &SEP_09,
            &date,
            &headers,
            body);
        println!("Res {:?}", res);
        assert!(res.is_ok());
        assert_eq!("AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20110909/us-east-1/host/aws4_request, SignedHeaders=content-type;date;host, Signature=b105eb10c6d318d2294de9d49dd8b031b55e3c3fe139f2e637da70511e9e7b71".to_string(),
                   res.unwrap())
    }

    #[test]
    fn test_signing_key() {
        let _log = env_logger::init();
        // from http://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
        let mut aws = Aws::new().unwrap();
        aws.env.auth.secret_key =
            "wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY".into();
        let key = aws.signing_key("20110909", "iam");
        let res : Vec<u8> =
            vec![152, 241, 216, 137, 254, 196, 244, 66, 26, 220, 82, 43,
                 171, 12, 225, 248, 46, 105, 41, 194, 98, 237, 21, 229,
                 169, 76, 144, 239, 209, 227, 176, 231];
        assert_eq!(res,key);
    }

    #[test]
    fn test_cleanup_xml() {
        assert_eq!(
            "<DescribeInstancesResponse>\n    <requestId>e7f7c20b-731d-45fc-af39-3457b51a9052</requestId>\n    <reservationSet/>\n</DescribeInstancesResponse>".to_string(),
            cleanup_xml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<DescribeInstancesResponse xmlns=\"http://ec2.amazonaws.com/doc/2014-10-01/\">\n    <requestId>e7f7c20b-731d-45fc-af39-3457b51a9052</requestId>\n    <reservationSet/>\n</DescribeInstancesResponse>"))
    }
}
