//! A macro for enum string serialisation

#[macro_export]
macro_rules! string_enum {
    ( $ety:ty, $($variant_ty:pat => $val:expr => $variant:expr),* ) => {

        impl ::serde::Deserialize for $ety {
            fn deserialize<D>(deserializer: &mut D) -> Result<$ety, D::Error>
                where D: ::serde::de::Deserializer,
            {
                let s: String = try!(
                    ::serde::Deserialize::deserialize(deserializer));
                match &s[..] {
                    $($val => Ok($variant)),* ,
                    _ => Err(::serde::de::Error::syntax_error())
                }
            }
        }

        impl ::serde::Serialize for $ety
        {
            fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
                where S: ::serde::ser::Serializer,
            {
                match *self {
                    $($variant_ty => $val.serialize(serializer)),*
                }
            }
        }

    }
}
