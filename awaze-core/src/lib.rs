#![feature(collections)]
#![feature(custom_attribute, custom_derive, plugin)]
#![plugin(serde_macros)]
#![plugin(regex_macros)]

#[macro_use] extern crate log;
#[macro_use] extern crate hyper;
#[macro_use] extern crate lazy_static;

#[cfg(test)] extern crate env_logger;

extern crate crypto;
extern crate mime;
extern crate serde;
extern crate time;
extern crate regex;

extern crate rust_urlencoded as urlencoded;

pub use ::error::*;
pub use ::string_enum::*;
pub use ::item_vec::*;
pub use ::core::Aws;

mod error;
mod amz_date;
mod header;
mod string_enum;
mod item_vec;
mod core;
mod awsencoded;
