
use ::architecture_values::*;
use ::date_time::*;
use ::device_type::*;
use ::group_identifier_list::*;
use ::hypervisor_type::*;
use ::iam_instance_profile::*;
use ::instance_block_device_mapping_list::*;
use ::instance_lifecycle_type::*;
use ::instance_network_interface_list::*;
use ::instance_state::*;
use ::instance_type::*;
use ::monitoring::*;
use ::placement::*;
use ::platform_values::*;
use ::product_code_list::*;
use ::state_reason::*;
use ::tag_list::*;
use ::virtualization_type::*;


/// <p>Describes an instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Instance {

    #[serde(rename_deserialize="amiLaunchIndex", rename_serialize="AmiLaunchIndex")]
    pub ami_launch_index: i32,
    #[serde(rename_deserialize="architecture", rename_serialize="Architecture")]
    pub architecture: ArchitectureValues,
    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<InstanceBlockDeviceMappingList>,
    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="ebsOptimized", rename_serialize="EbsOptimized")]
    pub ebs_optimized: bool,
    #[serde(rename_deserialize="hypervisor", rename_serialize="Hypervisor")]
    pub hypervisor: HypervisorType,
    #[serde(rename_deserialize="iamInstanceProfile", rename_serialize="IamInstanceProfile")]
    pub iam_instance_profile: Option<IamInstanceProfile>,
    #[serde(rename_deserialize="imageId", rename_serialize="ImageId")]
    pub image_id: String,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="instanceLifecycle", rename_serialize="InstanceLifecycle")]
    pub instance_lifecycle: Option<InstanceLifecycleType>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: InstanceType,
    #[serde(rename_deserialize="kernelId", rename_serialize="KernelId")]
    pub kernel_id: Option<String>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
    #[serde(rename_deserialize="launchTime", rename_serialize="LaunchTime")]
    pub launch_time: DateTime,
    #[serde(rename_deserialize="monitoring", rename_serialize="Monitoring")]
    pub monitoring: Monitoring,
    #[serde(rename_deserialize="networkInterfaceSet", rename_serialize="NetworkInterfaceSet")]
    pub network_interfaces: Option<InstanceNetworkInterfaceList>,
    #[serde(rename_deserialize="placement", rename_serialize="Placement")]
    pub placement: Placement,
    #[serde(rename_deserialize="platform", rename_serialize="Platform")]
    pub platform: Option<PlatformValues>,
    #[serde(rename_deserialize="privateDnsName", rename_serialize="PrivateDnsName")]
    pub private_dns_name: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="dnsName", rename_serialize="DnsName")]
    pub public_dns_name: Option<String>,
    #[serde(rename_deserialize="ipAddress", rename_serialize="IpAddress")]
    pub public_ip_address: Option<String>,
    #[serde(rename_deserialize="ramdiskId", rename_serialize="RamdiskId")]
    pub ramdisk_id: Option<String>,
    #[serde(rename_deserialize="rootDeviceName", rename_serialize="RootDeviceName")]
    pub root_device_name: Option<String>,
    #[serde(rename_deserialize="rootDeviceType", rename_serialize="RootDeviceType")]
    pub root_device_type: DeviceType,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub security_groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<bool>,
    #[serde(rename_deserialize="spotInstanceRequestId", rename_serialize="SpotInstanceRequestId")]
    pub spot_instance_request_id: Option<String>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<String>,
    #[serde(rename_deserialize="instanceState", rename_serialize="InstanceState")]
    pub state: InstanceState,
    #[serde(rename_deserialize="stateReason", rename_serialize="StateReason")]
    pub state_reason: Option<StateReason>,
    #[serde(rename_deserialize="reason", rename_serialize="Reason")]
    pub state_transition_reason: Option<String>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="virtualizationType", rename_serialize="VirtualizationType")]
    pub virtualization_type: VirtualizationType,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


