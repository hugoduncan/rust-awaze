



#[derive(Debug, Serialize, Deserialize)]
pub struct DiskImageVolumeDescription {

    #[serde(rename_deserialize="id", rename_serialize="Id")]
    pub id: String,
    #[serde(rename_deserialize="size", rename_serialize="Size")]
    pub size: Option<i64>,
}


