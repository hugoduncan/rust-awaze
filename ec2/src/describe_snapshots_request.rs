
use ::filter_list::*;
use ::owner_string_list::*;
use ::restorable_by_string_list::*;
use ::snapshot_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSnapshotsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="MaxResults", rename_serialize="MaxResults")]
    pub max_results: Option<i32>,
    #[serde(rename_deserialize="NextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="Owner", rename_serialize="Owner")]
    pub owner_ids: Option<OwnerStringList>,
    #[serde(rename_deserialize="RestorableBy", rename_serialize="RestorableBy")]
    pub restorable_by_user_ids: Option<RestorableByStringList>,
    #[serde(rename_deserialize="SnapshotId", rename_serialize="SnapshotId")]
    pub snapshot_ids: Option<SnapshotIdStringList>,
}


