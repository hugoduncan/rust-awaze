



#[derive(Debug, Serialize, Deserialize)]
pub struct ConfirmProductInstanceRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="ProductCode", rename_serialize="ProductCode")]
    pub product_code: String,
}


