



#[derive(Debug, Clone)]
pub enum VolumeAttachmentState {
   Attaching,Attached,Detaching,Detached,
}

string_enum!{ VolumeAttachmentState,
    VolumeAttachmentState::Attaching => "attaching" => VolumeAttachmentState::Attaching,
    VolumeAttachmentState::Attached => "attached" => VolumeAttachmentState::Attached,
    VolumeAttachmentState::Detaching => "detaching" => VolumeAttachmentState::Detaching,
    VolumeAttachmentState::Detached => "detached" => VolumeAttachmentState::Detached

}