
use awaze_core::ItemVec;
use ::dhcp_configuration::*;



pub type DhcpConfigurationList = ItemVec<DhcpConfiguration>;
