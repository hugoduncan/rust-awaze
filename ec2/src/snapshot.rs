
use ::date_time::*;
use ::snapshot_state::*;
use ::tag_list::*;


/// <p>Describes a snapshot.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Snapshot {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="encrypted", rename_serialize="Encrypted")]
    pub encrypted: Option<bool>,
    #[serde(rename_deserialize="kmsKeyId", rename_serialize="KmsKeyId")]
    pub kms_key_id: Option<String>,
    #[serde(rename_deserialize="ownerAlias", rename_serialize="OwnerAlias")]
    pub owner_alias: Option<String>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="progress", rename_serialize="Progress")]
    pub progress: Option<String>,
    #[serde(rename_deserialize="snapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="startTime", rename_serialize="StartTime")]
    pub start_time: Option<DateTime>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub state: Option<SnapshotState>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
    #[serde(rename_deserialize="volumeSize", rename_serialize="VolumeSize")]
    pub volume_size: Option<i32>,
}


