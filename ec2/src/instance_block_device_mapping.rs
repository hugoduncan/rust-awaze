
use ::ebs_instance_block_device::*;


/// <p>Describes a block device mapping.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceBlockDeviceMapping {

    #[serde(rename_deserialize="deviceName", rename_serialize="DeviceName")]
    pub device_name: Option<String>,
    #[serde(rename_deserialize="ebs", rename_serialize="Ebs")]
    pub ebs: Option<EbsInstanceBlockDevice>,
}


