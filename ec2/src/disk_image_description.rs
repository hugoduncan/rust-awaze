
use ::disk_image_format::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DiskImageDescription {

    #[serde(rename_deserialize="checksum", rename_serialize="Checksum")]
    pub checksum: Option<String>,
    #[serde(rename_deserialize="format", rename_serialize="Format")]
    pub format: DiskImageFormat,
    #[serde(rename_deserialize="importManifestUrl", rename_serialize="ImportManifestUrl")]
    pub import_manifest_url: String,
    #[serde(rename_deserialize="size", rename_serialize="Size")]
    pub size: i64,
}


