
use ::dhcp_configuration_value_list::*;


/// <p>Describes a DHCP configuration option.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct DhcpConfiguration {

    #[serde(rename_deserialize="key", rename_serialize="Key")]
    pub key: Option<String>,
    #[serde(rename_deserialize="valueSet", rename_serialize="ValueSet")]
    pub values: Option<DhcpConfigurationValueList>,
}


