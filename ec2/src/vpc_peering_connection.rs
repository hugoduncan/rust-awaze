
use ::date_time::*;
use ::tag_list::*;
use ::vpc_peering_connection_state_reason::*;
use ::vpc_peering_connection_vpc_info::*;


/// <p>Describes a VPC peering connection.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpcPeeringConnection {

    #[serde(rename_deserialize="accepterVpcInfo", rename_serialize="AccepterVpcInfo")]
    pub accepter_vpc_info: Option<VpcPeeringConnectionVpcInfo>,
    #[serde(rename_deserialize="expirationTime", rename_serialize="ExpirationTime")]
    pub expiration_time: Option<DateTime>,
    #[serde(rename_deserialize="requesterVpcInfo", rename_serialize="RequesterVpcInfo")]
    pub requester_vpc_info: Option<VpcPeeringConnectionVpcInfo>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<VpcPeeringConnectionStateReason>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcPeeringConnectionId", rename_serialize="VpcPeeringConnectionId")]
    pub vpc_peering_connection_id: Option<String>,
}


