



#[derive(Debug, Clone)]
pub enum AccountAttributeName {
   SupportedPlatforms,DefaultVpc,
}

string_enum!{ AccountAttributeName,
    AccountAttributeName::SupportedPlatforms => "supported-platforms" => AccountAttributeName::SupportedPlatforms,
    AccountAttributeName::DefaultVpc => "default-vpc" => AccountAttributeName::DefaultVpc

}