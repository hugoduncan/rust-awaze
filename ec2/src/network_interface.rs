
use ::group_identifier_list::*;
use ::network_interface_association::*;
use ::network_interface_attachment::*;
use ::network_interface_private_ip_address_list::*;
use ::network_interface_status::*;
use ::tag_list::*;


/// <p>Describes a network interface.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkInterface {

    #[serde(rename_deserialize="association", rename_serialize="Association")]
    pub association: Option<NetworkInterfaceAssociation>,
    #[serde(rename_deserialize="attachment", rename_serialize="Attachment")]
    pub attachment: Option<NetworkInterfaceAttachment>,
    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="macAddress", rename_serialize="MacAddress")]
    pub mac_address: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="privateDnsName", rename_serialize="PrivateDnsName")]
    pub private_dns_name: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="privateIpAddressesSet", rename_serialize="PrivateIpAddressesSet")]
    pub private_ip_addresses: Option<NetworkInterfacePrivateIpAddressList>,
    #[serde(rename_deserialize="requesterId", rename_serialize="RequesterId")]
    pub requester_id: Option<String>,
    #[serde(rename_deserialize="requesterManaged", rename_serialize="RequesterManaged")]
    pub requester_managed: Option<bool>,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<bool>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<NetworkInterfaceStatus>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tag_set: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


