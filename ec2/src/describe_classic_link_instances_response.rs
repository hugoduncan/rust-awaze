
use ::classic_link_instance_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeClassicLinkInstancesResponse {

    #[serde(rename_deserialize="instancesSet", rename_serialize="InstancesSet")]
    pub instances: Option<ClassicLinkInstanceList>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


