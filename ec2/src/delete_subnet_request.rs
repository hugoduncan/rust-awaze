



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteSubnetRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SubnetId", rename_serialize="SubnetId")]
    pub subnet_id: String,
}


