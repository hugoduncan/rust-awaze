



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSecurityGroupRequest {

    #[serde(rename_deserialize="GroupDescription", rename_serialize="GroupDescription")]
    pub description: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="GroupName", rename_serialize="GroupName")]
    pub group_name: String,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


