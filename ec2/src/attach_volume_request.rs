



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachVolumeRequest {

    #[serde(rename_deserialize="Device", rename_serialize="Device")]
    pub device: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="VolumeId", rename_serialize="VolumeId")]
    pub volume_id: String,
}


