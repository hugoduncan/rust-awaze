
use awaze_core::ItemVec;
use ::reserved_instances::*;



pub type ReservedInstancesList = ItemVec<ReservedInstances>;
