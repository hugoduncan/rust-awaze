
use ::export_task_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeExportTasksResponse {

    #[serde(rename_deserialize="exportTaskSet", rename_serialize="ExportTaskSet")]
    pub export_tasks: Option<ExportTaskList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


