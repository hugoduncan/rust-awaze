
use ::group_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachClassicLinkVpcRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SecurityGroupId", rename_serialize="SecurityGroupId")]
    pub groups: GroupIdStringList,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


