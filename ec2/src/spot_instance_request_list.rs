
use awaze_core::ItemVec;
use ::spot_instance_request::*;



pub type SpotInstanceRequestList = ItemVec<SpotInstanceRequest>;
