
use ::tenancy::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpcRequest {

    #[serde(rename_deserialize="CidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceTenancy", rename_serialize="InstanceTenancy")]
    pub instance_tenancy: Option<Tenancy>,
}


