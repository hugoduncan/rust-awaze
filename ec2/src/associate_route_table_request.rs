



#[derive(Debug, Serialize, Deserialize)]
pub struct AssociateRouteTableRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: String,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: String,
}


