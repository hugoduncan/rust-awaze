
use ::placement_strategy::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreatePlacementGroupRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: String,
    #[serde(rename_deserialize="strategy", rename_serialize="Strategy")]
    pub strategy: PlacementStrategy,
}


