
use ::reset_image_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ResetImageAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: ResetImageAttributeName,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_id: String,
}


