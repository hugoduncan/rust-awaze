
use ::network_acl::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateNetworkAclResponse {

    #[serde(rename_deserialize="networkAcl", rename_serialize="NetworkAcl")]
    pub network_acl: Option<NetworkAcl>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


