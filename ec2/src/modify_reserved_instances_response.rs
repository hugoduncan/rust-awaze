



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyReservedInstancesResponse {

    #[serde(rename_deserialize="reservedInstancesModificationId", rename_serialize="ReservedInstancesModificationId")]
    pub reserved_instances_modification_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


