
use ::export_task_state::*;
use ::export_to_s3_task::*;
use ::instance_export_details::*;


/// <p>Describes an export task.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ExportTask {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="exportTaskId", rename_serialize="ExportTaskId")]
    pub export_task_id: Option<String>,
    #[serde(rename_deserialize="exportToS3", rename_serialize="ExportToS3")]
    pub export_to_s3_task: Option<ExportToS3Task>,
    #[serde(rename_deserialize="instanceExport", rename_serialize="InstanceExport")]
    pub instance_export_details: Option<InstanceExportDetails>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<ExportTaskState>,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
}


