
use ::monitoring_state::*;


/// <p>Describes the monitoring for the instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Monitoring {

    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<MonitoringState>,
}


