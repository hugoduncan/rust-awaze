



#[derive(Debug, Clone)]
pub enum SummaryStatus {
   Ok,Impaired,InsufficientData,NotApplicable,
}

string_enum!{ SummaryStatus,
    SummaryStatus::Ok => "ok" => SummaryStatus::Ok,
    SummaryStatus::Impaired => "impaired" => SummaryStatus::Impaired,
    SummaryStatus::InsufficientData => "insufficient-data" => SummaryStatus::InsufficientData,
    SummaryStatus::NotApplicable => "not-applicable" => SummaryStatus::NotApplicable

}