
use awaze_core::ItemVec;
use ::reserved_instances_modification::*;



pub type ReservedInstancesModificationList = ItemVec<ReservedInstancesModification>;
