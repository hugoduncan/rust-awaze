
use awaze_core::ItemVec;
use ::placement_group::*;



pub type PlacementGroupList = ItemVec<PlacementGroup>;
