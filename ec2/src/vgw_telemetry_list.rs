
use awaze_core::ItemVec;
use ::vgw_telemetry::*;



pub type VgwTelemetryList = ItemVec<VgwTelemetry>;
