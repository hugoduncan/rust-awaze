
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesListingsRequest {

    #[serde(rename_deserialize="filters", rename_serialize="Filters")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
    #[serde(rename_deserialize="reservedInstancesListingId", rename_serialize="ReservedInstancesListingId")]
    pub reserved_instances_listing_id: Option<String>,
}


