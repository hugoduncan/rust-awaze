
use ::attachment_status::*;


/// <p>Describes an attachment between a virtual private gateway and a VPC.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpcAttachment {

    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<AttachmentStatus>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


