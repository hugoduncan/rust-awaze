
use awaze_core::ItemVec;
use ::tag_description::*;



pub type TagDescriptionList = ItemVec<TagDescription>;
