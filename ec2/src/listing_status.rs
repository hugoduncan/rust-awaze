



#[derive(Debug, Clone)]
pub enum ListingStatus {
   Active,Pending,Cancelled,Closed,
}

string_enum!{ ListingStatus,
    ListingStatus::Active => "active" => ListingStatus::Active,
    ListingStatus::Pending => "pending" => ListingStatus::Pending,
    ListingStatus::Cancelled => "cancelled" => ListingStatus::Cancelled,
    ListingStatus::Closed => "closed" => ListingStatus::Closed

}