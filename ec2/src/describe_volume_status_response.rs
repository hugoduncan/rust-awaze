
use ::volume_status_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVolumeStatusResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="volumeStatusSet", rename_serialize="VolumeStatusSet")]
    pub volume_statuses: Option<VolumeStatusList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


