


/// <p>Describes a range of ports.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PortRange {

    #[serde(rename_deserialize="from", rename_serialize="From")]
    pub from: Option<i32>,
    #[serde(rename_deserialize="to", rename_serialize="To")]
    pub to: Option<i32>,
}


