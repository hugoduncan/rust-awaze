
use awaze_core::ItemVec;
use ::reservation::*;



pub type ReservationList = ItemVec<Reservation>;
