
use awaze_core::ItemVec;



pub type ProductDescriptionList = ItemVec<String>;
