
use awaze_core::ItemVec;



pub type SpotInstanceRequestIdList = ItemVec<String>;
