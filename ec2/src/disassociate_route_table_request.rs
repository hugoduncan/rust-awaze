



#[derive(Debug, Serialize, Deserialize)]
pub struct DisassociateRouteTableRequest {

    #[serde(rename_deserialize="associationId", rename_serialize="AssociationId")]
    pub association_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


