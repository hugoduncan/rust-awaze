
use ::conversion_task_state::*;
use ::import_instance_task_details::*;
use ::import_volume_task_details::*;
use ::tag_list::*;


/// <p>Describes a conversion task.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ConversionTask {

    #[serde(rename_deserialize="conversionTaskId", rename_serialize="ConversionTaskId")]
    pub conversion_task_id: String,
    #[serde(rename_deserialize="expirationTime", rename_serialize="ExpirationTime")]
    pub expiration_time: Option<String>,
    #[serde(rename_deserialize="importInstance", rename_serialize="ImportInstance")]
    pub import_instance: Option<ImportInstanceTaskDetails>,
    #[serde(rename_deserialize="importVolume", rename_serialize="ImportVolume")]
    pub import_volume: Option<ImportVolumeTaskDetails>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: ConversionTaskState,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
}


