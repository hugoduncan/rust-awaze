



#[derive(Debug, Clone)]
pub enum ResourceType {
   CustomerGateway,DhcpOptions,Image,Instance,InternetGateway,NetworkAcl,NetworkInterface,ReservedInstances,RouteTable,Snapshot,SpotInstancesRequest,Subnet,SecurityGroup,Volume,Vpc,VpnConnection,VpnGateway,
}

string_enum!{ ResourceType,
    ResourceType::CustomerGateway => "customer-gateway" => ResourceType::CustomerGateway,
    ResourceType::DhcpOptions => "dhcp-options" => ResourceType::DhcpOptions,
    ResourceType::Image => "image" => ResourceType::Image,
    ResourceType::Instance => "instance" => ResourceType::Instance,
    ResourceType::InternetGateway => "internet-gateway" => ResourceType::InternetGateway,
    ResourceType::NetworkAcl => "network-acl" => ResourceType::NetworkAcl,
    ResourceType::NetworkInterface => "network-interface" => ResourceType::NetworkInterface,
    ResourceType::ReservedInstances => "reserved-instances" => ResourceType::ReservedInstances,
    ResourceType::RouteTable => "route-table" => ResourceType::RouteTable,
    ResourceType::Snapshot => "snapshot" => ResourceType::Snapshot,
    ResourceType::SpotInstancesRequest => "spot-instances-request" => ResourceType::SpotInstancesRequest,
    ResourceType::Subnet => "subnet" => ResourceType::Subnet,
    ResourceType::SecurityGroup => "security-group" => ResourceType::SecurityGroup,
    ResourceType::Volume => "volume" => ResourceType::Volume,
    ResourceType::Vpc => "vpc" => ResourceType::Vpc,
    ResourceType::VpnConnection => "vpn-connection" => ResourceType::VpnConnection,
    ResourceType::VpnGateway => "vpn-gateway" => ResourceType::VpnGateway

}