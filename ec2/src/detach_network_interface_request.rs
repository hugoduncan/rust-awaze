



#[derive(Debug, Serialize, Deserialize)]
pub struct DetachNetworkInterfaceRequest {

    #[serde(rename_deserialize="attachmentId", rename_serialize="AttachmentId")]
    pub attachment_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="force", rename_serialize="Force")]
    pub force: Option<bool>,
}


