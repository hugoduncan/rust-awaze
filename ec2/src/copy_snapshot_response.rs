



#[derive(Debug, Serialize, Deserialize)]
pub struct CopySnapshotResponse {

    #[serde(rename_deserialize="snapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


