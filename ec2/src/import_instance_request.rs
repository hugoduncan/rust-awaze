
use ::disk_image_list::*;
use ::import_instance_launch_specification::*;
use ::platform_values::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportInstanceRequest {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="diskImage", rename_serialize="DiskImage")]
    pub disk_images: Option<DiskImageList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="launchSpecification", rename_serialize="LaunchSpecification")]
    pub launch_specification: Option<ImportInstanceLaunchSpecification>,
    #[serde(rename_deserialize="platform", rename_serialize="Platform")]
    pub platform: PlatformValues,
}


