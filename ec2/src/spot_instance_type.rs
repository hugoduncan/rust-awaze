



#[derive(Debug, Clone)]
pub enum SpotInstanceType {
   OneTime,Persistent,
}

string_enum!{ SpotInstanceType,
    SpotInstanceType::OneTime => "one-time" => SpotInstanceType::OneTime,
    SpotInstanceType::Persistent => "persistent" => SpotInstanceType::Persistent

}