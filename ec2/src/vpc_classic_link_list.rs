
use awaze_core::ItemVec;
use ::vpc_classic_link::*;



pub type VpcClassicLinkList = ItemVec<VpcClassicLink>;
