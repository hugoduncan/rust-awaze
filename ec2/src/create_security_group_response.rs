



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSecurityGroupResponse {

    #[serde(rename_deserialize="groupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


