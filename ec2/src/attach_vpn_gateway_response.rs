
use ::vpc_attachment::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachVpnGatewayResponse {

    #[serde(rename_deserialize="attachment", rename_serialize="Attachment")]
    pub vpc_attachment: Option<VpcAttachment>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


