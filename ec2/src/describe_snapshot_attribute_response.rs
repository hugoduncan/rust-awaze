
use ::create_volume_permission_list::*;
use ::product_code_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSnapshotAttributeResponse {

    #[serde(rename_deserialize="createVolumePermission", rename_serialize="CreateVolumePermission")]
    pub create_volume_permissions: Option<CreateVolumePermissionList>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="snapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


