
use ::currency_code_values::*;


/// <p>Describes the limit price of a Reserved Instance offering.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstanceLimitPrice {

    #[serde(rename_deserialize="amount", rename_serialize="Amount")]
    pub amount: Option<f64>,
    #[serde(rename_deserialize="currencyCode", rename_serialize="CurrencyCode")]
    pub currency_code: Option<CurrencyCodeValues>,
}


