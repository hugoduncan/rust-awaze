
use awaze_core::ItemVec;
use ::price_schedule::*;



pub type PriceScheduleList = ItemVec<PriceSchedule>;
