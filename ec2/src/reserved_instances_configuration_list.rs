
use awaze_core::ItemVec;
use ::reserved_instances_configuration::*;



pub type ReservedInstancesConfigurationList = ItemVec<ReservedInstancesConfiguration>;
