
use ::instance_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct StopInstancesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="force", rename_serialize="Force")]
    pub force: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_ids: InstanceIdStringList,
}


