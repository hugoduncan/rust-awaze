
use awaze_core::ItemVec;
use ::instance_count::*;



pub type InstanceCountList = ItemVec<InstanceCount>;
