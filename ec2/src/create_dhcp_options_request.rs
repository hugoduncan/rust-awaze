
use ::new_dhcp_configuration_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateDhcpOptionsRequest {

    #[serde(rename_deserialize="dhcpConfiguration", rename_serialize="DhcpConfiguration")]
    pub dhcp_configurations: NewDhcpConfigurationList,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


