
use ::block_device_mapping_list::*;
use ::group_identifier_list::*;
use ::iam_instance_profile_specification::*;
use ::instance_network_interface_specification_list::*;
use ::instance_type::*;
use ::run_instances_monitoring_enabled::*;
use ::spot_placement::*;


/// <p>Describes the launch specification for an instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct LaunchSpecification {

    #[serde(rename_deserialize="addressingType", rename_serialize="AddressingType")]
    pub addressing_type: Option<String>,
    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingList>,
    #[serde(rename_deserialize="ebsOptimized", rename_serialize="EbsOptimized")]
    pub ebs_optimized: Option<bool>,
    #[serde(rename_deserialize="iamInstanceProfile", rename_serialize="IamInstanceProfile")]
    pub iam_instance_profile: Option<IamInstanceProfileSpecification>,
    #[serde(rename_deserialize="imageId", rename_serialize="ImageId")]
    pub image_id: Option<String>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="kernelId", rename_serialize="KernelId")]
    pub kernel_id: Option<String>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
    #[serde(rename_deserialize="monitoring", rename_serialize="Monitoring")]
    pub monitoring: Option<RunInstancesMonitoringEnabled>,
    #[serde(rename_deserialize="networkInterfaceSet", rename_serialize="NetworkInterfaceSet")]
    pub network_interfaces: Option<InstanceNetworkInterfaceSpecificationList>,
    #[serde(rename_deserialize="placement", rename_serialize="Placement")]
    pub placement: Option<SpotPlacement>,
    #[serde(rename_deserialize="ramdiskId", rename_serialize="RamdiskId")]
    pub ramdisk_id: Option<String>,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub security_groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="userData", rename_serialize="UserData")]
    pub user_data: Option<String>,
}


