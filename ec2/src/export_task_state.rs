



#[derive(Debug, Clone)]
pub enum ExportTaskState {
   Active,Cancelling,Cancelled,Completed,
}

string_enum!{ ExportTaskState,
    ExportTaskState::Active => "active" => ExportTaskState::Active,
    ExportTaskState::Cancelling => "cancelling" => ExportTaskState::Cancelling,
    ExportTaskState::Cancelled => "cancelled" => ExportTaskState::Cancelled,
    ExportTaskState::Completed => "completed" => ExportTaskState::Completed

}