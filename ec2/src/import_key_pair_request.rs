
use ::blob::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportKeyPairRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: String,
    #[serde(rename_deserialize="publicKeyMaterial", rename_serialize="PublicKeyMaterial")]
    pub public_key_material: Blob,
}


