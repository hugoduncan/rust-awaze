



#[derive(Debug, Clone)]
pub enum RIProductDescription {
   Linux,LinuxVpc,Windows,WindowsVpc,
}

string_enum!{ RIProductDescription,
    RIProductDescription::Linux => "Linux/UNIX" => RIProductDescription::Linux,
    RIProductDescription::LinuxVpc => "Linux/UNIX (Amazon VPC)" => RIProductDescription::LinuxVpc,
    RIProductDescription::Windows => "Windows" => RIProductDescription::Windows,
    RIProductDescription::WindowsVpc => "Windows (Amazon VPC)" => RIProductDescription::WindowsVpc

}