
use ::listing_state::*;


/// <p>Describes a Reserved Instance listing state.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceCount {

    #[serde(rename_deserialize="instanceCount", rename_serialize="InstanceCount")]
    pub instance_count: Option<i32>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<ListingState>,
}


