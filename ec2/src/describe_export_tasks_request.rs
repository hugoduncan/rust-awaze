
use ::export_task_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeExportTasksRequest {

    #[serde(rename_deserialize="exportTaskId", rename_serialize="ExportTaskId")]
    pub export_task_ids: Option<ExportTaskIdStringList>,
}


