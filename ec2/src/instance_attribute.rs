
use ::attribute_boolean_value::*;
use ::attribute_value::*;
use ::group_identifier_list::*;
use ::instance_block_device_mapping_list::*;
use ::product_code_list::*;


/// <p>Describes an instance attribute.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceAttribute {

    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<InstanceBlockDeviceMappingList>,
    #[serde(rename_deserialize="disableApiTermination", rename_serialize="DisableApiTermination")]
    pub disable_api_termination: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="ebsOptimized", rename_serialize="EbsOptimized")]
    pub ebs_optimized: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="instanceInitiatedShutdownBehavior", rename_serialize="InstanceInitiatedShutdownBehavior")]
    pub instance_initiated_shutdown_behavior: Option<AttributeValue>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<AttributeValue>,
    #[serde(rename_deserialize="kernel", rename_serialize="Kernel")]
    pub kernel_id: Option<AttributeValue>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="ramdisk", rename_serialize="Ramdisk")]
    pub ramdisk_id: Option<AttributeValue>,
    #[serde(rename_deserialize="rootDeviceName", rename_serialize="RootDeviceName")]
    pub root_device_name: Option<AttributeValue>,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<AttributeValue>,
    #[serde(rename_deserialize="userData", rename_serialize="UserData")]
    pub user_data: Option<AttributeValue>,
}


