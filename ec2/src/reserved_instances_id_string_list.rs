
use awaze_core::ItemVec;



pub type ReservedInstancesIdStringList = ItemVec<String>;
