
use ::date_time::*;
use ::instance_id_string_list::*;
use ::reason_codes_list::*;
use ::report_status_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ReportInstanceStatusRequest {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="endTime", rename_serialize="EndTime")]
    pub end_time: Option<DateTime>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instances: InstanceIdStringList,
    #[serde(rename_deserialize="reasonCode", rename_serialize="ReasonCode")]
    pub reason_codes: ReasonCodesList,
    #[serde(rename_deserialize="startTime", rename_serialize="StartTime")]
    pub start_time: Option<DateTime>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: ReportStatusType,
}


