



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteSnapshotRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SnapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: String,
}


