
use ::export_task::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateInstanceExportTaskResponse {

    #[serde(rename_deserialize="exportTask", rename_serialize="ExportTask")]
    pub export_task: Option<ExportTask>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


