
use ::spot_price_history_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSpotPriceHistoryResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="spotPriceHistorySet", rename_serialize="SpotPriceHistorySet")]
    pub spot_price_history: Option<SpotPriceHistoryList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


