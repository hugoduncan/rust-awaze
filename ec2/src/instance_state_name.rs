



#[derive(Debug, Clone)]
pub enum InstanceStateName {
   Pending,Running,ShuttingDown,Terminated,Stopping,Stopped,
}

string_enum!{ InstanceStateName,
    InstanceStateName::Pending => "pending" => InstanceStateName::Pending,
    InstanceStateName::Running => "running" => InstanceStateName::Running,
    InstanceStateName::ShuttingDown => "shutting-down" => InstanceStateName::ShuttingDown,
    InstanceStateName::Terminated => "terminated" => InstanceStateName::Terminated,
    InstanceStateName::Stopping => "stopping" => InstanceStateName::Stopping,
    InstanceStateName::Stopped => "stopped" => InstanceStateName::Stopped

}