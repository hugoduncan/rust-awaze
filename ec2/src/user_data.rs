



#[derive(Debug, Serialize, Deserialize)]
pub struct UserData {

    #[serde(rename_deserialize="data", rename_serialize="Data")]
    pub data: Option<String>,
}


