
use awaze_core::ItemVec;
use ::instance_status_details::*;



pub type InstanceStatusDetailsList = ItemVec<InstanceStatusDetails>;
