


/// <p>Describes an IP range.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct IpRange {

    #[serde(rename_deserialize="cidrIp", rename_serialize="CidrIp")]
    pub cidr_ip: Option<String>,
}


