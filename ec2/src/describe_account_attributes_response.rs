
use ::account_attribute_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAccountAttributesResponse {

    #[serde(rename_deserialize="accountAttributeSet", rename_serialize="AccountAttributeSet")]
    pub account_attributes: Option<AccountAttributeList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


