



#[derive(Debug, Clone)]
pub enum ShutdownBehavior {
   Stop,Terminate,
}

string_enum!{ ShutdownBehavior,
    ShutdownBehavior::Stop => "stop" => ShutdownBehavior::Stop,
    ShutdownBehavior::Terminate => "terminate" => ShutdownBehavior::Terminate

}