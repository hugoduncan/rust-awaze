
use ::reserved_instance_limit_price::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct PurchaseReservedInstancesOfferingRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceCount", rename_serialize="InstanceCount")]
    pub instance_count: i32,
    #[serde(rename_deserialize="limitPrice", rename_serialize="LimitPrice")]
    pub limit_price: Option<ReservedInstanceLimitPrice>,
    #[serde(rename_deserialize="ReservedInstancesOfferingId", rename_serialize="ReservedInstancesOfferingId")]
    pub reserved_instances_offering_id: String,
}


