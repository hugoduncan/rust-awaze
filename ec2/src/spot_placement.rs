


/// <p>Describes Spot Instance placement.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotPlacement {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
}


