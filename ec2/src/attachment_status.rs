



#[derive(Debug, Clone)]
pub enum AttachmentStatus {
   Attaching,Attached,Detaching,Detached,
}

string_enum!{ AttachmentStatus,
    AttachmentStatus::Attaching => "attaching" => AttachmentStatus::Attaching,
    AttachmentStatus::Attached => "attached" => AttachmentStatus::Attached,
    AttachmentStatus::Detaching => "detaching" => AttachmentStatus::Detaching,
    AttachmentStatus::Detached => "detached" => AttachmentStatus::Detached

}