
use ::filter_list::*;
use ::vpn_connection_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpnConnectionsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="VpnConnectionId", rename_serialize="VpnConnectionId")]
    pub vpn_connection_ids: Option<VpnConnectionIdStringList>,
}


