
use awaze_core::ItemVec;
use ::group_identifier::*;



pub type GroupIdentifierList = ItemVec<GroupIdentifier>;
