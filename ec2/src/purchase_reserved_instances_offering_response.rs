



#[derive(Debug, Serialize, Deserialize)]
pub struct PurchaseReservedInstancesOfferingResponse {

    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


