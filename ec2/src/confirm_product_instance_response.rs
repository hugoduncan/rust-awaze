



#[derive(Debug, Serialize, Deserialize)]
pub struct ConfirmProductInstanceResponse {

    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


