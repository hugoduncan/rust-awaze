
use awaze_core::ItemVec;
use ::spot_price::*;



pub type SpotPriceHistoryList = ItemVec<SpotPrice>;
