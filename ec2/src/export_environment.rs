



#[derive(Debug, Clone)]
pub enum ExportEnvironment {
   Citrix,Vmware,Microsoft,
}

string_enum!{ ExportEnvironment,
    ExportEnvironment::Citrix => "citrix" => ExportEnvironment::Citrix,
    ExportEnvironment::Vmware => "vmware" => ExportEnvironment::Vmware,
    ExportEnvironment::Microsoft => "microsoft" => ExportEnvironment::Microsoft

}