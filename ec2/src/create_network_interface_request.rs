
use ::private_ip_address_specification_list::*;
use ::security_group_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateNetworkInterfaceRequest {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SecurityGroupId", rename_serialize="SecurityGroupId")]
    pub groups: Option<SecurityGroupIdStringList>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="privateIpAddresses", rename_serialize="PrivateIpAddresses")]
    pub private_ip_addresses: Option<PrivateIpAddressSpecificationList>,
    #[serde(rename_deserialize="secondaryPrivateIpAddressCount", rename_serialize="SecondaryPrivateIpAddressCount")]
    pub secondary_private_ip_address_count: Option<i32>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: String,
}


