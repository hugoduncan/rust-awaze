
use ::subnet_state::*;
use ::tag_list::*;


/// <p>Describes a subnet.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Subnet {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: String,
    #[serde(rename_deserialize="availableIpAddressCount", rename_serialize="AvailableIpAddressCount")]
    pub available_ip_address_count: i32,
    #[serde(rename_deserialize="cidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: String,
    #[serde(rename_deserialize="defaultForAz", rename_serialize="DefaultForAz")]
    pub default_for_az: bool,
    #[serde(rename_deserialize="mapPublicIpOnLaunch", rename_serialize="MapPublicIpOnLaunch")]
    pub map_public_ip_on_launch: bool,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: SubnetState,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: String,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


