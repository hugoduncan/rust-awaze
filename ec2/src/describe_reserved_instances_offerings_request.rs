
use ::filter_list::*;
use ::instance_type::*;
use ::offering_type_values::*;
use ::reserved_instances_offering_id_string_list::*;
use ::ri_product_description::*;
use ::tenancy::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesOfferingsRequest {

    #[serde(rename_deserialize="AvailabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="IncludeMarketplace", rename_serialize="IncludeMarketplace")]
    pub include_marketplace: Option<bool>,
    #[serde(rename_deserialize="instanceTenancy", rename_serialize="InstanceTenancy")]
    pub instance_tenancy: Option<Tenancy>,
    #[serde(rename_deserialize="InstanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="MaxDuration", rename_serialize="MaxDuration")]
    pub max_duration: Option<i64>,
    #[serde(rename_deserialize="MaxInstanceCount", rename_serialize="MaxInstanceCount")]
    pub max_instance_count: Option<i32>,
    #[serde(rename_deserialize="maxResults", rename_serialize="MaxResults")]
    pub max_results: Option<i32>,
    #[serde(rename_deserialize="MinDuration", rename_serialize="MinDuration")]
    pub min_duration: Option<i64>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="offeringType", rename_serialize="OfferingType")]
    pub offering_type: Option<OfferingTypeValues>,
    #[serde(rename_deserialize="ProductDescription", rename_serialize="ProductDescription")]
    pub product_description: Option<RIProductDescription>,
    #[serde(rename_deserialize="ReservedInstancesOfferingId", rename_serialize="ReservedInstancesOfferingId")]
    pub reserved_instances_offering_ids: Option<ReservedInstancesOfferingIdStringList>,
}


