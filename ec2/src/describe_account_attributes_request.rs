
use ::account_attribute_name_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAccountAttributesRequest {

    #[serde(rename_deserialize="attributeName", rename_serialize="AttributeName")]
    pub attribute_names: Option<AccountAttributeNameStringList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


