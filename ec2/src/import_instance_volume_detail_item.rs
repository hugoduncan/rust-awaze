
use ::disk_image_description::*;
use ::disk_image_volume_description::*;


/// <p>Describes an import volume task.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ImportInstanceVolumeDetailItem {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: String,
    #[serde(rename_deserialize="bytesConverted", rename_serialize="BytesConverted")]
    pub bytes_converted: i64,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="image", rename_serialize="Image")]
    pub image: DiskImageDescription,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: String,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
    #[serde(rename_deserialize="volume", rename_serialize="Volume")]
    pub volume: DiskImageVolumeDescription,
}


