
use awaze_core::ItemVec;



pub type ReservedInstancesModificationIdStringList = ItemVec<String>;
