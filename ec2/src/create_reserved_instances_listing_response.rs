
use ::reserved_instances_listing_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateReservedInstancesListingResponse {

    #[serde(rename_deserialize="reservedInstancesListingsSet", rename_serialize="ReservedInstancesListingsSet")]
    pub reserved_instances_listings: Option<ReservedInstancesListingList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


