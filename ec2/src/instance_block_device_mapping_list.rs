
use awaze_core::ItemVec;
use ::instance_block_device_mapping::*;



pub type InstanceBlockDeviceMappingList = ItemVec<InstanceBlockDeviceMapping>;
