



#[derive(Debug, Clone)]
pub enum NetworkInterfaceStatus {
   Available,Attaching,InUse,Detaching,
}

string_enum!{ NetworkInterfaceStatus,
    NetworkInterfaceStatus::Available => "available" => NetworkInterfaceStatus::Available,
    NetworkInterfaceStatus::Attaching => "attaching" => NetworkInterfaceStatus::Attaching,
    NetworkInterfaceStatus::InUse => "in-use" => NetworkInterfaceStatus::InUse,
    NetworkInterfaceStatus::Detaching => "detaching" => NetworkInterfaceStatus::Detaching

}