



#[derive(Debug, Clone)]
pub enum VolumeStatusInfoStatus {
   Ok,Impaired,InsufficientData,
}

string_enum!{ VolumeStatusInfoStatus,
    VolumeStatusInfoStatus::Ok => "ok" => VolumeStatusInfoStatus::Ok,
    VolumeStatusInfoStatus::Impaired => "impaired" => VolumeStatusInfoStatus::Impaired,
    VolumeStatusInfoStatus::InsufficientData => "insufficient-data" => VolumeStatusInfoStatus::InsufficientData

}