
use ::network_acl_association_list::*;
use ::network_acl_entry_list::*;
use ::tag_list::*;


/// <p>Describes a network ACL.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkAcl {

    #[serde(rename_deserialize="associationSet", rename_serialize="AssociationSet")]
    pub associations: Option<NetworkAclAssociationList>,
    #[serde(rename_deserialize="entrySet", rename_serialize="EntrySet")]
    pub entries: Option<NetworkAclEntryList>,
    #[serde(rename_deserialize="default", rename_serialize="Default")]
    pub is_default: Option<bool>,
    #[serde(rename_deserialize="networkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


