


/// <p>Describes a volume status operation code.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeStatusAction {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<String>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="eventId", rename_serialize="EventId")]
    pub event_id: Option<String>,
    #[serde(rename_deserialize="eventType", rename_serialize="EventType")]
    pub event_type: Option<String>,
}


