
use ::resource_id_list::*;
use ::tag_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteTagsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="resourceId", rename_serialize="ResourceId")]
    pub resources: ResourceIdList,
    #[serde(rename_deserialize="tag", rename_serialize="Tag")]
    pub tags: Option<TagList>,
}


