
use ::filter_list::*;
use ::instance_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeInstanceStatusRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="includeAllInstances", rename_serialize="IncludeAllInstances")]
    pub include_all_instances: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_ids: Option<InstanceIdStringList>,
    #[serde(rename_deserialize="MaxResults", rename_serialize="MaxResults")]
    pub max_results: Option<i32>,
    #[serde(rename_deserialize="NextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
}


