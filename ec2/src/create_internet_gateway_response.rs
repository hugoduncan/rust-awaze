
use ::internet_gateway::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateInternetGatewayResponse {

    #[serde(rename_deserialize="internetGateway", rename_serialize="InternetGateway")]
    pub internet_gateway: Option<InternetGateway>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


