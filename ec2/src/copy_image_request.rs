



#[derive(Debug, Serialize, Deserialize)]
pub struct CopyImageRequest {

    #[serde(rename_deserialize="ClientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="Description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Name", rename_serialize="Name")]
    pub name: String,
    #[serde(rename_deserialize="SourceImageId", rename_serialize="SourceImageId")]
    pub source_image_id: String,
    #[serde(rename_deserialize="SourceRegion", rename_serialize="SourceRegion")]
    pub source_region: String,
}


