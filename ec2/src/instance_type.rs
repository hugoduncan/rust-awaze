



#[derive(Debug, Clone)]
pub enum InstanceType {
   T1Micro,M1Small,M1Medium,M1Large,M1Xlarge,M3Medium,M3Large,M3Xlarge,M32xlarge,T2Micro,T2Small,T2Medium,M2Xlarge,M22xlarge,M24xlarge,Cr18xlarge,I2Xlarge,I22xlarge,I24xlarge,I28xlarge,Hi14xlarge,Hs18xlarge,C1Medium,C1Xlarge,C3Large,C3Xlarge,C32xlarge,C34xlarge,C38xlarge,C4Large,C4Xlarge,C42xlarge,C44xlarge,C48xlarge,Cc14xlarge,Cc28xlarge,G22xlarge,Cg14xlarge,R3Large,R3Xlarge,R32xlarge,R34xlarge,R38xlarge,D2Xlarge,D22xlarge,D24xlarge,D28xlarge,
}

string_enum!{ InstanceType,
    InstanceType::T1Micro => "t1.micro" => InstanceType::T1Micro,
    InstanceType::M1Small => "m1.small" => InstanceType::M1Small,
    InstanceType::M1Medium => "m1.medium" => InstanceType::M1Medium,
    InstanceType::M1Large => "m1.large" => InstanceType::M1Large,
    InstanceType::M1Xlarge => "m1.xlarge" => InstanceType::M1Xlarge,
    InstanceType::M3Medium => "m3.medium" => InstanceType::M3Medium,
    InstanceType::M3Large => "m3.large" => InstanceType::M3Large,
    InstanceType::M3Xlarge => "m3.xlarge" => InstanceType::M3Xlarge,
    InstanceType::M32xlarge => "m3.2xlarge" => InstanceType::M32xlarge,
    InstanceType::T2Micro => "t2.micro" => InstanceType::T2Micro,
    InstanceType::T2Small => "t2.small" => InstanceType::T2Small,
    InstanceType::T2Medium => "t2.medium" => InstanceType::T2Medium,
    InstanceType::M2Xlarge => "m2.xlarge" => InstanceType::M2Xlarge,
    InstanceType::M22xlarge => "m2.2xlarge" => InstanceType::M22xlarge,
    InstanceType::M24xlarge => "m2.4xlarge" => InstanceType::M24xlarge,
    InstanceType::Cr18xlarge => "cr1.8xlarge" => InstanceType::Cr18xlarge,
    InstanceType::I2Xlarge => "i2.xlarge" => InstanceType::I2Xlarge,
    InstanceType::I22xlarge => "i2.2xlarge" => InstanceType::I22xlarge,
    InstanceType::I24xlarge => "i2.4xlarge" => InstanceType::I24xlarge,
    InstanceType::I28xlarge => "i2.8xlarge" => InstanceType::I28xlarge,
    InstanceType::Hi14xlarge => "hi1.4xlarge" => InstanceType::Hi14xlarge,
    InstanceType::Hs18xlarge => "hs1.8xlarge" => InstanceType::Hs18xlarge,
    InstanceType::C1Medium => "c1.medium" => InstanceType::C1Medium,
    InstanceType::C1Xlarge => "c1.xlarge" => InstanceType::C1Xlarge,
    InstanceType::C3Large => "c3.large" => InstanceType::C3Large,
    InstanceType::C3Xlarge => "c3.xlarge" => InstanceType::C3Xlarge,
    InstanceType::C32xlarge => "c3.2xlarge" => InstanceType::C32xlarge,
    InstanceType::C34xlarge => "c3.4xlarge" => InstanceType::C34xlarge,
    InstanceType::C38xlarge => "c3.8xlarge" => InstanceType::C38xlarge,
    InstanceType::C4Large => "c4.large" => InstanceType::C4Large,
    InstanceType::C4Xlarge => "c4.xlarge" => InstanceType::C4Xlarge,
    InstanceType::C42xlarge => "c4.2xlarge" => InstanceType::C42xlarge,
    InstanceType::C44xlarge => "c4.4xlarge" => InstanceType::C44xlarge,
    InstanceType::C48xlarge => "c4.8xlarge" => InstanceType::C48xlarge,
    InstanceType::Cc14xlarge => "cc1.4xlarge" => InstanceType::Cc14xlarge,
    InstanceType::Cc28xlarge => "cc2.8xlarge" => InstanceType::Cc28xlarge,
    InstanceType::G22xlarge => "g2.2xlarge" => InstanceType::G22xlarge,
    InstanceType::Cg14xlarge => "cg1.4xlarge" => InstanceType::Cg14xlarge,
    InstanceType::R3Large => "r3.large" => InstanceType::R3Large,
    InstanceType::R3Xlarge => "r3.xlarge" => InstanceType::R3Xlarge,
    InstanceType::R32xlarge => "r3.2xlarge" => InstanceType::R32xlarge,
    InstanceType::R34xlarge => "r3.4xlarge" => InstanceType::R34xlarge,
    InstanceType::R38xlarge => "r3.8xlarge" => InstanceType::R38xlarge,
    InstanceType::D2Xlarge => "d2.xlarge" => InstanceType::D2Xlarge,
    InstanceType::D22xlarge => "d2.2xlarge" => InstanceType::D22xlarge,
    InstanceType::D24xlarge => "d2.4xlarge" => InstanceType::D24xlarge,
    InstanceType::D28xlarge => "d2.8xlarge" => InstanceType::D28xlarge

}