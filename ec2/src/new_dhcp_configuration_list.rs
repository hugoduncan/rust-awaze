
use awaze_core::ItemVec;
use ::new_dhcp_configuration::*;



pub type NewDhcpConfigurationList = ItemVec<NewDhcpConfiguration>;
