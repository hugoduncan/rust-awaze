
use ::vpc_classic_link_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcClassicLinkResponse {

    #[serde(rename_deserialize="vpcSet", rename_serialize="VpcSet")]
    pub vpcs: Option<VpcClassicLinkList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


