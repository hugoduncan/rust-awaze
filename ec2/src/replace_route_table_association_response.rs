



#[derive(Debug, Serialize, Deserialize)]
pub struct ReplaceRouteTableAssociationResponse {

    #[serde(rename_deserialize="newAssociationId", rename_serialize="NewAssociationId")]
    pub new_association_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


