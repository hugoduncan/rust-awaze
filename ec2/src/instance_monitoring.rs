
use ::monitoring::*;


/// <p>Describes the monitoring information of the instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceMonitoring {

    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="monitoring", rename_serialize="Monitoring")]
    pub monitoring: Option<Monitoring>,
}


