



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachVpnGatewayRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
    #[serde(rename_deserialize="VpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_id: String,
}


