
use ::export_environment::*;
use ::export_to_s3_task_specification::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateInstanceExportTaskRequest {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="exportToS3", rename_serialize="ExportToS3")]
    pub export_to_s3_task: Option<ExportToS3TaskSpecification>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="targetEnvironment", rename_serialize="TargetEnvironment")]
    pub target_environment: Option<ExportEnvironment>,
}


