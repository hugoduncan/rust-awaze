
use awaze_core::ItemVec;
use ::route_table_association::*;



pub type RouteTableAssociationList = ItemVec<RouteTableAssociation>;
