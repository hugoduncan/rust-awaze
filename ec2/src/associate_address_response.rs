



#[derive(Debug, Serialize, Deserialize)]
pub struct AssociateAddressResponse {

    #[serde(rename_deserialize="associationId", rename_serialize="AssociationId")]
    pub association_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


