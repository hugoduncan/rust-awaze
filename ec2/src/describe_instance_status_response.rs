
use ::instance_status_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeInstanceStatusResponse {

    #[serde(rename_deserialize="instanceStatusSet", rename_serialize="InstanceStatusSet")]
    pub instance_statuses: Option<InstanceStatusList>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


