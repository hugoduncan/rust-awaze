



#[derive(Debug, Clone)]
pub enum NetworkInterfaceAttribute {
   Description,GroupSet,SourceDestCheck,Attachment,
}

string_enum!{ NetworkInterfaceAttribute,
    NetworkInterfaceAttribute::Description => "description" => NetworkInterfaceAttribute::Description,
    NetworkInterfaceAttribute::GroupSet => "groupSet" => NetworkInterfaceAttribute::GroupSet,
    NetworkInterfaceAttribute::SourceDestCheck => "sourceDestCheck" => NetworkInterfaceAttribute::SourceDestCheck,
    NetworkInterfaceAttribute::Attachment => "attachment" => NetworkInterfaceAttribute::Attachment

}