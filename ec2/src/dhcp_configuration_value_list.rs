
use awaze_core::ItemVec;
use ::attribute_value::*;



pub type DhcpConfigurationValueList = ItemVec<AttributeValue>;
