
use awaze_core::ItemVec;
use ::key_pair_info::*;



pub type KeyPairList = ItemVec<KeyPairInfo>;
