
use ::vpn_gateway::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpnGatewayResponse {

    #[serde(rename_deserialize="vpnGateway", rename_serialize="VpnGateway")]
    pub vpn_gateway: Option<VpnGateway>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


