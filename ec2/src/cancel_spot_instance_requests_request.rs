
use ::spot_instance_request_id_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelSpotInstanceRequestsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SpotInstanceRequestId", rename_serialize="SpotInstanceRequestId")]
    pub spot_instance_request_ids: SpotInstanceRequestIdList,
}


