


/// <p>Describes a message about an Availability Zone.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AvailabilityZoneMessage {

    #[serde(rename_deserialize="message", rename_serialize="Message")]
    pub message: Option<String>,
}


