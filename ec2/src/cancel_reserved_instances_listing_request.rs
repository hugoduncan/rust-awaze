



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelReservedInstancesListingRequest {

    #[serde(rename_deserialize="reservedInstancesListingId", rename_serialize="ReservedInstancesListingId")]
    pub reserved_instances_listing_id: String,
}


