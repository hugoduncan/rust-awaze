
use ::filter_list::*;
use ::value_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkAclsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="NetworkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_ids: Option<ValueStringList>,
}


