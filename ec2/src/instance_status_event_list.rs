
use awaze_core::ItemVec;
use ::instance_status_event::*;



pub type InstanceStatusEventList = ItemVec<InstanceStatusEvent>;
