
use awaze_core::ItemVec;



pub type PrivateIpAddressStringList = ItemVec<String>;
