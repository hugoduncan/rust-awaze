
use awaze_core::ItemVec;
use ::export_task::*;



pub type ExportTaskList = ItemVec<ExportTask>;
