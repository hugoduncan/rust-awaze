



#[derive(Debug, Clone)]
pub enum DiskImageFormat {
   VMDK,RAW,VHD,
}

string_enum!{ DiskImageFormat,
    DiskImageFormat::VMDK => "VMDK" => DiskImageFormat::VMDK,
    DiskImageFormat::RAW => "RAW" => DiskImageFormat::RAW,
    DiskImageFormat::VHD => "VHD" => DiskImageFormat::VHD

}