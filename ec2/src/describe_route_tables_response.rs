
use ::route_table_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeRouteTablesResponse {

    #[serde(rename_deserialize="routeTableSet", rename_serialize="RouteTableSet")]
    pub route_tables: Option<RouteTableList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


