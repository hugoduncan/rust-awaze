


/// <p>Describes a tag.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Tag {

    #[serde(rename_deserialize="key", rename_serialize="Key")]
    pub key: String,
    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: String,
}


