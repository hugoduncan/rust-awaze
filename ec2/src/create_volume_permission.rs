
use ::permission_group::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVolumePermission {

    #[serde(rename_deserialize="group", rename_serialize="Group")]
    pub group: Option<PermissionGroup>,
    #[serde(rename_deserialize="userId", rename_serialize="UserId")]
    pub user_id: Option<String>,
}


