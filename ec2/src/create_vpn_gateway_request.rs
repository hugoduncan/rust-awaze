
use ::gateway_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpnGatewayRequest {

    #[serde(rename_deserialize="AvailabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="GatewayType", rename_serialize="GatewayType")]
    pub gateway_type: Option<GatewayType>,
}


