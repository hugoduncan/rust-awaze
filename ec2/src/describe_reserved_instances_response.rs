
use ::reserved_instances_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesResponse {

    #[serde(rename_deserialize="reservedInstancesSet", rename_serialize="ReservedInstancesSet")]
    pub reserved_instances: Option<ReservedInstancesList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


