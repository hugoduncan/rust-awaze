
use awaze_core::ItemVec;
use ::recurring_charge::*;



pub type RecurringChargesList = ItemVec<RecurringCharge>;
