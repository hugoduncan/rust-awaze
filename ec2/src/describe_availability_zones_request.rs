
use ::filter_list::*;
use ::zone_name_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAvailabilityZonesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="ZoneName", rename_serialize="ZoneName")]
    pub zone_names: Option<ZoneNameStringList>,
}


