
use ::date_time::*;
use ::instance_type::*;
use ::ri_product_description::*;


/// <p>Describes the maximum hourly price (bid) for any Spot Instance launched to fulfill the request.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotPrice {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="productDescription", rename_serialize="ProductDescription")]
    pub product_description: Option<RIProductDescription>,
    #[serde(rename_deserialize="spotPrice", rename_serialize="SpotPrice")]
    pub spot_price: Option<String>,
    #[serde(rename_deserialize="timestamp", rename_serialize="Timestamp")]
    pub timestamp: Option<DateTime>,
}


