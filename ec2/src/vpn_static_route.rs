
use ::vpn_state::*;
use ::vpn_static_route_source::*;


/// <p>Describes a static route for a VPN connection.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpnStaticRoute {

    #[serde(rename_deserialize="destinationCidrBlock", rename_serialize="DestinationCidrBlock")]
    pub destination_cidr_block: Option<String>,
    #[serde(rename_deserialize="source", rename_serialize="Source")]
    pub source: Option<VpnStaticRouteSource>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<VpnState>,
}


