
use ::bundle_task_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeBundleTasksResponse {

    #[serde(rename_deserialize="bundleInstanceTasksSet", rename_serialize="BundleInstanceTasksSet")]
    pub bundle_tasks: Option<BundleTaskList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


