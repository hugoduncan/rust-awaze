



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteInternetGatewayRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="internetGatewayId", rename_serialize="InternetGatewayId")]
    pub internet_gateway_id: String,
}


