
use ::blob::*;


/// <p>Describes the storage parameters for S3 and S3 buckets for an instance store-backed AMI.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct S3Storage {

    #[serde(rename_deserialize="AWSAccessKeyId", rename_serialize="AWSAccessKeyId")]
    pub aws_access_key_id: Option<String>,
    #[serde(rename_deserialize="bucket", rename_serialize="Bucket")]
    pub bucket: Option<String>,
    #[serde(rename_deserialize="prefix", rename_serialize="Prefix")]
    pub prefix: Option<String>,
    #[serde(rename_deserialize="uploadPolicy", rename_serialize="UploadPolicy")]
    pub upload_policy: Option<Blob>,
    #[serde(rename_deserialize="uploadPolicySignature", rename_serialize="UploadPolicySignature")]
    pub upload_policy_signature: Option<String>,
}


