
use ::date_time::*;
use ::volume_attachment_state::*;


/// <p>Describes volume attachment details.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeAttachment {

    #[serde(rename_deserialize="attachTime", rename_serialize="AttachTime")]
    pub attach_time: Option<DateTime>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="device", rename_serialize="Device")]
    pub device: Option<String>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub state: Option<VolumeAttachmentState>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
}


