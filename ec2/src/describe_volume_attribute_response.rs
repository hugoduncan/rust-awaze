
use ::attribute_boolean_value::*;
use ::product_code_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVolumeAttributeResponse {

    #[serde(rename_deserialize="autoEnableIO", rename_serialize="AutoEnableIO")]
    pub auto_enable_io: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


