


/// <p>Describes association information for an Elastic IP address.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkInterfaceAssociation {

    #[serde(rename_deserialize="allocationId", rename_serialize="AllocationId")]
    pub allocation_id: Option<String>,
    #[serde(rename_deserialize="associationId", rename_serialize="AssociationId")]
    pub association_id: Option<String>,
    #[serde(rename_deserialize="ipOwnerId", rename_serialize="IpOwnerId")]
    pub ip_owner_id: Option<String>,
    #[serde(rename_deserialize="publicDnsName", rename_serialize="PublicDnsName")]
    pub public_dns_name: Option<String>,
    #[serde(rename_deserialize="publicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
}


