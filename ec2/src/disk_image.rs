
use ::disk_image_detail::*;
use ::volume_detail::*;


/// <p>Describes a disk image.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct DiskImage {

    #[serde(rename_deserialize="Description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="Image", rename_serialize="Image")]
    pub image: Option<DiskImageDetail>,
    #[serde(rename_deserialize="Volume", rename_serialize="Volume")]
    pub volume: Option<VolumeDetail>,
}


