



#[derive(Debug, Clone)]
pub enum StatusType {
   Passed,Failed,InsufficientData,
}

string_enum!{ StatusType,
    StatusType::Passed => "passed" => StatusType::Passed,
    StatusType::Failed => "failed" => StatusType::Failed,
    StatusType::InsufficientData => "insufficient-data" => StatusType::InsufficientData

}