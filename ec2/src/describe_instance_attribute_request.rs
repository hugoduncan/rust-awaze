
use ::instance_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeInstanceAttributeRequest {

    #[serde(rename_deserialize="attribute", rename_serialize="Attribute")]
    pub attribute: InstanceAttributeName,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
}


