
use ::volume_status_actions_list::*;
use ::volume_status_events_list::*;
use ::volume_status_info::*;


/// <p>Describes the volume status.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeStatusItem {

    #[serde(rename_deserialize="actionsSet", rename_serialize="ActionsSet")]
    pub actions: Option<VolumeStatusActionsList>,
    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="eventsSet", rename_serialize="EventsSet")]
    pub events: Option<VolumeStatusEventsList>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
    #[serde(rename_deserialize="volumeStatus", rename_serialize="VolumeStatus")]
    pub volume_status: Option<VolumeStatusInfo>,
}


