
use ::filter_list::*;
use ::vpc_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_ids: Option<VpcIdStringList>,
}


