



#[derive(Debug, Serialize, Deserialize)]
pub struct AssociateAddressRequest {

    #[serde(rename_deserialize="AllocationId", rename_serialize="AllocationId")]
    pub allocation_id: Option<String>,
    #[serde(rename_deserialize="allowReassociation", rename_serialize="AllowReassociation")]
    pub allow_reassociation: Option<bool>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="PublicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
}


