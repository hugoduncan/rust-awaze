


/// <p>Describes an attachment change.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkInterfaceAttachmentChanges {

    #[serde(rename_deserialize="attachmentId", rename_serialize="AttachmentId")]
    pub attachment_id: Option<String>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
}


