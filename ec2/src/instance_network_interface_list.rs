
use awaze_core::ItemVec;
use ::instance_network_interface::*;



pub type InstanceNetworkInterfaceList = ItemVec<InstanceNetworkInterface>;
