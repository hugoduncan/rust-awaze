



#[derive(Debug, Serialize, Deserialize)]
pub struct DetachInternetGatewayRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="internetGatewayId", rename_serialize="InternetGatewayId")]
    pub internet_gateway_id: String,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


