
use ::filter_list::*;
use ::network_interface_id_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkInterfacesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="NetworkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_ids: Option<NetworkInterfaceIdList>,
}


