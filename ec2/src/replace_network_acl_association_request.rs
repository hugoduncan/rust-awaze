



#[derive(Debug, Serialize, Deserialize)]
pub struct ReplaceNetworkAclAssociationRequest {

    #[serde(rename_deserialize="associationId", rename_serialize="AssociationId")]
    pub association_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="networkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_id: String,
}


