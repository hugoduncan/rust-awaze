
use ::describe_conversion_task_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeConversionTasksResponse {

    #[serde(rename_deserialize="conversionTasks", rename_serialize="ConversionTasks")]
    pub conversion_tasks: Option<DescribeConversionTaskList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


