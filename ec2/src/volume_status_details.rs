
use ::volume_status_name::*;


/// <p>Describes a volume status.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeStatusDetails {

    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: Option<VolumeStatusName>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<String>,
}


