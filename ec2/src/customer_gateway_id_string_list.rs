
use awaze_core::ItemVec;



pub type CustomerGatewayIdStringList = ItemVec<String>;
