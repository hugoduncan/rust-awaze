



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateRouteRequest {

    #[serde(rename_deserialize="destinationCidrBlock", rename_serialize="DestinationCidrBlock")]
    pub destination_cidr_block: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="gatewayId", rename_serialize="GatewayId")]
    pub gateway_id: Option<String>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: String,
    #[serde(rename_deserialize="vpcPeeringConnectionId", rename_serialize="VpcPeeringConnectionId")]
    pub vpc_peering_connection_id: Option<String>,
}


