
use ::volume_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVolumeAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: Option<VolumeAttributeName>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VolumeId", rename_serialize="VolumeId")]
    pub volume_id: String,
}


