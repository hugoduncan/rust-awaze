


/// <p>Describes a region.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Region {

    #[serde(rename_deserialize="regionEndpoint", rename_serialize="RegionEndpoint")]
    pub endpoint: Option<String>,
    #[serde(rename_deserialize="regionName", rename_serialize="RegionName")]
    pub region_name: Option<String>,
}


