
use ::value_string_list::*;


/// <p>A filter name and value pair that is used to return a more specific list of results. Filters can be used to match a set of resources by various criteria, such as tags, attributes, or IDs.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Filter {

    #[serde(rename_deserialize="Name", rename_serialize="Name")]
    pub name: Option<String>,
    #[serde(rename_deserialize="Value", rename_serialize="Value")]
    pub values: Option<ValueStringList>,
}


