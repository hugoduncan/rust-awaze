
use ::internet_gateway_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeInternetGatewaysResponse {

    #[serde(rename_deserialize="internetGatewaySet", rename_serialize="InternetGatewaySet")]
    pub internet_gateways: Option<InternetGatewayList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


