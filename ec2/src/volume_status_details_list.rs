
use awaze_core::ItemVec;
use ::volume_status_details::*;



pub type VolumeStatusDetailsList = ItemVec<VolumeStatusDetails>;
