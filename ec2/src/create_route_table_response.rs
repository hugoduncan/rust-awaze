
use ::route_table::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateRouteTableResponse {

    #[serde(rename_deserialize="routeTable", rename_serialize="RouteTable")]
    pub route_table: Option<RouteTable>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


