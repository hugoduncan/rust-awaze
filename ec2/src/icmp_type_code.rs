


/// <p>Describes the ICMP type and code.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct IcmpTypeCode {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<i32>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub icmp_type: Option<i32>,
}


