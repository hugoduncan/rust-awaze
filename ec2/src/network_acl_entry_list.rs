
use awaze_core::ItemVec;
use ::network_acl_entry::*;



pub type NetworkAclEntryList = ItemVec<NetworkAclEntry>;
