
use ::address_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAddressesResponse {

    #[serde(rename_deserialize="addressesSet", rename_serialize="AddressesSet")]
    pub addresses: Option<AddressList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


