
use ::instance_state::*;
use ::instance_status_event_list::*;
use ::instance_status_summary::*;


/// <p>Describes the status of an instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceStatus {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="eventsSet", rename_serialize="EventsSet")]
    pub events: Option<InstanceStatusEventList>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="instanceState", rename_serialize="InstanceState")]
    pub instance_state: Option<InstanceState>,
    #[serde(rename_deserialize="instanceStatus", rename_serialize="InstanceStatus")]
    pub instance_status: Option<InstanceStatusSummary>,
    #[serde(rename_deserialize="systemStatus", rename_serialize="SystemStatus")]
    pub system_status: Option<InstanceStatusSummary>,
}


