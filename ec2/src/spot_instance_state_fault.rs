


/// <p>Describes a Spot Instance state change.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotInstanceStateFault {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<String>,
    #[serde(rename_deserialize="message", rename_serialize="Message")]
    pub message: Option<String>,
}


