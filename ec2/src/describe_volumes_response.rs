
use ::volume_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVolumesResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="volumeSet", rename_serialize="VolumeSet")]
    pub volumes: Option<VolumeList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


