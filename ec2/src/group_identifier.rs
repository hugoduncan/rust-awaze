


/// <p>Describes a security group.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct GroupIdentifier {

    #[serde(rename_deserialize="groupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
}


