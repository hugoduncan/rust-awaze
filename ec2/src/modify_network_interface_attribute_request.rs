
use ::attribute_boolean_value::*;
use ::attribute_value::*;
use ::network_interface_attachment_changes::*;
use ::security_group_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyNetworkInterfaceAttributeRequest {

    #[serde(rename_deserialize="attachment", rename_serialize="Attachment")]
    pub attachment: Option<NetworkInterfaceAttachmentChanges>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<AttributeValue>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SecurityGroupId", rename_serialize="SecurityGroupId")]
    pub groups: Option<SecurityGroupIdStringList>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<AttributeBooleanValue>,
}


