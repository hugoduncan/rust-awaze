
use awaze_core::ItemVec;
use ::pricing_detail::*;



pub type PricingDetailsList = ItemVec<PricingDetail>;
