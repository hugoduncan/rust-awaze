



#[derive(Debug, Clone)]
pub enum TelemetryStatus {
   UP,DOWN,
}

string_enum!{ TelemetryStatus,
    TelemetryStatus::UP => "UP" => TelemetryStatus::UP,
    TelemetryStatus::DOWN => "DOWN" => TelemetryStatus::DOWN

}