
use ::attachment_status::*;
use ::date_time::*;


/// <p>Describes a parameter used to set up an Amazon EBS volume in a block device mapping.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct EbsInstanceBlockDevice {

    #[serde(rename_deserialize="attachTime", rename_serialize="AttachTime")]
    pub attach_time: Option<DateTime>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<AttachmentStatus>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
}


