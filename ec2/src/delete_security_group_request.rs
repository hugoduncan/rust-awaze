



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteSecurityGroupRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="GroupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="GroupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
}


