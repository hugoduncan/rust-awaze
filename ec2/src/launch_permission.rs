
use ::permission_group::*;


/// <p>Describes a launch permission.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct LaunchPermission {

    #[serde(rename_deserialize="group", rename_serialize="Group")]
    pub group: Option<PermissionGroup>,
    #[serde(rename_deserialize="userId", rename_serialize="UserId")]
    pub user_id: Option<String>,
}


