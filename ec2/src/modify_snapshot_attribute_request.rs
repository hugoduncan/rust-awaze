
use ::create_volume_permission_modifications::*;
use ::group_name_string_list::*;
use ::snapshot_attribute_name::*;
use ::user_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifySnapshotAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: Option<SnapshotAttributeName>,
    #[serde(rename_deserialize="CreateVolumePermission", rename_serialize="CreateVolumePermission")]
    pub create_volume_permission: Option<CreateVolumePermissionModifications>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="UserGroup", rename_serialize="UserGroup")]
    pub group_names: Option<GroupNameStringList>,
    #[serde(rename_deserialize="OperationType", rename_serialize="OperationType")]
    pub operation_type: Option<String>,
    #[serde(rename_deserialize="SnapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: String,
    #[serde(rename_deserialize="UserId", rename_serialize="UserId")]
    pub user_ids: Option<UserIdStringList>,
}


