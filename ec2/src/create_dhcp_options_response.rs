
use ::dhcp_options::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateDhcpOptionsResponse {

    #[serde(rename_deserialize="dhcpOptions", rename_serialize="DhcpOptions")]
    pub dhcp_options: Option<DhcpOptions>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


