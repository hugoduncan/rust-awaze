


/// <p>Describes an error for <a>BundleInstance</a>.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct BundleTaskError {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<String>,
    #[serde(rename_deserialize="message", rename_serialize="Message")]
    pub message: Option<String>,
}


