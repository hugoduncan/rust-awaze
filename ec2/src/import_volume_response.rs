
use ::conversion_task::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportVolumeResponse {

    #[serde(rename_deserialize="conversionTask", rename_serialize="ConversionTask")]
    pub conversion_task: Option<ConversionTask>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


