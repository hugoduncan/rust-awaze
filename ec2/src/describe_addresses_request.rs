
use ::allocation_id_list::*;
use ::filter_list::*;
use ::public_ip_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAddressesRequest {

    #[serde(rename_deserialize="AllocationId", rename_serialize="AllocationId")]
    pub allocation_ids: Option<AllocationIdList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="PublicIp", rename_serialize="PublicIp")]
    pub public_ips: Option<PublicIpStringList>,
}


