
use ::attribute_boolean_value::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcAttributeResponse {

    #[serde(rename_deserialize="enableDnsHostnames", rename_serialize="EnableDnsHostnames")]
    pub enable_dns_hostnames: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="enableDnsSupport", rename_serialize="EnableDnsSupport")]
    pub enable_dns_support: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


