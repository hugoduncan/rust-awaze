
use ::spot_datafeed_subscription::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSpotDatafeedSubscriptionResponse {

    #[serde(rename_deserialize="spotDatafeedSubscription", rename_serialize="SpotDatafeedSubscription")]
    pub spot_datafeed_subscription: Option<SpotDatafeedSubscription>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


