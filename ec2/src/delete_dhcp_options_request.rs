



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteDhcpOptionsRequest {

    #[serde(rename_deserialize="DhcpOptionsId", rename_serialize="DhcpOptionsId")]
    pub dhcp_options_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


