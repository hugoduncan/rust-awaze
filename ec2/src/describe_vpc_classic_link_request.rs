
use ::filter_list::*;
use ::vpc_classic_link_id_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcClassicLinkRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_ids: Option<VpcClassicLinkIdList>,
}


