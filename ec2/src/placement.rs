
use ::tenancy::*;


/// <p>Describes the placement for the instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Placement {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
    #[serde(rename_deserialize="tenancy", rename_serialize="Tenancy")]
    pub tenancy: Option<Tenancy>,
}


