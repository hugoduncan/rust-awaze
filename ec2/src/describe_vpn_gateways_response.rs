
use ::vpn_gateway_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpnGatewaysResponse {

    #[serde(rename_deserialize="vpnGatewaySet", rename_serialize="VpnGatewaySet")]
    pub vpn_gateways: Option<VpnGatewayList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


