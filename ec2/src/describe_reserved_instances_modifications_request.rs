
use ::filter_list::*;
use ::reserved_instances_modification_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesModificationsRequest {

    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="ReservedInstancesModificationId", rename_serialize="ReservedInstancesModificationId")]
    pub reserved_instances_modification_ids: Option<ReservedInstancesModificationIdStringList>,
}


