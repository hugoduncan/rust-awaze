
use awaze_core::ItemVec;
use ::security_group::*;



pub type SecurityGroupList = ItemVec<SecurityGroup>;
