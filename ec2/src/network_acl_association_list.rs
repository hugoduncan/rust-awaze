
use awaze_core::ItemVec;
use ::network_acl_association::*;



pub type NetworkAclAssociationList = ItemVec<NetworkAclAssociation>;
