
use ::route_origin::*;
use ::route_state::*;


/// <p>Describes a route in a route table.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Route {

    #[serde(rename_deserialize="destinationCidrBlock", rename_serialize="DestinationCidrBlock")]
    pub destination_cidr_block: Option<String>,
    #[serde(rename_deserialize="gatewayId", rename_serialize="GatewayId")]
    pub gateway_id: Option<String>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="instanceOwnerId", rename_serialize="InstanceOwnerId")]
    pub instance_owner_id: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="origin", rename_serialize="Origin")]
    pub origin: Option<RouteOrigin>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<RouteState>,
    #[serde(rename_deserialize="vpcPeeringConnectionId", rename_serialize="VpcPeeringConnectionId")]
    pub vpc_peering_connection_id: Option<String>,
}


