
use ::instance_state_name::*;


/// <p>Describes the current state of the instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceState {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<i32>,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: Option<InstanceStateName>,
}


