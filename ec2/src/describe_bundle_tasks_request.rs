
use ::bundle_id_string_list::*;
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeBundleTasksRequest {

    #[serde(rename_deserialize="BundleId", rename_serialize="BundleId")]
    pub bundle_ids: Option<BundleIdStringList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
}


