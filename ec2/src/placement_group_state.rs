



#[derive(Debug, Clone)]
pub enum PlacementGroupState {
   Pending,Available,Deleting,Deleted,
}

string_enum!{ PlacementGroupState,
    PlacementGroupState::Pending => "pending" => PlacementGroupState::Pending,
    PlacementGroupState::Available => "available" => PlacementGroupState::Available,
    PlacementGroupState::Deleting => "deleting" => PlacementGroupState::Deleting,
    PlacementGroupState::Deleted => "deleted" => PlacementGroupState::Deleted

}