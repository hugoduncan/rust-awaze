
use ::date_time::*;
use ::telemetry_status::*;


/// <p>Describes telemetry for a VPN tunnel.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VgwTelemetry {

    #[serde(rename_deserialize="acceptedRouteCount", rename_serialize="AcceptedRouteCount")]
    pub accepted_route_count: Option<i32>,
    #[serde(rename_deserialize="lastStatusChange", rename_serialize="LastStatusChange")]
    pub last_status_change: Option<DateTime>,
    #[serde(rename_deserialize="outsideIpAddress", rename_serialize="OutsideIpAddress")]
    pub outside_ip_address: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<TelemetryStatus>,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
}


