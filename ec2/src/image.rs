
use ::architecture_values::*;
use ::block_device_mapping_list::*;
use ::device_type::*;
use ::hypervisor_type::*;
use ::image_state::*;
use ::image_type_values::*;
use ::platform_values::*;
use ::product_code_list::*;
use ::state_reason::*;
use ::tag_list::*;
use ::virtualization_type::*;


/// <p>Describes an image.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Image {

    #[serde(rename_deserialize="architecture", rename_serialize="Architecture")]
    pub architecture: Option<ArchitectureValues>,
    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingList>,
    #[serde(rename_deserialize="creationDate", rename_serialize="CreationDate")]
    pub creation_date: Option<String>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="hypervisor", rename_serialize="Hypervisor")]
    pub hypervisor: Option<HypervisorType>,
    #[serde(rename_deserialize="imageId", rename_serialize="ImageId")]
    pub image_id: Option<String>,
    #[serde(rename_deserialize="imageLocation", rename_serialize="ImageLocation")]
    pub image_location: Option<String>,
    #[serde(rename_deserialize="imageOwnerAlias", rename_serialize="ImageOwnerAlias")]
    pub image_owner_alias: Option<String>,
    #[serde(rename_deserialize="imageType", rename_serialize="ImageType")]
    pub image_type: Option<ImageTypeValues>,
    #[serde(rename_deserialize="kernelId", rename_serialize="KernelId")]
    pub kernel_id: Option<String>,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: Option<String>,
    #[serde(rename_deserialize="imageOwnerId", rename_serialize="ImageOwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="platform", rename_serialize="Platform")]
    pub platform: Option<PlatformValues>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="isPublic", rename_serialize="IsPublic")]
    pub public: Option<bool>,
    #[serde(rename_deserialize="ramdiskId", rename_serialize="RamdiskId")]
    pub ramdisk_id: Option<String>,
    #[serde(rename_deserialize="rootDeviceName", rename_serialize="RootDeviceName")]
    pub root_device_name: Option<String>,
    #[serde(rename_deserialize="rootDeviceType", rename_serialize="RootDeviceType")]
    pub root_device_type: Option<DeviceType>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<String>,
    #[serde(rename_deserialize="imageState", rename_serialize="ImageState")]
    pub state: Option<ImageState>,
    #[serde(rename_deserialize="stateReason", rename_serialize="StateReason")]
    pub state_reason: Option<StateReason>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="virtualizationType", rename_serialize="VirtualizationType")]
    pub virtualization_type: Option<VirtualizationType>,
}


