


/// <p>Describes an association between a route table and a subnet.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct RouteTableAssociation {

    #[serde(rename_deserialize="main", rename_serialize="Main")]
    pub main: Option<bool>,
    #[serde(rename_deserialize="routeTableAssociationId", rename_serialize="RouteTableAssociationId")]
    pub route_table_association_id: Option<String>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: Option<String>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
}


