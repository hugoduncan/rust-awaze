
use ::product_code_values::*;


/// <p>Describes a product code.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ProductCode {

    #[serde(rename_deserialize="productCode", rename_serialize="ProductCode")]
    pub product_code_id: Option<String>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub product_code_type: Option<ProductCodeValues>,
}


