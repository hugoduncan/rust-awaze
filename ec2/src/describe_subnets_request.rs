
use ::filter_list::*;
use ::subnet_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSubnetsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="SubnetId", rename_serialize="SubnetId")]
    pub subnet_ids: Option<SubnetIdStringList>,
}


