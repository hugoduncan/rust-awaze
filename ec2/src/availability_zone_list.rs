
use awaze_core::ItemVec;
use ::availability_zone::*;



pub type AvailabilityZoneList = ItemVec<AvailabilityZone>;
