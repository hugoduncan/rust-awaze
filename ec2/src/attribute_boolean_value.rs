


/// <p>The value to use when a resource attribute accepts a Boolean value.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AttributeBooleanValue {

    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: Option<bool>,
}


