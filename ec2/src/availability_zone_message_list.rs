
use awaze_core::ItemVec;
use ::availability_zone_message::*;



pub type AvailabilityZoneMessageList = ItemVec<AvailabilityZoneMessage>;
