
use ::filter_list::*;
use ::group_id_string_list::*;
use ::group_name_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSecurityGroupsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="GroupId", rename_serialize="GroupId")]
    pub group_ids: Option<GroupIdStringList>,
    #[serde(rename_deserialize="GroupName", rename_serialize="GroupName")]
    pub group_names: Option<GroupNameStringList>,
}


