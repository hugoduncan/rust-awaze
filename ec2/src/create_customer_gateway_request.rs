
use ::gateway_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateCustomerGatewayRequest {

    #[serde(rename_deserialize="BgpAsn", rename_serialize="BgpAsn")]
    pub bgp_asn: i32,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="IpAddress", rename_serialize="IpAddress")]
    pub public_ip: String,
    #[serde(rename_deserialize="GatewayType", rename_serialize="GatewayType")]
    pub gateway_type: Option<GatewayType>,
}


