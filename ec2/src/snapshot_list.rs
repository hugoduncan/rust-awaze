
use awaze_core::ItemVec;
use ::snapshot::*;



pub type SnapshotList = ItemVec<Snapshot>;
