
use ::reserved_instances_offering_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesOfferingsResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="reservedInstancesOfferingsSet", rename_serialize="ReservedInstancesOfferingsSet")]
    pub reserved_instances_offerings: Option<ReservedInstancesOfferingList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


