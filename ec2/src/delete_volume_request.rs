



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteVolumeRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VolumeId", rename_serialize="VolumeId")]
    pub volume_id: String,
}


