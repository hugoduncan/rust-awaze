
use awaze_core::ItemVec;
use ::volume_attachment::*;



pub type VolumeAttachmentList = ItemVec<VolumeAttachment>;
