
use ::reserved_instances_configuration_list::*;
use ::reserved_instances_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyReservedInstancesRequest {

    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="ReservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_ids: ReservedInstancesIdStringList,
    #[serde(rename_deserialize="ReservedInstancesConfigurationSetItemType", rename_serialize="ReservedInstancesConfigurationSetItemType")]
    pub target_configurations: ReservedInstancesConfigurationList,
}


