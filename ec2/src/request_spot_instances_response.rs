
use ::spot_instance_request_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct RequestSpotInstancesResponse {

    #[serde(rename_deserialize="spotInstanceRequestSet", rename_serialize="SpotInstanceRequestSet")]
    pub spot_instance_requests: Option<SpotInstanceRequestList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


