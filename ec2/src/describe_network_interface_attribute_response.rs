
use ::attribute_boolean_value::*;
use ::attribute_value::*;
use ::group_identifier_list::*;
use ::network_interface_attachment::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkInterfaceAttributeResponse {

    #[serde(rename_deserialize="attachment", rename_serialize="Attachment")]
    pub attachment: Option<NetworkInterfaceAttachment>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<AttributeValue>,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


