
use awaze_core::ItemVec;
use ::vpc_attachment::*;



pub type VpcAttachmentList = ItemVec<VpcAttachment>;
