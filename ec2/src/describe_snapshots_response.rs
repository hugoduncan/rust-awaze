
use ::snapshot_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSnapshotsResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="snapshotSet", rename_serialize="SnapshotSet")]
    pub snapshots: Option<SnapshotList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


