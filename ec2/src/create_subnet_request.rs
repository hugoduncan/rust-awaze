



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSubnetRequest {

    #[serde(rename_deserialize="AvailabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="CidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


