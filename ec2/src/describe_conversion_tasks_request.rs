
use ::conversion_id_string_list::*;
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeConversionTasksRequest {

    #[serde(rename_deserialize="conversionTaskId", rename_serialize="ConversionTaskId")]
    pub conversion_task_ids: Option<ConversionIdStringList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
}


