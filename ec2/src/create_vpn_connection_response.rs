
use ::vpn_connection::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpnConnectionResponse {

    #[serde(rename_deserialize="vpnConnection", rename_serialize="VpnConnection")]
    pub vpn_connection: Option<VpnConnection>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


