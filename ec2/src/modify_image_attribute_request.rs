
use ::attribute_value::*;
use ::launch_permission_modifications::*;
use ::product_code_string_list::*;
use ::user_group_string_list::*;
use ::user_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyImageAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: Option<String>,
    #[serde(rename_deserialize="Description", rename_serialize="Description")]
    pub description: Option<AttributeValue>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_id: String,
    #[serde(rename_deserialize="LaunchPermission", rename_serialize="LaunchPermission")]
    pub launch_permission: Option<LaunchPermissionModifications>,
    #[serde(rename_deserialize="OperationType", rename_serialize="OperationType")]
    pub operation_type: Option<String>,
    #[serde(rename_deserialize="ProductCode", rename_serialize="ProductCode")]
    pub product_codes: Option<ProductCodeStringList>,
    #[serde(rename_deserialize="UserGroup", rename_serialize="UserGroup")]
    pub user_groups: Option<UserGroupStringList>,
    #[serde(rename_deserialize="UserId", rename_serialize="UserId")]
    pub user_ids: Option<UserIdStringList>,
    #[serde(rename_deserialize="Value", rename_serialize="Value")]
    pub value: Option<String>,
}


