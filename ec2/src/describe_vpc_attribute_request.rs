
use ::vpc_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: Option<VpcAttributeName>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


