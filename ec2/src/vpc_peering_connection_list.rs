
use awaze_core::ItemVec;
use ::vpc_peering_connection::*;



pub type VpcPeeringConnectionList = ItemVec<VpcPeeringConnection>;
