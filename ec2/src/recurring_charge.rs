
use ::recurring_charge_frequency::*;


/// <p>Describes a recurring charge.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct RecurringCharge {

    #[serde(rename_deserialize="amount", rename_serialize="Amount")]
    pub amount: Option<f64>,
    #[serde(rename_deserialize="frequency", rename_serialize="Frequency")]
    pub frequency: Option<RecurringChargeFrequency>,
}


