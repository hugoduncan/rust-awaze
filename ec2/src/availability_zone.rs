
use ::availability_zone_message_list::*;
use ::availability_zone_state::*;


/// <p>Describes an Availability Zone.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AvailabilityZone {

    #[serde(rename_deserialize="messageSet", rename_serialize="MessageSet")]
    pub messages: Option<AvailabilityZoneMessageList>,
    #[serde(rename_deserialize="regionName", rename_serialize="RegionName")]
    pub region_name: Option<String>,
    #[serde(rename_deserialize="zoneState", rename_serialize="ZoneState")]
    pub state: Option<AvailabilityZoneState>,
    #[serde(rename_deserialize="zoneName", rename_serialize="ZoneName")]
    pub zone_name: Option<String>,
}


