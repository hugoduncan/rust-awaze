



#[derive(Debug, Clone)]
pub enum BundleTaskState {
   Pending,WaitingForShutdown,Bundling,Storing,Cancelling,Complete,Failed,
}

string_enum!{ BundleTaskState,
    BundleTaskState::Pending => "pending" => BundleTaskState::Pending,
    BundleTaskState::WaitingForShutdown => "waiting-for-shutdown" => BundleTaskState::WaitingForShutdown,
    BundleTaskState::Bundling => "bundling" => BundleTaskState::Bundling,
    BundleTaskState::Storing => "storing" => BundleTaskState::Storing,
    BundleTaskState::Cancelling => "cancelling" => BundleTaskState::Cancelling,
    BundleTaskState::Complete => "complete" => BundleTaskState::Complete,
    BundleTaskState::Failed => "failed" => BundleTaskState::Failed

}