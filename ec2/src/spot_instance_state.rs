



#[derive(Debug, Clone)]
pub enum SpotInstanceState {
   Open,Active,Closed,Cancelled,Failed,
}

string_enum!{ SpotInstanceState,
    SpotInstanceState::Open => "open" => SpotInstanceState::Open,
    SpotInstanceState::Active => "active" => SpotInstanceState::Active,
    SpotInstanceState::Closed => "closed" => SpotInstanceState::Closed,
    SpotInstanceState::Cancelled => "cancelled" => SpotInstanceState::Cancelled,
    SpotInstanceState::Failed => "failed" => SpotInstanceState::Failed

}