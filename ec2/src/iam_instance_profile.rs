


/// <p>Describes an IAM instance profile.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct IamInstanceProfile {

    #[serde(rename_deserialize="arn", rename_serialize="Arn")]
    pub arn: Option<String>,
    #[serde(rename_deserialize="id", rename_serialize="Id")]
    pub id: Option<String>,
}


