


/// <p>The value to use for a resource attribute.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AttributeValue {

    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: Option<String>,
}


