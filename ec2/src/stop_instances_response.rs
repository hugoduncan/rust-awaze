
use ::instance_state_change_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct StopInstancesResponse {

    #[serde(rename_deserialize="instancesSet", rename_serialize="InstancesSet")]
    pub stopping_instances: Option<InstanceStateChangeList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


