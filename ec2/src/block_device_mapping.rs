
use ::ebs_block_device::*;


/// <p>Describes a block device mapping.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct BlockDeviceMapping {

    #[serde(rename_deserialize="deviceName", rename_serialize="DeviceName")]
    pub device_name: Option<String>,
    #[serde(rename_deserialize="ebs", rename_serialize="Ebs")]
    pub ebs: Option<EbsBlockDevice>,
    #[serde(rename_deserialize="noDevice", rename_serialize="NoDevice")]
    pub no_device: Option<String>,
    #[serde(rename_deserialize="virtualName", rename_serialize="VirtualName")]
    pub virtual_name: Option<String>,
}


