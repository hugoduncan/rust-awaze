



#[derive(Debug, Clone)]
pub enum OfferingTypeValues {
   HeavyUtilization,MediumUtilization,LightUtilization,NoUpfront,PartialUpfront,AllUpfront,
}

string_enum!{ OfferingTypeValues,
    OfferingTypeValues::HeavyUtilization => "Heavy Utilization" => OfferingTypeValues::HeavyUtilization,
    OfferingTypeValues::MediumUtilization => "Medium Utilization" => OfferingTypeValues::MediumUtilization,
    OfferingTypeValues::LightUtilization => "Light Utilization" => OfferingTypeValues::LightUtilization,
    OfferingTypeValues::NoUpfront => "No Upfront" => OfferingTypeValues::NoUpfront,
    OfferingTypeValues::PartialUpfront => "Partial Upfront" => OfferingTypeValues::PartialUpfront,
    OfferingTypeValues::AllUpfront => "All Upfront" => OfferingTypeValues::AllUpfront

}