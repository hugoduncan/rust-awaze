
use ::private_ip_address_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct UnassignPrivateIpAddressesRequest {

    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_addresses: PrivateIpAddressStringList,
}


