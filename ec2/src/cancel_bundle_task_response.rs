
use ::bundle_task::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelBundleTaskResponse {

    #[serde(rename_deserialize="bundleInstanceTask", rename_serialize="BundleInstanceTask")]
    pub bundle_task: Option<BundleTask>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


