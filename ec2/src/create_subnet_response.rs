
use ::subnet::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSubnetResponse {

    #[serde(rename_deserialize="subnet", rename_serialize="Subnet")]
    pub subnet: Option<Subnet>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


