
use awaze_core::ItemVec;
use ::create_volume_permission::*;



pub type CreateVolumePermissionList = ItemVec<CreateVolumePermission>;
