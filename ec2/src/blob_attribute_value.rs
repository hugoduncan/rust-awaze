
use ::blob::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct BlobAttributeValue {

    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: Option<Blob>,
}


