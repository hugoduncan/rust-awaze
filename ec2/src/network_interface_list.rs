
use awaze_core::ItemVec;
use ::network_interface::*;



pub type NetworkInterfaceList = ItemVec<NetworkInterface>;
