
use ::s3_storage::*;


/// <p>Describes the storage location for an instance store-backed AMI.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Storage {

    #[serde(rename_deserialize="S3", rename_serialize="S3")]
    pub s3: Option<S3Storage>,
}


