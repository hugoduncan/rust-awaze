
use ::vpn_connection_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpnConnectionsResponse {

    #[serde(rename_deserialize="vpnConnectionSet", rename_serialize="VpnConnectionSet")]
    pub vpn_connections: Option<VpnConnectionList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


