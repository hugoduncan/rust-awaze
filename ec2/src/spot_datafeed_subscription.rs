
use ::datafeed_subscription_state::*;
use ::spot_instance_state_fault::*;


/// <p>Describes the data feed for a Spot Instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotDatafeedSubscription {

    #[serde(rename_deserialize="bucket", rename_serialize="Bucket")]
    pub bucket: Option<String>,
    #[serde(rename_deserialize="fault", rename_serialize="Fault")]
    pub fault: Option<SpotInstanceStateFault>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="prefix", rename_serialize="Prefix")]
    pub prefix: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<DatafeedSubscriptionState>,
}


