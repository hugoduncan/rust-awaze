
use ::group_identifier_list::*;
use ::tag_list::*;


/// <p>Describes a linked EC2-Classic instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ClassicLinkInstance {

    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


