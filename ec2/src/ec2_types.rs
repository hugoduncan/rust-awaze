/// <p>Describes an image attribute.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ImageAttribute {


#[serde(rename="imageId")]
pub image_id: String,

#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="kernel")]
pub kernel: AttributeValue,

#[serde(rename="launchPermission")]
pub launch_permission: LaunchPermissionList,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: AttributeValue,

#[serde(rename="ramdisk")]
pub ramdisk: AttributeValue,

#[serde(rename="description")]
pub description: AttributeValue,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportInstanceLaunchSpecification {


#[serde(rename="GroupName")]
pub group_name: SecurityGroupStringList,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="placement")]
pub placement: Placement,

#[serde(rename="monitoring")]
pub monitoring: bool,

#[serde(rename="architecture")]
pub architecture: ArchitectureValues,

#[serde(rename="userData")]
pub user_data: UserData,

#[serde(rename="instanceInitiatedShutdownBehavior")]
pub instance_initiated_shutdown_behavior: ShutdownBehavior,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="GroupId")]
pub group_id: SecurityGroupIdStringList,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="additionalInfo")]
pub additional_info: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateNetworkInterfaceResult {


#[serde(rename="networkInterface")]
pub network_interface: NetworkInterface,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateNetworkAclEntryRequest {


#[serde(rename="ruleNumber")]
pub rule_number: i32,

#[serde(rename="egress")]
pub egress: bool,

#[serde(rename="ruleAction")]
pub rule_action: RuleAction,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="protocol")]
pub protocol: String,

#[serde(rename="portRange")]
pub port_range: PortRange,

#[serde(rename="cidrBlock")]
pub cidr_block: String,

#[serde(rename="Icmp")]
pub icmp: IcmpTypeCode,

#[serde(rename="networkAclId")]
pub network_acl_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotDatafeedSubscriptionResult {


#[serde(rename="spotDatafeedSubscription")]
pub spot_datafeed_subscription: SpotDatafeedSubscription,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DiskImageDetail {


#[serde(rename="format")]
pub format: DiskImageFormat,

#[serde(rename="importManifestUrl")]
pub import_manifest_url: String,

#[serde(rename="bytes")]
pub bytes: i64,
}



/// <p>Describes a key pair.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct KeyPairInfo {


#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="keyFingerprint")]
pub key_fingerprint: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelConversionRequest {


#[serde(rename="reasonMessage")]
pub reason_message: String,

#[serde(rename="conversionTaskId")]
pub conversion_task_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct TerminateInstancesResult {


#[serde(rename="instancesSet")]
pub instances_set: InstanceStateChangeList,
}



/// <p>Describes a Reserved Instance offering.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesOffering {


#[serde(rename="recurringCharges")]
pub recurring_charges: RecurringChargesList,

#[serde(rename="instanceTenancy")]
pub instance_tenancy: Tenancy,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="usagePrice")]
pub usage_price: f32,

#[serde(rename="reservedInstancesOfferingId")]
pub reserved_instances_offering_id: String,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="marketplace")]
pub marketplace: bool,

#[serde(rename="pricingDetailsSet")]
pub pricing_details_set: PricingDetailsList,

#[serde(rename="productDescription")]
pub product_description: RIProductDescription,

#[serde(rename="currencyCode")]
pub currency_code: CurrencyCodeValues,

#[serde(rename="duration")]
pub duration: i64,

#[serde(rename="offeringType")]
pub offering_type: OfferingTypeValues,

#[serde(rename="fixedPrice")]
pub fixed_price: f32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelSpotInstanceRequestsResult {


#[serde(rename="spotInstanceRequestSet")]
pub spot_instance_request_set: CancelledSpotInstanceRequestList,
}



/// <p>Describes an Amazon EBS block device.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct EbsBlockDevice {


#[serde(rename="snapshotId")]
pub snapshot_id: String,

#[serde(rename="iops")]
pub iops: i32,

#[serde(rename="encrypted")]
pub encrypted: bool,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="volumeType")]
pub volume_type: VolumeType,

#[serde(rename="volumeSize")]
pub volume_size: i32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpnGatewaysResult {


#[serde(rename="vpnGatewaySet")]
pub vpn_gateway_set: VpnGatewayList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSubnetsResult {


#[serde(rename="subnetSet")]
pub subnet_set: SubnetList,
}



/// <p>Describes an instance event.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceStatusEvent {


#[serde(rename="description")]
pub description: String,

#[serde(rename="notAfter")]
pub not_after: DateTime,

#[serde(rename="notBefore")]
pub not_before: DateTime,

#[serde(rename="code")]
pub code: EventCode,
}



/// <p>Describes a tag.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct TagDescription {


#[serde(rename="resourceType")]
pub resource_type: ResourceType,

#[serde(rename="resourceId")]
pub resource_id: String,

#[serde(rename="value")]
pub value: String,

#[serde(rename="key")]
pub key: String,
}



/// <p>Describes a security group rule.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct IpPermission {


#[serde(rename="toPort")]
pub to_port: i32,

#[serde(rename="ipRanges")]
pub ip_ranges: IpRangeList,

#[serde(rename="ipProtocol")]
pub ip_protocol: String,

#[serde(rename="groups")]
pub groups: UserIdGroupPairList,

#[serde(rename="fromPort")]
pub from_port: i32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribePlacementGroupsResult {


#[serde(rename="placementGroupSet")]
pub placement_group_set: PlacementGroupList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct BlobAttributeValue {


#[serde(rename="value")]
pub value: Blob,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct NewDhcpConfiguration {


#[serde(rename="key")]
pub key: String,

#[serde(rename="Value")]
pub value: ValueStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumeStatusResult {


#[serde(rename="volumeStatusSet")]
pub volume_status_set: VolumeStatusList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DisassociateAddressRequest {


#[serde(rename="AssociationId")]
pub association_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="PublicIp")]
pub public_ip: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeImagesResult {


#[serde(rename="imagesSet")]
pub images_set: ImageList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpnConnectionResult {


#[serde(rename="vpnConnection")]
pub vpn_connection: VpnConnection,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct GetConsoleOutputResult {


#[serde(rename="output")]
pub output: String,

#[serde(rename="timestamp")]
pub timestamp: DateTime,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateNetworkAclRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcId")]
pub vpc_id: String,
}



/// <p>Describes the instance status.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceStatusDetails {


#[serde(rename="impairedSince")]
pub impaired_since: DateTime,

#[serde(rename="name")]
pub name: StatusName,

#[serde(rename="status")]
pub status: StatusType,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachClassicLinkVpcRequest {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SecurityGroupId")]
pub security_group_id: GroupIdStringList,

#[serde(rename="vpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct UserData {


#[serde(rename="data")]
pub data: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RunInstancesRequest {


#[serde(rename="ebsOptimized")]
pub ebs_optimized: bool,

#[serde(rename="instanceInitiatedShutdownBehavior")]
pub instance_initiated_shutdown_behavior: ShutdownBehavior,

#[serde(rename="SecurityGroupId")]
pub security_group_id: SecurityGroupIdStringList,

#[serde(rename="InstanceType")]
pub instance_type: InstanceType,

#[serde(rename="RamdiskId")]
pub ramdisk_id: String,

#[serde(rename="ImageId")]
pub image_id: String,

#[serde(rename="additionalInfo")]
pub additional_info: String,

#[serde(rename="Placement")]
pub placement: Placement,

#[serde(rename="Monitoring")]
pub monitoring: RunInstancesMonitoringEnabled,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="UserData")]
pub user_data: String,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="SecurityGroup")]
pub security_group: SecurityGroupStringList,

#[serde(rename="MaxCount")]
pub max_count: i32,

#[serde(rename="MinCount")]
pub min_count: i32,

#[serde(rename="KernelId")]
pub kernel_id: String,

#[serde(rename="networkInterface")]
pub network_interface: InstanceNetworkInterfaceSpecificationList,

#[serde(rename="iamInstanceProfile")]
pub iam_instance_profile: IamInstanceProfileSpecification,

#[serde(rename="BlockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingRequestList,

#[serde(rename="disableApiTermination")]
pub disable_api_termination: bool,

#[serde(rename="KeyName")]
pub key_name: String,

#[serde(rename="SubnetId")]
pub subnet_id: String,

#[serde(rename="clientToken")]
pub client_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkInterfaceAttributeResult {


#[serde(rename="description")]
pub description: AttributeValue,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: AttributeBooleanValue,

#[serde(rename="attachment")]
pub attachment: NetworkInterfaceAttachment,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpcPeeringConnectionResult {


#[serde(rename="vpcPeeringConnection")]
pub vpc_peering_connection: VpcPeeringConnection,
}



/// <p>Describes the launch specification for an instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct LaunchSpecification {


#[serde(rename="placement")]
pub placement: SpotPlacement,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="userData")]
pub user_data: String,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="ramdiskId")]
pub ramdisk_id: String,

#[serde(rename="iamInstanceProfile")]
pub iam_instance_profile: IamInstanceProfileSpecification,

#[serde(rename="kernelId")]
pub kernel_id: String,

#[serde(rename="networkInterfaceSet")]
pub network_interface_set: InstanceNetworkInterfaceSpecificationList,

#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="monitoring")]
pub monitoring: RunInstancesMonitoringEnabled,

#[serde(rename="imageId")]
pub image_id: String,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingList,

#[serde(rename="ebsOptimized")]
pub ebs_optimized: bool,

#[serde(rename="addressingType")]
pub addressing_type: String,
}



/// <p>Describes a parameter used to set up an Amazon EBS volume in a block device mapping.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct EbsInstanceBlockDevice {


#[serde(rename="volumeId")]
pub volume_id: String,

#[serde(rename="attachTime")]
pub attach_time: DateTime,

#[serde(rename="status")]
pub status: AttachmentStatus,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteSpotDatafeedSubscriptionRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct StartInstancesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,

#[serde(rename="additionalInfo")]
pub additional_info: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DisableVgwRoutePropagationRequest {


#[serde(rename="RouteTableId")]
pub route_table_id: String,

#[serde(rename="GatewayId")]
pub gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateImageRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingRequestList,

#[serde(rename="description")]
pub description: String,

#[serde(rename="name")]
pub name: String,

#[serde(rename="noReboot")]
pub no_reboot: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssociateRouteTableRequest {


#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="routeTableId")]
pub route_table_id: String,
}



/// <p>Describes a range of ports.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PortRange {


#[serde(rename="to")]
pub to: i32,

#[serde(rename="from")]
pub from: i32,
}



/// <p>Describes a route in a route table.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Route {


#[serde(rename="instanceOwnerId")]
pub instance_owner_id: String,

#[serde(rename="gatewayId")]
pub gateway_id: String,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,

#[serde(rename="origin")]
pub origin: RouteOrigin,

#[serde(rename="destinationCidrBlock")]
pub destination_cidr_block: String,

#[serde(rename="state")]
pub state: RouteState,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="instanceId")]
pub instance_id: String,
}



/// <p>Describes a Reserved Instance listing.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesListing {


#[serde(rename="reservedInstancesListingId")]
pub reserved_instances_listing_id: String,

#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="updateDate")]
pub update_date: DateTime,

#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,

#[serde(rename="createDate")]
pub create_date: DateTime,

#[serde(rename="priceSchedules")]
pub price_schedules: PriceScheduleList,

#[serde(rename="clientToken")]
pub client_token: String,

#[serde(rename="status")]
pub status: ListingStatus,

#[serde(rename="instanceCounts")]
pub instance_counts: InstanceCountList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesOfferingsRequest {


#[serde(rename="ReservedInstancesOfferingId")]
pub reserved_instances_offering_id: ReservedInstancesOfferingIdStringList,

#[serde(rename="AvailabilityZone")]
pub availability_zone: String,

#[serde(rename="offeringType")]
pub offering_type: OfferingTypeValues,

#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="MinDuration")]
pub min_duration: i64,

#[serde(rename="IncludeMarketplace")]
pub include_marketplace: bool,

#[serde(rename="InstanceType")]
pub instance_type: InstanceType,

#[serde(rename="ProductDescription")]
pub product_description: RIProductDescription,

#[serde(rename="instanceTenancy")]
pub instance_tenancy: Tenancy,

#[serde(rename="maxResults")]
pub max_results: i32,

#[serde(rename="MaxDuration")]
pub max_duration: i64,

#[serde(rename="MaxInstanceCount")]
pub max_instance_count: i32,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcPeeringConnectionsRequest {


#[serde(rename="VpcPeeringConnectionId")]
pub vpc_peering_connection_id: ValueStringList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesResult {


#[serde(rename="reservedInstancesSet")]
pub reserved_instances_set: ReservedInstancesList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RejectVpcPeeringConnectionResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteSecurityGroupRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="GroupId")]
pub group_id: String,

#[serde(rename="GroupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateCustomerGatewayResult {


#[serde(rename="customerGateway")]
pub customer_gateway: CustomerGateway,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkAclsResult {


#[serde(rename="networkAclSet")]
pub network_acl_set: NetworkAclList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReleaseAddressRequest {


#[serde(rename="PublicIp")]
pub public_ip: String,

#[serde(rename="AllocationId")]
pub allocation_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes an Elastic IP address.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Address {


#[serde(rename="publicIp")]
pub public_ip: String,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="allocationId")]
pub allocation_id: String,

#[serde(rename="associationId")]
pub association_id: String,

#[serde(rename="networkInterfaceOwnerId")]
pub network_interface_owner_id: String,

#[serde(rename="domain")]
pub domain: DomainType,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,
}



/// <p>Describes a volume.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Volume {


#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="iops")]
pub iops: i32,

#[serde(rename="kmsKeyId")]
pub kms_key_id: String,

#[serde(rename="volumeId")]
pub volume_id: String,

#[serde(rename="status")]
pub status: VolumeState,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="snapshotId")]
pub snapshot_id: String,

#[serde(rename="volumeType")]
pub volume_type: VolumeType,

#[serde(rename="encrypted")]
pub encrypted: bool,

#[serde(rename="attachmentSet")]
pub attachment_set: VolumeAttachmentList,

#[serde(rename="createTime")]
pub create_time: DateTime,

#[serde(rename="size")]
pub size: i32,
}



/// <p>Describes an attachment change.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkInterfaceAttachmentChanges {


#[serde(rename="attachmentId")]
pub attachment_id: String,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,
}



/// <p>Describes a volume status operation code.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeStatusAction {


#[serde(rename="code")]
pub code: String,

#[serde(rename="eventType")]
pub event_type: String,

#[serde(rename="description")]
pub description: String,

#[serde(rename="eventId")]
pub event_id: String,
}



/// <p>A filter name and value pair that is used to return a more specific list of results. Filters can be used to match a set of resources by various criteria, such as tags, attributes, or IDs.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Filter {


#[serde(rename="Value")]
pub value: ValueStringList,

#[serde(rename="Name")]
pub name: String,
}



/// <p>Describes a network interface.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceNetworkInterface {


#[serde(rename="privateIpAddressesSet")]
pub private_ip_addresses_set: InstancePrivateIpAddressList,

#[serde(rename="association")]
pub association: InstanceNetworkInterfaceAssociation,

#[serde(rename="privateDnsName")]
pub private_dns_name: String,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="attachment")]
pub attachment: InstanceNetworkInterfaceAttachment,

#[serde(rename="macAddress")]
pub mac_address: String,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="description")]
pub description: String,

#[serde(rename="status")]
pub status: NetworkInterfaceStatus,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: bool,
}



/// <p>Describes a reservation.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Reservation {


#[serde(rename="reservationId")]
pub reservation_id: String,

#[serde(rename="requesterId")]
pub requester_id: String,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="instancesSet")]
pub instances_set: InstanceList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeClassicLinkInstancesRequest {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="maxResults")]
pub max_results: i32,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyReservedInstancesRequest {


#[serde(rename="clientToken")]
pub client_token: String,

#[serde(rename="ReservedInstancesId")]
pub reserved_instances_id: ReservedInstancesIdStringList,

#[serde(rename="ReservedInstancesConfigurationSetItemType")]
pub reserved_instances_configuration_set_item_type: ReservedInstancesConfigurationList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpnGatewayResult {


#[serde(rename="vpnGateway")]
pub vpn_gateway: VpnGateway,
}



/// <p>Describes a snapshot.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Snapshot {


#[serde(rename="progress")]
pub progress: String,

#[serde(rename="encrypted")]
pub encrypted: bool,

#[serde(rename="startTime")]
pub start_time: DateTime,

#[serde(rename="ownerAlias")]
pub owner_alias: String,

#[serde(rename="kmsKeyId")]
pub kms_key_id: String,

#[serde(rename="volumeId")]
pub volume_id: String,

#[serde(rename="snapshotId")]
pub snapshot_id: String,

#[serde(rename="description")]
pub description: String,

#[serde(rename="status")]
pub status: SnapshotState,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="volumeSize")]
pub volume_size: i32,

#[serde(rename="tagSet")]
pub tag_set: TagList,
}



/// <p>Describes the volume status.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeStatusItem {


#[serde(rename="volumeStatus")]
pub volume_status: VolumeStatusInfo,

#[serde(rename="actionsSet")]
pub actions_set: VolumeStatusActionsList,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="eventsSet")]
pub events_set: VolumeStatusEventsList,

#[serde(rename="volumeId")]
pub volume_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachInternetGatewayRequest {


#[serde(rename="internetGatewayId")]
pub internet_gateway_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcId")]
pub vpc_id: String,
}



/// <p>Describes an entry in a network ACL.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkAclEntry {


#[serde(rename="protocol")]
pub protocol: String,

#[serde(rename="ruleNumber")]
pub rule_number: i32,

#[serde(rename="icmpTypeCode")]
pub icmp_type_code: IcmpTypeCode,

#[serde(rename="egress")]
pub egress: bool,

#[serde(rename="ruleAction")]
pub rule_action: RuleAction,

#[serde(rename="cidrBlock")]
pub cidr_block: String,

#[serde(rename="portRange")]
pub port_range: PortRange,
}



/// <p>Describes a network interface.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkInterface {


#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="association")]
pub association: NetworkInterfaceAssociation,

#[serde(rename="status")]
pub status: NetworkInterfaceStatus,

#[serde(rename="requesterId")]
pub requester_id: String,

#[serde(rename="macAddress")]
pub mac_address: String,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: bool,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="privateIpAddressesSet")]
pub private_ip_addresses_set: NetworkInterfacePrivateIpAddressList,

#[serde(rename="requesterManaged")]
pub requester_managed: bool,

#[serde(rename="attachment")]
pub attachment: NetworkInterfaceAttachment,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="description")]
pub description: String,

#[serde(rename="privateDnsName")]
pub private_dns_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RevokeSecurityGroupEgressRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="ipProtocol")]
pub ip_protocol: String,

#[serde(rename="sourceSecurityGroupOwnerId")]
pub source_security_group_owner_id: String,

#[serde(rename="ipPermissions")]
pub ip_permissions: IpPermissionList,

#[serde(rename="toPort")]
pub to_port: i32,

#[serde(rename="fromPort")]
pub from_port: i32,

#[serde(rename="cidrIp")]
pub cidr_ip: String,

#[serde(rename="groupId")]
pub group_id: String,

#[serde(rename="sourceSecurityGroupName")]
pub source_security_group_name: String,
}



/// <p>Describes a product code.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ProductCode {


#[serde(rename="ProductType")]
pub product_type: ProductCodeValues,

#[serde(rename="productCode")]
pub product_code: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeKeyPairsResult {


#[serde(rename="keySet")]
pub key_set: KeyPairList,
}



/// <p>Describes a private IP address.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstancePrivateIpAddress {


#[serde(rename="privateDnsName")]
pub private_dns_name: String,

#[serde(rename="association")]
pub association: InstanceNetworkInterfaceAssociation,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="primary")]
pub primary: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInstancesResult {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="reservationSet")]
pub reservation_set: ReservationList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeExportTasksResult {


#[serde(rename="exportTaskSet")]
pub export_task_set: ExportTaskList,
}



/// <p>Describes an error for <a>BundleInstance</a>.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct BundleTaskError {


#[serde(rename="message")]
pub message: String,

#[serde(rename="code")]
pub code: String,
}



/// <p>Describes a block device mapping entry.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceBlockDeviceMappingSpecification {


#[serde(rename="noDevice")]
pub no_device: String,

#[serde(rename="ebs")]
pub ebs: EbsInstanceBlockDeviceSpecification,

#[serde(rename="virtualName")]
pub virtual_name: String,

#[serde(rename="deviceName")]
pub device_name: String,
}



/// <p>Describes the status of a Spot Instance request.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotInstanceStatus {


#[serde(rename="message")]
pub message: String,

#[serde(rename="updateTime")]
pub update_time: DateTime,

#[serde(rename="code")]
pub code: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AuthorizeSecurityGroupEgressRequest {


#[serde(rename="cidrIp")]
pub cidr_ip: String,

#[serde(rename="sourceSecurityGroupName")]
pub source_security_group_name: String,

#[serde(rename="groupId")]
pub group_id: String,

#[serde(rename="toPort")]
pub to_port: i32,

#[serde(rename="sourceSecurityGroupOwnerId")]
pub source_security_group_owner_id: String,

#[serde(rename="fromPort")]
pub from_port: i32,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="ipProtocol")]
pub ip_protocol: String,

#[serde(rename="ipPermissions")]
pub ip_permissions: IpPermissionList,
}



/// <p>Describes a block device mapping.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceBlockDeviceMapping {


#[serde(rename="deviceName")]
pub device_name: String,

#[serde(rename="ebs")]
pub ebs: EbsInstanceBlockDevice,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct StopInstancesRequest {


#[serde(rename="force")]
pub force: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcClassicLinkRequest {


#[serde(rename="VpcId")]
pub vpc_id: VpcClassicLinkIdList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyInstanceAttributeRequest {


#[serde(rename="instanceType")]
pub instance_type: AttributeValue,

#[serde(rename="SourceDestCheck")]
pub source_dest_check: AttributeBooleanValue,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: InstanceBlockDeviceMappingSpecificationList,

#[serde(rename="userData")]
pub user_data: BlobAttributeValue,

#[serde(rename="kernel")]
pub kernel: AttributeValue,

#[serde(rename="GroupId")]
pub group_id: GroupIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="attribute")]
pub attribute: InstanceAttributeName,

#[serde(rename="instanceInitiatedShutdownBehavior")]
pub instance_initiated_shutdown_behavior: AttributeValue,

#[serde(rename="disableApiTermination")]
pub disable_api_termination: AttributeBooleanValue,

#[serde(rename="ebsOptimized")]
pub ebs_optimized: AttributeBooleanValue,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: AttributeValue,

#[serde(rename="value")]
pub value: String,

#[serde(rename="ramdisk")]
pub ramdisk: AttributeValue,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelReservedInstancesListingRequest {


#[serde(rename="reservedInstancesListingId")]
pub reserved_instances_listing_id: String,
}



/// <p>Describes a request to cancel a Spot Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelledSpotInstanceRequest {


#[serde(rename="state")]
pub state: CancelSpotInstanceRequestState,

#[serde(rename="spotInstanceRequestId")]
pub spot_instance_request_id: String,
}



/// <p>Describes the launch specification for an instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct RequestSpotLaunchSpecification {


#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="SecurityGroupId")]
pub security_group_id: ValueStringList,

#[serde(rename="placement")]
pub placement: SpotPlacement,

#[serde(rename="ramdiskId")]
pub ramdisk_id: String,

#[serde(rename="SecurityGroup")]
pub security_group: ValueStringList,

#[serde(rename="imageId")]
pub image_id: String,

#[serde(rename="addressingType")]
pub addressing_type: String,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingList,

#[serde(rename="monitoring")]
pub monitoring: RunInstancesMonitoringEnabled,

#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="userData")]
pub user_data: String,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="ebsOptimized")]
pub ebs_optimized: bool,

#[serde(rename="iamInstanceProfile")]
pub iam_instance_profile: IamInstanceProfileSpecification,

#[serde(rename="kernelId")]
pub kernel_id: String,

#[serde(rename="NetworkInterface")]
pub network_interface: InstanceNetworkInterfaceSpecificationList,
}



/// <p>Describes a virtual private gateway propagating route.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PropagatingVgw {


#[serde(rename="gatewayId")]
pub gateway_id: String,
}



/// <p>Describes an attachment between a virtual private gateway and a VPC.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpcAttachment {


#[serde(rename="state")]
pub state: AttachmentStatus,

#[serde(rename="vpcId")]
pub vpc_id: String,
}



/// <p>Describes the limit price of a Reserved Instance offering.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstanceLimitPrice {


#[serde(rename="currencyCode")]
pub currency_code: CurrencyCodeValues,

#[serde(rename="amount")]
pub amount: f64,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DisableVpcClassicLinkResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}



/// <p>Describes whether a VPC is enabled for ClassicLink.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpcClassicLink {


#[serde(rename="classicLinkEnabled")]
pub classic_link_enabled: bool,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="vpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelReservedInstancesListingResult {


#[serde(rename="reservedInstancesListingsSet")]
pub reserved_instances_listings_set: ReservedInstancesListingList,
}



/// <p>Describes a Reserved Instance listing state.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceCount {


#[serde(rename="instanceCount")]
pub instance_count: i32,

#[serde(rename="state")]
pub state: ListingState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateKeyPairRequest {


#[serde(rename="KeyName")]
pub key_name: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ConfirmProductInstanceResult {


#[serde(rename="ownerId")]
pub owner_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateDhcpOptionsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="dhcpConfiguration")]
pub dhcp_configuration: NewDhcpConfigurationList,
}



/// <p>Describes an instance attribute.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceAttribute {


#[serde(rename="ramdisk")]
pub ramdisk: AttributeValue,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: AttributeBooleanValue,

#[serde(rename="kernel")]
pub kernel: AttributeValue,

#[serde(rename="instanceType")]
pub instance_type: AttributeValue,

#[serde(rename="instanceInitiatedShutdownBehavior")]
pub instance_initiated_shutdown_behavior: AttributeValue,

#[serde(rename="userData")]
pub user_data: AttributeValue,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: InstanceBlockDeviceMappingList,

#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="ebsOptimized")]
pub ebs_optimized: AttributeBooleanValue,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: AttributeValue,

#[serde(rename="disableApiTermination")]
pub disable_api_termination: AttributeBooleanValue,

#[serde(rename="rootDeviceName")]
pub root_device_name: AttributeValue,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,
}



/// <p>Describes an instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Instance {


#[serde(rename="sourceDestCheck")]
pub source_dest_check: bool,

#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="privateDnsName")]
pub private_dns_name: String,

#[serde(rename="launchTime")]
pub launch_time: DateTime,

#[serde(rename="amiLaunchIndex")]
pub ami_launch_index: i32,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: String,

#[serde(rename="architecture")]
pub architecture: ArchitectureValues,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="ebsOptimized")]
pub ebs_optimized: bool,

#[serde(rename="ramdiskId")]
pub ramdisk_id: String,

#[serde(rename="instanceState")]
pub instance_state: InstanceState,

#[serde(rename="kernelId")]
pub kernel_id: String,

#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="clientToken")]
pub client_token: String,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="hypervisor")]
pub hypervisor: HypervisorType,

#[serde(rename="rootDeviceName")]
pub root_device_name: String,

#[serde(rename="networkInterfaceSet")]
pub network_interface_set: InstanceNetworkInterfaceList,

#[serde(rename="dnsName")]
pub dns_name: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="reason")]
pub reason: String,

#[serde(rename="spotInstanceRequestId")]
pub spot_instance_request_id: String,

#[serde(rename="rootDeviceType")]
pub root_device_type: DeviceType,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="stateReason")]
pub state_reason: StateReason,

#[serde(rename="ipAddress")]
pub ip_address: String,

#[serde(rename="monitoring")]
pub monitoring: Monitoring,

#[serde(rename="instanceLifecycle")]
pub instance_lifecycle: InstanceLifecycleType,

#[serde(rename="virtualizationType")]
pub virtualization_type: VirtualizationType,

#[serde(rename="iamInstanceProfile")]
pub iam_instance_profile: IamInstanceProfile,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: InstanceBlockDeviceMappingList,

#[serde(rename="platform")]
pub platform: PlatformValues,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="imageId")]
pub image_id: String,

#[serde(rename="placement")]
pub placement: Placement,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkInterfacesRequest {


#[serde(rename="filter")]
pub filter: FilterList,

#[serde(rename="NetworkInterfaceId")]
pub network_interface_id: NetworkInterfaceIdList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeImageAttributeRequest {


#[serde(rename="ImageId")]
pub image_id: String,

#[serde(rename="Attribute")]
pub attribute: ImageAttributeName,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DisableVpcClassicLinkRequest {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSecurityGroupResult {


#[serde(rename="groupId")]
pub group_id: String,
}



/// <p>Describes a Reserved Instance modification.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesModification {


#[serde(rename="reservedInstancesSet")]
pub reserved_instances_set: ReservedIntancesIds,

#[serde(rename="status")]
pub status: String,

#[serde(rename="clientToken")]
pub client_token: String,

#[serde(rename="modificationResultSet")]
pub modification_result_set: ReservedInstancesModificationResultList,

#[serde(rename="createDate")]
pub create_date: DateTime,

#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="reservedInstancesModificationId")]
pub reserved_instances_modification_id: String,

#[serde(rename="updateDate")]
pub update_date: DateTime,

#[serde(rename="effectiveDate")]
pub effective_date: DateTime,
}



/// <p>Describes a VPC in a VPC peering connection.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpcPeeringConnectionVpcInfo {


#[serde(rename="cidrBlock")]
pub cidr_block: String,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="ownerId")]
pub owner_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ResetNetworkInterfaceAttributeRequest {


#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpcResult {


#[serde(rename="vpc")]
pub vpc: Vpc,
}



/// <p>Describes a volume status.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeStatusDetails {


#[serde(rename="status")]
pub status: String,

#[serde(rename="name")]
pub name: VolumeStatusName,
}



/// <p>Describes a state change.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct StateReason {


#[serde(rename="message")]
pub message: String,

#[serde(rename="code")]
pub code: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CopyImageRequest {


#[serde(rename="SourceRegion")]
pub source_region: String,

#[serde(rename="Description")]
pub description: String,

#[serde(rename="Name")]
pub name: String,

#[serde(rename="SourceImageId")]
pub source_image_id: String,

#[serde(rename="ClientToken")]
pub client_token: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachClassicLinkVpcRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteDhcpOptionsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="DhcpOptionsId")]
pub dhcp_options_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportVolumeRequest {


#[serde(rename="description")]
pub description: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="volume")]
pub volume: VolumeDetail,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="image")]
pub image: DiskImageDetail,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotDatafeedSubscriptionRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes a static route for a VPN connection.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpnStaticRoute {


#[serde(rename="state")]
pub state: VpnState,

#[serde(rename="destinationCidrBlock")]
pub destination_cidr_block: String,

#[serde(rename="source")]
pub source: VpnStaticRouteSource,
}



/// <p>Describes a bundle task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct BundleTask {


#[serde(rename="error")]
pub error: BundleTaskError,

#[serde(rename="storage")]
pub storage: Storage,

#[serde(rename="state")]
pub state: BundleTaskState,

#[serde(rename="updateTime")]
pub update_time: DateTime,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="progress")]
pub progress: String,

#[serde(rename="startTime")]
pub start_time: DateTime,

#[serde(rename="bundleId")]
pub bundle_id: String,
}



/// <p>Describes an image.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Image {


#[serde(rename="imageOwnerAlias")]
pub image_owner_alias: String,

#[serde(rename="platform")]
pub platform: PlatformValues,

#[serde(rename="imageState")]
pub image_state: ImageState,

#[serde(rename="isPublic")]
pub is_public: bool,

#[serde(rename="creationDate")]
pub creation_date: String,

#[serde(rename="imageType")]
pub image_type: ImageTypeValues,

#[serde(rename="rootDeviceType")]
pub root_device_type: DeviceType,

#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="description")]
pub description: String,

#[serde(rename="ramdiskId")]
pub ramdisk_id: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="rootDeviceName")]
pub root_device_name: String,

#[serde(rename="stateReason")]
pub state_reason: StateReason,

#[serde(rename="blockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingList,

#[serde(rename="virtualizationType")]
pub virtualization_type: VirtualizationType,

#[serde(rename="name")]
pub name: String,

#[serde(rename="imageOwnerId")]
pub image_owner_id: String,

#[serde(rename="imageLocation")]
pub image_location: String,

#[serde(rename="kernelId")]
pub kernel_id: String,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: String,

#[serde(rename="hypervisor")]
pub hypervisor: HypervisorType,

#[serde(rename="imageId")]
pub image_id: String,

#[serde(rename="architecture")]
pub architecture: ArchitectureValues,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportInstanceTaskDetails {


#[serde(rename="description")]
pub description: String,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="platform")]
pub platform: PlatformValues,

#[serde(rename="volumes")]
pub volumes: ImportInstanceVolumeDetailSet,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteRouteTableRequest {


#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreatePlacementGroupRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="groupName")]
pub group_name: String,

#[serde(rename="strategy")]
pub strategy: PlacementStrategy,
}



/// <p>Describes a secondary private IP address for a network interface.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PrivateIpAddressSpecification {


#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="primary")]
pub primary: bool,
}



/// <p>Describes a value of an account attribute.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AccountAttributeValue {


#[serde(rename="attributeValue")]
pub attribute_value: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceRouteRequest {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="gatewayId")]
pub gateway_id: String,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,

#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="destinationCidrBlock")]
pub destination_cidr_block: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,
}



/// <p>Describes an Internet gateway.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InternetGateway {


#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="internetGatewayId")]
pub internet_gateway_id: String,

#[serde(rename="attachmentSet")]
pub attachment_set: InternetGatewayAttachmentList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumeStatusRequest {


#[serde(rename="NextToken")]
pub next_token: String,

#[serde(rename="VolumeId")]
pub volume_id: VolumeIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="MaxResults")]
pub max_results: i32,

#[serde(rename="Filter")]
pub filter: FilterList,
}



/// <p>Describes an export task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ExportTask {


#[serde(rename="instanceExport")]
pub instance_export: InstanceExportDetails,

#[serde(rename="exportToS3")]
pub export_to_s3: ExportToS3Task,

#[serde(rename="state")]
pub state: ExportTaskState,

#[serde(rename="exportTaskId")]
pub export_task_id: String,

#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="description")]
pub description: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct TerminateInstancesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RegisterImageResult {


#[serde(rename="imageId")]
pub image_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct StopInstancesResult {


#[serde(rename="instancesSet")]
pub instances_set: InstanceStateChangeList,
}



/// <p>Describes a VPC.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Vpc {


#[serde(rename="cidrBlock")]
pub cidr_block: String,

#[serde(rename="state")]
pub state: VpcState,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="instanceTenancy")]
pub instance_tenancy: Tenancy,

#[serde(rename="isDefault")]
pub is_default: bool,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="dhcpOptionsId")]
pub dhcp_options_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeBundleTasksResult {


#[serde(rename="bundleInstanceTasksSet")]
pub bundle_instance_tasks_set: BundleTaskList,
}



/// <p>Describes a recurring charge.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct RecurringCharge {


#[serde(rename="amount")]
pub amount: f64,

#[serde(rename="frequency")]
pub frequency: RecurringChargeFrequency,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceRouteTableAssociationResult {


#[serde(rename="newAssociationId")]
pub new_association_id: String,
}



/// <p>Describes the current state of the instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceState {


#[serde(rename="name")]
pub name: InstanceStateName,

#[serde(rename="code")]
pub code: i32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumeAttributeRequest {


#[serde(rename="VolumeId")]
pub volume_id: String,

#[serde(rename="Attribute")]
pub attribute: VolumeAttributeName,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportKeyPairResult {


#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="keyFingerprint")]
pub key_fingerprint: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RebootInstancesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesModificationsRequest {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="ReservedInstancesModificationId")]
pub reserved_instances_modification_id: ReservedInstancesModificationIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachVolumeRequest {


#[serde(rename="Device")]
pub device: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VolumeId")]
pub volume_id: String,

#[serde(rename="InstanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSnapshotsRequest {


#[serde(rename="NextToken")]
pub next_token: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="MaxResults")]
pub max_results: i32,

#[serde(rename="RestorableBy")]
pub restorable_by: RestorableByStringList,

#[serde(rename="SnapshotId")]
pub snapshot_id: SnapshotIdStringList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="Owner")]
pub owner: OwnerStringList,
}



/// <p>Describes a Spot Instance state change.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotInstanceStateFault {


#[serde(rename="code")]
pub code: String,

#[serde(rename="message")]
pub message: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesOfferingsResult {


#[serde(rename="reservedInstancesOfferingsSet")]
pub reserved_instances_offerings_set: ReservedInstancesOfferingList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachVpnGatewayRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VpcId")]
pub vpc_id: String,

#[serde(rename="VpnGatewayId")]
pub vpn_gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumeAttributeResult {


#[serde(rename="autoEnableIO")]
pub auto_enable_io: AttributeBooleanValue,

#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="volumeId")]
pub volume_id: String,
}



/// <p>Describes a network interface attachment.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceNetworkInterfaceAttachment {


#[serde(rename="attachmentId")]
pub attachment_id: String,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="deviceIndex")]
pub device_index: i32,

#[serde(rename="attachTime")]
pub attach_time: DateTime,

#[serde(rename="status")]
pub status: AttachmentStatus,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeRegionsResult {


#[serde(rename="regionInfo")]
pub region_info: RegionList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVolumeRequest {


#[serde(rename="SnapshotId")]
pub snapshot_id: String,

#[serde(rename="KmsKeyId")]
pub kms_key_id: String,

#[serde(rename="Iops")]
pub iops: i32,

#[serde(rename="AvailabilityZone")]
pub availability_zone: String,

#[serde(rename="Size")]
pub size: i32,

#[serde(rename="encrypted")]
pub encrypted: bool,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VolumeType")]
pub volume_type: VolumeType,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpnConnectionRouteRequest {


#[serde(rename="VpnConnectionId")]
pub vpn_connection_id: String,

#[serde(rename="DestinationCidrBlock")]
pub destination_cidr_block: String,
}



/// <p>Describes Spot Instance placement.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotPlacement {


#[serde(rename="groupName")]
pub group_name: String,

#[serde(rename="availabilityZone")]
pub availability_zone: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReportInstanceStatusRequest {


#[serde(rename="endTime")]
pub end_time: DateTime,

#[serde(rename="instanceId")]
pub instance_id: InstanceIdStringList,

#[serde(rename="status")]
pub status: ReportStatusType,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="description")]
pub description: String,

#[serde(rename="startTime")]
pub start_time: DateTime,

#[serde(rename="reasonCode")]
pub reason_code: ReasonCodesList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportKeyPairRequest {


#[serde(rename="publicKeyMaterial")]
pub public_key_material: Blob,

#[serde(rename="keyName")]
pub key_name: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpcPeeringConnectionRequest {


#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateRouteTableRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceRouteTableAssociationRequest {


#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="associationId")]
pub association_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes an Amazon EBS volume.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeDetail {


#[serde(rename="size")]
pub size: i64,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateRouteTableResult {


#[serde(rename="routeTable")]
pub route_table: RouteTable,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInstanceAttributeRequest {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="attribute")]
pub attribute: InstanceAttributeName,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteInternetGatewayRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="internetGatewayId")]
pub internet_gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkAclsRequest {


#[serde(rename="NetworkAclId")]
pub network_acl_id: ValueStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifySnapshotAttributeRequest {


#[serde(rename="UserGroup")]
pub user_group: GroupNameStringList,

#[serde(rename="CreateVolumePermission")]
pub create_volume_permission: CreateVolumePermissionModifications,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SnapshotId")]
pub snapshot_id: String,

#[serde(rename="UserId")]
pub user_id: UserIdStringList,

#[serde(rename="Attribute")]
pub attribute: SnapshotAttributeName,

#[serde(rename="OperationType")]
pub operation_type: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateCustomerGatewayRequest {


#[serde(rename="IpAddress")]
pub ip_address: String,

#[serde(rename="GatewayType")]
pub gateway_type: GatewayType,

#[serde(rename="BgpAsn")]
pub bgp_asn: i32,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ResetInstanceAttributeRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="attribute")]
pub attribute: InstanceAttributeName,
}



/// <p>Describes a Reserved Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstances {


#[serde(rename="duration")]
pub duration: i64,

#[serde(rename="usagePrice")]
pub usage_price: f32,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,

#[serde(rename="instanceTenancy")]
pub instance_tenancy: Tenancy,

#[serde(rename="fixedPrice")]
pub fixed_price: f32,

#[serde(rename="start")]
pub start: DateTime,

#[serde(rename="productDescription")]
pub product_description: RIProductDescription,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="recurringCharges")]
pub recurring_charges: RecurringChargesList,

#[serde(rename="instanceCount")]
pub instance_count: i32,

#[serde(rename="state")]
pub state: ReservedInstanceState,

#[serde(rename="currencyCode")]
pub currency_code: CurrencyCodeValues,

#[serde(rename="offeringType")]
pub offering_type: OfferingTypeValues,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,

#[serde(rename="end")]
pub end: DateTime,
}



/// <p>Describes VPN connection options.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpnConnectionOptions {


#[serde(rename="staticRoutesOnly")]
pub static_routes_only: bool,
}



/// <p>Describes the monitoring for the instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Monitoring {


#[serde(rename="state")]
pub state: MonitoringState,
}



/// <p>Describes the status of a VPC peering connection.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpcPeeringConnectionStateReason {


#[serde(rename="message")]
pub message: String,

#[serde(rename="code")]
pub code: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct GetPasswordDataRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAccountAttributesRequest {


#[serde(rename="attributeName")]
pub attribute_name: AccountAttributeNameStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachVpnGatewayResult {


#[serde(rename="attachment")]
pub attachment: VpcAttachment,
}



/// <p>Describes an Availability Zone.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AvailabilityZone {


#[serde(rename="messageSet")]
pub message_set: AvailabilityZoneMessageList,

#[serde(rename="regionName")]
pub region_name: String,

#[serde(rename="zoneName")]
pub zone_name: String,

#[serde(rename="zoneState")]
pub zone_state: AvailabilityZoneState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInternetGatewaysResult {


#[serde(rename="internetGatewaySet")]
pub internet_gateway_set: InternetGatewayList,
}



/// <p>Describes the price for a Reserved Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PriceSchedule {


#[serde(rename="term")]
pub term: i64,

#[serde(rename="currencyCode")]
pub currency_code: CurrencyCodeValues,

#[serde(rename="price")]
pub price: f64,

#[serde(rename="active")]
pub active: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceNetworkAclEntryRequest {


#[serde(rename="portRange")]
pub port_range: PortRange,

#[serde(rename="ruleAction")]
pub rule_action: RuleAction,

#[serde(rename="protocol")]
pub protocol: String,

#[serde(rename="ruleNumber")]
pub rule_number: i32,

#[serde(rename="networkAclId")]
pub network_acl_id: String,

#[serde(rename="egress")]
pub egress: bool,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Icmp")]
pub icmp: IcmpTypeCode,

#[serde(rename="cidrBlock")]
pub cidr_block: String,
}



/// <p>Describes the status of a volume.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeStatusInfo {


#[serde(rename="status")]
pub status: VolumeStatusInfoStatus,

#[serde(rename="details")]
pub details: VolumeStatusDetailsList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AcceptVpcPeeringConnectionRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeExportTasksRequest {


#[serde(rename="exportTaskId")]
pub export_task_id: ExportTaskIdStringList,
}



/// <p>The value to use for a resource attribute.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AttributeValue {


#[serde(rename="value")]
pub value: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteNetworkAclRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="networkAclId")]
pub network_acl_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct EbsInstanceBlockDeviceSpecification {


#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="volumeId")]
pub volume_id: String,
}



/// <p>Describes a set of DHCP options.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct DhcpOptions {


#[serde(rename="dhcpConfigurationSet")]
pub dhcp_configuration_set: DhcpConfigurationList,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="dhcpOptionsId")]
pub dhcp_options_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VolumeId")]
pub volume_id: VolumeIdStringList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="maxResults")]
pub max_results: i32,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct EnableVpcClassicLinkRequest {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteRouteRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="destinationCidrBlock")]
pub destination_cidr_block: String,
}



/// <p>Describes a launch permission modification.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct LaunchPermissionModifications {


#[serde(rename="Remove")]
pub remove: LaunchPermissionList,

#[serde(rename="Add")]
pub add: LaunchPermissionList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeletePlacementGroupRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="groupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateInternetGatewayResult {


#[serde(rename="internetGateway")]
pub internet_gateway: InternetGateway,
}



/// <p>Describe a Spot Instance request.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotInstanceRequest {


#[serde(rename="launchGroup")]
pub launch_group: String,

#[serde(rename="fault")]
pub fault: SpotInstanceStateFault,

#[serde(rename="launchSpecification")]
pub launch_specification: LaunchSpecification,

#[serde(rename="status")]
pub status: SpotInstanceStatus,

#[serde(rename="validUntil")]
pub valid_until: DateTime,

#[serde(rename="createTime")]
pub create_time: DateTime,

#[serde(rename="spotPrice")]
pub spot_price: String,

#[serde(rename="availabilityZoneGroup")]
pub availability_zone_group: String,

#[serde(rename="productDescription")]
pub product_description: RIProductDescription,

#[serde(rename="validFrom")]
pub valid_from: DateTime,

#[serde(rename="SpotType")]
pub spot_type: SpotInstanceType,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="spotInstanceRequestId")]
pub spot_instance_request_id: String,

#[serde(rename="state")]
pub state: SpotInstanceState,

#[serde(rename="launchedAvailabilityZone")]
pub launched_availability_zone: String,
}



/// <p>Describes a network ACL.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkAcl {


#[serde(rename="networkAclId")]
pub network_acl_id: String,

#[serde(rename="default")]
pub default: bool,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="associationSet")]
pub association_set: NetworkAclAssociationList,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="entrySet")]
pub entry_set: NetworkAclEntryList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeClassicLinkInstancesResult {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="instancesSet")]
pub instances_set: ClassicLinkInstanceList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportInstanceRequest {


#[serde(rename="diskImage")]
pub disk_image: DiskImageList,

#[serde(rename="launchSpecification")]
pub launch_specification: ImportInstanceLaunchSpecification,

#[serde(rename="description")]
pub description: String,

#[serde(rename="platform")]
pub platform: PlatformValues,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcsRequest {


#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VpcId")]
pub vpc_id: VpcIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAvailabilityZonesRequest {


#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="ZoneName")]
pub zone_name: ZoneNameStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes a block device mapping.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct BlockDeviceMapping {


#[serde(rename="virtualName")]
pub virtual_name: String,

#[serde(rename="noDevice")]
pub no_device: String,

#[serde(rename="deviceName")]
pub device_name: String,

#[serde(rename="ebs")]
pub ebs: EbsBlockDevice,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RequestSpotInstancesResult {


#[serde(rename="spotInstanceRequestSet")]
pub spot_instance_request_set: SpotInstanceRequestList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct GetPasswordDataResult {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="passwordData")]
pub password_data: String,

#[serde(rename="timestamp")]
pub timestamp: DateTime,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CopySnapshotRequest {


#[serde(rename="Description")]
pub description: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SourceRegion")]
pub source_region: String,

#[serde(rename="SourceSnapshotId")]
pub source_snapshot_id: String,

#[serde(rename="presignedUrl")]
pub presigned_url: String,

#[serde(rename="destinationRegion")]
pub destination_region: String,
}



/// <p>Describes the data feed for a Spot Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotDatafeedSubscription {


#[serde(rename="prefix")]
pub prefix: String,

#[serde(rename="fault")]
pub fault: SpotInstanceStateFault,

#[serde(rename="bucket")]
pub bucket: String,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="state")]
pub state: DatafeedSubscriptionState,
}



/// <p>Describes a disk image.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct DiskImage {


#[serde(rename="Description")]
pub description: String,

#[serde(rename="Image")]
pub image: DiskImageDetail,

#[serde(rename="Volume")]
pub volume: VolumeDetail,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpnGatewayRequest {


#[serde(rename="GatewayType")]
pub gateway_type: GatewayType,

#[serde(rename="AvailabilityZone")]
pub availability_zone: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpcPeeringConnectionResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}



/// <p>Describes an import volume task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportVolumeTaskDetails {


#[serde(rename="description")]
pub description: String,

#[serde(rename="bytesConverted")]
pub bytes_converted: i64,

#[serde(rename="image")]
pub image: DiskImageDescription,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="volume")]
pub volume: DiskImageVolumeDescription,
}



/// <p>Describes an association between a route table and a subnet.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct RouteTableAssociation {


#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="routeTableAssociationId")]
pub route_table_association_id: String,

#[serde(rename="main")]
pub main: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSecurityGroupRequest {


#[serde(rename="GroupDescription")]
pub group_description: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VpcId")]
pub vpc_id: String,

#[serde(rename="GroupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeregisterImageRequest {


#[serde(rename="ImageId")]
pub image_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkInterfaceAttributeRequest {


#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="attribute")]
pub attribute: NetworkInterfaceAttribute,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelBundleTaskResult {


#[serde(rename="bundleInstanceTask")]
pub bundle_instance_task: BundleTask,
}



/// <p>Describes association information for an Elastic IP address.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceNetworkInterfaceAssociation {


#[serde(rename="publicIp")]
pub public_ip: String,

#[serde(rename="publicDnsName")]
pub public_dns_name: String,

#[serde(rename="ipOwnerId")]
pub ip_owner_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSecurityGroupsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="GroupId")]
pub group_id: GroupIdStringList,

#[serde(rename="GroupName")]
pub group_name: GroupNameStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelExportTaskRequest {


#[serde(rename="exportTaskId")]
pub export_task_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ResetImageAttributeRequest {


#[serde(rename="Attribute")]
pub attribute: ResetImageAttributeName,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="ImageId")]
pub image_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CopySnapshotResult {


#[serde(rename="snapshotId")]
pub snapshot_id: String,
}



/// <p>Describes the storage location for an instance store-backed AMI.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Storage {


#[serde(rename="S3")]
pub s3: S3Storage,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RegisterImageRequest {


#[serde(rename="kernelId")]
pub kernel_id: String,

#[serde(rename="name")]
pub name: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="ramdiskId")]
pub ramdisk_id: String,

#[serde(rename="virtualizationType")]
pub virtualization_type: String,

#[serde(rename="sriovNetSupport")]
pub sriov_net_support: String,

#[serde(rename="architecture")]
pub architecture: ArchitectureValues,

#[serde(rename="ImageLocation")]
pub image_location: String,

#[serde(rename="BlockDeviceMapping")]
pub block_device_mapping: BlockDeviceMappingRequestList,

#[serde(rename="description")]
pub description: String,

#[serde(rename="rootDeviceName")]
pub root_device_name: String,
}



/// <p>Describes the configuration settings for the modified Reserved Instances.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesConfiguration {


#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="instanceCount")]
pub instance_count: i32,

#[serde(rename="platform")]
pub platform: String,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,
}



/// <p>Describes a placement group.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PlacementGroup {


#[serde(rename="strategy")]
pub strategy: PlacementStrategy,

#[serde(rename="state")]
pub state: PlacementGroupState,

#[serde(rename="groupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesListingsResult {


#[serde(rename="reservedInstancesListingsSet")]
pub reserved_instances_listings_set: ReservedInstancesListingList,
}



/// <p>Describes a network interface.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceNetworkInterfaceSpecification {


#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="associatePublicIpAddress")]
pub associate_public_ip_address: bool,

#[serde(rename="secondaryPrivateIpAddressCount")]
pub secondary_private_ip_address_count: i32,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="SecurityGroupId")]
pub security_group_id: SecurityGroupIdStringList,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="privateIpAddressesSet")]
pub private_ip_addresses_set: PrivateIpAddressSpecificationList,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="deviceIndex")]
pub device_index: i32,

#[serde(rename="description")]
pub description: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteTagsRequest {


#[serde(rename="resourceId")]
pub resource_id: ResourceIdList,

#[serde(rename="tag")]
pub tag: TagList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes a network interface attachment.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkInterfaceAttachment {


#[serde(rename="attachmentId")]
pub attachment_id: String,

#[serde(rename="instanceOwnerId")]
pub instance_owner_id: String,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="attachTime")]
pub attach_time: DateTime,

#[serde(rename="deviceIndex")]
pub device_index: i32,

#[serde(rename="status")]
pub status: AttachmentStatus,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVolumesResult {


#[serde(rename="volumeSet")]
pub volume_set: VolumeList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpnConnectionRequest {


#[serde(rename="VpnConnectionId")]
pub vpn_connection_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotPriceHistoryRequest {


#[serde(rename="endTime")]
pub end_time: DateTime,

#[serde(rename="startTime")]
pub start_time: DateTime,

#[serde(rename="InstanceType")]
pub instance_type: InstanceTypeList,

#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="ProductDescription")]
pub product_description: ProductDescriptionList,

#[serde(rename="maxResults")]
pub max_results: i32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssociateDhcpOptionsRequest {


#[serde(rename="VpcId")]
pub vpc_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="DhcpOptionsId")]
pub dhcp_options_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct UnmonitorInstancesRequest {


#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct EnableVolumeIORequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="volumeId")]
pub volume_id: String,
}



/// <p>Describes an import volume task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportInstanceVolumeDetailItem {


#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="description")]
pub description: String,

#[serde(rename="status")]
pub status: String,

#[serde(rename="bytesConverted")]
pub bytes_converted: i64,

#[serde(rename="volume")]
pub volume: DiskImageVolumeDescription,

#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="image")]
pub image: DiskImageDescription,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpnConnectionsRequest {


#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="VpnConnectionId")]
pub vpn_connection_id: VpnConnectionIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotInstanceRequestsRequest {


#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SpotInstanceRequestId")]
pub spot_instance_request_id: SpotInstanceRequestIdList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct StartInstancesResult {


#[serde(rename="instancesSet")]
pub instances_set: InstanceStateChangeList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeBundleTasksRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="BundleId")]
pub bundle_id: BundleIdStringList,

#[serde(rename="Filter")]
pub filter: FilterList,
}



/// <p>Describes a DHCP configuration option.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct DhcpConfiguration {


#[serde(rename="key")]
pub key: String,

#[serde(rename="valueSet")]
pub value_set: DhcpConfigurationValueList,
}



/// <p>Describes the status of an instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceStatusSummary {


#[serde(rename="status")]
pub status: SummaryStatus,

#[serde(rename="details")]
pub details: InstanceStatusDetailsList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct BundleInstanceResult {


#[serde(rename="bundleInstanceTask")]
pub bundle_instance_task: BundleTask,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteNetworkInterfaceRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyReservedInstancesResult {


#[serde(rename="reservedInstancesModificationId")]
pub reserved_instances_modification_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSnapshotAttributeResult {


#[serde(rename="productCodes")]
pub product_codes: ProductCodeList,

#[serde(rename="createVolumePermission")]
pub create_volume_permission: CreateVolumePermissionList,

#[serde(rename="snapshotId")]
pub snapshot_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAvailabilityZonesResult {


#[serde(rename="availabilityZoneInfo")]
pub availability_zone_info: AvailabilityZoneList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteCustomerGatewayRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="CustomerGatewayId")]
pub customer_gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachClassicLinkVpcResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}



/// <p>Describes the monitoring information of the instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceMonitoring {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="monitoring")]
pub monitoring: Monitoring,
}



/// <p>Describes the ID of a Reserved Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesId {


#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVolumeRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VolumeId")]
pub volume_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSnapshotRequest {


#[serde(rename="VolumeId")]
pub volume_id: String,

#[serde(rename="Description")]
pub description: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AllocateAddressResult {


#[serde(rename="domain")]
pub domain: DomainType,

#[serde(rename="publicIp")]
pub public_ip: String,

#[serde(rename="allocationId")]
pub allocation_id: String,
}



/// <p>Describes the ICMP type and code.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct IcmpTypeCode {


#[serde(rename="code")]
pub code: i32,

#[serde(rename="IcmpType")]
pub icmp_type: i32,
}



/// <p>Describes the price for a Reserved Instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PriceScheduleSpecification {


#[serde(rename="price")]
pub price: f64,

#[serde(rename="term")]
pub term: i64,

#[serde(rename="currencyCode")]
pub currency_code: CurrencyCodeValues,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachNetworkInterfaceRequest {


#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="deviceIndex")]
pub device_index: i32,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSubnetsRequest {


#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="SubnetId")]
pub subnet_id: SubnetIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachVolumeRequest {


#[serde(rename="Device")]
pub device: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="VolumeId")]
pub volume_id: String,

#[serde(rename="InstanceId")]
pub instance_id: String,

#[serde(rename="Force")]
pub force: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpnConnectionsResult {


#[serde(rename="vpnConnectionSet")]
pub vpn_connection_set: VpnConnectionList,
}



/// <p>Describes a key pair.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct KeyPair {


#[serde(rename="keyFingerprint")]
pub key_fingerprint: String,

#[serde(rename="keyMaterial")]
pub key_material: String,

#[serde(rename="keyName")]
pub key_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyVolumeAttributeRequest {


#[serde(rename="AutoEnableIO")]
pub auto_enable_io: AttributeBooleanValue,

#[serde(rename="VolumeId")]
pub volume_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DiskImageVolumeDescription {


#[serde(rename="size")]
pub size: i64,

#[serde(rename="id")]
pub id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyVpcAttributeRequest {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="EnableDnsHostnames")]
pub enable_dns_hostnames: AttributeBooleanValue,

#[serde(rename="EnableDnsSupport")]
pub enable_dns_support: AttributeBooleanValue,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct UnmonitorInstancesResult {


#[serde(rename="instancesSet")]
pub instances_set: InstanceMonitoringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachVpnGatewayRequest {


#[serde(rename="VpcId")]
pub vpc_id: String,

#[serde(rename="VpnGatewayId")]
pub vpn_gateway_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssociateAddressRequest {


#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="PublicIp")]
pub public_ip: String,

#[serde(rename="InstanceId")]
pub instance_id: String,

#[serde(rename="allowReassociation")]
pub allow_reassociation: bool,

#[serde(rename="AllocationId")]
pub allocation_id: String,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcPeeringConnectionsResult {


#[serde(rename="vpcPeeringConnectionSet")]
pub vpc_peering_connection_set: VpcPeeringConnectionList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct PurchaseReservedInstancesOfferingResult {


#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeTagsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="maxResults")]
pub max_results: i32,
}



/// <p>Describes an IP range.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct IpRange {


#[serde(rename="cidrIp")]
pub cidr_ip: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceNetworkAclAssociationRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="networkAclId")]
pub network_acl_id: String,

#[serde(rename="associationId")]
pub association_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeRouteTablesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="RouteTableId")]
pub route_table_id: ValueStringList,

#[serde(rename="Filter")]
pub filter: FilterList,
}



/// <p>Describes volume attachment details.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeAttachment {


#[serde(rename="attachTime")]
pub attach_time: DateTime,

#[serde(rename="volumeId")]
pub volume_id: String,

#[serde(rename="deleteOnTermination")]
pub delete_on_termination: bool,

#[serde(rename="device")]
pub device: String,

#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="status")]
pub status: VolumeAttachmentState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpcPeeringConnectionRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="peerVpcId")]
pub peer_vpc_id: String,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="peerOwnerId")]
pub peer_owner_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateInstanceExportTaskResult {


#[serde(rename="exportTask")]
pub export_task: ExportTask,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeDhcpOptionsResult {


#[serde(rename="dhcpOptionsSet")]
pub dhcp_options_set: DhcpOptionsList,
}



/// <p>Describes a customer gateway.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct CustomerGateway {


#[serde(rename="ipAddress")]
pub ip_address: String,

#[serde(rename="bgpAsn")]
pub bgp_asn: String,

#[serde(rename="GatewayType")]
pub gateway_type: String,

#[serde(rename="customerGatewayId")]
pub customer_gateway_id: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="state")]
pub state: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachNetworkInterfaceResult {


#[serde(rename="attachmentId")]
pub attachment_id: String,
}



/// <p>Describes a VPC peering connection.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpcPeeringConnection {


#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="expirationTime")]
pub expiration_time: DateTime,

#[serde(rename="accepterVpcInfo")]
pub accepter_vpc_info: VpcPeeringConnectionVpcInfo,

#[serde(rename="status")]
pub status: VpcPeeringConnectionStateReason,

#[serde(rename="requesterVpcInfo")]
pub requester_vpc_info: VpcPeeringConnectionVpcInfo,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSpotDatafeedSubscriptionRequest {


#[serde(rename="prefix")]
pub prefix: String,

#[serde(rename="bucket")]
pub bucket: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes an IAM instance profile.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct IamInstanceProfile {


#[serde(rename="id")]
pub id: String,

#[serde(rename="arn")]
pub arn: String,
}



/// <p>Describes the storage parameters for S3 and S3 buckets for an instance store-backed AMI.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct S3Storage {


#[serde(rename="bucket")]
pub bucket: String,

#[serde(rename="prefix")]
pub prefix: String,

#[serde(rename="AWSAccessKeyId")]
pub aws_access_key_id: String,

#[serde(rename="uploadPolicySignature")]
pub upload_policy_signature: String,

#[serde(rename="uploadPolicy")]
pub upload_policy: Blob,
}



/// <p>Describes the status of an instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceStatus {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="eventsSet")]
pub events_set: InstanceStatusEventList,

#[serde(rename="instanceStatus")]
pub instance_status: InstanceStatusSummary,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="instanceState")]
pub instance_state: InstanceState,

#[serde(rename="systemStatus")]
pub system_status: InstanceStatusSummary,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct PurchaseReservedInstancesOfferingRequest {


#[serde(rename="ReservedInstancesOfferingId")]
pub reserved_instances_offering_id: String,

#[serde(rename="InstanceCount")]
pub instance_count: i32,

#[serde(rename="limitPrice")]
pub limit_price: ReservedInstanceLimitPrice,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AcceptVpcPeeringConnectionResult {


#[serde(rename="vpcPeeringConnection")]
pub vpc_peering_connection: VpcPeeringConnection,
}



/// <p>Describes a volume status event.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VolumeStatusEvent {


#[serde(rename="notAfter")]
pub not_after: DateTime,

#[serde(rename="description")]
pub description: String,

#[serde(rename="notBefore")]
pub not_before: DateTime,

#[serde(rename="eventType")]
pub event_type: String,

#[serde(rename="eventId")]
pub event_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeRouteTablesResult {


#[serde(rename="routeTableSet")]
pub route_table_set: RouteTableList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpnGatewayRequest {


#[serde(rename="VpnGatewayId")]
pub vpn_gateway_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeCustomerGatewaysRequest {


#[serde(rename="CustomerGatewayId")]
pub customer_gateway_id: CustomerGatewayIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}



/// <p>Describes the maximum hourly price (bid) for any Spot Instance launched to fulfill the request.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SpotPrice {


#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="productDescription")]
pub product_description: RIProductDescription,

#[serde(rename="timestamp")]
pub timestamp: DateTime,

#[serde(rename="spotPrice")]
pub spot_price: String,

#[serde(rename="instanceType")]
pub instance_type: InstanceType,
}



/// <p>Describes a message about an Availability Zone.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AvailabilityZoneMessage {


#[serde(rename="message")]
pub message: String,
}



/// <p>Describes the placement for the instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Placement {


#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="tenancy")]
pub tenancy: Tenancy,

#[serde(rename="groupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesModificationsResult {


#[serde(rename="reservedInstancesModificationsSet")]
pub reserved_instances_modifications_set: ReservedInstancesModificationList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct EnableVgwRoutePropagationRequest {


#[serde(rename="RouteTableId")]
pub route_table_id: String,

#[serde(rename="GatewayId")]
pub gateway_id: String,
}



/// <p>Describes a Reserved Instance offering.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct PricingDetail {


#[serde(rename="price")]
pub price: f64,

#[serde(rename="count")]
pub count: i32,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifySubnetAttributeRequest {


#[serde(rename="MapPublicIpOnLaunch")]
pub map_public_ip_on_launch: AttributeBooleanValue,

#[serde(rename="subnetId")]
pub subnet_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotPriceHistoryResult {


#[serde(rename="spotPriceHistorySet")]
pub spot_price_history_set: SpotPriceHistoryList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssociateRouteTableResult {


#[serde(rename="associationId")]
pub association_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AuthorizeSecurityGroupIngressRequest {


#[serde(rename="SourceSecurityGroupName")]
pub source_security_group_name: String,

#[serde(rename="GroupName")]
pub group_name: String,

#[serde(rename="IpPermissions")]
pub ip_permissions: IpPermissionList,

#[serde(rename="ToPort")]
pub to_port: i32,

#[serde(rename="IpProtocol")]
pub ip_protocol: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SourceSecurityGroupOwnerId")]
pub source_security_group_owner_id: String,

#[serde(rename="CidrIp")]
pub cidr_ip: String,

#[serde(rename="FromPort")]
pub from_port: i32,

#[serde(rename="GroupId")]
pub group_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DetachNetworkInterfaceRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="attachmentId")]
pub attachment_id: String,

#[serde(rename="force")]
pub force: bool,
}



/// <p>Describes an instance export task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceExportDetails {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="targetEnvironment")]
pub target_environment: ExportEnvironment,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateTagsRequest {


#[serde(rename="ResourceId")]
pub resource_id: ResourceIdList,

#[serde(rename="Tag")]
pub tag: TagList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteNetworkAclEntryRequest {


#[serde(rename="networkAclId")]
pub network_acl_id: String,

#[serde(rename="ruleNumber")]
pub rule_number: i32,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="egress")]
pub egress: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportInstanceResult {


#[serde(rename="conversionTask")]
pub conversion_task: ConversionTask,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInstancesRequest {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="maxResults")]
pub max_results: i32,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeDhcpOptionsRequest {


#[serde(rename="DhcpOptionsId")]
pub dhcp_options_id: DhcpOptionsIdStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSnapshotAttributeRequest {


#[serde(rename="Attribute")]
pub attribute: SnapshotAttributeName,

#[serde(rename="SnapshotId")]
pub snapshot_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes the private IP address of a network interface.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkInterfacePrivateIpAddress {


#[serde(rename="privateDnsName")]
pub private_dns_name: String,

#[serde(rename="association")]
pub association: NetworkInterfaceAssociation,

#[serde(rename="primary")]
pub primary: bool,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInstanceStatusResult {


#[serde(rename="instanceStatusSet")]
pub instance_status_set: InstanceStatusList,

#[serde(rename="nextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct EnableVpcClassicLinkResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVolumePermission {


#[serde(rename="userId")]
pub user_id: String,

#[serde(rename="group")]
pub group: PermissionGroup,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSpotDatafeedSubscriptionResult {


#[serde(rename="spotDatafeedSubscription")]
pub spot_datafeed_subscription: SpotDatafeedSubscription,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateReservedInstancesListingResult {


#[serde(rename="reservedInstancesListingsSet")]
pub reserved_instances_listings_set: ReservedInstancesListingList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAccountAttributesResult {


#[serde(rename="accountAttributeSet")]
pub account_attribute_set: AccountAttributeList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DiskImageDescription {


#[serde(rename="size")]
pub size: i64,

#[serde(rename="format")]
pub format: DiskImageFormat,

#[serde(rename="checksum")]
pub checksum: String,

#[serde(rename="importManifestUrl")]
pub import_manifest_url: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachClassicLinkVpcResult {


#[serde(rename="ReturnValue")]
pub return_value: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AllocateAddressRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Domain")]
pub domain: DomainType,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateDhcpOptionsResult {


#[serde(rename="dhcpOptions")]
pub dhcp_options: DhcpOptions,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ConfirmProductInstanceRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: String,

#[serde(rename="ProductCode")]
pub product_code: String,
}



/// <p>Describes a tag.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Tag {


#[serde(rename="value")]
pub value: String,

#[serde(rename="key")]
pub key: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeConversionTasksResult {


#[serde(rename="conversionTasks")]
pub conversion_tasks: DescribeConversionTaskList,
}



/// <p>Describes a route table.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct RouteTable {


#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="propagatingVgwSet")]
pub propagating_vgw_set: PropagatingVgwList,

#[serde(rename="routeSet")]
pub route_set: RouteList,

#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="associationSet")]
pub association_set: RouteTableAssociationList,

#[serde(rename="vpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSpotInstanceRequestsResult {


#[serde(rename="spotInstanceRequestSet")]
pub spot_instance_request_set: SpotInstanceRequestList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ImportVolumeResult {


#[serde(rename="conversionTask")]
pub conversion_task: ConversionTask,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAddressesResult {


#[serde(rename="addressesSet")]
pub addresses_set: AddressList,
}



/// <p>Describes VPN connection options.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpnConnectionOptionsSpecification {


#[serde(rename="staticRoutesOnly")]
pub static_routes_only: bool,
}



/// <p>Describes telemetry for a VPN tunnel.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VgwTelemetry {


#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="acceptedRouteCount")]
pub accepted_route_count: i32,

#[serde(rename="status")]
pub status: TelemetryStatus,

#[serde(rename="outsideIpAddress")]
pub outside_ip_address: String,

#[serde(rename="lastStatusChange")]
pub last_status_change: DateTime,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpnConnectionRequest {


#[serde(rename="options")]
pub options: VpnConnectionOptionsSpecification,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="CustomerGatewayId")]
pub customer_gateway_id: String,

#[serde(rename="ConnectionType")]
pub connection_type: String,

#[serde(rename="VpnGatewayId")]
pub vpn_gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateImageResult {


#[serde(rename="imageId")]
pub image_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelSpotInstanceRequestsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SpotInstanceRequestId")]
pub spot_instance_request_id: SpotInstanceRequestIdList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyImageAttributeRequest {


#[serde(rename="Attribute")]
pub attribute: String,

#[serde(rename="Value")]
pub value: String,

#[serde(rename="Description")]
pub description: AttributeValue,

#[serde(rename="LaunchPermission")]
pub launch_permission: LaunchPermissionModifications,

#[serde(rename="UserId")]
pub user_id: UserIdStringList,

#[serde(rename="ProductCode")]
pub product_code: ProductCodeStringList,

#[serde(rename="UserGroup")]
pub user_group: UserGroupStringList,

#[serde(rename="ImageId")]
pub image_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="OperationType")]
pub operation_type: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RequestSpotInstancesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="instanceCount")]
pub instance_count: i32,

#[serde(rename="availabilityZoneGroup")]
pub availability_zone_group: String,

#[serde(rename="validUntil")]
pub valid_until: DateTime,

#[serde(rename="validFrom")]
pub valid_from: DateTime,

#[serde(rename="SpotType")]
pub spot_type: SpotInstanceType,

#[serde(rename="launchGroup")]
pub launch_group: String,

#[serde(rename="LaunchSpecification")]
pub launch_specification: RequestSpotLaunchSpecification,

#[serde(rename="spotPrice")]
pub spot_price: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateNetworkInterfaceRequest {


#[serde(rename="privateIpAddresses")]
pub private_ip_addresses: PrivateIpAddressSpecificationList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SecurityGroupId")]
pub security_group_id: SecurityGroupIdStringList,

#[serde(rename="description")]
pub description: String,

#[serde(rename="secondaryPrivateIpAddressCount")]
pub secondary_private_ip_address_count: i32,

#[serde(rename="privateIpAddress")]
pub private_ip_address: String,

#[serde(rename="subnetId")]
pub subnet_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSubnetResult {


#[serde(rename="subnet")]
pub subnet: Subnet,
}



/// <p>Describes a security group</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct SecurityGroup {


#[serde(rename="groupDescription")]
pub group_description: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="groupId")]
pub group_id: String,

#[serde(rename="ownerId")]
pub owner_id: String,

#[serde(rename="groupName")]
pub group_name: String,

#[serde(rename="ipPermissionsEgress")]
pub ip_permissions_egress: IpPermissionList,

#[serde(rename="ipPermissions")]
pub ip_permissions: IpPermissionList,

#[serde(rename="vpcId")]
pub vpc_id: String,
}



/// <p>Describes a conversion task.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ConversionTask {


#[serde(rename="statusMessage")]
pub status_message: String,

#[serde(rename="conversionTaskId")]
pub conversion_task_id: String,

#[serde(rename="state")]
pub state: ConversionTaskState,

#[serde(rename="importInstance")]
pub import_instance: ImportInstanceTaskDetails,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="importVolume")]
pub import_volume: ImportVolumeTaskDetails,

#[serde(rename="expirationTime")]
pub expiration_time: String,
}



/// <p>Describes a security group and AWS account ID pair. </p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct UserIdGroupPair {


#[serde(rename="groupId")]
pub group_id: String,

#[serde(rename="groupName")]
pub group_name: String,

#[serde(rename="userId")]
pub user_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeKeyPairsRequest {


#[serde(rename="KeyName")]
pub key_name: KeyNameStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}



/// <p>Describes an instance state change.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InstanceStateChange {


#[serde(rename="instanceId")]
pub instance_id: String,

#[serde(rename="currentState")]
pub current_state: InstanceState,

#[serde(rename="previousState")]
pub previous_state: InstanceState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ResetSnapshotAttributeRequest {


#[serde(rename="Attribute")]
pub attribute: SnapshotAttributeName,

#[serde(rename="SnapshotId")]
pub snapshot_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInternetGatewaysRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="internetGatewayId")]
pub internet_gateway_id: ValueStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReservedInstancesModificationResult {


#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,

#[serde(rename="targetConfiguration")]
pub target_configuration: ReservedInstancesConfiguration,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ModifyNetworkInterfaceAttributeRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="description")]
pub description: AttributeValue,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="attachment")]
pub attachment: NetworkInterfaceAttachmentChanges,

#[serde(rename="sourceDestCheck")]
pub source_dest_check: AttributeBooleanValue,

#[serde(rename="SecurityGroupId")]
pub security_group_id: SecurityGroupIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVolumePermissionModifications {


#[serde(rename="Remove")]
pub remove: CreateVolumePermissionList,

#[serde(rename="Add")]
pub add: CreateVolumePermissionList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcAttributeResult {


#[serde(rename="enableDnsHostnames")]
pub enable_dns_hostnames: AttributeBooleanValue,

#[serde(rename="enableDnsSupport")]
pub enable_dns_support: AttributeBooleanValue,

#[serde(rename="vpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct MonitorInstancesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RejectVpcPeeringConnectionRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssociateAddressResult {


#[serde(rename="associationId")]
pub association_id: String,
}



/// <p>Describes an association between a network ACL and a subnet.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkAclAssociation {


#[serde(rename="networkAclId")]
pub network_acl_id: String,

#[serde(rename="networkAclAssociationId")]
pub network_acl_association_id: String,

#[serde(rename="subnetId")]
pub subnet_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpnGatewaysRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="VpnGatewayId")]
pub vpn_gateway_id: VpnGatewayIdStringList,
}



/// <p>Describes the attachment of a VPC to an Internet gateway.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct InternetGatewayAttachment {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="state")]
pub state: AttachmentStatus,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteSubnetRequest {


#[serde(rename="SubnetId")]
pub subnet_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateRouteRequest {


#[serde(rename="destinationCidrBlock")]
pub destination_cidr_block: String,

#[serde(rename="gatewayId")]
pub gateway_id: String,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,

#[serde(rename="vpcPeeringConnectionId")]
pub vpc_peering_connection_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="routeTableId")]
pub route_table_id: String,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeRegionsRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="RegionName")]
pub region_name: RegionNameStringList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcsResult {


#[serde(rename="vpcSet")]
pub vpc_set: VpcList,
}



/// <p>Describes an account attribute.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AccountAttribute {


#[serde(rename="attributeName")]
pub attribute_name: String,

#[serde(rename="attributeValueSet")]
pub attribute_value_set: AccountAttributeValueList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CancelBundleTaskRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="BundleId")]
pub bundle_id: String,
}



/// <p>Describes association information for an Elastic IP address.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct NetworkInterfaceAssociation {


#[serde(rename="publicIp")]
pub public_ip: String,

#[serde(rename="ipOwnerId")]
pub ip_owner_id: String,

#[serde(rename="publicDnsName")]
pub public_dns_name: String,

#[serde(rename="associationId")]
pub association_id: String,

#[serde(rename="allocationId")]
pub allocation_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSecurityGroupsResult {


#[serde(rename="securityGroupInfo")]
pub security_group_info: SecurityGroupList,
}



/// <p>The value to use when a resource attribute accepts a Boolean value.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct AttributeBooleanValue {


#[serde(rename="value")]
pub value: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteSnapshotRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SnapshotId")]
pub snapshot_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ExportToS3TaskSpecification {


#[serde(rename="containerFormat")]
pub container_format: ContainerFormat,

#[serde(rename="s3Bucket")]
pub s3_bucket: String,

#[serde(rename="diskImageFormat")]
pub disk_image_format: DiskImageFormat,

#[serde(rename="s3Prefix")]
pub s3_prefix: String,
}



/// <p>Describes the monitoring for the instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct RunInstancesMonitoringEnabled {


#[serde(rename="enabled")]
pub enabled: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ExportToS3Task {


#[serde(rename="containerFormat")]
pub container_format: ContainerFormat,

#[serde(rename="s3Key")]
pub s3_key: String,

#[serde(rename="s3Bucket")]
pub s3_bucket: String,

#[serde(rename="diskImageFormat")]
pub disk_image_format: DiskImageFormat,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteKeyPairRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="KeyName")]
pub key_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcAttributeRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Attribute")]
pub attribute: VpcAttributeName,

#[serde(rename="VpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeTagsResult {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="tagSet")]
pub tag_set: TagDescriptionList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribePlacementGroupsRequest {


#[serde(rename="groupName")]
pub group_name: PlacementGroupStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="Filter")]
pub filter: FilterList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct MonitorInstancesResult {


#[serde(rename="instancesSet")]
pub instances_set: InstanceMonitoringList,
}



/// <p>Describes a subnet.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Subnet {


#[serde(rename="defaultForAz")]
pub default_for_az: bool,

#[serde(rename="availableIpAddressCount")]
pub available_ip_address_count: i32,

#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="subnetId")]
pub subnet_id: String,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="mapPublicIpOnLaunch")]
pub map_public_ip_on_launch: bool,

#[serde(rename="cidrBlock")]
pub cidr_block: String,

#[serde(rename="state")]
pub state: SubnetState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpcRequest {


#[serde(rename="VpcId")]
pub vpc_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct RevokeSecurityGroupIngressRequest {


#[serde(rename="GroupId")]
pub group_id: String,

#[serde(rename="FromPort")]
pub from_port: i32,

#[serde(rename="CidrIp")]
pub cidr_ip: String,

#[serde(rename="ToPort")]
pub to_port: i32,

#[serde(rename="IpPermissions")]
pub ip_permissions: IpPermissionList,

#[serde(rename="SourceSecurityGroupOwnerId")]
pub source_security_group_owner_id: String,

#[serde(rename="IpProtocol")]
pub ip_protocol: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="SourceSecurityGroupName")]
pub source_security_group_name: String,

#[serde(rename="GroupName")]
pub group_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateNetworkAclResult {


#[serde(rename="networkAcl")]
pub network_acl: NetworkAcl,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeCustomerGatewaysResult {


#[serde(rename="customerGatewaySet")]
pub customer_gateway_set: CustomerGatewayList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct UnassignPrivateIpAddressesRequest {


#[serde(rename="privateIpAddress")]
pub private_ip_address: PrivateIpAddressStringList,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateInstanceExportTaskRequest {


#[serde(rename="exportToS3")]
pub export_to_s3: ExportToS3TaskSpecification,

#[serde(rename="targetEnvironment")]
pub target_environment: ExportEnvironment,

#[serde(rename="description")]
pub description: String,

#[serde(rename="instanceId")]
pub instance_id: String,
}



/// <p>Describes a region.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct Region {


#[serde(rename="regionEndpoint")]
pub region_endpoint: String,

#[serde(rename="regionName")]
pub region_name: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CopyImageResult {


#[serde(rename="imageId")]
pub image_id: String,
}



/// <p>Describes a linked EC2-Classic instance.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct ClassicLinkInstance {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="groupSet")]
pub group_set: GroupIdentifierList,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="instanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateSubnetRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="AvailabilityZone")]
pub availability_zone: String,

#[serde(rename="CidrBlock")]
pub cidr_block: String,

#[serde(rename="VpcId")]
pub vpc_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeInstanceStatusRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: InstanceIdStringList,

#[serde(rename="MaxResults")]
pub max_results: i32,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="includeAllInstances")]
pub include_all_instances: bool,

#[serde(rename="NextToken")]
pub next_token: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateVpcRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="CidrBlock")]
pub cidr_block: String,

#[serde(rename="instanceTenancy")]
pub instance_tenancy: Tenancy,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DisassociateRouteTableRequest {


#[serde(rename="associationId")]
pub association_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeAddressesRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="AllocationId")]
pub allocation_id: AllocationIdList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="PublicIp")]
pub public_ip: PublicIpStringList,
}



/// <p>Describes a VPN connection.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpnConnection {


#[serde(rename="vpnGatewayId")]
pub vpn_gateway_id: String,

#[serde(rename="state")]
pub state: VpnState,

#[serde(rename="vpnConnectionId")]
pub vpn_connection_id: String,

#[serde(rename="customerGatewayId")]
pub customer_gateway_id: String,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="customerGatewayConfiguration")]
pub customer_gateway_configuration: String,

#[serde(rename="options")]
pub options: VpnConnectionOptions,

#[serde(rename="vgwTelemetry")]
pub vgw_telemetry: VgwTelemetryList,

#[serde(rename="GatewayType")]
pub gateway_type: GatewayType,

#[serde(rename="routes")]
pub routes: VpnStaticRouteList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeConversionTasksRequest {


#[serde(rename="filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="conversionTaskId")]
pub conversion_task_id: ConversionIdStringList,
}



/// <p>Describes a security group.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct GroupIdentifier {


#[serde(rename="groupName")]
pub group_name: String,

#[serde(rename="groupId")]
pub group_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeNetworkInterfacesResult {


#[serde(rename="networkInterfaceSet")]
pub network_interface_set: NetworkInterfaceList,
}



/// <p>Describes a launch permission.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct LaunchPermission {


#[serde(rename="userId")]
pub user_id: String,

#[serde(rename="group")]
pub group: PermissionGroup,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateInternetGatewayRequest {


#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesRequest {


#[serde(rename="offeringType")]
pub offering_type: OfferingTypeValues,

#[serde(rename="ReservedInstancesId")]
pub reserved_instances_id: ReservedInstancesIdStringList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="dryRun")]
pub dry_run: bool,
}



/// <p>Describes an IAM instance profile.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct IamInstanceProfileSpecification {


#[serde(rename="name")]
pub name: String,

#[serde(rename="arn")]
pub arn: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DeleteVpnConnectionRouteRequest {


#[serde(rename="VpnConnectionId")]
pub vpn_connection_id: String,

#[serde(rename="DestinationCidrBlock")]
pub destination_cidr_block: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct CreateReservedInstancesListingRequest {


#[serde(rename="instanceCount")]
pub instance_count: i32,

#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,

#[serde(rename="clientToken")]
pub client_token: String,

#[serde(rename="priceSchedules")]
pub price_schedules: PriceScheduleSpecificationList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AttachInternetGatewayRequest {


#[serde(rename="vpcId")]
pub vpc_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="internetGatewayId")]
pub internet_gateway_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct AssignPrivateIpAddressesRequest {


#[serde(rename="privateIpAddress")]
pub private_ip_address: PrivateIpAddressStringList,

#[serde(rename="allowReassignment")]
pub allow_reassignment: bool,

#[serde(rename="secondaryPrivateIpAddressCount")]
pub secondary_private_ip_address_count: i32,

#[serde(rename="networkInterfaceId")]
pub network_interface_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeSnapshotsResult {


#[serde(rename="nextToken")]
pub next_token: String,

#[serde(rename="snapshotSet")]
pub snapshot_set: SnapshotList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct ReplaceNetworkAclAssociationResult {


#[serde(rename="newAssociationId")]
pub new_association_id: String,
}



/// <p>Describes a virtual private gateway.</p>
#[derive(Debug ,Serialize,Deserialize)]
pub struct VpnGateway {


#[serde(rename="vpnGatewayId")]
pub vpn_gateway_id: String,

#[serde(rename="availabilityZone")]
pub availability_zone: String,

#[serde(rename="GatewayType")]
pub gateway_type: GatewayType,

#[serde(rename="attachments")]
pub attachments: VpcAttachmentList,

#[serde(rename="tagSet")]
pub tag_set: TagList,

#[serde(rename="state")]
pub state: VpnState,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct GetConsoleOutputRequest {


#[serde(rename="InstanceId")]
pub instance_id: String,

#[serde(rename="dryRun")]
pub dry_run: bool,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeReservedInstancesListingsRequest {


#[serde(rename="reservedInstancesId")]
pub reserved_instances_id: String,

#[serde(rename="filters")]
pub filters: FilterList,

#[serde(rename="reservedInstancesListingId")]
pub reserved_instances_listing_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeVpcClassicLinkResult {


#[serde(rename="vpcSet")]
pub vpc_set: VpcClassicLinkList,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct BundleInstanceRequest {


#[serde(rename="Storage")]
pub storage: Storage,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="InstanceId")]
pub instance_id: String,
}




#[derive(Debug ,Serialize,Deserialize)]
pub struct DescribeImagesRequest {


#[serde(rename="ImageId")]
pub image_id: ImageIdStringList,

#[serde(rename="Filter")]
pub filter: FilterList,

#[serde(rename="Owner")]
pub owner: OwnerStringList,

#[serde(rename="dryRun")]
pub dry_run: bool,

#[serde(rename="ExecutableBy")]
pub executable_by: ExecutableByStringList,
}





pub type UserGroupStringList = Vec<String>;


pub type ImageIdStringList = Vec<String>;


pub type ReasonCodesList = Vec<ReportInstanceReasonCodes>;


pub type PricingDetailsList = Vec<PricingDetail>;


pub type PropagatingVgwList = Vec<PropagatingVgw>;


pub type VolumeAttachmentList = Vec<VolumeAttachment>;


pub type VpnConnectionIdStringList = Vec<String>;


pub type InternetGatewayAttachmentList = Vec<InternetGatewayAttachment>;


pub type TagDescriptionList = Vec<TagDescription>;


pub type GroupIdStringList = Vec<String>;


pub type VpcIdStringList = Vec<String>;


pub type BundleTaskList = Vec<BundleTask>;


pub type NetworkInterfaceList = Vec<NetworkInterface>;


pub type ExportTaskIdStringList = Vec<String>;


pub type SubnetIdStringList = Vec<String>;


pub type ReservedIntancesIds = Vec<ReservedInstancesId>;


pub type InstanceStateChangeList = Vec<InstanceStateChange>;


pub type ReservedInstancesModificationResultList = Vec<ReservedInstancesModificationResult>;


pub type AccountAttributeNameStringList = Vec<AccountAttributeName>;


pub type DhcpConfigurationList = Vec<DhcpConfiguration>;


pub type ImportInstanceVolumeDetailSet = Vec<ImportInstanceVolumeDetailItem>;


pub type ReservedInstancesModificationList = Vec<ReservedInstancesModification>;


pub type PrivateIpAddressStringList = Vec<String>;


pub type RouteList = Vec<Route>;


pub type ImageList = Vec<Image>;


pub type SecurityGroupIdStringList = Vec<String>;


pub type BundleIdStringList = Vec<String>;


pub type VpnStaticRouteList = Vec<VpnStaticRoute>;


pub type InstanceBlockDeviceMappingSpecificationList = Vec<InstanceBlockDeviceMappingSpecification>;


pub type DescribeConversionTaskList = Vec<ConversionTask>;


pub type VolumeList = Vec<Volume>;


pub type SecurityGroupList = Vec<SecurityGroup>;


pub type AvailabilityZoneList = Vec<AvailabilityZone>;


pub type SpotPriceHistoryList = Vec<SpotPrice>;


pub type IpRangeList = Vec<IpRange>;


pub type RouteTableAssociationList = Vec<RouteTableAssociation>;


pub type KeyNameStringList = Vec<String>;


pub type VpcList = Vec<Vpc>;


pub type OwnerStringList = Vec<String>;


pub type LaunchPermissionList = Vec<LaunchPermission>;


pub type IpPermissionList = Vec<IpPermission>;


pub type ReservationList = Vec<Reservation>;


pub type SnapshotList = Vec<Snapshot>;


pub type PlacementGroupList = Vec<PlacementGroup>;


pub type SecurityGroupStringList = Vec<String>;


pub type VolumeStatusList = Vec<VolumeStatusItem>;


pub type ConversionIdStringList = Vec<String>;


pub type NetworkInterfaceIdList = Vec<String>;


pub type NewDhcpConfigurationList = Vec<NewDhcpConfiguration>;


pub type VpcAttachmentList = Vec<VpcAttachment>;


pub type PriceScheduleSpecificationList = Vec<PriceScheduleSpecification>;


pub type ReservedInstancesList = Vec<ReservedInstances>;


pub type PlacementGroupStringList = Vec<String>;


pub type ClassicLinkInstanceList = Vec<ClassicLinkInstance>;


pub type InstancePrivateIpAddressList = Vec<InstancePrivateIpAddress>;


pub type CustomerGatewayIdStringList = Vec<String>;


pub type RecurringChargesList = Vec<RecurringCharge>;


pub type InstanceBlockDeviceMappingList = Vec<InstanceBlockDeviceMapping>;


pub type InstanceList = Vec<Instance>;


pub type ProductCodeStringList = Vec<String>;


pub type UserIdStringList = Vec<String>;


pub type DhcpConfigurationValueList = Vec<AttributeValue>;


pub type NetworkAclList = Vec<NetworkAcl>;


pub type VpcPeeringConnectionList = Vec<VpcPeeringConnection>;


pub type VolumeStatusEventsList = Vec<VolumeStatusEvent>;


pub type AccountAttributeValueList = Vec<AccountAttributeValue>;


pub type ResourceIdList = Vec<String>;


pub type AccountAttributeList = Vec<AccountAttribute>;


pub type RouteTableList = Vec<RouteTable>;


pub type CustomerGatewayList = Vec<CustomerGateway>;


pub type DiskImageList = Vec<DiskImage>;


pub type AllocationIdList = Vec<String>;


pub type CreateVolumePermissionList = Vec<CreateVolumePermission>;


pub type InstanceStatusDetailsList = Vec<InstanceStatusDetails>;


pub type GroupIdentifierList = Vec<GroupIdentifier>;


pub type FilterList = Vec<Filter>;


pub type VolumeStatusActionsList = Vec<VolumeStatusAction>;


pub type VpcClassicLinkList = Vec<VpcClassicLink>;


pub type AddressList = Vec<Address>;


pub type PriceScheduleList = Vec<PriceSchedule>;


pub type NetworkInterfacePrivateIpAddressList = Vec<NetworkInterfacePrivateIpAddress>;


pub type GroupNameStringList = Vec<String>;


pub type KeyPairList = Vec<KeyPairInfo>;


pub type CancelledSpotInstanceRequestList = Vec<CancelledSpotInstanceRequest>;


pub type VpnGatewayList = Vec<VpnGateway>;


pub type SnapshotIdStringList = Vec<String>;


pub type ValueStringList = Vec<String>;


pub type InstanceNetworkInterfaceList = Vec<InstanceNetworkInterface>;


pub type InstanceCountList = Vec<InstanceCount>;


pub type BlockDeviceMappingList = Vec<BlockDeviceMapping>;


pub type BlockDeviceMappingRequestList = Vec<BlockDeviceMapping>;


pub type InternetGatewayList = Vec<InternetGateway>;


pub type RestorableByStringList = Vec<String>;


pub type ZoneNameStringList = Vec<String>;


pub type InstanceTypeList = Vec<InstanceType>;


pub type InstanceMonitoringList = Vec<InstanceMonitoring>;


pub type ExecutableByStringList = Vec<String>;


pub type PrivateIpAddressSpecificationList = Vec<PrivateIpAddressSpecification>;


pub type TagList = Vec<Tag>;


pub type VpcClassicLinkIdList = Vec<String>;


pub type VolumeStatusDetailsList = Vec<VolumeStatusDetails>;


pub type RegionList = Vec<Region>;


pub type InstanceStatusList = Vec<InstanceStatus>;


pub type RegionNameStringList = Vec<String>;


pub type DhcpOptionsList = Vec<DhcpOptions>;


pub type SubnetList = Vec<Subnet>;


pub type InstanceNetworkInterfaceSpecificationList = Vec<InstanceNetworkInterfaceSpecification>;


pub type ReservedInstancesConfigurationList = Vec<ReservedInstancesConfiguration>;


pub type AvailabilityZoneMessageList = Vec<AvailabilityZoneMessage>;


pub type ReservedInstancesModificationIdStringList = Vec<String>;


pub type ReservedInstancesListingList = Vec<ReservedInstancesListing>;


pub type DhcpOptionsIdStringList = Vec<String>;


pub type ProductDescriptionList = Vec<String>;


pub type VpnGatewayIdStringList = Vec<String>;


pub type SpotInstanceRequestIdList = Vec<String>;


pub type ExportTaskList = Vec<ExportTask>;


pub type SpotInstanceRequestList = Vec<SpotInstanceRequest>;


pub type InstanceStatusEventList = Vec<InstanceStatusEvent>;


pub type ReservedInstancesIdStringList = Vec<String>;


pub type VolumeIdStringList = Vec<String>;


pub type ReservedInstancesOfferingIdStringList = Vec<String>;


pub type ProductCodeList = Vec<ProductCode>;


pub type VgwTelemetryList = Vec<VgwTelemetry>;


pub type UserIdGroupPairList = Vec<UserIdGroupPair>;


pub type ReservedInstancesOfferingList = Vec<ReservedInstancesOffering>;


pub type NetworkAclEntryList = Vec<NetworkAclEntry>;


pub type PublicIpStringList = Vec<String>;


pub type NetworkAclAssociationList = Vec<NetworkAclAssociation>;


pub type InstanceIdStringList = Vec<String>;


pub type Blob = Vec<u8>;


pub type VpnConnectionList = Vec<VpnConnection>;



#[derive(Debug ,Serialize,Deserialize)]
pub enum ResetImageAttributeName {
   LaunchPermission,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum InstanceStateName {
   Pending,Running,ShuttingDown,Terminated,Stopping,Stopped,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum StatusName {
   Reachability,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum StatusType {
   Passed,Failed,InsufficientData,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ResourceType {
   CustomerGateway,DhcpOptions,Image,Instance,InternetGateway,NetworkAcl,NetworkInterface,ReservedInstances,RouteTable,Snapshot,SpotInstancesRequest,Subnet,SecurityGroup,Volume,Vpc,VpnConnection,VpnGateway,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum CancelSpotInstanceRequestState {
   Active,Open,Closed,Cancelled,Completed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeAttachmentState {
   Attaching,Attached,Detaching,Detached,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VpnStaticRouteSource {
   Static,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum HypervisorType {
   Ovm,Xen,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum RouteState {
   Active,Blackhole,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum RuleAction {
   Allow,Deny,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum PlacementGroupState {
   Pending,Available,Deleting,Deleted,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ConversionTaskState {
   Active,Cancelling,Cancelled,Completed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum EventCode {
   InstanceReboot,SystemReboot,SystemMaintenance,InstanceRetirement,InstanceStop,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ContainerFormat {
   Ova,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum OfferingTypeValues {
   HeavyUtilization,MediumUtilization,LightUtilization,NoUpfront,PartialUpfront,AllUpfront,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ListingStatus {
   Active,Pending,Cancelled,Closed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum DatafeedSubscriptionState {
   Active,Inactive,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ReportInstanceReasonCodes {
   InstanceStuckInState,Unresponsive,NotAcceptingCredentials,PasswordNotAvailable,PerformanceNetwork,PerformanceInstanceStore,PerformanceEbsVolume,PerformanceOther,Other,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum DiskImageFormat {
   VMDK,RAW,VHD,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum TelemetryStatus {
   UP,DOWN,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum InstanceAttributeName {
   InstanceType,Kernel,Ramdisk,UserData,DisableApiTermination,InstanceInitiatedShutdownBehavior,RootDeviceName,BlockDeviceMapping,ProductCodes,SourceDestCheck,GroupSet,EbsOptimized,SriovNetSupport,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum InstanceType {
   T1Micro,M1Small,M1Medium,M1Large,M1Xlarge,M3Medium,M3Large,M3Xlarge,M32xlarge,T2Micro,T2Small,T2Medium,M2Xlarge,M22xlarge,M24xlarge,Cr18xlarge,I2Xlarge,I22xlarge,I24xlarge,I28xlarge,Hi14xlarge,Hs18xlarge,C1Medium,C1Xlarge,C3Large,C3Xlarge,C32xlarge,C34xlarge,C38xlarge,C4Large,C4Xlarge,C42xlarge,C44xlarge,C48xlarge,Cc14xlarge,Cc28xlarge,G22xlarge,Cg14xlarge,R3Large,R3Xlarge,R32xlarge,R34xlarge,R38xlarge,D2Xlarge,D22xlarge,D24xlarge,D28xlarge,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeState {
   Creating,Available,InUse,Deleting,Deleted,Error,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum AttachmentStatus {
   Attaching,Attached,Detaching,Detached,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum InstanceLifecycleType {
   Spot,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SummaryStatus {
   Ok,Impaired,InsufficientData,NotApplicable,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ImageTypeValues {
   Machine,Kernel,Ramdisk,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ArchitectureValues {
   I386,X8664,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ReservedInstanceState {
   PaymentPending,Active,PaymentFailed,Retired,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum BundleTaskState {
   Pending,WaitingForShutdown,Bundling,Storing,Cancelling,Complete,Failed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum NetworkInterfaceAttribute {
   Description,GroupSet,SourceDestCheck,Attachment,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ExportTaskState {
   Active,Cancelling,Cancelled,Completed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VpcAttributeName {
   EnableDnsSupport,EnableDnsHostnames,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SpotInstanceType {
   OneTime,Persistent,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum PlacementStrategy {
   Cluster,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum DeviceType {
   Ebs,InstanceStore,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum PermissionGroup {
   All,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SnapshotState {
   Pending,Completed,Error,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ExportEnvironment {
   Citrix,Vmware,Microsoft,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ShutdownBehavior {
   Stop,Terminate,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ProductCodeValues {
   Devpay,Marketplace,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ImageState {
   Available,Deregistered,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeAttributeName {
   AutoEnableIO,ProductCodes,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VpcState {
   Pending,Available,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum NetworkInterfaceStatus {
   Available,Attaching,InUse,Detaching,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum DomainType {
   Vpc,Standard,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SubnetState {
   Pending,Available,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum Tenancy {
   Default,Dedicated,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VirtualizationType {
   Hvm,Paravirtual,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeStatusName {
   IoEnabled,IoPerformance,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum RecurringChargeFrequency {
   Hourly,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum RouteOrigin {
   CreateRouteTable,CreateRoute,EnableVgwRoutePropagation,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SpotInstanceState {
   Open,Active,Closed,Cancelled,Failed,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum GatewayType {
   Ipsec1,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeStatusInfoStatus {
   Ok,Impaired,InsufficientData,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VolumeType {
   Standard,Io1,Gp2,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum RIProductDescription {
   Linux,LinuxVpc,Windows,WindowsVpc,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ImageAttributeName {
   Description,Kernel,Ramdisk,LaunchPermission,ProductCodes,BlockDeviceMapping,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum MonitoringState {
   Disabled,Enabled,Pending,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum AvailabilityZoneState {
   Available,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum CurrencyCodeValues {
   USD,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ListingState {
   Available,Sold,Cancelled,Pending,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum VpnState {
   Pending,Available,Deleting,Deleted,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum SnapshotAttributeName {
   ProductCodes,CreateVolumePermission,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum PlatformValues {
   Windows,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum AccountAttributeName {
   SupportedPlatforms,DefaultVpc,
}


#[derive(Debug ,Serialize,Deserialize)]
pub enum ReportStatusType {
   Ok,Impaired,
}



pub type DateTime = u64;
