
use awaze_core::ItemVec;
use ::subnet::*;



pub type SubnetList = ItemVec<Subnet>;
