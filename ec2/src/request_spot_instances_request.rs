
use ::date_time::*;
use ::request_spot_launch_specification::*;
use ::spot_instance_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct RequestSpotInstancesRequest {

    #[serde(rename_deserialize="availabilityZoneGroup", rename_serialize="AvailabilityZoneGroup")]
    pub availability_zone_group: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="instanceCount", rename_serialize="InstanceCount")]
    pub instance_count: Option<i32>,
    #[serde(rename_deserialize="launchGroup", rename_serialize="LaunchGroup")]
    pub launch_group: Option<String>,
    #[serde(rename_deserialize="LaunchSpecification", rename_serialize="LaunchSpecification")]
    pub launch_specification: Option<RequestSpotLaunchSpecification>,
    #[serde(rename_deserialize="spotPrice", rename_serialize="SpotPrice")]
    pub spot_price: String,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub spot_type: Option<SpotInstanceType>,
    #[serde(rename_deserialize="validFrom", rename_serialize="ValidFrom")]
    pub valid_from: Option<DateTime>,
    #[serde(rename_deserialize="validUntil", rename_serialize="ValidUntil")]
    pub valid_until: Option<DateTime>,
}


