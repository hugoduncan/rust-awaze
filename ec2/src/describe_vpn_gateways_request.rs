
use ::filter_list::*;
use ::vpn_gateway_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpnGatewaysRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="VpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_ids: Option<VpnGatewayIdStringList>,
}


