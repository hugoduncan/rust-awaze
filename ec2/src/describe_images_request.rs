
use ::executable_by_string_list::*;
use ::filter_list::*;
use ::image_id_string_list::*;
use ::owner_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeImagesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ExecutableBy", rename_serialize="ExecutableBy")]
    pub executable_users: Option<ExecutableByStringList>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_ids: Option<ImageIdStringList>,
    #[serde(rename_deserialize="Owner", rename_serialize="Owner")]
    pub owners: Option<OwnerStringList>,
}


