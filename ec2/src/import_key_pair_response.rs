



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportKeyPairResponse {

    #[serde(rename_deserialize="keyFingerprint", rename_serialize="KeyFingerprint")]
    pub key_fingerprint: Option<String>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


