



#[derive(Debug, Serialize, Deserialize)]
pub struct EnableVgwRoutePropagationRequest {

    #[serde(rename_deserialize="GatewayId", rename_serialize="GatewayId")]
    pub gateway_id: String,
    #[serde(rename_deserialize="RouteTableId", rename_serialize="RouteTableId")]
    pub route_table_id: String,
}


