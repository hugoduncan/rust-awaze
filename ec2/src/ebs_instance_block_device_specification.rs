



#[derive(Debug, Serialize, Deserialize)]
pub struct EbsInstanceBlockDeviceSpecification {

    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
}


