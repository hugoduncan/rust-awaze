
use ::date_time::*;
use ::event_code::*;


/// <p>Describes an instance event.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceStatusEvent {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<EventCode>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="notAfter", rename_serialize="NotAfter")]
    pub not_after: Option<DateTime>,
    #[serde(rename_deserialize="notBefore", rename_serialize="NotBefore")]
    pub not_before: Option<DateTime>,
}


