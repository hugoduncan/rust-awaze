



#[derive(Debug, Serialize, Deserialize)]
pub struct EnableVpcClassicLinkRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


