
use awaze_core::ItemVec;
use ::propagating_vgw::*;



pub type PropagatingVgwList = ItemVec<PropagatingVgw>;
