


/// <p>Describes the monitoring for the instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct RunInstancesMonitoringEnabled {

    #[serde(rename_deserialize="enabled", rename_serialize="Enabled")]
    pub enabled: bool,
}


