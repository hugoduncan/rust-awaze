



#[derive(Debug, Serialize, Deserialize)]
pub struct DisassociateAddressRequest {

    #[serde(rename_deserialize="AssociationId", rename_serialize="AssociationId")]
    pub association_id: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="PublicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
}


