
use ::value_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct NewDhcpConfiguration {

    #[serde(rename_deserialize="key", rename_serialize="Key")]
    pub key: Option<String>,
    #[serde(rename_deserialize="Value", rename_serialize="Value")]
    pub values: Option<ValueStringList>,
}


