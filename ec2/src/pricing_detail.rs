


/// <p>Describes a Reserved Instance offering.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PricingDetail {

    #[serde(rename_deserialize="count", rename_serialize="Count")]
    pub count: Option<i32>,
    #[serde(rename_deserialize="price", rename_serialize="Price")]
    pub price: Option<f64>,
}


