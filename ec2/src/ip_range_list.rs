
use awaze_core::ItemVec;
use ::ip_range::*;



pub type IpRangeList = ItemVec<IpRange>;
