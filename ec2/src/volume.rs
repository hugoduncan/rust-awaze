
use ::date_time::*;
use ::tag_list::*;
use ::volume_attachment_list::*;
use ::volume_state::*;
use ::volume_type::*;


/// <p>Describes a volume.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Volume {

    #[serde(rename_deserialize="attachmentSet", rename_serialize="AttachmentSet")]
    pub attachments: Option<VolumeAttachmentList>,
    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="createTime", rename_serialize="CreateTime")]
    pub create_time: Option<DateTime>,
    #[serde(rename_deserialize="encrypted", rename_serialize="Encrypted")]
    pub encrypted: Option<bool>,
    #[serde(rename_deserialize="iops", rename_serialize="Iops")]
    pub iops: Option<i32>,
    #[serde(rename_deserialize="kmsKeyId", rename_serialize="KmsKeyId")]
    pub kms_key_id: Option<String>,
    #[serde(rename_deserialize="size", rename_serialize="Size")]
    pub size: Option<i32>,
    #[serde(rename_deserialize="snapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub state: Option<VolumeState>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="volumeId", rename_serialize="VolumeId")]
    pub volume_id: Option<String>,
    #[serde(rename_deserialize="volumeType", rename_serialize="VolumeType")]
    pub volume_type: Option<VolumeType>,
}


