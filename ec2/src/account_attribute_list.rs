
use awaze_core::ItemVec;
use ::account_attribute::*;



pub type AccountAttributeList = ItemVec<AccountAttribute>;
