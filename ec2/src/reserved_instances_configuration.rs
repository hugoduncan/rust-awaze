
use ::instance_type::*;


/// <p>Describes the configuration settings for the modified Reserved Instances.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesConfiguration {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="instanceCount", rename_serialize="InstanceCount")]
    pub instance_count: Option<i32>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="platform", rename_serialize="Platform")]
    pub platform: Option<String>,
}


