



#[derive(Debug, Serialize, Deserialize)]
pub struct RejectVpcPeeringConnectionRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="vpcPeeringConnectionId", rename_serialize="VpcPeeringConnectionId")]
    pub vpc_peering_connection_id: String,
}


