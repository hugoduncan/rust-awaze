
use awaze_core::ItemVec;
use ::reserved_instances_listing::*;



pub type ReservedInstancesListingList = ItemVec<ReservedInstancesListing>;
