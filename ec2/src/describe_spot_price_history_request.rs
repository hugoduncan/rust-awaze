
use ::date_time::*;
use ::filter_list::*;
use ::instance_type_list::*;
use ::product_description_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSpotPriceHistoryRequest {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="endTime", rename_serialize="EndTime")]
    pub end_time: Option<DateTime>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="InstanceType", rename_serialize="InstanceType")]
    pub instance_types: Option<InstanceTypeList>,
    #[serde(rename_deserialize="maxResults", rename_serialize="MaxResults")]
    pub max_results: Option<i32>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="ProductDescription", rename_serialize="ProductDescription")]
    pub product_descriptions: Option<ProductDescriptionList>,
    #[serde(rename_deserialize="startTime", rename_serialize="StartTime")]
    pub start_time: Option<DateTime>,
}


