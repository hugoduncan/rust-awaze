
use ::network_interface::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateNetworkInterfaceResponse {

    #[serde(rename_deserialize="networkInterface", rename_serialize="NetworkInterface")]
    pub network_interface: Option<NetworkInterface>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


