



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelConversionRequest {

    #[serde(rename_deserialize="conversionTaskId", rename_serialize="ConversionTaskId")]
    pub conversion_task_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="reasonMessage", rename_serialize="ReasonMessage")]
    pub reason_message: Option<String>,
}


