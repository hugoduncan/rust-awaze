
use awaze_core::ItemVec;
use ::volume_status_event::*;



pub type VolumeStatusEventsList = ItemVec<VolumeStatusEvent>;
