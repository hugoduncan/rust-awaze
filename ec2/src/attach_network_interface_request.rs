



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachNetworkInterfaceRequest {

    #[serde(rename_deserialize="deviceIndex", rename_serialize="DeviceIndex")]
    pub device_index: i32,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
}


