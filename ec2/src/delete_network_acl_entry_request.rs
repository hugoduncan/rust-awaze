



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteNetworkAclEntryRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="egress", rename_serialize="Egress")]
    pub egress: bool,
    #[serde(rename_deserialize="networkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_id: String,
    #[serde(rename_deserialize="ruleNumber", rename_serialize="RuleNumber")]
    pub rule_number: i32,
}


