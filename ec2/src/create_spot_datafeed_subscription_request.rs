



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateSpotDatafeedSubscriptionRequest {

    #[serde(rename_deserialize="bucket", rename_serialize="Bucket")]
    pub bucket: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="prefix", rename_serialize="Prefix")]
    pub prefix: Option<String>,
}


