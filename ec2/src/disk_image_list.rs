
use awaze_core::ItemVec;
use ::disk_image::*;



pub type DiskImageList = ItemVec<DiskImage>;
