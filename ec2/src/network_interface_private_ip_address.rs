
use ::network_interface_association::*;


/// <p>Describes the private IP address of a network interface.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkInterfacePrivateIpAddress {

    #[serde(rename_deserialize="association", rename_serialize="Association")]
    pub association: Option<NetworkInterfaceAssociation>,
    #[serde(rename_deserialize="primary", rename_serialize="Primary")]
    pub primary: Option<bool>,
    #[serde(rename_deserialize="privateDnsName", rename_serialize="PrivateDnsName")]
    pub private_dns_name: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
}


