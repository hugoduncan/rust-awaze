



#[derive(Debug, Clone)]
pub enum ConversionTaskState {
   Active,Cancelling,Cancelled,Completed,
}

string_enum!{ ConversionTaskState,
    ConversionTaskState::Active => "active" => ConversionTaskState::Active,
    ConversionTaskState::Cancelling => "cancelling" => ConversionTaskState::Cancelling,
    ConversionTaskState::Cancelled => "cancelled" => ConversionTaskState::Cancelled,
    ConversionTaskState::Completed => "completed" => ConversionTaskState::Completed

}