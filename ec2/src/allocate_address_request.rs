
use ::domain_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AllocateAddressRequest {

    #[serde(rename_deserialize="Domain", rename_serialize="Domain")]
    pub domain: Option<DomainType>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


