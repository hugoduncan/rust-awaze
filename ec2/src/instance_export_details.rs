
use ::export_environment::*;


/// <p>Describes an instance export task.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceExportDetails {

    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="targetEnvironment", rename_serialize="TargetEnvironment")]
    pub target_environment: Option<ExportEnvironment>,
}


