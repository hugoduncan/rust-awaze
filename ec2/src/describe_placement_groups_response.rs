
use ::placement_group_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribePlacementGroupsResponse {

    #[serde(rename_deserialize="placementGroupSet", rename_serialize="PlacementGroupSet")]
    pub placement_groups: Option<PlacementGroupList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


