
use ::cancel_spot_instance_request_state::*;


/// <p>Describes a request to cancel a Spot Instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct CancelledSpotInstanceRequest {

    #[serde(rename_deserialize="spotInstanceRequestId", rename_serialize="SpotInstanceRequestId")]
    pub spot_instance_request_id: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<CancelSpotInstanceRequestState>,
}


