
use ::tag_list::*;
use ::tenancy::*;
use ::vpc_state::*;


/// <p>Describes a VPC.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Vpc {

    #[serde(rename_deserialize="cidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: String,
    #[serde(rename_deserialize="dhcpOptionsId", rename_serialize="DhcpOptionsId")]
    pub dhcp_options_id: String,
    #[serde(rename_deserialize="instanceTenancy", rename_serialize="InstanceTenancy")]
    pub instance_tenancy: Tenancy,
    #[serde(rename_deserialize="isDefault", rename_serialize="IsDefault")]
    pub is_default: Option<bool>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: VpcState,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


