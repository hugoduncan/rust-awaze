
use ::vpc::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpcResponse {

    #[serde(rename_deserialize="vpc", rename_serialize="Vpc")]
    pub vpc: Vpc,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


