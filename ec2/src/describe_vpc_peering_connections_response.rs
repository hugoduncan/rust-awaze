
use ::vpc_peering_connection_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcPeeringConnectionsResponse {

    #[serde(rename_deserialize="vpcPeeringConnectionSet", rename_serialize="VpcPeeringConnectionSet")]
    pub vpc_peering_connections: Option<VpcPeeringConnectionList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


