
use ::volume_type::*;


/// <p>Describes an Amazon EBS block device.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct EbsBlockDevice {

    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="encrypted", rename_serialize="Encrypted")]
    pub encrypted: Option<bool>,
    #[serde(rename_deserialize="iops", rename_serialize="Iops")]
    pub iops: Option<i32>,
    #[serde(rename_deserialize="snapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="volumeSize", rename_serialize="VolumeSize")]
    pub volume_size: Option<i32>,
    #[serde(rename_deserialize="volumeType", rename_serialize="VolumeType")]
    pub volume_type: Option<VolumeType>,
}


