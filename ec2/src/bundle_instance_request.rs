
use ::storage::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct BundleInstanceRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="Storage", rename_serialize="Storage")]
    pub storage: Storage,
}


