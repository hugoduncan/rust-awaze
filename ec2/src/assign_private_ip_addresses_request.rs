
use ::private_ip_address_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AssignPrivateIpAddressesRequest {

    #[serde(rename_deserialize="allowReassignment", rename_serialize="AllowReassignment")]
    pub allow_reassignment: Option<bool>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_addresses: Option<PrivateIpAddressStringList>,
    #[serde(rename_deserialize="secondaryPrivateIpAddressCount", rename_serialize="SecondaryPrivateIpAddressCount")]
    pub secondary_private_ip_address_count: Option<i32>,
}


