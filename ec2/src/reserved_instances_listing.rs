
use ::date_time::*;
use ::instance_count_list::*;
use ::listing_status::*;
use ::price_schedule_list::*;
use ::tag_list::*;


/// <p>Describes a Reserved Instance listing.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesListing {

    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="createDate", rename_serialize="CreateDate")]
    pub create_date: Option<DateTime>,
    #[serde(rename_deserialize="instanceCounts", rename_serialize="InstanceCounts")]
    pub instance_counts: Option<InstanceCountList>,
    #[serde(rename_deserialize="priceSchedules", rename_serialize="PriceSchedules")]
    pub price_schedules: Option<PriceScheduleList>,
    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
    #[serde(rename_deserialize="reservedInstancesListingId", rename_serialize="ReservedInstancesListingId")]
    pub reserved_instances_listing_id: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<ListingStatus>,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="updateDate", rename_serialize="UpdateDate")]
    pub update_date: Option<DateTime>,
}


