
use ::ebs_instance_block_device_specification::*;


/// <p>Describes a block device mapping entry.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceBlockDeviceMappingSpecification {

    #[serde(rename_deserialize="deviceName", rename_serialize="DeviceName")]
    pub device_name: Option<String>,
    #[serde(rename_deserialize="ebs", rename_serialize="Ebs")]
    pub ebs: Option<EbsInstanceBlockDeviceSpecification>,
    #[serde(rename_deserialize="noDevice", rename_serialize="NoDevice")]
    pub no_device: Option<String>,
    #[serde(rename_deserialize="virtualName", rename_serialize="VirtualName")]
    pub virtual_name: Option<String>,
}


