
use ::vpc_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeVpcsResponse {

    #[serde(rename_deserialize="vpcSet", rename_serialize="VpcSet")]
    pub vpcs: VpcList,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


