
use ::price_schedule_specification_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateReservedInstancesListingRequest {

    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: String,
    #[serde(rename_deserialize="instanceCount", rename_serialize="InstanceCount")]
    pub instance_count: i32,
    #[serde(rename_deserialize="priceSchedules", rename_serialize="PriceSchedules")]
    pub price_schedules: PriceScheduleSpecificationList,
    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: String,
}


