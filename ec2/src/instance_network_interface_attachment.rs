
use ::attachment_status::*;
use ::date_time::*;


/// <p>Describes a network interface attachment.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceNetworkInterfaceAttachment {

    #[serde(rename_deserialize="attachTime", rename_serialize="AttachTime")]
    pub attach_time: Option<DateTime>,
    #[serde(rename_deserialize="attachmentId", rename_serialize="AttachmentId")]
    pub attachment_id: Option<String>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="deviceIndex", rename_serialize="DeviceIndex")]
    pub device_index: Option<i32>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<AttachmentStatus>,
}


