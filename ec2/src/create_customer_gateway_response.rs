
use ::customer_gateway::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateCustomerGatewayResponse {

    #[serde(rename_deserialize="customerGateway", rename_serialize="CustomerGateway")]
    pub customer_gateway: Option<CustomerGateway>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


