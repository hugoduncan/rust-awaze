
use awaze_core::ItemVec;
use ::internet_gateway_attachment::*;



pub type InternetGatewayAttachmentList = ItemVec<InternetGatewayAttachment>;
