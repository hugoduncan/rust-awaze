
use awaze_core::ItemVec;
use ::instance_type::*;



pub type InstanceTypeList = ItemVec<InstanceType>;
