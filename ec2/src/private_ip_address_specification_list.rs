
use awaze_core::ItemVec;
use ::private_ip_address_specification::*;



pub type PrivateIpAddressSpecificationList = ItemVec<PrivateIpAddressSpecification>;
