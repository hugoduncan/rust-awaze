
use ::block_device_mapping_request_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateImageRequest {

    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingRequestList>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: String,
    #[serde(rename_deserialize="noReboot", rename_serialize="NoReboot")]
    pub no_reboot: Option<bool>,
}


