
use ::account_attribute_value_list::*;


/// <p>Describes an account attribute.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AccountAttribute {

    #[serde(rename_deserialize="attributeName", rename_serialize="AttributeName")]
    pub attribute_name: Option<String>,
    #[serde(rename_deserialize="attributeValueSet", rename_serialize="AttributeValueSet")]
    pub attribute_values: Option<AccountAttributeValueList>,
}


