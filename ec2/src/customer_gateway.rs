
use ::tag_list::*;


/// <p>Describes a customer gateway.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct CustomerGateway {

    #[serde(rename_deserialize="bgpAsn", rename_serialize="BgpAsn")]
    pub bgp_asn: Option<String>,
    #[serde(rename_deserialize="customerGatewayId", rename_serialize="CustomerGatewayId")]
    pub customer_gateway_id: Option<String>,
    #[serde(rename_deserialize="ipAddress", rename_serialize="IpAddress")]
    pub ip_address: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub gateway_type: Option<String>,
}


