
use ::date_time::*;
use ::reserved_instances_modification_result_list::*;
use ::reserved_intances_ids::*;


/// <p>Describes a Reserved Instance modification.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesModification {

    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="createDate", rename_serialize="CreateDate")]
    pub create_date: Option<DateTime>,
    #[serde(rename_deserialize="effectiveDate", rename_serialize="EffectiveDate")]
    pub effective_date: Option<DateTime>,
    #[serde(rename_deserialize="modificationResultSet", rename_serialize="ModificationResultSet")]
    pub modification_results: Option<ReservedInstancesModificationResultList>,
    #[serde(rename_deserialize="reservedInstancesSet", rename_serialize="ReservedInstancesSet")]
    pub reserved_instances_ids: Option<ReservedIntancesIds>,
    #[serde(rename_deserialize="reservedInstancesModificationId", rename_serialize="ReservedInstancesModificationId")]
    pub reserved_instances_modification_id: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<String>,
    #[serde(rename_deserialize="statusMessage", rename_serialize="StatusMessage")]
    pub status_message: Option<String>,
    #[serde(rename_deserialize="updateDate", rename_serialize="UpdateDate")]
    pub update_date: Option<DateTime>,
}


