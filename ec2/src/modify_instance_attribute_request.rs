
use ::attribute_boolean_value::*;
use ::attribute_value::*;
use ::blob_attribute_value::*;
use ::group_id_string_list::*;
use ::instance_attribute_name::*;
use ::instance_block_device_mapping_specification_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyInstanceAttributeRequest {

    #[serde(rename_deserialize="attribute", rename_serialize="Attribute")]
    pub attribute: Option<InstanceAttributeName>,
    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<InstanceBlockDeviceMappingSpecificationList>,
    #[serde(rename_deserialize="disableApiTermination", rename_serialize="DisableApiTermination")]
    pub disable_api_termination: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ebsOptimized", rename_serialize="EbsOptimized")]
    pub ebs_optimized: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="GroupId", rename_serialize="GroupId")]
    pub groups: Option<GroupIdStringList>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: String,
    #[serde(rename_deserialize="instanceInitiatedShutdownBehavior", rename_serialize="InstanceInitiatedShutdownBehavior")]
    pub instance_initiated_shutdown_behavior: Option<AttributeValue>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<AttributeValue>,
    #[serde(rename_deserialize="kernel", rename_serialize="Kernel")]
    pub kernel: Option<AttributeValue>,
    #[serde(rename_deserialize="ramdisk", rename_serialize="Ramdisk")]
    pub ramdisk: Option<AttributeValue>,
    #[serde(rename_deserialize="SourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<AttributeValue>,
    #[serde(rename_deserialize="userData", rename_serialize="UserData")]
    pub user_data: Option<BlobAttributeValue>,
    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: Option<String>,
}


