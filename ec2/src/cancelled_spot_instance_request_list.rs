
use awaze_core::ItemVec;
use ::cancelled_spot_instance_request::*;



pub type CancelledSpotInstanceRequestList = ItemVec<CancelledSpotInstanceRequest>;
