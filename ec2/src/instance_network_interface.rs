
use ::group_identifier_list::*;
use ::instance_network_interface_association::*;
use ::instance_network_interface_attachment::*;
use ::instance_private_ip_address_list::*;
use ::network_interface_status::*;


/// <p>Describes a network interface.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceNetworkInterface {

    #[serde(rename_deserialize="association", rename_serialize="Association")]
    pub association: Option<InstanceNetworkInterfaceAssociation>,
    #[serde(rename_deserialize="attachment", rename_serialize="Attachment")]
    pub attachment: Option<InstanceNetworkInterfaceAttachment>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="macAddress", rename_serialize="MacAddress")]
    pub mac_address: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="privateDnsName", rename_serialize="PrivateDnsName")]
    pub private_dns_name: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="privateIpAddressesSet", rename_serialize="PrivateIpAddressesSet")]
    pub private_ip_addresses: Option<InstancePrivateIpAddressList>,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<bool>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<NetworkInterfaceStatus>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


