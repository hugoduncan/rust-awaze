



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpcPeeringConnectionRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="peerOwnerId", rename_serialize="PeerOwnerId")]
    pub peer_owner_id: Option<String>,
    #[serde(rename_deserialize="peerVpcId", rename_serialize="PeerVpcId")]
    pub peer_vpc_id: Option<String>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


