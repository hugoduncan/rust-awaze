
use ::resource_id_list::*;
use ::tag_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateTagsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ResourceId", rename_serialize="ResourceId")]
    pub resources: ResourceIdList,
    #[serde(rename_deserialize="Tag", rename_serialize="Tag")]
    pub tags: TagList,
}


