
use ::icmp_type_code::*;
use ::port_range::*;
use ::rule_action::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateNetworkAclEntryRequest {

    #[serde(rename_deserialize="cidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="egress", rename_serialize="Egress")]
    pub egress: bool,
    #[serde(rename_deserialize="Icmp", rename_serialize="Icmp")]
    pub icmp_type_code: Option<IcmpTypeCode>,
    #[serde(rename_deserialize="networkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_id: String,
    #[serde(rename_deserialize="portRange", rename_serialize="PortRange")]
    pub port_range: Option<PortRange>,
    #[serde(rename_deserialize="protocol", rename_serialize="Protocol")]
    pub protocol: String,
    #[serde(rename_deserialize="ruleAction", rename_serialize="RuleAction")]
    pub rule_action: RuleAction,
    #[serde(rename_deserialize="ruleNumber", rename_serialize="RuleNumber")]
    pub rule_number: i32,
}


