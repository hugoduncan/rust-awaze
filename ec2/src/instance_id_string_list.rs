
use awaze_core::ItemVec;



pub type InstanceIdStringList = ItemVec<String>;
