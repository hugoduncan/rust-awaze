
use awaze_core::ItemVec;
use ::volume_status_action::*;



pub type VolumeStatusActionsList = ItemVec<VolumeStatusAction>;
