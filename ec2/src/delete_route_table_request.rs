



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteRouteTableRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: String,
}


