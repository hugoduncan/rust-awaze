
use ::bundle_task_error::*;
use ::bundle_task_state::*;
use ::date_time::*;
use ::storage::*;


/// <p>Describes a bundle task.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct BundleTask {

    #[serde(rename_deserialize="bundleId", rename_serialize="BundleId")]
    pub bundle_id: Option<String>,
    #[serde(rename_deserialize="error", rename_serialize="Error")]
    pub bundle_task_error: Option<BundleTaskError>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="progress", rename_serialize="Progress")]
    pub progress: Option<String>,
    #[serde(rename_deserialize="startTime", rename_serialize="StartTime")]
    pub start_time: Option<DateTime>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<BundleTaskState>,
    #[serde(rename_deserialize="storage", rename_serialize="Storage")]
    pub storage: Option<Storage>,
    #[serde(rename_deserialize="updateTime", rename_serialize="UpdateTime")]
    pub update_time: Option<DateTime>,
}


