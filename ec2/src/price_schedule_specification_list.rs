
use awaze_core::ItemVec;
use ::price_schedule_specification::*;



pub type PriceScheduleSpecificationList = ItemVec<PriceScheduleSpecification>;
