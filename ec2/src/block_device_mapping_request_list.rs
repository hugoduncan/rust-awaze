
use awaze_core::ItemVec;
use ::block_device_mapping::*;



pub type BlockDeviceMappingRequestList = ItemVec<BlockDeviceMapping>;
