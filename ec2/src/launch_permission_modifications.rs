
use ::launch_permission_list::*;


/// <p>Describes a launch permission modification.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct LaunchPermissionModifications {

    #[serde(rename_deserialize="Add", rename_serialize="Add")]
    pub add: Option<LaunchPermissionList>,
    #[serde(rename_deserialize="Remove", rename_serialize="Remove")]
    pub remove: Option<LaunchPermissionList>,
}


