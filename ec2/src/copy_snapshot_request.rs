



#[derive(Debug, Serialize, Deserialize)]
pub struct CopySnapshotRequest {

    #[serde(rename_deserialize="Description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="destinationRegion", rename_serialize="DestinationRegion")]
    pub destination_region: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="presignedUrl", rename_serialize="PresignedUrl")]
    pub presigned_url: Option<String>,
    #[serde(rename_deserialize="SourceRegion", rename_serialize="SourceRegion")]
    pub source_region: String,
    #[serde(rename_deserialize="SourceSnapshotId", rename_serialize="SourceSnapshotId")]
    pub source_snapshot_id: String,
}


