



#[derive(Debug, Clone)]
pub enum SnapshotState {
   Pending,Completed,Error,
}

string_enum!{ SnapshotState,
    SnapshotState::Pending => "pending" => SnapshotState::Pending,
    SnapshotState::Completed => "completed" => SnapshotState::Completed,
    SnapshotState::Error => "error" => SnapshotState::Error

}