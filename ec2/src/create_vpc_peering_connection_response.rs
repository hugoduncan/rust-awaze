
use ::vpc_peering_connection::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpcPeeringConnectionResponse {

    #[serde(rename_deserialize="vpcPeeringConnection", rename_serialize="VpcPeeringConnection")]
    pub vpc_peering_connection: Option<VpcPeeringConnection>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


