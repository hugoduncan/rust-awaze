
use awaze_core::ItemVec;
use ::instance_private_ip_address::*;



pub type InstancePrivateIpAddressList = ItemVec<InstancePrivateIpAddress>;
