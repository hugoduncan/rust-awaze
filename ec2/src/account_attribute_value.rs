


/// <p>Describes a value of an account attribute.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct AccountAttributeValue {

    #[serde(rename_deserialize="attributeValue", rename_serialize="AttributeValue")]
    pub attribute_value: Option<String>,
}


