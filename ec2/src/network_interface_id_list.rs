
use awaze_core::ItemVec;



pub type NetworkInterfaceIdList = ItemVec<String>;
