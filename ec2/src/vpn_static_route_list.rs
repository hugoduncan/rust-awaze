
use awaze_core::ItemVec;
use ::vpn_static_route::*;



pub type VpnStaticRouteList = ItemVec<VpnStaticRoute>;
