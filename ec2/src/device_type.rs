



#[derive(Debug, Clone)]
pub enum DeviceType {
   Ebs,InstanceStore,
}

string_enum!{ DeviceType,
    DeviceType::Ebs => "ebs" => DeviceType::Ebs,
    DeviceType::InstanceStore => "instance-store" => DeviceType::InstanceStore

}