
use ::import_instance_volume_detail_set::*;
use ::platform_values::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportInstanceTaskDetails {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="platform", rename_serialize="Platform")]
    pub platform: Option<PlatformValues>,
    #[serde(rename_deserialize="volumes", rename_serialize="Volumes")]
    pub volumes: ImportInstanceVolumeDetailSet,
}


