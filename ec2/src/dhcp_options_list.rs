
use awaze_core::ItemVec;
use ::dhcp_options::*;



pub type DhcpOptionsList = ItemVec<DhcpOptions>;
