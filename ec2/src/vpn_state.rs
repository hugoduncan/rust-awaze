



#[derive(Debug, Clone)]
pub enum VpnState {
   Pending,Available,Deleting,Deleted,
}

string_enum!{ VpnState,
    VpnState::Pending => "pending" => VpnState::Pending,
    VpnState::Available => "available" => VpnState::Available,
    VpnState::Deleting => "deleting" => VpnState::Deleting,
    VpnState::Deleted => "deleted" => VpnState::Deleted

}