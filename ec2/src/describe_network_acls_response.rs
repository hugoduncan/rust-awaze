
use ::network_acl_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkAclsResponse {

    #[serde(rename_deserialize="networkAclSet", rename_serialize="NetworkAclSet")]
    pub network_acls: Option<NetworkAclList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


