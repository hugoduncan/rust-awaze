
use ::ip_permission_list::*;
use ::tag_list::*;


/// <p>Describes a security group</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SecurityGroup {

    #[serde(rename_deserialize="groupDescription", rename_serialize="GroupDescription")]
    pub description: Option<String>,
    #[serde(rename_deserialize="groupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
    #[serde(rename_deserialize="ipPermissions", rename_serialize="IpPermissions")]
    pub ip_permissions: Option<IpPermissionList>,
    #[serde(rename_deserialize="ipPermissionsEgress", rename_serialize="IpPermissionsEgress")]
    pub ip_permissions_egress: Option<IpPermissionList>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


