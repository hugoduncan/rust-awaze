



#[derive(Debug, Clone)]
pub enum ArchitectureValues {
   I386,X8664,
}

string_enum!{ ArchitectureValues,
    ArchitectureValues::I386 => "i386" => ArchitectureValues::I386,
    ArchitectureValues::X8664 => "x86_64" => ArchitectureValues::X8664

}