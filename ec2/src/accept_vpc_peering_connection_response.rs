
use ::vpc_peering_connection::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AcceptVpcPeeringConnectionResponse {

    #[serde(rename_deserialize="vpcPeeringConnection", rename_serialize="VpcPeeringConnection")]
    pub vpc_peering_connection: Option<VpcPeeringConnection>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


