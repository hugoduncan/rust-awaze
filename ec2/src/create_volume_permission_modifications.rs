
use ::create_volume_permission_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVolumePermissionModifications {

    #[serde(rename_deserialize="Add", rename_serialize="Add")]
    pub add: Option<CreateVolumePermissionList>,
    #[serde(rename_deserialize="Remove", rename_serialize="Remove")]
    pub remove: Option<CreateVolumePermissionList>,
}


