



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteVpnConnectionRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VpnConnectionId", rename_serialize="VpnConnectionId")]
    pub vpn_connection_id: String,
}


