



#[derive(Debug, Clone)]
pub enum RouteOrigin {
   CreateRouteTable,CreateRoute,EnableVgwRoutePropagation,
}

string_enum!{ RouteOrigin,
    RouteOrigin::CreateRouteTable => "CreateRouteTable" => RouteOrigin::CreateRouteTable,
    RouteOrigin::CreateRoute => "CreateRoute" => RouteOrigin::CreateRoute,
    RouteOrigin::EnableVgwRoutePropagation => "EnableVgwRoutePropagation" => RouteOrigin::EnableVgwRoutePropagation

}