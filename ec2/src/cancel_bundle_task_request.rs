



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelBundleTaskRequest {

    #[serde(rename_deserialize="BundleId", rename_serialize="BundleId")]
    pub bundle_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


