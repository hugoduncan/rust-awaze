



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpnConnectionRouteRequest {

    #[serde(rename_deserialize="DestinationCidrBlock", rename_serialize="DestinationCidrBlock")]
    pub destination_cidr_block: String,
    #[serde(rename_deserialize="VpnConnectionId", rename_serialize="VpnConnectionId")]
    pub vpn_connection_id: String,
}


