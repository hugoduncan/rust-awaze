
use awaze_core::ItemVec;
use ::ip_permission::*;



pub type IpPermissionList = ItemVec<IpPermission>;
