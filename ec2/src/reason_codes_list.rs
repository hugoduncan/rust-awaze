
use awaze_core::ItemVec;
use ::report_instance_reason_codes::*;



pub type ReasonCodesList = ItemVec<ReportInstanceReasonCodes>;
