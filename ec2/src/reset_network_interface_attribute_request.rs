



#[derive(Debug, Serialize, Deserialize)]
pub struct ResetNetworkInterfaceAttributeRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
    #[serde(rename_deserialize="sourceDestCheck", rename_serialize="SourceDestCheck")]
    pub source_dest_check: Option<String>,
}


