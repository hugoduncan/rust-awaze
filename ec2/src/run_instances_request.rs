
use ::block_device_mapping_request_list::*;
use ::iam_instance_profile_specification::*;
use ::instance_network_interface_specification_list::*;
use ::instance_type::*;
use ::placement::*;
use ::run_instances_monitoring_enabled::*;
use ::security_group_id_string_list::*;
use ::security_group_string_list::*;
use ::shutdown_behavior::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct RunInstancesRequest {

    #[serde(rename_deserialize="additionalInfo", rename_serialize="AdditionalInfo")]
    pub additional_info: Option<String>,
    #[serde(rename_deserialize="BlockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingRequestList>,
    #[serde(rename_deserialize="clientToken", rename_serialize="ClientToken")]
    pub client_token: Option<String>,
    #[serde(rename_deserialize="disableApiTermination", rename_serialize="DisableApiTermination")]
    pub disable_api_termination: Option<bool>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ebsOptimized", rename_serialize="EbsOptimized")]
    pub ebs_optimized: Option<bool>,
    #[serde(rename_deserialize="iamInstanceProfile", rename_serialize="IamInstanceProfile")]
    pub iam_instance_profile: Option<IamInstanceProfileSpecification>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_id: String,
    #[serde(rename_deserialize="instanceInitiatedShutdownBehavior", rename_serialize="InstanceInitiatedShutdownBehavior")]
    pub instance_initiated_shutdown_behavior: Option<ShutdownBehavior>,
    #[serde(rename_deserialize="InstanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="KernelId", rename_serialize="KernelId")]
    pub kernel_id: Option<String>,
    #[serde(rename_deserialize="KeyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
    #[serde(rename_deserialize="MaxCount", rename_serialize="MaxCount")]
    pub max_count: i32,
    #[serde(rename_deserialize="MinCount", rename_serialize="MinCount")]
    pub min_count: i32,
    #[serde(rename_deserialize="Monitoring", rename_serialize="Monitoring")]
    pub monitoring: Option<RunInstancesMonitoringEnabled>,
    #[serde(rename_deserialize="networkInterface", rename_serialize="NetworkInterface")]
    pub network_interfaces: Option<InstanceNetworkInterfaceSpecificationList>,
    #[serde(rename_deserialize="Placement", rename_serialize="Placement")]
    pub placement: Option<Placement>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="RamdiskId", rename_serialize="RamdiskId")]
    pub ramdisk_id: Option<String>,
    #[serde(rename_deserialize="SecurityGroupId", rename_serialize="SecurityGroupId")]
    pub security_group_ids: Option<SecurityGroupIdStringList>,
    #[serde(rename_deserialize="SecurityGroup", rename_serialize="SecurityGroup")]
    pub security_groups: Option<SecurityGroupStringList>,
    #[serde(rename_deserialize="SubnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="UserData", rename_serialize="UserData")]
    pub user_data: Option<String>,
}


