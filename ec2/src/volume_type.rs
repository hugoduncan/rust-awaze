



#[derive(Debug, Clone)]
pub enum VolumeType {
   Standard,Io1,Gp2,
}

string_enum!{ VolumeType,
    VolumeType::Standard => "standard" => VolumeType::Standard,
    VolumeType::Io1 => "io1" => VolumeType::Io1,
    VolumeType::Gp2 => "gp2" => VolumeType::Gp2

}