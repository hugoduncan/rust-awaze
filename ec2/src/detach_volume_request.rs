



#[derive(Debug, Serialize, Deserialize)]
pub struct DetachVolumeRequest {

    #[serde(rename_deserialize="Device", rename_serialize="Device")]
    pub device: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Force", rename_serialize="Force")]
    pub force: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="VolumeId", rename_serialize="VolumeId")]
    pub volume_id: String,
}


