


/// <p>Describes a key pair.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct KeyPairInfo {

    #[serde(rename_deserialize="keyFingerprint", rename_serialize="KeyFingerprint")]
    pub key_fingerprint: Option<String>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
}


