



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteCustomerGatewayRequest {

    #[serde(rename_deserialize="CustomerGatewayId", rename_serialize="CustomerGatewayId")]
    pub customer_gateway_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


