
use awaze_core::ItemVec;
use ::import_instance_volume_detail_item::*;



pub type ImportInstanceVolumeDetailSet = ItemVec<ImportInstanceVolumeDetailItem>;
