
use ::cancelled_spot_instance_request_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelSpotInstanceRequestsResponse {

    #[serde(rename_deserialize="spotInstanceRequestSet", rename_serialize="SpotInstanceRequestSet")]
    pub cancelled_spot_instance_requests: Option<CancelledSpotInstanceRequestList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


