



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteVpcPeeringConnectionResponse {

    #[serde(rename_deserialize="return", rename_serialize="Return")]
    pub return_value: Option<bool>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


