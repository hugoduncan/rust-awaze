



#[derive(Debug, Clone)]
pub enum VolumeStatusName {
   IoEnabled,IoPerformance,
}

string_enum!{ VolumeStatusName,
    VolumeStatusName::IoEnabled => "io-enabled" => VolumeStatusName::IoEnabled,
    VolumeStatusName::IoPerformance => "io-performance" => VolumeStatusName::IoPerformance

}