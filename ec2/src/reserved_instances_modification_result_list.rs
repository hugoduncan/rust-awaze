
use awaze_core::ItemVec;
use ::reserved_instances_modification_response::*;



pub type ReservedInstancesModificationResultList = ItemVec<ReservedInstancesModificationResponse>;
