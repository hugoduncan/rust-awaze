
use awaze_core::ItemVec;
use ::internet_gateway::*;



pub type InternetGatewayList = ItemVec<InternetGateway>;
