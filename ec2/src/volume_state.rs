



#[derive(Debug, Clone)]
pub enum VolumeState {
   Creating,Available,InUse,Deleting,Deleted,Error,
}

string_enum!{ VolumeState,
    VolumeState::Creating => "creating" => VolumeState::Creating,
    VolumeState::Available => "available" => VolumeState::Available,
    VolumeState::InUse => "in-use" => VolumeState::InUse,
    VolumeState::Deleting => "deleting" => VolumeState::Deleting,
    VolumeState::Deleted => "deleted" => VolumeState::Deleted,
    VolumeState::Error => "error" => VolumeState::Error

}