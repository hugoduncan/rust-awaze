


/// <p>Describes an Amazon EBS volume.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeDetail {

    #[serde(rename_deserialize="size", rename_serialize="Size")]
    pub size: i64,
}


