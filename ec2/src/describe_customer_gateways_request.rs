
use ::customer_gateway_id_string_list::*;
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeCustomerGatewaysRequest {

    #[serde(rename_deserialize="CustomerGatewayId", rename_serialize="CustomerGatewayId")]
    pub customer_gateway_ids: Option<CustomerGatewayIdStringList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
}


