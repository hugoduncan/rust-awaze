



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteSpotDatafeedSubscriptionRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
}


