



#[derive(Debug, Clone)]
pub enum HypervisorType {
   Ovm,Xen,
}

string_enum!{ HypervisorType,
    HypervisorType::Ovm => "ovm" => HypervisorType::Ovm,
    HypervisorType::Xen => "xen" => HypervisorType::Xen

}