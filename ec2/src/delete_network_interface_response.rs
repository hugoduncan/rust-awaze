



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteNetworkInterfaceResponse {

    #[serde(rename_deserialize="return", rename_serialize="Return")]
    pub result: bool,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


