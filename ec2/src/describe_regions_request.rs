
use ::filter_list::*;
use ::region_name_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeRegionsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="RegionName", rename_serialize="RegionName")]
    pub region_names: Option<RegionNameStringList>,
}


