
use ::date_time::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct GetPasswordDataResponse {

    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="passwordData", rename_serialize="PasswordData")]
    pub password_data: Option<String>,
    #[serde(rename_deserialize="timestamp", rename_serialize="Timestamp")]
    pub timestamp: Option<DateTime>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


