
use ::instance_status_details_list::*;
use ::summary_status::*;


/// <p>Describes the status of an instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceStatusSummary {

    #[serde(rename_deserialize="details", rename_serialize="Details")]
    pub details: Option<InstanceStatusDetailsList>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<SummaryStatus>,
}


