
use ::private_ip_address_specification_list::*;
use ::security_group_id_string_list::*;


/// <p>Describes a network interface.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceNetworkInterfaceSpecification {

    #[serde(rename_deserialize="associatePublicIpAddress", rename_serialize="AssociatePublicIpAddress")]
    pub associate_public_ip_address: Option<bool>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="deviceIndex", rename_serialize="DeviceIndex")]
    pub device_index: Option<i32>,
    #[serde(rename_deserialize="SecurityGroupId", rename_serialize="SecurityGroupId")]
    pub groups: Option<SecurityGroupIdStringList>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="privateIpAddressesSet", rename_serialize="PrivateIpAddresses")]
    pub private_ip_addresses: Option<PrivateIpAddressSpecificationList>,
    #[serde(rename_deserialize="secondaryPrivateIpAddressCount", rename_serialize="SecondaryPrivateIpAddressCount")]
    pub secondary_private_ip_address_count: Option<i32>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
}


