
use awaze_core::ItemVec;



pub type ExecutableByStringList = ItemVec<String>;
