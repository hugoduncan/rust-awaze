
use ::subnet_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSubnetsResponse {

    #[serde(rename_deserialize="subnetSet", rename_serialize="SubnetSet")]
    pub subnets: SubnetList,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


