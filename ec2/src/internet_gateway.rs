
use ::internet_gateway_attachment_list::*;
use ::tag_list::*;


/// <p>Describes an Internet gateway.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InternetGateway {

    #[serde(rename_deserialize="attachmentSet", rename_serialize="AttachmentSet")]
    pub attachments: Option<InternetGatewayAttachmentList>,
    #[serde(rename_deserialize="internetGatewayId", rename_serialize="InternetGatewayId")]
    pub internet_gateway_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
}


