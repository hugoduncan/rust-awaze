
use ::date_time::*;


/// <p>Describes the status of a Spot Instance request.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotInstanceStatus {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<String>,
    #[serde(rename_deserialize="message", rename_serialize="Message")]
    pub message: Option<String>,
    #[serde(rename_deserialize="updateTime", rename_serialize="UpdateTime")]
    pub update_time: Option<DateTime>,
}


