
use awaze_core::ItemVec;
use ::reserved_instances_offering::*;



pub type ReservedInstancesOfferingList = ItemVec<ReservedInstancesOffering>;
