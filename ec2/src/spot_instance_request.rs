
use ::date_time::*;
use ::launch_specification::*;
use ::ri_product_description::*;
use ::spot_instance_state::*;
use ::spot_instance_state_fault::*;
use ::spot_instance_status::*;
use ::spot_instance_type::*;
use ::tag_list::*;


/// <p>Describe a Spot Instance request.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct SpotInstanceRequest {

    #[serde(rename_deserialize="availabilityZoneGroup", rename_serialize="AvailabilityZoneGroup")]
    pub availability_zone_group: Option<String>,
    #[serde(rename_deserialize="createTime", rename_serialize="CreateTime")]
    pub create_time: Option<DateTime>,
    #[serde(rename_deserialize="fault", rename_serialize="Fault")]
    pub fault: Option<SpotInstanceStateFault>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="launchGroup", rename_serialize="LaunchGroup")]
    pub launch_group: Option<String>,
    #[serde(rename_deserialize="launchSpecification", rename_serialize="LaunchSpecification")]
    pub launch_specification: Option<LaunchSpecification>,
    #[serde(rename_deserialize="launchedAvailabilityZone", rename_serialize="LaunchedAvailabilityZone")]
    pub launched_availability_zone: Option<String>,
    #[serde(rename_deserialize="productDescription", rename_serialize="ProductDescription")]
    pub product_description: Option<RIProductDescription>,
    #[serde(rename_deserialize="spotInstanceRequestId", rename_serialize="SpotInstanceRequestId")]
    pub spot_instance_request_id: Option<String>,
    #[serde(rename_deserialize="spotPrice", rename_serialize="SpotPrice")]
    pub spot_price: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<SpotInstanceState>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<SpotInstanceStatus>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub spot_type: Option<SpotInstanceType>,
    #[serde(rename_deserialize="validFrom", rename_serialize="ValidFrom")]
    pub valid_from: Option<DateTime>,
    #[serde(rename_deserialize="validUntil", rename_serialize="ValidUntil")]
    pub valid_until: Option<DateTime>,
}


