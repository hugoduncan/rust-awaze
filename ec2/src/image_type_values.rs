



#[derive(Debug, Clone)]
pub enum ImageTypeValues {
   Machine,Kernel,Ramdisk,
}

string_enum!{ ImageTypeValues,
    ImageTypeValues::Machine => "machine" => ImageTypeValues::Machine,
    ImageTypeValues::Kernel => "kernel" => ImageTypeValues::Kernel,
    ImageTypeValues::Ramdisk => "ramdisk" => ImageTypeValues::Ramdisk

}