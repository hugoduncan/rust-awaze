
use ::date_time::*;


/// <p>Describes a volume status event.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeStatusEvent {

    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="eventId", rename_serialize="EventId")]
    pub event_id: Option<String>,
    #[serde(rename_deserialize="eventType", rename_serialize="EventType")]
    pub event_type: Option<String>,
    #[serde(rename_deserialize="notAfter", rename_serialize="NotAfter")]
    pub not_after: Option<DateTime>,
    #[serde(rename_deserialize="notBefore", rename_serialize="NotBefore")]
    pub not_before: Option<DateTime>,
}


