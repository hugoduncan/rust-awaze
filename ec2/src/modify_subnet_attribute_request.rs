
use ::attribute_boolean_value::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifySubnetAttributeRequest {

    #[serde(rename_deserialize="MapPublicIpOnLaunch", rename_serialize="MapPublicIpOnLaunch")]
    pub map_public_ip_on_launch: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: String,
}


