
use ::currency_code_values::*;
use ::instance_type::*;
use ::offering_type_values::*;
use ::pricing_details_list::*;
use ::recurring_charges_list::*;
use ::ri_product_description::*;
use ::tenancy::*;


/// <p>Describes a Reserved Instance offering.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesOffering {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="currencyCode", rename_serialize="CurrencyCode")]
    pub currency_code: Option<CurrencyCodeValues>,
    #[serde(rename_deserialize="duration", rename_serialize="Duration")]
    pub duration: Option<i64>,
    #[serde(rename_deserialize="fixedPrice", rename_serialize="FixedPrice")]
    pub fixed_price: Option<f32>,
    #[serde(rename_deserialize="instanceTenancy", rename_serialize="InstanceTenancy")]
    pub instance_tenancy: Option<Tenancy>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="marketplace", rename_serialize="Marketplace")]
    pub marketplace: Option<bool>,
    #[serde(rename_deserialize="offeringType", rename_serialize="OfferingType")]
    pub offering_type: Option<OfferingTypeValues>,
    #[serde(rename_deserialize="pricingDetailsSet", rename_serialize="PricingDetailsSet")]
    pub pricing_details: Option<PricingDetailsList>,
    #[serde(rename_deserialize="productDescription", rename_serialize="ProductDescription")]
    pub product_description: Option<RIProductDescription>,
    #[serde(rename_deserialize="recurringCharges", rename_serialize="RecurringCharges")]
    pub recurring_charges: Option<RecurringChargesList>,
    #[serde(rename_deserialize="reservedInstancesOfferingId", rename_serialize="ReservedInstancesOfferingId")]
    pub reserved_instances_offering_id: Option<String>,
    #[serde(rename_deserialize="usagePrice", rename_serialize="UsagePrice")]
    pub usage_price: Option<f32>,
}


