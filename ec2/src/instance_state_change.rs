
use ::instance_state::*;


/// <p>Describes an instance state change.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceStateChange {

    #[serde(rename_deserialize="currentState", rename_serialize="CurrentState")]
    pub current_state: Option<InstanceState>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="previousState", rename_serialize="PreviousState")]
    pub previous_state: Option<InstanceState>,
}


