
use ::accept_vpc_peering_connection_request::*;
use ::accept_vpc_peering_connection_response::*;
use ::allocate_address_request::*;
use ::allocate_address_response::*;
use ::assign_private_ip_addresses_request::*;
use ::assign_private_ip_addresses_response::*;
use ::associate_address_request::*;
use ::associate_address_response::*;
use ::associate_dhcp_options_request::*;
use ::associate_dhcp_options_response::*;
use ::associate_route_table_request::*;
use ::associate_route_table_response::*;
use ::attach_classic_link_vpc_request::*;
use ::attach_classic_link_vpc_response::*;
use ::attach_internet_gateway_request::*;
use ::attach_internet_gateway_response::*;
use ::attach_network_interface_request::*;
use ::attach_network_interface_response::*;
use ::attach_volume_request::*;
use ::attach_vpn_gateway_request::*;
use ::attach_vpn_gateway_response::*;
use ::authorize_security_group_egress_request::*;
use ::authorize_security_group_egress_response::*;
use ::authorize_security_group_ingress_request::*;
use ::authorize_security_group_ingress_response::*;
use ::bundle_instance_request::*;
use ::bundle_instance_response::*;
use ::cancel_bundle_task_request::*;
use ::cancel_bundle_task_response::*;
use ::cancel_conversion_request::*;
use ::cancel_conversion_response::*;
use ::cancel_export_task_request::*;
use ::cancel_export_task_response::*;
use ::cancel_reserved_instances_listing_request::*;
use ::cancel_reserved_instances_listing_response::*;
use ::cancel_spot_instance_requests_request::*;
use ::cancel_spot_instance_requests_response::*;
use ::confirm_product_instance_request::*;
use ::confirm_product_instance_response::*;
use ::copy_image_request::*;
use ::copy_image_response::*;
use ::copy_snapshot_request::*;
use ::copy_snapshot_response::*;
use ::create_customer_gateway_request::*;
use ::create_customer_gateway_response::*;
use ::create_dhcp_options_request::*;
use ::create_dhcp_options_response::*;
use ::create_image_request::*;
use ::create_image_response::*;
use ::create_instance_export_task_request::*;
use ::create_instance_export_task_response::*;
use ::create_internet_gateway_request::*;
use ::create_internet_gateway_response::*;
use ::create_key_pair_request::*;
use ::create_network_acl_entry_request::*;
use ::create_network_acl_entry_response::*;
use ::create_network_acl_request::*;
use ::create_network_acl_response::*;
use ::create_network_interface_request::*;
use ::create_network_interface_response::*;
use ::create_placement_group_request::*;
use ::create_placement_group_response::*;
use ::create_reserved_instances_listing_request::*;
use ::create_reserved_instances_listing_response::*;
use ::create_route_request::*;
use ::create_route_response::*;
use ::create_route_table_request::*;
use ::create_route_table_response::*;
use ::create_security_group_request::*;
use ::create_security_group_response::*;
use ::create_snapshot_request::*;
use ::create_spot_datafeed_subscription_request::*;
use ::create_spot_datafeed_subscription_response::*;
use ::create_subnet_request::*;
use ::create_subnet_response::*;
use ::create_tags_request::*;
use ::create_tags_response::*;
use ::create_volume_request::*;
use ::create_vpc_peering_connection_request::*;
use ::create_vpc_peering_connection_response::*;
use ::create_vpc_request::*;
use ::create_vpc_response::*;
use ::create_vpn_connection_request::*;
use ::create_vpn_connection_response::*;
use ::create_vpn_connection_route_request::*;
use ::create_vpn_connection_route_response::*;
use ::create_vpn_gateway_request::*;
use ::create_vpn_gateway_response::*;
use ::delete_customer_gateway_request::*;
use ::delete_customer_gateway_response::*;
use ::delete_dhcp_options_request::*;
use ::delete_dhcp_options_response::*;
use ::delete_internet_gateway_request::*;
use ::delete_internet_gateway_response::*;
use ::delete_key_pair_request::*;
use ::delete_key_pair_response::*;
use ::delete_network_acl_entry_request::*;
use ::delete_network_acl_entry_response::*;
use ::delete_network_acl_request::*;
use ::delete_network_acl_response::*;
use ::delete_network_interface_request::*;
use ::delete_network_interface_response::*;
use ::delete_placement_group_request::*;
use ::delete_placement_group_response::*;
use ::delete_route_request::*;
use ::delete_route_response::*;
use ::delete_route_table_request::*;
use ::delete_route_table_response::*;
use ::delete_security_group_request::*;
use ::delete_security_group_response::*;
use ::delete_snapshot_request::*;
use ::delete_snapshot_response::*;
use ::delete_spot_datafeed_subscription_request::*;
use ::delete_spot_datafeed_subscription_response::*;
use ::delete_subnet_request::*;
use ::delete_subnet_response::*;
use ::delete_tags_request::*;
use ::delete_tags_response::*;
use ::delete_volume_request::*;
use ::delete_volume_response::*;
use ::delete_vpc_peering_connection_request::*;
use ::delete_vpc_peering_connection_response::*;
use ::delete_vpc_request::*;
use ::delete_vpc_response::*;
use ::delete_vpn_connection_request::*;
use ::delete_vpn_connection_response::*;
use ::delete_vpn_connection_route_request::*;
use ::delete_vpn_connection_route_response::*;
use ::delete_vpn_gateway_request::*;
use ::delete_vpn_gateway_response::*;
use ::deregister_image_request::*;
use ::deregister_image_response::*;
use ::describe_account_attributes_request::*;
use ::describe_account_attributes_response::*;
use ::describe_addresses_request::*;
use ::describe_addresses_response::*;
use ::describe_availability_zones_request::*;
use ::describe_availability_zones_response::*;
use ::describe_bundle_tasks_request::*;
use ::describe_bundle_tasks_response::*;
use ::describe_classic_link_instances_request::*;
use ::describe_classic_link_instances_response::*;
use ::describe_conversion_tasks_request::*;
use ::describe_conversion_tasks_response::*;
use ::describe_customer_gateways_request::*;
use ::describe_customer_gateways_response::*;
use ::describe_dhcp_options_request::*;
use ::describe_dhcp_options_response::*;
use ::describe_export_tasks_request::*;
use ::describe_export_tasks_response::*;
use ::describe_image_attribute_request::*;
use ::describe_images_request::*;
use ::describe_images_response::*;
use ::describe_instance_attribute_request::*;
use ::describe_instance_status_request::*;
use ::describe_instance_status_response::*;
use ::describe_instances_request::*;
use ::describe_instances_response::*;
use ::describe_internet_gateways_request::*;
use ::describe_internet_gateways_response::*;
use ::describe_key_pairs_request::*;
use ::describe_key_pairs_response::*;
use ::describe_network_acls_request::*;
use ::describe_network_acls_response::*;
use ::describe_network_interface_attribute_request::*;
use ::describe_network_interface_attribute_response::*;
use ::describe_network_interfaces_request::*;
use ::describe_network_interfaces_response::*;
use ::describe_placement_groups_request::*;
use ::describe_placement_groups_response::*;
use ::describe_regions_request::*;
use ::describe_regions_response::*;
use ::describe_reserved_instances_listings_request::*;
use ::describe_reserved_instances_listings_response::*;
use ::describe_reserved_instances_modifications_request::*;
use ::describe_reserved_instances_modifications_response::*;
use ::describe_reserved_instances_offerings_request::*;
use ::describe_reserved_instances_offerings_response::*;
use ::describe_reserved_instances_request::*;
use ::describe_reserved_instances_response::*;
use ::describe_route_tables_request::*;
use ::describe_route_tables_response::*;
use ::describe_security_groups_request::*;
use ::describe_security_groups_response::*;
use ::describe_snapshot_attribute_request::*;
use ::describe_snapshot_attribute_response::*;
use ::describe_snapshots_request::*;
use ::describe_snapshots_response::*;
use ::describe_spot_datafeed_subscription_request::*;
use ::describe_spot_datafeed_subscription_response::*;
use ::describe_spot_instance_requests_request::*;
use ::describe_spot_instance_requests_response::*;
use ::describe_spot_price_history_request::*;
use ::describe_spot_price_history_response::*;
use ::describe_subnets_request::*;
use ::describe_subnets_response::*;
use ::describe_tags_request::*;
use ::describe_tags_response::*;
use ::describe_volume_attribute_request::*;
use ::describe_volume_attribute_response::*;
use ::describe_volume_status_request::*;
use ::describe_volume_status_response::*;
use ::describe_volumes_request::*;
use ::describe_volumes_response::*;
use ::describe_vpc_attribute_request::*;
use ::describe_vpc_attribute_response::*;
use ::describe_vpc_classic_link_request::*;
use ::describe_vpc_classic_link_response::*;
use ::describe_vpc_peering_connections_request::*;
use ::describe_vpc_peering_connections_response::*;
use ::describe_vpcs_request::*;
use ::describe_vpcs_response::*;
use ::describe_vpn_connections_request::*;
use ::describe_vpn_connections_response::*;
use ::describe_vpn_gateways_request::*;
use ::describe_vpn_gateways_response::*;
use ::detach_classic_link_vpc_request::*;
use ::detach_classic_link_vpc_response::*;
use ::detach_internet_gateway_request::*;
use ::detach_internet_gateway_response::*;
use ::detach_network_interface_request::*;
use ::detach_network_interface_response::*;
use ::detach_volume_request::*;
use ::detach_vpn_gateway_request::*;
use ::detach_vpn_gateway_response::*;
use ::disable_vgw_route_propagation_request::*;
use ::disable_vgw_route_propagation_response::*;
use ::disable_vpc_classic_link_request::*;
use ::disable_vpc_classic_link_response::*;
use ::disassociate_address_request::*;
use ::disassociate_address_response::*;
use ::disassociate_route_table_request::*;
use ::disassociate_route_table_response::*;
use ::enable_vgw_route_propagation_request::*;
use ::enable_vgw_route_propagation_response::*;
use ::enable_volume_io_request::*;
use ::enable_volume_io_response::*;
use ::enable_vpc_classic_link_request::*;
use ::enable_vpc_classic_link_response::*;
use ::get_console_output_request::*;
use ::get_console_output_response::*;
use ::get_password_data_request::*;
use ::get_password_data_response::*;
use ::image_attribute::*;
use ::import_instance_request::*;
use ::import_instance_response::*;
use ::import_key_pair_request::*;
use ::import_key_pair_response::*;
use ::import_volume_request::*;
use ::import_volume_response::*;
use ::instance_attribute::*;
use ::key_pair::*;
use ::modify_image_attribute_request::*;
use ::modify_image_attribute_response::*;
use ::modify_instance_attribute_request::*;
use ::modify_instance_attribute_response::*;
use ::modify_network_interface_attribute_request::*;
use ::modify_network_interface_attribute_response::*;
use ::modify_reserved_instances_request::*;
use ::modify_reserved_instances_response::*;
use ::modify_snapshot_attribute_request::*;
use ::modify_snapshot_attribute_response::*;
use ::modify_subnet_attribute_request::*;
use ::modify_subnet_attribute_response::*;
use ::modify_volume_attribute_request::*;
use ::modify_volume_attribute_response::*;
use ::modify_vpc_attribute_request::*;
use ::modify_vpc_attribute_response::*;
use ::monitor_instances_request::*;
use ::monitor_instances_response::*;
use ::purchase_reserved_instances_offering_request::*;
use ::purchase_reserved_instances_offering_response::*;
use ::reboot_instances_request::*;
use ::reboot_instances_response::*;
use ::register_image_request::*;
use ::register_image_response::*;
use ::reject_vpc_peering_connection_request::*;
use ::reject_vpc_peering_connection_response::*;
use ::release_address_request::*;
use ::release_address_response::*;
use ::replace_network_acl_association_request::*;
use ::replace_network_acl_association_response::*;
use ::replace_network_acl_entry_request::*;
use ::replace_network_acl_entry_response::*;
use ::replace_route_request::*;
use ::replace_route_response::*;
use ::replace_route_table_association_request::*;
use ::replace_route_table_association_response::*;
use ::report_instance_status_request::*;
use ::report_instance_status_response::*;
use ::request_spot_instances_request::*;
use ::request_spot_instances_response::*;
use ::reservation::*;
use ::reset_image_attribute_request::*;
use ::reset_image_attribute_response::*;
use ::reset_instance_attribute_request::*;
use ::reset_instance_attribute_response::*;
use ::reset_network_interface_attribute_request::*;
use ::reset_network_interface_attribute_response::*;
use ::reset_snapshot_attribute_request::*;
use ::reset_snapshot_attribute_response::*;
use ::revoke_security_group_egress_request::*;
use ::revoke_security_group_egress_response::*;
use ::revoke_security_group_ingress_request::*;
use ::revoke_security_group_ingress_response::*;
use ::run_instances_request::*;
use ::snapshot::*;
use ::start_instances_request::*;
use ::start_instances_response::*;
use ::stop_instances_request::*;
use ::stop_instances_response::*;
use ::terminate_instances_request::*;
use ::terminate_instances_response::*;
use ::unassign_private_ip_addresses_request::*;
use ::unassign_private_ip_addresses_response::*;
use ::unmonitor_instances_request::*;
use ::unmonitor_instances_response::*;
use ::volume::*;
use ::volume_attachment::*;
use awaze_core::*;
use hyper::method::Method;
use serde::Serialize;
use serde::xml::de;


/// <fullname>Amazon Elastic Compute Cloud</fullname> <p>Amazon Elastic Compute Cloud (Amazon EC2) provides resizable computing capacity in the Amazon Web Services (AWS) cloud. Using Amazon EC2 eliminates your need to invest in hardware up front, so you can develop and deploy applications faster.</p>
#[derive(Debug)]
pub struct Ec2 {
    /// AWS connection

    pub conn: Aws,
}


impl Ec2 {
  
/// Make a request for the service
pub fn request<I: Serialize + ::std::fmt::Debug>(&mut self,method: Method,uri: &str,action: &str,arg: &I,) -> Result<String, Error> {
    self.conn.request(method, "ec2", "2014-10-01", uri, action, arg)
}
/// <p>Accept a VPC peering connection request. To accept a request, the VPC peering connection must be in the <code>pending-acceptance</code> state, and you must be the owner of the peer VPC. Use the <code>DescribeVpcPeeringConnections</code> request to view your outstanding VPC peering connection requests.</p>
pub fn accept_vpc_peering_connection(&mut self,arg: &AcceptVpcPeeringConnectionRequest,) -> Result<AcceptVpcPeeringConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AcceptVpcPeeringConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Acquires an Elastic IP address.</p> <p>An Elastic IP address is for use either in the EC2-Classic platform or in a VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html">Elastic IP Addresses</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn allocate_address(&mut self,arg: &AllocateAddressRequest,) -> Result<AllocateAddressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AllocateAddress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Assigns one or more secondary private IP addresses to the specified network interface. You can specify one or more specific secondary IP addresses, or you can specify the number of secondary IP addresses to be automatically assigned within the subnet's CIDR block range. The number of secondary IP addresses that you can assign to an instance varies by instance type. For information about instance types, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html">Instance Types</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>. For more information about Elastic IP addresses, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html">Elastic IP Addresses</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>AssignPrivateIpAddresses is available only in EC2-VPC.</p>
pub fn assign_private_ip_addresses(&mut self,arg: &AssignPrivateIpAddressesRequest,) -> Result<AssignPrivateIpAddressesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AssignPrivateIpAddresses", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Associates an Elastic IP address with an instance or a network interface.</p> <p>An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html">Elastic IP Addresses</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>[EC2-Classic, VPC in an EC2-VPC-only account] If the Elastic IP address is already associated with a different instance, it is disassociated from that instance and associated with the specified instance.</p> <p>[VPC in an EC2-Classic account] If you don't specify a private IP address, the Elastic IP address is associated with the primary IP address. If the Elastic IP address is already associated with a different instance or a network interface, you get an error unless you allow reassociation.</p> <p>This is an idempotent operation. If you perform the operation more than once, Amazon EC2 doesn't return an error.</p>
pub fn associate_address(&mut self,arg: &AssociateAddressRequest,) -> Result<AssociateAddressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AssociateAddress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Associates a set of DHCP options (that you've previously created) with the specified VPC, or associates no DHCP options with the VPC.</p> <p>After you associate the options with the VPC, any existing instances and all new instances that you launch in that VPC use the options. You don't need to restart or relaunch the instances. They automatically pick up the changes within a few hours, depending on how frequently the instance renews its DHCP lease. You can explicitly renew the lease using the operating system on the instance.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_DHCP_Options.html">DHCP Options Sets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn associate_dhcp_options(&mut self,arg: &AssociateDhcpOptionsRequest,) -> Result<AssociateDhcpOptionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AssociateDhcpOptions", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Associates a subnet with a route table. The subnet and route table must be in the same VPC. This association causes traffic originating from the subnet to be routed according to the routes in the route table. The action returns an association ID, which you need in order to disassociate the route table from the subnet later. A route table can be associated with multiple subnets.</p> <p>For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn associate_route_table(&mut self,arg: &AssociateRouteTableRequest,) -> Result<AssociateRouteTableResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AssociateRouteTable", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Links an EC2-Classic instance to a ClassicLink-enabled VPC through one or more of the VPC's security groups. You cannot link an EC2-Classic instance to more than one VPC at a time. You can only link an instance that's in the <code>running</code> state. An instance is automatically unlinked from a VPC when it's stopped - you can link it to the VPC again when you restart it.</p> <p>After you've linked an instance, you cannot change the VPC security groups that are associated with it. To change the security groups, you must first unlink the instance, and then link it again. </p> <p>Linking your instance to a VPC is sometimes referred to as <i>attaching</i> your instance.</p>
pub fn attach_classic_link_vpc(&mut self,arg: &AttachClassicLinkVpcRequest,) -> Result<AttachClassicLinkVpcResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AttachClassicLinkVpc", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Attaches an Internet gateway to a VPC, enabling connectivity between the Internet and the VPC. For more information about your VPC and Internet gateway, see the <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/">Amazon Virtual Private Cloud User Guide</a>.</p>
pub fn attach_internet_gateway(&mut self,arg: &AttachInternetGatewayRequest,) -> Result<AttachInternetGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AttachInternetGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Attaches a network interface to an instance.</p>
pub fn attach_network_interface(&mut self,arg: &AttachNetworkInterfaceRequest,) -> Result<AttachNetworkInterfaceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AttachNetworkInterface", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Attaches an Amazon EBS volume to a running or stopped instance and exposes it to the instance with the specified device name.</p> <p>Encrypted Amazon EBS volumes may only be attached to instances that support Amazon EBS encryption. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html">Amazon EBS Encryption</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>For a list of supported device names, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-attaching-volume.html">Attaching an Amazon EBS Volume to an Instance</a>. Any device names that aren't reserved for instance store volumes can be used for Amazon EBS volumes. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html">Amazon EC2 Instance Store</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>If a volume has an AWS Marketplace product code:</p> <ul> <li>The volume can be attached only to a stopped instance.</li> <li>AWS Marketplace product codes are copied from the volume to the instance.</li> <li>You must be subscribed to the product.</li> <li>The instance type and operating system of the instance must support the product. For example, you can't detach a volume from a Windows instance and attach it to a Linux instance.</li> </ul> <p>For an overview of the AWS Marketplace, see <a href="https://aws.amazon.com/marketplace/help/200900000">Introducing AWS Marketplace</a>.</p> <p>For more information about Amazon EBS volumes, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-attaching-volume.html">Attaching Amazon EBS Volumes</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn attach_volume(&mut self,arg: &AttachVolumeRequest,) -> Result<VolumeAttachment, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AttachVolume", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Attaches a virtual private gateway to a VPC. For more information, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn attach_vpn_gateway(&mut self,arg: &AttachVpnGatewayRequest,) -> Result<AttachVpnGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AttachVpnGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Adds one or more egress rules to a security group for use with a VPC. Specifically, this action permits instances to send traffic to one or more destination CIDR IP address ranges, or to one or more destination security groups for the same VPC.</p> <important> <p>You can have up to 50 rules per security group (covering both ingress and egress rules).</p> </important> <p>A security group is for use with instances either in the EC2-Classic platform or in a specific VPC. This action doesn't apply to security groups for use in EC2-Classic. For more information, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_SecurityGroups.html">Security Groups for Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p> <p>Each rule consists of the protocol (for example, TCP), plus either a CIDR range or a source group. For the TCP and UDP protocols, you must also specify the destination port or port range. For the ICMP protocol, you must also specify the ICMP type and code. You can use -1 for the type or code to mean all types or all codes.</p> <p>Rule changes are propagated to affected instances as quickly as possible. However, a small delay might occur.</p>
pub fn authorize_security_group_egress(&mut self,arg: &AuthorizeSecurityGroupEgressRequest,) -> Result<AuthorizeSecurityGroupEgressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AuthorizeSecurityGroupEgress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Adds one or more ingress rules to a security group.</p> <important> <p>EC2-Classic: You can have up to 100 rules per group.</p> <p>EC2-VPC: You can have up to 50 rules per group (covering both ingress and egress rules).</p> </important> <p>Rule changes are propagated to instances within the security group as quickly as possible. However, a small delay might occur.</p> <p>[EC2-Classic] This action gives one or more CIDR IP address ranges permission to access a security group in your account, or gives one or more security groups (called the <i>source groups</i>) permission to access a security group for your account. A source group can be for your own AWS account, or another.</p> <p>[EC2-VPC] This action gives one or more CIDR IP address ranges permission to access a security group in your VPC, or gives one or more other security groups (called the <i>source groups</i>) permission to access a security group for your VPC. The security groups must all be for the same VPC.</p>
pub fn authorize_security_group_ingress(&mut self,arg: &AuthorizeSecurityGroupIngressRequest,) -> Result<AuthorizeSecurityGroupIngressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "AuthorizeSecurityGroupIngress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Bundles an Amazon instance store-backed Windows instance.</p> <p>During bundling, only the root device volume (C:\) is bundled. Data on other instance store volumes is not preserved.</p> <note> <p>This action is not applicable for Linux/Unix instances or Windows instances that are backed by Amazon EBS.</p> </note> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/Creating_InstanceStoreBacked_WinAMI.html">Creating an Instance Store-Backed Windows AMI</a>.</p>
pub fn bundle_instance(&mut self,arg: &BundleInstanceRequest,) -> Result<BundleInstanceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "BundleInstance", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Cancels a bundling operation for an instance store-backed Windows instance.</p>
pub fn cancel_bundle_task(&mut self,arg: &CancelBundleTaskRequest,) -> Result<CancelBundleTaskResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CancelBundleTask", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Cancels an active conversion task. The task can be the import of an instance or volume. The action removes all artifacts of the conversion, including a partially uploaded volume or instance. If the conversion is complete or is in the process of transferring the final disk image, the command fails and returns an exception.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/UploadingYourInstancesandVolumes.html">Using the Command Line Tools to Import Your Virtual Machine to Amazon EC2</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn cancel_conversion_task(&mut self,arg: &CancelConversionRequest,) -> Result<CancelConversionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CancelConversionTask", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Cancels an active export task. The request removes all artifacts of the export, including any partially-created Amazon S3 objects. If the export task is complete or is in the process of transferring the final disk image, the command fails and returns an error.</p>
pub fn cancel_export_task(&mut self,arg: &CancelExportTaskRequest,) -> Result<CancelExportTaskResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CancelExportTask", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Cancels the specified Reserved Instance listing in the Reserved Instance Marketplace.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-market-general.html">Reserved Instance Marketplace</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn cancel_reserved_instances_listing(&mut self,arg: &CancelReservedInstancesListingRequest,) -> Result<CancelReservedInstancesListingResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CancelReservedInstancesListing", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Cancels one or more Spot Instance requests. Spot Instances are instances that Amazon EC2 starts on your behalf when the bid price that you specify exceeds the current Spot Price. Amazon EC2 periodically sets the Spot Price based on available Spot Instance capacity and current Spot Instance requests. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-requests.html">Spot Instance Requests</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <important> <p>Canceling a Spot Instance request does not terminate running Spot Instances associated with the request.</p> </important>
pub fn cancel_spot_instance_requests(&mut self,arg: &CancelSpotInstanceRequestsRequest,) -> Result<CancelSpotInstanceRequestsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CancelSpotInstanceRequests", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Determines whether a product code is associated with an instance. This action can only be used by the owner of the product code. It is useful when a product code owner needs to verify whether another user's instance is eligible for support.</p>
pub fn confirm_product_instance(&mut self,arg: &ConfirmProductInstanceRequest,) -> Result<ConfirmProductInstanceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ConfirmProductInstance", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Initiates the copy of an AMI from the specified source region to the current region. You specify the destination region by using its endpoint when making the request. AMIs that use encrypted Amazon EBS snapshots cannot be copied with this method.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/CopyingAMIs.html">Copying AMIs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn copy_image(&mut self,arg: &CopyImageRequest,) -> Result<CopyImageResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CopyImage", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Copies a point-in-time snapshot of an Amazon EBS volume and stores it in Amazon S3. You can copy the snapshot within the same region or from one region to another. You can use the snapshot to create Amazon EBS volumes or Amazon Machine Images (AMIs). The snapshot is copied to the regional endpoint that you send the HTTP request to.</p> <p>Copies of encrypted Amazon EBS snapshots remain encrypted. Copies of unencrypted snapshots remain unencrypted.</p> <note> <p>Copying snapshots that were encrypted with non-default AWS Key Management Service (KMS) master keys is not supported at this time. </p> </note> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-copy-snapshot.html">Copying an Amazon EBS Snapshot</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn copy_snapshot(&mut self,arg: &CopySnapshotRequest,) -> Result<CopySnapshotResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CopySnapshot", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Provides information to AWS about your VPN customer gateway device. The customer gateway is the appliance at your end of the VPN connection. (The device on the AWS side of the VPN connection is the virtual private gateway.) You must provide the Internet-routable IP address of the customer gateway's external interface. The IP address must be static and can't be behind a device performing network address translation (NAT).</p> <p>For devices that use Border Gateway Protocol (BGP), you can also provide the device's BGP Autonomous System Number (ASN). You can use an existing ASN assigned to your network. If you don't have an ASN already, you can use a private ASN (in the 64512 - 65534 range).</p> <note> <p>Amazon EC2 supports all 2-byte ASN numbers in the range of 1 - 65534, with the exception of 7224, which is reserved in the <code>us-east-1</code> region, and 9059, which is reserved in the <code>eu-west-1</code> region.</p> </note> <p>For more information about VPN customer gateways, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_customer_gateway(&mut self,arg: &CreateCustomerGatewayRequest,) -> Result<CreateCustomerGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateCustomerGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a set of DHCP options for your VPC. After creating the set, you must associate it with the VPC, causing all existing and new instances that you launch in the VPC to use this set of DHCP options. The following are the individual DHCP options you can specify. For more information about the options, see <a href="http://www.ietf.org/rfc/rfc2132.txt">RFC 2132</a>.</p> <ul> <li> <code>domain-name-servers</code> - The IP addresses of up to four domain name servers, or <code>AmazonProvidedDNS</code>. The default DHCP option set specifies <code>AmazonProvidedDNS</code>. If specifying more than one domain name server, specify the IP addresses in a single parameter, separated by commas.</li> <li> <code>domain-name</code> - If you're using AmazonProvidedDNS in <code>us-east-1</code>, specify <code>ec2.internal</code>. If you're using AmazonProvidedDNS in another region, specify <code>region.compute.internal</code> (for example, <code>ap-northeast-1.compute.internal</code>). Otherwise, specify a domain name (for example, <code>MyCompany.com</code>). <b>Important</b>: Some Linux operating systems accept multiple domain names separated by spaces. However, Windows and other Linux operating systems treat the value as a single domain, which results in unexpected behavior. If your DHCP options set is associated with a VPC that has instances with multiple operating systems, specify only one domain name.</li> <li> <code>ntp-servers</code> - The IP addresses of up to four Network Time Protocol (NTP) servers.</li> <li> <code>netbios-name-servers</code> - The IP addresses of up to four NetBIOS name servers.</li> <li> <code>netbios-node-type</code> - The NetBIOS node type (1, 2, 4, or 8). We recommend that you specify 2 (broadcast and multicast are not currently supported). For more information about these node types, see <a href="http://www.ietf.org/rfc/rfc2132.txt">RFC 2132</a>. </li> </ul> <p>Your VPC automatically starts out with a set of DHCP options that includes only a DNS server that we provide (AmazonProvidedDNS). If you create a set of options, and if your VPC has an Internet gateway, make sure to set the <code>domain-name-servers</code> option either to <code>AmazonProvidedDNS</code> or to a domain name server of your choice. For more information about DHCP options, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_DHCP_Options.html">DHCP Options Sets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_dhcp_options(&mut self,arg: &CreateDhcpOptionsRequest,) -> Result<CreateDhcpOptionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateDhcpOptions", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an Amazon EBS-backed AMI from an Amazon EBS-backed instance that is either running or stopped.</p> <p>If you customized your instance with instance store volumes or EBS volumes in addition to the root device volume, the new AMI contains block device mapping information for those volumes. When you launch an instance from this new AMI, the instance automatically launches with those additional volumes.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami-ebs.html">Creating Amazon EBS-Backed Linux AMIs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_image(&mut self,arg: &CreateImageRequest,) -> Result<CreateImageResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateImage", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Exports a running or stopped instance to an Amazon S3 bucket.</p> <p>For information about the supported operating systems, image formats, and known limitations for the types of instances you can export, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ExportingEC2Instances.html">Exporting EC2 Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_instance_export_task(&mut self,arg: &CreateInstanceExportTaskRequest,) -> Result<CreateInstanceExportTaskResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateInstanceExportTask", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an Internet gateway for use with a VPC. After creating the Internet gateway, you attach it to a VPC using <a>AttachInternetGateway</a>.</p> <p>For more information about your VPC and Internet gateway, see the <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/">Amazon Virtual Private Cloud User Guide</a>.</p>
pub fn create_internet_gateway(&mut self,arg: &CreateInternetGatewayRequest,) -> Result<CreateInternetGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateInternetGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a 2048-bit RSA key pair with the specified name. Amazon EC2 stores the public key and displays the private key for you to save to a file. The private key is returned as an unencrypted PEM encoded PKCS#8 private key. If a key with the specified name already exists, Amazon EC2 returns an error.</p> <p>You can have up to five thousand key pairs per region.</p> <p>The key pair returned to you is available only in the region in which you create it. To create a key pair that is available in all regions, use <a>ImportKeyPair</a>.</p> <p>For more information about key pairs, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html">Key Pairs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_key_pair(&mut self,arg: &CreateKeyPairRequest,) -> Result<KeyPair, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateKeyPair", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a network ACL in a VPC. Network ACLs provide an optional layer of security (in addition to security groups) for the instances in your VPC.</p> <p>For more information about network ACLs, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html">Network ACLs</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_network_acl(&mut self,arg: &CreateNetworkAclRequest,) -> Result<CreateNetworkAclResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateNetworkAcl", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an entry (a rule) in a network ACL with the specified rule number. Each network ACL has a set of numbered ingress rules and a separate set of numbered egress rules. When determining whether a packet should be allowed in or out of a subnet associated with the ACL, we process the entries in the ACL according to the rule numbers, in ascending order. Each network ACL has a set of ingress rules and a separate set of egress rules.</p> <p>We recommend that you leave room between the rule numbers (for example, 100, 110, 120, ...), and not number them one right after the other (for example, 101, 102, 103, ...). This makes it easier to add a rule between existing ones without having to renumber the rules.</p> <p>After you add an entry, you can't modify it; you must either replace it, or create an entry and delete the old one.</p> <p>For more information about network ACLs, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html">Network ACLs</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_network_acl_entry(&mut self,arg: &CreateNetworkAclEntryRequest,) -> Result<CreateNetworkAclEntryResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateNetworkAclEntry", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a network interface in the specified subnet.</p> <p>For more information about network interfaces, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-eni.html">Elastic Network Interfaces</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_network_interface(&mut self,arg: &CreateNetworkInterfaceRequest,) -> Result<CreateNetworkInterfaceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateNetworkInterface", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a placement group that you launch cluster instances into. You must give the group a name that's unique within the scope of your account.</p> <p>For more information about placement groups and cluster instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using_cluster_computing.html">Cluster Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_placement_group(&mut self,arg: &CreatePlacementGroupRequest,) -> Result<CreatePlacementGroupResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreatePlacementGroup", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a listing for Amazon EC2 Reserved Instances to be sold in the Reserved Instance Marketplace. You can submit one Reserved Instance listing at a time. To get a list of your Reserved Instances, you can use the <a>DescribeReservedInstances</a> operation.</p> <p>The Reserved Instance Marketplace matches sellers who want to resell Reserved Instance capacity that they no longer need with buyers who want to purchase additional capacity. Reserved Instances bought and sold through the Reserved Instance Marketplace work like any other Reserved Instances. </p> <p>To sell your Reserved Instances, you must first register as a Seller in the Reserved Instance Marketplace. After completing the registration process, you can create a Reserved Instance Marketplace listing of some or all of your Reserved Instances, and specify the upfront price to receive for them. Your Reserved Instance listings then become available for purchase. To view the details of your Reserved Instance listing, you can use the <a>DescribeReservedInstancesListings</a> operation.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-market-general.html">Reserved Instance Marketplace</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_reserved_instances_listing(&mut self,arg: &CreateReservedInstancesListingRequest,) -> Result<CreateReservedInstancesListingResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateReservedInstancesListing", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a route in a route table within a VPC.</p> <p>You must specify one of the following targets: Internet gateway or virtual private gateway, NAT instance, VPC peering connection, or network interface.</p> <p>When determining how to route traffic, we use the route with the most specific match. For example, let's say the traffic is destined for <code>192.0.2.3</code>, and the route table includes the following two routes:</p> <ul> <li> <p><code>192.0.2.0/24</code> (goes to some target A)</p> </li> <li> <p><code>192.0.2.0/28</code> (goes to some target B)</p> </li> </ul> <p>Both routes apply to the traffic destined for <code>192.0.2.3</code>. However, the second route in the list covers a smaller number of IP addresses and is therefore more specific, so we use that route to determine where to target the traffic.</p> <p>For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_route(&mut self,arg: &CreateRouteRequest,) -> Result<CreateRouteResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateRoute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a route table for the specified VPC. After you create a route table, you can add routes and associate the table with a subnet.</p> <p>For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_route_table(&mut self,arg: &CreateRouteTableRequest,) -> Result<CreateRouteTableResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateRouteTable", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a security group.</p> <p>A security group is for use with instances either in the EC2-Classic platform or in a specific VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html">Amazon EC2 Security Groups</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i> and <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_SecurityGroups.html">Security Groups for Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p> <important> <p>EC2-Classic: You can have up to 500 security groups.</p> <p>EC2-VPC: You can create up to 100 security groups per VPC.</p> </important> <p>When you create a security group, you specify a friendly name of your choice. You can have a security group for use in EC2-Classic with the same name as a security group for use in a VPC. However, you can't have two security groups for use in EC2-Classic with the same name or two security groups for use in a VPC with the same name.</p> <p>You have a default security group for use in EC2-Classic and a default security group for use in your VPC. If you don't specify a security group when you launch an instance, the instance is launched into the appropriate default security group. A default security group includes a default rule that grants instances unrestricted network access to each other.</p> <p>You can add or remove rules from your security groups using <a>AuthorizeSecurityGroupIngress</a>, <a>AuthorizeSecurityGroupEgress</a>, <a>RevokeSecurityGroupIngress</a>, and <a>RevokeSecurityGroupEgress</a>.</p>
pub fn create_security_group(&mut self,arg: &CreateSecurityGroupRequest,) -> Result<CreateSecurityGroupResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateSecurityGroup", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a snapshot of an Amazon EBS volume and stores it in Amazon S3. You can use snapshots for backups, to make copies of Amazon EBS volumes, and to save data before shutting down an instance.</p> <p>When a snapshot is created, any AWS Marketplace product codes that are associated with the source volume are propagated to the snapshot.</p> <p>You can take a snapshot of an attached volume that is in use. However, snapshots only capture data that has been written to your Amazon EBS volume at the time the snapshot command is issued; this may exclude any data that has been cached by any applications or the operating system. If you can pause any file systems on the volume long enough to take a snapshot, your snapshot should be complete. However, if you cannot pause all file writes to the volume, you should unmount the volume from within the instance, issue the snapshot command, and then remount the volume to ensure a consistent and complete snapshot. You may remount and use your volume while the snapshot status is <code>pending</code>.</p> <p>To create a snapshot for Amazon EBS volumes that serve as root devices, you should stop the instance before taking the snapshot.</p> <p>Snapshots that are taken from encrypted volumes are automatically encrypted. Volumes that are created from encrypted snapshots are also automatically encrypted. Your encrypted volumes and any associated snapshots always remain protected.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html">Amazon Elastic Block Store</a> and <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html">Amazon EBS Encryption</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_snapshot(&mut self,arg: &CreateSnapshotRequest,) -> Result<Snapshot, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateSnapshot", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a data feed for Spot Instances, enabling you to view Spot Instance usage logs. You can create one data feed per AWS account. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-data-feeds.html">Spot Instance Data Feed</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_spot_datafeed_subscription(&mut self,arg: &CreateSpotDatafeedSubscriptionRequest,) -> Result<CreateSpotDatafeedSubscriptionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateSpotDatafeedSubscription", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a subnet in an existing VPC.</p> <p>When you create each subnet, you provide the VPC ID and the CIDR block you want for the subnet. After you create a subnet, you can't change its CIDR block. The subnet's CIDR block can be the same as the VPC's CIDR block (assuming you want only a single subnet in the VPC), or a subset of the VPC's CIDR block. If you create more than one subnet in a VPC, the subnets' CIDR blocks must not overlap. The smallest subnet (and VPC) you can create uses a /28 netmask (16 IP addresses), and the largest uses a /16 netmask (65,536 IP addresses).</p> <important> <p>AWS reserves both the first four and the last IP address in each subnet's CIDR block. They're not available for use.</p> </important> <p>If you add more than one subnet to a VPC, they're set up in a star topology with a logical router in the middle.</p> <p>If you launch an instance in a VPC using an Amazon EBS-backed AMI, the IP address doesn't change if you stop and restart the instance (unlike a similar instance launched outside a VPC, which gets a new IP address when restarted). It's therefore possible to have a subnet with no running instances (they're all stopped), but no remaining IP addresses available.</p> <p>For more information about subnets, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html">Your VPC and Subnets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_subnet(&mut self,arg: &CreateSubnetRequest,) -> Result<CreateSubnetResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateSubnet", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Adds or overwrites one or more tags for the specified Amazon EC2 resource or resources. Each resource can have a maximum of 10 tags. Each tag consists of a key and optional value. Tag keys must be unique per resource.</p> <p>For more information about tags, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html">Tagging Your Resources</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_tags(&mut self,arg: &CreateTagsRequest,) -> Result<CreateTagsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateTags", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an Amazon EBS volume that can be attached to an instance in the same Availability Zone. The volume is created in the regional endpoint that you send the HTTP request to. For more information see <a href="http://docs.aws.amazon.com/general/latest/gr/rande.html">Regions and Endpoints</a>.</p> <p>You can create a new empty volume or restore a volume from an Amazon EBS snapshot. Any AWS Marketplace product codes from the snapshot are propagated to the volume.</p> <p>You can create encrypted volumes with the <code>Encrypted</code> parameter. Encrypted volumes may only be attached to instances that support Amazon EBS encryption. Volumes that are created from encrypted snapshots are also automatically encrypted. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html">Amazon EBS Encryption</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-creating-volume.html">Creating or Restoring an Amazon EBS Volume</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn create_volume(&mut self,arg: &CreateVolumeRequest,) -> Result<Volume, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVolume", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a VPC with the specified CIDR block.</p> <p>The smallest VPC you can create uses a /28 netmask (16 IP addresses), and the largest uses a /16 netmask (65,536 IP addresses). To help you decide how big to make your VPC, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html">Your VPC and Subnets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p> <p>By default, each instance you launch in the VPC has the default DHCP options, which includes only a default DNS server that we provide (AmazonProvidedDNS). For more information about DHCP options, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_DHCP_Options.html">DHCP Options Sets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_vpc(&mut self,arg: &CreateVpcRequest,) -> Result<CreateVpcResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVpc", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Requests a VPC peering connection between two VPCs: a requester VPC that you own and a peer VPC with which to create the connection. The peer VPC can belong to another AWS account. The requester VPC and peer VPC cannot have overlapping CIDR blocks.</p> <p>The owner of the peer VPC must accept the peering request to activate the peering connection. The VPC peering connection request expires after 7 days, after which it cannot be accepted or rejected.</p> <p>A <code>CreateVpcPeeringConnection</code> request between VPCs with overlapping CIDR blocks results in the VPC peering connection having a status of <code>failed</code>.</p>
pub fn create_vpc_peering_connection(&mut self,arg: &CreateVpcPeeringConnectionRequest,) -> Result<CreateVpcPeeringConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVpcPeeringConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a VPN connection between an existing virtual private gateway and a VPN customer gateway. The only supported connection type is <code>ipsec.1</code>.</p> <p>The response includes information that you need to give to your network administrator to configure your customer gateway.</p> <important> <p>We strongly recommend that you use HTTPS when calling this operation because the response contains sensitive cryptographic information for configuring your customer gateway.</p> </important> <p>If you decide to shut down your VPN connection for any reason and later create a new VPN connection, you must reconfigure your customer gateway with the new information returned from this call.</p> <p>For more information about VPN connections, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_vpn_connection(&mut self,arg: &CreateVpnConnectionRequest,) -> Result<CreateVpnConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVpnConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a static route associated with a VPN connection between an existing virtual private gateway and a VPN customer gateway. The static route allows traffic to be routed from the virtual private gateway to the VPN customer gateway.</p> <p>For more information about VPN connections, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_vpn_connection_route(&mut self,arg: &CreateVpnConnectionRouteRequest,) -> Result<CreateVpnConnectionRouteResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVpnConnectionRoute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a virtual private gateway. A virtual private gateway is the endpoint on the VPC side of your VPN connection. You can create a virtual private gateway before creating the VPC itself.</p> <p>For more information about virtual private gateways, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn create_vpn_gateway(&mut self,arg: &CreateVpnGatewayRequest,) -> Result<CreateVpnGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "CreateVpnGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified customer gateway. You must delete the VPN connection before you can delete the customer gateway.</p>
pub fn delete_customer_gateway(&mut self,arg: &DeleteCustomerGatewayRequest,) -> Result<DeleteCustomerGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteCustomerGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified set of DHCP options. You must disassociate the set of DHCP options before you can delete it. You can disassociate the set of DHCP options by associating either a new set of options or the default set of options with the VPC.</p>
pub fn delete_dhcp_options(&mut self,arg: &DeleteDhcpOptionsRequest,) -> Result<DeleteDhcpOptionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteDhcpOptions", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified Internet gateway. You must detach the Internet gateway from the VPC before you can delete it.</p>
pub fn delete_internet_gateway(&mut self,arg: &DeleteInternetGatewayRequest,) -> Result<DeleteInternetGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteInternetGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified key pair, by removing the public key from Amazon EC2.</p>
pub fn delete_key_pair(&mut self,arg: &DeleteKeyPairRequest,) -> Result<DeleteKeyPairResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteKeyPair", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified network ACL. You can't delete the ACL if it's associated with any subnets. You can't delete the default network ACL.</p>
pub fn delete_network_acl(&mut self,arg: &DeleteNetworkAclRequest,) -> Result<DeleteNetworkAclResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteNetworkAcl", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified ingress or egress entry (rule) from the specified network ACL.</p>
pub fn delete_network_acl_entry(&mut self,arg: &DeleteNetworkAclEntryRequest,) -> Result<DeleteNetworkAclEntryResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteNetworkAclEntry", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified network interface. You must detach the network interface before you can delete it.</p>
pub fn delete_network_interface(&mut self,arg: &DeleteNetworkInterfaceRequest,) -> Result<DeleteNetworkInterfaceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteNetworkInterface", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified placement group. You must terminate all instances in the placement group before you can delete the placement group. For more information about placement groups and cluster instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using_cluster_computing.html">Cluster Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn delete_placement_group(&mut self,arg: &DeletePlacementGroupRequest,) -> Result<DeletePlacementGroupResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeletePlacementGroup", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified route from the specified route table.</p>
pub fn delete_route(&mut self,arg: &DeleteRouteRequest,) -> Result<DeleteRouteResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteRoute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified route table. You must disassociate the route table from any subnets before you can delete it. You can't delete the main route table.</p>
pub fn delete_route_table(&mut self,arg: &DeleteRouteTableRequest,) -> Result<DeleteRouteTableResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteRouteTable", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes a security group.</p> <p>If you attempt to delete a security group that is associated with an instance, or is referenced by another security group, the operation fails with <code>InvalidGroup.InUse</code> in EC2-Classic or <code>DependencyViolation</code> in EC2-VPC.</p>
pub fn delete_security_group(&mut self,arg: &DeleteSecurityGroupRequest,) -> Result<DeleteSecurityGroupResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteSecurityGroup", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified snapshot.</p> <p>When you make periodic snapshots of a volume, the snapshots are incremental, and only the blocks on the device that have changed since your last snapshot are saved in the new snapshot. When you delete a snapshot, only the data not needed for any other snapshot is removed. So regardless of which prior snapshots have been deleted, all active snapshots will have access to all the information needed to restore the volume.</p> <p>You cannot delete a snapshot of the root device of an Amazon EBS volume used by a registered AMI. You must first de-register the AMI before you can delete the snapshot.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-deleting-snapshot.html">Deleting an Amazon EBS Snapshot</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn delete_snapshot(&mut self,arg: &DeleteSnapshotRequest,) -> Result<DeleteSnapshotResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteSnapshot", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the data feed for Spot Instances. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-data-feeds.html">Spot Instance Data Feed</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn delete_spot_datafeed_subscription(&mut self,arg: &DeleteSpotDatafeedSubscriptionRequest,) -> Result<DeleteSpotDatafeedSubscriptionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteSpotDatafeedSubscription", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified subnet. You must terminate all running instances in the subnet before you can delete the subnet.</p>
pub fn delete_subnet(&mut self,arg: &DeleteSubnetRequest,) -> Result<DeleteSubnetResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteSubnet", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified set of tags from the specified set of resources. This call is designed to follow a <code>DescribeTags</code> request.</p> <p>For more information about tags, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html">Tagging Your Resources</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn delete_tags(&mut self,arg: &DeleteTagsRequest,) -> Result<DeleteTagsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteTags", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified Amazon EBS volume. The volume must be in the <code>available</code> state (not attached to an instance).</p> <note> <p>The volume may remain in the <code>deleting</code> state for several minutes.</p> </note> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-deleting-volume.html">Deleting an Amazon EBS Volume</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn delete_volume(&mut self,arg: &DeleteVolumeRequest,) -> Result<DeleteVolumeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVolume", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified VPC. You must detach or delete all gateways and resources that are associated with the VPC before you can delete it. For example, you must terminate all instances running in the VPC, delete all security groups associated with the VPC (except the default one), delete all route tables associated with the VPC (except the default one), and so on.</p>
pub fn delete_vpc(&mut self,arg: &DeleteVpcRequest,) -> Result<DeleteVpcResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVpc", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes a VPC peering connection. Either the owner of the requester VPC or the owner of the peer VPC can delete the VPC peering connection if it's in the <code>active</code> state. The owner of the requester VPC can delete a VPC peering connection in the <code>pending-acceptance</code> state. </p>
pub fn delete_vpc_peering_connection(&mut self,arg: &DeleteVpcPeeringConnectionRequest,) -> Result<DeleteVpcPeeringConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVpcPeeringConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified VPN connection.</p> <p>If you're deleting the VPC and its associated components, we recommend that you detach the virtual private gateway from the VPC and delete the VPC before deleting the VPN connection. If you believe that the tunnel credentials for your VPN connection have been compromised, you can delete the VPN connection and create a new one that has new keys, without needing to delete the VPC or virtual private gateway. If you create a new VPN connection, you must reconfigure the customer gateway using the new configuration information returned with the new VPN connection ID.</p>
pub fn delete_vpn_connection(&mut self,arg: &DeleteVpnConnectionRequest,) -> Result<DeleteVpnConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVpnConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified static route associated with a VPN connection between an existing virtual private gateway and a VPN customer gateway. The static route allows traffic to be routed from the virtual private gateway to the VPN customer gateway.</p>
pub fn delete_vpn_connection_route(&mut self,arg: &DeleteVpnConnectionRouteRequest,) -> Result<DeleteVpnConnectionRouteResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVpnConnectionRoute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deletes the specified virtual private gateway. We recommend that before you delete a virtual private gateway, you detach it from the VPC and delete the VPN connection. Note that you don't need to delete the virtual private gateway if you plan to delete and recreate the VPN connection between your VPC and your network.</p>
pub fn delete_vpn_gateway(&mut self,arg: &DeleteVpnGatewayRequest,) -> Result<DeleteVpnGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeleteVpnGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Deregisters the specified AMI. After you deregister an AMI, it can't be used to launch new instances.</p> <p>This command does not delete the AMI.</p>
pub fn deregister_image(&mut self,arg: &DeregisterImageRequest,) -> Result<DeregisterImageResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DeregisterImage", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes attributes of your AWS account. The following are the supported account attributes:</p> <ul> <li> <p><code>supported-platforms</code>: Indicates whether your account can launch instances into EC2-Classic and EC2-VPC, or only into EC2-VPC.</p> </li> <li> <p><code>default-vpc</code>: The ID of the default VPC for your account, or <code>none</code>.</p> </li> <li> <p><code>max-instances</code>: The maximum number of On-Demand instances that you can run.</p> </li> <li> <p><code>vpc-max-security-groups-per-interface</code>: The maximum number of security groups that you can assign to a network interface.</p> </li> <li> <p><code>max-elastic-ips</code>: The maximum number of Elastic IP addresses that you can allocate for use with EC2-Classic. </p> </li> <li> <p><code>vpc-max-elastic-ips</code>: The maximum number of Elastic IP addresses that you can allocate for use with EC2-VPC.</p> </li> </ul>
pub fn describe_account_attributes(&mut self,arg: &DescribeAccountAttributesRequest,) -> Result<DescribeAccountAttributesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeAccountAttributes", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your Elastic IP addresses.</p> <p>An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html">Elastic IP Addresses</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_addresses(&mut self,arg: &DescribeAddressesRequest,) -> Result<DescribeAddressesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeAddresses", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of the Availability Zones that are available to you. The results include zones only for the region you're currently using. If there is an event impacting an Availability Zone, you can use this request to view the state and any provided message for that Availability Zone.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html">Regions and Availability Zones</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_availability_zones(&mut self,arg: &DescribeAvailabilityZonesRequest,) -> Result<DescribeAvailabilityZonesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeAvailabilityZones", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your bundling tasks.</p> <note><p>Completed bundle tasks are listed for only a limited time. If your bundle task is no longer in the list, you can still register an AMI from it. Just use <code>RegisterImage</code> with the Amazon S3 bucket name and image manifest name you provided to the bundle task.</p></note>
pub fn describe_bundle_tasks(&mut self,arg: &DescribeBundleTasksRequest,) -> Result<DescribeBundleTasksResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeBundleTasks", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your linked EC2-Classic instances. This request only returns information about EC2-Classic instances linked to a VPC through ClassicLink; you cannot use this request to return information about other instances.</p>
pub fn describe_classic_link_instances(&mut self,arg: &DescribeClassicLinkInstancesRequest,) -> Result<DescribeClassicLinkInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeClassicLinkInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your conversion tasks. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/UploadingYourInstancesandVolumes.html">Using the Command Line Tools to Import Your Virtual Machine to Amazon EC2</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_conversion_tasks(&mut self,arg: &DescribeConversionTasksRequest,) -> Result<DescribeConversionTasksResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeConversionTasks", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your VPN customer gateways.</p> <p>For more information about VPN customer gateways, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_customer_gateways(&mut self,arg: &DescribeCustomerGatewaysRequest,) -> Result<DescribeCustomerGatewaysResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeCustomerGateways", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your DHCP options sets.</p> <p>For more information about DHCP options sets, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_DHCP_Options.html">DHCP Options Sets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_dhcp_options(&mut self,arg: &DescribeDhcpOptionsRequest,) -> Result<DescribeDhcpOptionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeDhcpOptions", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your export tasks.</p>
pub fn describe_export_tasks(&mut self,arg: &DescribeExportTasksRequest,) -> Result<DescribeExportTasksResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeExportTasks", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified attribute of the specified AMI. You can specify only one attribute at a time.</p>
pub fn describe_image_attribute(&mut self,arg: &DescribeImageAttributeRequest,) -> Result<ImageAttribute, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeImageAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of the images (AMIs, AKIs, and ARIs) available to you. Images available to you include public images, private images that you own, and private images owned by other AWS accounts but for which you have explicit launch permissions.</p> <note><p>Deregistered images are included in the returned results for an unspecified interval after deregistration.</p></note>
pub fn describe_images(&mut self,arg: &DescribeImagesRequest,) -> Result<DescribeImagesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeImages", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified attribute of the specified instance. You can specify only one attribute at a time. Valid attribute values are: <code>instanceType</code> | <code>kernel</code> | <code>ramdisk</code> | <code>userData</code> | <code>disableApiTermination</code> | <code>instanceInitiatedShutdownBehavior</code> | <code>rootDeviceName</code> | <code>blockDeviceMapping</code> | <code>productCodes</code> | <code>sourceDestCheck</code> | <code>groupSet</code> | <code>ebsOptimized</code> | <code>sriovNetSupport</code></p>
pub fn describe_instance_attribute(&mut self,arg: &DescribeInstanceAttributeRequest,) -> Result<InstanceAttribute, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeInstanceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the status of one or more instances, including any scheduled events.</p> <p>Instance status has two main components:</p> <ul> <li> <p>System Status reports impaired functionality that stems from issues related to the systems that support an instance, such as such as hardware failures and network connectivity problems. This call reports such problems as impaired reachability.</p> </li> <li> <p>Instance Status reports impaired functionality that arises from problems internal to the instance. This call reports such problems as impaired reachability.</p> </li> </ul> <p>Instance status provides information about four types of scheduled events for an instance that may require your attention:</p> <ul> <li> <p>Scheduled Reboot: When Amazon EC2 determines that an instance must be rebooted, the instances status returns one of two event codes: <code>system-reboot</code> or <code>instance-reboot</code>. System reboot commonly occurs if certain maintenance or upgrade operations require a reboot of the underlying host that supports an instance. Instance reboot commonly occurs if the instance must be rebooted, rather than the underlying host. Rebooting events include a scheduled start and end time.</p> </li> <li> <p>System Maintenance: When Amazon EC2 determines that an instance requires maintenance that requires power or network impact, the instance status is the event code <code>system-maintenance</code>. System maintenance is either power maintenance or network maintenance. For power maintenance, your instance will be unavailable for a brief period of time and then rebooted. For network maintenance, your instance will experience a brief loss of network connectivity. System maintenance events include a scheduled start and end time. You will also be notified by email if one of your instances is set for system maintenance. The email message indicates when your instance is scheduled for maintenance.</p> </li> <li> <p>Scheduled Retirement: When Amazon EC2 determines that an instance must be shut down, the instance status is the event code <code>instance-retirement</code>. Retirement commonly occurs when the underlying host is degraded and must be replaced. Retirement events include a scheduled start and end time. You will also be notified by email if one of your instances is set to retiring. The email message indicates when your instance will be permanently retired.</p> </li> <li> <p>Scheduled Stop: When Amazon EC2 determines that an instance must be shut down, the instances status returns an event code called <code>instance-stop</code>. Stop events include a scheduled start and end time. You will also be notified by email if one of your instances is set to stop. The email message indicates when your instance will be stopped.</p> </li> </ul> <p>When your instance is retired, it will either be terminated (if its root device type is the instance-store) or stopped (if its root device type is an EBS volume). Instances stopped due to retirement will not be restarted, but you can do so manually. You can also avoid retirement of EBS-backed instances by manually restarting your instance when its event code is <code>instance-retirement</code>. This ensures that your instance is started on a different underlying host.</p> <p>For more information about failed status checks, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstances.html">Troubleshooting Instances with Failed Status Checks</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>. For more information about working with scheduled events, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-instances-status-check_sched.html#schedevents_actions">Working with an Instance That Has a Scheduled Event</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_instance_status(&mut self,arg: &DescribeInstanceStatusRequest,) -> Result<DescribeInstanceStatusResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeInstanceStatus", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your instances.</p> <p>If you specify one or more instance IDs, Amazon EC2 returns information for those instances. If you do not specify instance IDs, Amazon EC2 returns information for all relevant instances. If you specify an instance ID that is not valid, an error is returned. If you specify an instance that you do not own, it is not included in the returned results.</p> <p>Recently terminated instances might appear in the returned results. This interval is usually less than one hour.</p>
pub fn describe_instances(&mut self,arg: &DescribeInstancesRequest,) -> Result<DescribeInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your Internet gateways.</p>
pub fn describe_internet_gateways(&mut self,arg: &DescribeInternetGatewaysRequest,) -> Result<DescribeInternetGatewaysResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeInternetGateways", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your key pairs.</p> <p>For more information about key pairs, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html">Key Pairs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_key_pairs(&mut self,arg: &DescribeKeyPairsRequest,) -> Result<DescribeKeyPairsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeKeyPairs", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your network ACLs.</p> <p>For more information about network ACLs, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html">Network ACLs</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_network_acls(&mut self,arg: &DescribeNetworkAclsRequest,) -> Result<DescribeNetworkAclsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeNetworkAcls", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes a network interface attribute. You can specify only one attribute at a time.</p>
pub fn describe_network_interface_attribute(&mut self,arg: &DescribeNetworkInterfaceAttributeRequest,) -> Result<DescribeNetworkInterfaceAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeNetworkInterfaceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your network interfaces.</p>
pub fn describe_network_interfaces(&mut self,arg: &DescribeNetworkInterfacesRequest,) -> Result<DescribeNetworkInterfacesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeNetworkInterfaces", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your placement groups. For more information about placement groups and cluster instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using_cluster_computing.html">Cluster Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_placement_groups(&mut self,arg: &DescribePlacementGroupsRequest,) -> Result<DescribePlacementGroupsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribePlacementGroups", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more regions that are currently available to you.</p> <p>For a list of the regions supported by Amazon EC2, see <a href="http://docs.aws.amazon.com/general/latest/gr/rande.html#ec2_region">Regions and Endpoints</a>.</p>
pub fn describe_regions(&mut self,arg: &DescribeRegionsRequest,) -> Result<DescribeRegionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeRegions", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of the Reserved Instances that you purchased.</p> <p>For more information about Reserved Instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts-on-demand-reserved-instances.html">Reserved Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_reserved_instances(&mut self,arg: &DescribeReservedInstancesRequest,) -> Result<DescribeReservedInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeReservedInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes your account's Reserved Instance listings in the Reserved Instance Marketplace.</p> <p>The Reserved Instance Marketplace matches sellers who want to resell Reserved Instance capacity that they no longer need with buyers who want to purchase additional capacity. Reserved Instances bought and sold through the Reserved Instance Marketplace work like any other Reserved Instances. </p> <p>As a seller, you choose to list some or all of your Reserved Instances, and you specify the upfront price to receive for them. Your Reserved Instances are then listed in the Reserved Instance Marketplace and are available for purchase. </p> <p>As a buyer, you specify the configuration of the Reserved Instance to purchase, and the Marketplace matches what you're searching for with what's available. The Marketplace first sells the lowest priced Reserved Instances to you, and continues to sell available Reserved Instance listings to you until your demand is met. You are charged based on the total price of all of the listings that you purchase.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-market-general.html">Reserved Instance Marketplace</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_reserved_instances_listings(&mut self,arg: &DescribeReservedInstancesListingsRequest,) -> Result<DescribeReservedInstancesListingsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeReservedInstancesListings", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the modifications made to your Reserved Instances. If no parameter is specified, information about all your Reserved Instances modification requests is returned. If a modification ID is specified, only information about the specific modification is returned.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-modifying.html">Modifying Reserved Instances</a> in the Amazon Elastic Compute Cloud User Guide for Linux.</p>
pub fn describe_reserved_instances_modifications(&mut self,arg: &DescribeReservedInstancesModificationsRequest,) -> Result<DescribeReservedInstancesModificationsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeReservedInstancesModifications", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes Reserved Instance offerings that are available for purchase. With Reserved Instances, you purchase the right to launch instances for a period of time. During that time period, you do not receive insufficient capacity errors, and you pay a lower usage rate than the rate charged for On-Demand instances for the actual time used.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-market-general.html">Reserved Instance Marketplace</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_reserved_instances_offerings(&mut self,arg: &DescribeReservedInstancesOfferingsRequest,) -> Result<DescribeReservedInstancesOfferingsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeReservedInstancesOfferings", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your route tables.</p> <p>For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_route_tables(&mut self,arg: &DescribeRouteTablesRequest,) -> Result<DescribeRouteTablesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeRouteTables", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your security groups.</p> <p>A security group is for use with instances either in the EC2-Classic platform or in a specific VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html">Amazon EC2 Security Groups</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i> and <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_SecurityGroups.html">Security Groups for Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_security_groups(&mut self,arg: &DescribeSecurityGroupsRequest,) -> Result<DescribeSecurityGroupsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSecurityGroups", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified attribute of the specified snapshot. You can specify only one attribute at a time.</p> <p>For more information about Amazon EBS snapshots, see <a href='http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSSnapshots.html'>Amazon EBS Snapshots</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_snapshot_attribute(&mut self,arg: &DescribeSnapshotAttributeRequest,) -> Result<DescribeSnapshotAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSnapshotAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of the Amazon EBS snapshots available to you. Available snapshots include public snapshots available for any AWS account to launch, private snapshots that you own, and private snapshots owned by another AWS account but for which you've been given explicit create volume permissions.</p> <p>The create volume permissions fall into the following categories:</p> <ul> <li> <i>public</i>: The owner of the snapshot granted create volume permissions for the snapshot to the <code>all</code> group. All AWS accounts have create volume permissions for these snapshots.</li> <li> <i>explicit</i>: The owner of the snapshot granted create volume permissions to a specific AWS account.</li> <li> <i>implicit</i>: An AWS account has implicit create volume permissions for all snapshots it owns.</li> </ul> <p>The list of snapshots returned can be modified by specifying snapshot IDs, snapshot owners, or AWS accounts with create volume permissions. If no options are specified, Amazon EC2 returns all snapshots for which you have create volume permissions.</p> <p>If you specify one or more snapshot IDs, only snapshots that have the specified IDs are returned. If you specify an invalid snapshot ID, an error is returned. If you specify a snapshot ID for which you do not have access, it is not included in the returned results.</p> <p>If you specify one or more snapshot owners, only snapshots from the specified owners and for which you have access are returned. The results can include the AWS account IDs of the specified owners, <code>amazon</code> for snapshots owned by Amazon, or <code>self</code> for snapshots that you own.</p> <p>If you specify a list of restorable users, only snapshots with create snapshot permissions for those users are returned. You can specify AWS account IDs (if you own the snapshots), <code>self</code> for snapshots for which you own or have explicit permissions, or <code>all</code> for public snapshots.</p> <p>If you are describing a long list of snapshots, you can paginate the output to make the list more manageable. The <code>MaxResults</code> parameter sets the maximum number of results returned in a single page. If the list of results exceeds your <code>MaxResults</code> value, then that number of results is returned along with a <code>NextToken</code> value that can be passed to a subsequent <code>DescribeSnapshots</code> request to retrieve the remaining results.</p> <p>For more information about Amazon EBS snapshots, see <a href='http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSSnapshots.html'>Amazon EBS Snapshots</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_snapshots(&mut self,arg: &DescribeSnapshotsRequest,) -> Result<DescribeSnapshotsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSnapshots", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the data feed for Spot Instances. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-data-feeds.html">Spot Instance Data Feed</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_spot_datafeed_subscription(&mut self,arg: &DescribeSpotDatafeedSubscriptionRequest,) -> Result<DescribeSpotDatafeedSubscriptionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSpotDatafeedSubscription", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the Spot Instance requests that belong to your account. Spot Instances are instances that Amazon EC2 launches when the bid price that you specify exceeds the current Spot Price. Amazon EC2 periodically sets the Spot Price based on available Spot Instance capacity and current Spot Instance requests. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-requests.html">Spot Instance Requests</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>You can use <code>DescribeSpotInstanceRequests</code> to find a running Spot Instance by examining the response. If the status of the Spot Instance is <code>fulfilled</code>, the instance ID appears in the response and contains the identifier of the instance. Alternatively, you can use <a>DescribeInstances</a> with a filter to look for instances where the instance lifecycle is <code>spot</code>.</p>
pub fn describe_spot_instance_requests(&mut self,arg: &DescribeSpotInstanceRequestsRequest,) -> Result<DescribeSpotInstanceRequestsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSpotInstanceRequests", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the Spot Price history. The prices returned are listed in chronological order, from the oldest to the most recent, for up to the past 90 days. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-spot-instances-history.html">Spot Instance Pricing History</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>When you specify a start and end time, this operation returns the prices of the instance types within the time range that you specified and the time when the price changed. The price is valid within the time period that you specified; the response merely indicates the last time that the price changed.</p>
pub fn describe_spot_price_history(&mut self,arg: &DescribeSpotPriceHistoryRequest,) -> Result<DescribeSpotPriceHistoryResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSpotPriceHistory", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your subnets.</p> <p>For more information about subnets, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html">Your VPC and Subnets</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_subnets(&mut self,arg: &DescribeSubnetsRequest,) -> Result<DescribeSubnetsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeSubnets", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of the tags for your EC2 resources.</p> <p>For more information about tags, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html">Tagging Your Resources</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_tags(&mut self,arg: &DescribeTagsRequest,) -> Result<DescribeTagsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeTags", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified attribute of the specified volume. You can specify only one attribute at a time.</p> <p>For more information about Amazon EBS volumes, see <a href='http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumes.html'>Amazon EBS Volumes</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_volume_attribute(&mut self,arg: &DescribeVolumeAttributeRequest,) -> Result<DescribeVolumeAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVolumeAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the status of the specified volumes. Volume status provides the result of the checks performed on your volumes to determine events that can impair the performance of your volumes. The performance of a volume can be affected if an issue occurs on the volume's underlying host. If the volume's underlying host experiences a power outage or system issue, after the system is restored, there could be data inconsistencies on the volume. Volume events notify you if this occurs. Volume actions notify you if any action needs to be taken in response to the event.</p> <p>The <code>DescribeVolumeStatus</code> operation provides the following information about the specified volumes:</p> <p><i>Status</i>: Reflects the current status of the volume. The possible values are <code>ok</code>, <code>impaired</code> , <code>warning</code>, or <code>insufficient-data</code>. If all checks pass, the overall status of the volume is <code>ok</code>. If the check fails, the overall status is <code>impaired</code>. If the status is <code>insufficient-data</code>, then the checks may still be taking place on your volume at the time. We recommend that you retry the request. For more information on volume status, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-volume-status.html">Monitoring the Status of Your Volumes</a>.</p> <p><i>Events</i>: Reflect the cause of a volume status and may require you to take action. For example, if your volume returns an <code>impaired</code> status, then the volume event might be <code>potential-data-inconsistency</code>. This means that your volume has been affected by an issue with the underlying host, has all I/O operations disabled, and may have inconsistent data.</p> <p><i>Actions</i>: Reflect the actions you may have to take in response to an event. For example, if the status of the volume is <code>impaired</code> and the volume event shows <code>potential-data-inconsistency</code>, then the action shows <code>enable-volume-io</code>. This means that you may want to enable the I/O operations for the volume by calling the <a>EnableVolumeIO</a> action and then check the volume for data consistency.</p> <note> <p>Volume status is based on the volume status checks, and does not reflect the volume state. Therefore, volume status does not indicate volumes in the <code>error</code> state (for example, when a volume is incapable of accepting I/O.)</p> </note>
pub fn describe_volume_status(&mut self,arg: &DescribeVolumeStatusRequest,) -> Result<DescribeVolumeStatusResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVolumeStatus", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified Amazon EBS volumes.</p> <p>If you are describing a long list of volumes, you can paginate the output to make the list more manageable. The <code>MaxResults</code> parameter sets the maximum number of results returned in a single page. If the list of results exceeds your <code>MaxResults</code> value, then that number of results is returned along with a <code>NextToken</code> value that can be passed to a subsequent <code>DescribeVolumes</code> request to retrieve the remaining results.</p> <p>For more information about Amazon EBS volumes, see <a href='http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumes.html'>Amazon EBS Volumes</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn describe_volumes(&mut self,arg: &DescribeVolumesRequest,) -> Result<DescribeVolumesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVolumes", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the specified attribute of the specified VPC. You can specify only one attribute at a time.</p>
pub fn describe_vpc_attribute(&mut self,arg: &DescribeVpcAttributeRequest,) -> Result<DescribeVpcAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpcAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes the ClassicLink status of one or more VPCs. </p>
pub fn describe_vpc_classic_link(&mut self,arg: &DescribeVpcClassicLinkRequest,) -> Result<DescribeVpcClassicLinkResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpcClassicLink", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your VPC peering connections.</p>
pub fn describe_vpc_peering_connections(&mut self,arg: &DescribeVpcPeeringConnectionsRequest,) -> Result<DescribeVpcPeeringConnectionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpcPeeringConnections", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your VPCs.</p>
pub fn describe_vpcs(&mut self,arg: &DescribeVpcsRequest,) -> Result<DescribeVpcsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpcs", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your VPN connections.</p> <p>For more information about VPN connections, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding a Hardware Virtual Private Gateway to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_vpn_connections(&mut self,arg: &DescribeVpnConnectionsRequest,) -> Result<DescribeVpnConnectionsResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpnConnections", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Describes one or more of your virtual private gateways.</p> <p>For more information about virtual private gateways, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_VPN.html">Adding an IPsec Hardware VPN to Your VPC</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn describe_vpn_gateways(&mut self,arg: &DescribeVpnGatewaysRequest,) -> Result<DescribeVpnGatewaysResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DescribeVpnGateways", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Unlinks (detaches) a linked EC2-Classic instance from a VPC. After the instance has been unlinked, the VPC security groups are no longer associated with it. An instance is automatically unlinked from a VPC when it's stopped.</p>
pub fn detach_classic_link_vpc(&mut self,arg: &DetachClassicLinkVpcRequest,) -> Result<DetachClassicLinkVpcResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DetachClassicLinkVpc", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Detaches an Internet gateway from a VPC, disabling connectivity between the Internet and the VPC. The VPC must not contain any running instances with Elastic IP addresses.</p>
pub fn detach_internet_gateway(&mut self,arg: &DetachInternetGatewayRequest,) -> Result<DetachInternetGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DetachInternetGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Detaches a network interface from an instance.</p>
pub fn detach_network_interface(&mut self,arg: &DetachNetworkInterfaceRequest,) -> Result<DetachNetworkInterfaceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DetachNetworkInterface", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Detaches an Amazon EBS volume from an instance. Make sure to unmount any file systems on the device within your operating system before detaching the volume. Failure to do so results in the volume being stuck in a busy state while detaching.</p> <p>If an Amazon EBS volume is the root device of an instance, it can't be detached while the instance is running. To detach the root volume, stop the instance first.</p> <p>When a volume with an AWS Marketplace product code is detached from an instance, the product code is no longer associated with the instance.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-detaching-volume.html">Detaching an Amazon EBS Volume</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn detach_volume(&mut self,arg: &DetachVolumeRequest,) -> Result<VolumeAttachment, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DetachVolume", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Detaches a virtual private gateway from a VPC. You do this if you're planning to turn off the VPC and not use it anymore. You can confirm a virtual private gateway has been completely detached from a VPC by describing the virtual private gateway (any attachments to the virtual private gateway are also described).</p> <p>You must wait for the attachment's state to switch to <code>detached</code> before you can delete the VPC or attach a different VPC to the virtual private gateway.</p>
pub fn detach_vpn_gateway(&mut self,arg: &DetachVpnGatewayRequest,) -> Result<DetachVpnGatewayResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DetachVpnGateway", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Disables a virtual private gateway (VGW) from propagating routes to a specified route table of a VPC.</p>
pub fn disable_vgw_route_propagation(&mut self,arg: &DisableVgwRoutePropagationRequest,) -> Result<DisableVgwRoutePropagationResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DisableVgwRoutePropagation", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Disables ClassicLink for a VPC. You cannot disable ClassicLink for a VPC that has EC2-Classic instances linked to it.</p>
pub fn disable_vpc_classic_link(&mut self,arg: &DisableVpcClassicLinkRequest,) -> Result<DisableVpcClassicLinkResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DisableVpcClassicLink", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Disassociates an Elastic IP address from the instance or network interface it's associated with.</p> <p>An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html">Elastic IP Addresses</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>This is an idempotent operation. If you perform the operation more than once, Amazon EC2 doesn't return an error.</p>
pub fn disassociate_address(&mut self,arg: &DisassociateAddressRequest,) -> Result<DisassociateAddressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DisassociateAddress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Disassociates a subnet from a route table.</p> <p>After you perform this action, the subnet no longer uses the routes in the route table. Instead, it uses the routes in the VPC's main route table. For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn disassociate_route_table(&mut self,arg: &DisassociateRouteTableRequest,) -> Result<DisassociateRouteTableResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "DisassociateRouteTable", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Enables a virtual private gateway (VGW) to propagate routes to the specified route table of a VPC.</p>
pub fn enable_vgw_route_propagation(&mut self,arg: &EnableVgwRoutePropagationRequest,) -> Result<EnableVgwRoutePropagationResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "EnableVgwRoutePropagation", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Enables I/O operations for a volume that had I/O operations disabled because the data on the volume was potentially inconsistent.</p>
pub fn enable_volume_io(&mut self,arg: &EnableVolumeIORequest,) -> Result<EnableVolumeIOResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "EnableVolumeIO", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Enables a VPC for ClassicLink. You can then link EC2-Classic instances to your ClassicLink-enabled VPC to allow communication over private IP addresses. You cannot enable your VPC for ClassicLink if any of your VPC's route tables have existing routes for address ranges within the <code>10.0.0.0/8</code> IP address range, excluding local routes for VPCs in the <code>10.0.0.0/16</code> and <code>10.1.0.0/16</code> IP address ranges. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/vpc-classiclink.html">ClassicLink</a> in the Amazon Elastic Compute Cloud User Guide for Linux.</p>
pub fn enable_vpc_classic_link(&mut self,arg: &EnableVpcClassicLinkRequest,) -> Result<EnableVpcClassicLinkResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "EnableVpcClassicLink", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Gets the console output for the specified instance.</p> <p>Instances do not have a physical monitor through which you can view their console output. They also lack physical controls that allow you to power up, reboot, or shut them down. To allow these actions, we provide them through the Amazon EC2 API and command line interface.</p> <p>Instance console output is buffered and posted shortly after instance boot, reboot, and termination. Amazon EC2 preserves the most recent 64 KB output which is available for at least one hour after the most recent post.</p> <p>For Linux instances, the instance console output displays the exact console output that would normally be displayed on a physical monitor attached to a computer. This output is buffered because the instance produces it and then posts it to a store where the instance's owner can retrieve it.</p> <p>For Windows instances, the instance console output includes output from the EC2Config service.</p>
pub fn get_console_output(&mut self,arg: &GetConsoleOutputRequest,) -> Result<GetConsoleOutputResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "GetConsoleOutput", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Retrieves the encrypted administrator password for an instance running Windows.</p> <p>The Windows password is generated at boot if the <code>EC2Config</code> service plugin, <code>Ec2SetPassword</code>, is enabled. This usually only happens the first time an AMI is launched, and then <code>Ec2SetPassword</code> is automatically disabled. The password is not generated for rebundled AMIs unless <code>Ec2SetPassword</code> is enabled before bundling.</p> <p>The password is encrypted using the key pair that you specified when you launched the instance. You must provide the corresponding key pair file.</p> <p>Password generation and encryption takes a few moments. We recommend that you wait up to 15 minutes after launching an instance before trying to retrieve the generated password.</p>
pub fn get_password_data(&mut self,arg: &GetPasswordDataRequest,) -> Result<GetPasswordDataResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "GetPasswordData", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an import instance task using metadata from the specified disk image. After importing the image, you then upload it using the <code>ec2-import-volume</code> command in the EC2 command line tools. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/UploadingYourInstancesandVolumes.html">Using the Command Line Tools to Import Your Virtual Machine to Amazon EC2</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn import_instance(&mut self,arg: &ImportInstanceRequest,) -> Result<ImportInstanceResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ImportInstance", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Imports the public key from an RSA key pair that you created with a third-party tool. Compare this with <a>CreateKeyPair</a>, in which AWS creates the key pair and gives the keys to you (AWS keeps a copy of the public key). With ImportKeyPair, you create the key pair and give AWS just the public key. The private key is never transferred between you and AWS.</p> <p>For more information about key pairs, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html">Key Pairs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn import_key_pair(&mut self,arg: &ImportKeyPairRequest,) -> Result<ImportKeyPairResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ImportKeyPair", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates an import volume task using metadata from the specified disk image. After importing the image, you then upload it using the <code>ec2-import-volume</code> command in the Amazon EC2 command-line interface (CLI) tools. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/UploadingYourInstancesandVolumes.html">Using the Command Line Tools to Import Your Virtual Machine to Amazon EC2</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn import_volume(&mut self,arg: &ImportVolumeRequest,) -> Result<ImportVolumeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ImportVolume", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies the specified attribute of the specified AMI. You can specify only one attribute at a time.</p> <note><p>AWS Marketplace product codes cannot be modified. Images with an AWS Marketplace product code cannot be made public.</p></note>
pub fn modify_image_attribute(&mut self,arg: &ModifyImageAttributeRequest,) -> Result<ModifyImageAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyImageAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies the specified attribute of the specified instance. You can specify only one attribute at a time.</p> <p>To modify some attributes, the instance must be stopped. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_ChangingAttributesWhileInstanceStopped.html">Modifying Attributes of a Stopped Instance</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn modify_instance_attribute(&mut self,arg: &ModifyInstanceAttributeRequest,) -> Result<ModifyInstanceAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyInstanceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies the specified network interface attribute. You can specify only one attribute at a time.</p>
pub fn modify_network_interface_attribute(&mut self,arg: &ModifyNetworkInterfaceAttributeRequest,) -> Result<ModifyNetworkInterfaceAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyNetworkInterfaceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies the Availability Zone, instance count, instance type, or network platform (EC2-Classic or EC2-VPC) of your Reserved Instances. The Reserved Instances to be modified must be identical, except for Availability Zone, network platform, and instance type.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-modifying.html">Modifying Reserved Instances</a> in the Amazon Elastic Compute Cloud User Guide for Linux.</p>
pub fn modify_reserved_instances(&mut self,arg: &ModifyReservedInstancesRequest,) -> Result<ModifyReservedInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyReservedInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Adds or removes permission settings for the specified snapshot. You may add or remove specified AWS account IDs from a snapshot's list of create volume permissions, but you cannot do both in a single API call. If you need to both add and remove account IDs for a snapshot, you must use multiple API calls.</p> <p>For more information on modifying snapshot permissions, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modifying-snapshot-permissions.html">Sharing Snapshots</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <note> <p>Snapshots with AWS Marketplace product codes cannot be made public.</p> </note>
pub fn modify_snapshot_attribute(&mut self,arg: &ModifySnapshotAttributeRequest,) -> Result<ModifySnapshotAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifySnapshotAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies a subnet attribute.</p>
pub fn modify_subnet_attribute(&mut self,arg: &ModifySubnetAttributeRequest,) -> Result<ModifySubnetAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifySubnetAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies a volume attribute.</p> <p>By default, all I/O operations for the volume are suspended when the data on the volume is determined to be potentially inconsistent, to prevent undetectable, latent data corruption. The I/O access to the volume can be resumed by first enabling I/O access and then checking the data consistency on your volume.</p> <p>You can change the default behavior to resume I/O operations. We recommend that you change this only for boot volumes or for volumes that are stateless or disposable.</p>
pub fn modify_volume_attribute(&mut self,arg: &ModifyVolumeAttributeRequest,) -> Result<ModifyVolumeAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyVolumeAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Modifies the specified attribute of the specified VPC.</p>
pub fn modify_vpc_attribute(&mut self,arg: &ModifyVpcAttributeRequest,) -> Result<ModifyVpcAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ModifyVpcAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Enables monitoring for a running instance. For more information about monitoring instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-cloudwatch.html">Monitoring Your Instances and Volumes</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn monitor_instances(&mut self,arg: &MonitorInstancesRequest,) -> Result<MonitorInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "MonitorInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Purchases a Reserved Instance for use with your account. With Amazon EC2 Reserved Instances, you obtain a capacity reservation for a certain instance configuration over a specified period of time. You pay a lower usage rate than with On-Demand instances for the time that you actually use the capacity reservation.</p> <p>Use <a>DescribeReservedInstancesOfferings</a> to get a list of Reserved Instance offerings that match your specifications. After you've purchased a Reserved Instance, you can check for your new Reserved Instance with <a>DescribeReservedInstances</a>.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts-on-demand-reserved-instances.html">Reserved Instances</a> and <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ri-market-general.html">Reserved Instance Marketplace</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn purchase_reserved_instances_offering(&mut self,arg: &PurchaseReservedInstancesOfferingRequest,) -> Result<PurchaseReservedInstancesOfferingResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "PurchaseReservedInstancesOffering", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Requests a reboot of one or more instances. This operation is asynchronous; it only queues a request to reboot the specified instances. The operation succeeds if the instances are valid and belong to you. Requests to reboot terminated instances are ignored.</p> <p>If a Linux/Unix instance does not cleanly shut down within four minutes, Amazon EC2 performs a hard reboot.</p> <p>For more information about troubleshooting, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-console.html">Getting Console Output and Rebooting Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn reboot_instances(&mut self,arg: &RebootInstancesRequest,) -> Result<RebootInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RebootInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Registers an AMI. When you're creating an AMI, this is the final step you must complete before you can launch an instance from the AMI. For more information about creating AMIs, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami.html">Creating Your Own AMIs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <note><p>For Amazon EBS-backed instances, <a>CreateImage</a> creates and registers the AMI in a single request, so you don't have to register the AMI yourself.</p></note> <p>You can also use <code>RegisterImage</code> to create an Amazon EBS-backed AMI from a snapshot of a root device volume. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_LaunchingInstanceFromSnapshot.html">Launching an Instance from a Snapshot</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>If needed, you can deregister an AMI at any time. Any modifications you make to an AMI backed by an instance store volume invalidates its registration. If you make changes to an image, deregister the previous image and register the new image.</p> <note><p>You can't register an image where a secondary (non-root) snapshot has AWS Marketplace product codes.</p></note>
pub fn register_image(&mut self,arg: &RegisterImageRequest,) -> Result<RegisterImageResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RegisterImage", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Rejects a VPC peering connection request. The VPC peering connection must be in the <code>pending-acceptance</code> state. Use the <a>DescribeVpcPeeringConnections</a> request to view your outstanding VPC peering connection requests. To delete an active VPC peering connection, or to delete a VPC peering connection request that you initiated, use <a>DeleteVpcPeeringConnection</a>.</p>
pub fn reject_vpc_peering_connection(&mut self,arg: &RejectVpcPeeringConnectionRequest,) -> Result<RejectVpcPeeringConnectionResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RejectVpcPeeringConnection", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Releases the specified Elastic IP address.</p> <p>After releasing an Elastic IP address, it is released to the IP address pool and might be unavailable to you. Be sure to update your DNS records and any servers or devices that communicate with the address. If you attempt to release an Elastic IP address that you already released, you'll get an <code>AuthFailure</code> error if the address is already allocated to another AWS account.</p> <p>[EC2-Classic, default VPC] Releasing an Elastic IP address automatically disassociates it from any instance that it's associated with. To disassociate an Elastic IP address without releasing it, use <a>DisassociateAddress</a>.</p> <p>[Nondefault VPC] You must use <a>DisassociateAddress</a> to disassociate the Elastic IP address before you try to release it. Otherwise, Amazon EC2 returns an error (<code>InvalidIPAddress.InUse</code>).</p>
pub fn release_address(&mut self,arg: &ReleaseAddressRequest,) -> Result<ReleaseAddressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReleaseAddress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Changes which network ACL a subnet is associated with. By default when you create a subnet, it's automatically associated with the default network ACL. For more information about network ACLs, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html">Network ACLs</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn replace_network_acl_association(&mut self,arg: &ReplaceNetworkAclAssociationRequest,) -> Result<ReplaceNetworkAclAssociationResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReplaceNetworkAclAssociation", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Replaces an entry (rule) in a network ACL. For more information about network ACLs, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html">Network ACLs</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn replace_network_acl_entry(&mut self,arg: &ReplaceNetworkAclEntryRequest,) -> Result<ReplaceNetworkAclEntryResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReplaceNetworkAclEntry", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Replaces an existing route within a route table in a VPC. You must provide only one of the following: Internet gateway or virtual private gateway, NAT instance, VPC peering connection, or network interface.</p> <p>For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn replace_route(&mut self,arg: &ReplaceRouteRequest,) -> Result<ReplaceRouteResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReplaceRoute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Changes the route table associated with a given subnet in a VPC. After the operation completes, the subnet uses the routes in the new route table it's associated with. For more information about route tables, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html">Route Tables</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p> <p>You can also use ReplaceRouteTableAssociation to change which table is the main route table in the VPC. You just specify the main route table's association ID and the route table to be the new main route table.</p>
pub fn replace_route_table_association(&mut self,arg: &ReplaceRouteTableAssociationRequest,) -> Result<ReplaceRouteTableAssociationResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReplaceRouteTableAssociation", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Submits feedback about the status of an instance. The instance must be in the <code>running</code> state. If your experience with the instance differs from the instance status returned by <a>DescribeInstanceStatus</a>, use <a>ReportInstanceStatus</a> to report your experience with the instance. Amazon EC2 collects this information to improve the accuracy of status checks.</p> <p>Use of this action does not change the value returned by <a>DescribeInstanceStatus</a>.</p>
pub fn report_instance_status(&mut self,arg: &ReportInstanceStatusRequest,) -> Result<ReportInstanceStatusResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ReportInstanceStatus", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Creates a Spot Instance request. Spot Instances are instances that Amazon EC2 launches when the bid price that you specify exceeds the current Spot Price. Amazon EC2 periodically sets the Spot Price based on available Spot Instance capacity and current Spot Instance requests. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-requests.html">Spot Instance Requests</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn request_spot_instances(&mut self,arg: &RequestSpotInstancesRequest,) -> Result<RequestSpotInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RequestSpotInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Resets an attribute of an AMI to its default value.</p> <note><p> The productCodes attribute can't be reset. </p></note>
pub fn reset_image_attribute(&mut self,arg: &ResetImageAttributeRequest,) -> Result<ResetImageAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ResetImageAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Resets an attribute of an instance to its default value. To reset the <code>kernel</code> or <code>ramdisk</code>, the instance must be in a stopped state. To reset the <code>SourceDestCheck</code>, the instance can be either running or stopped.</p> <p>The <code>SourceDestCheck</code> attribute controls whether source/destination checking is enabled. The default value is <code>true</code>, which means checking is enabled. This value must be <code>false</code> for a NAT instance to perform NAT. For more information, see <a href="http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_NAT_Instance.html">NAT Instances</a> in the <i>Amazon Virtual Private Cloud User Guide</i>.</p>
pub fn reset_instance_attribute(&mut self,arg: &ResetInstanceAttributeRequest,) -> Result<ResetInstanceAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ResetInstanceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Resets a network interface attribute. You can specify only one attribute at a time.</p>
pub fn reset_network_interface_attribute(&mut self,arg: &ResetNetworkInterfaceAttributeRequest,) -> Result<ResetNetworkInterfaceAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ResetNetworkInterfaceAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Resets permission settings for the specified snapshot.</p> <p>For more information on modifying snapshot permissions, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modifying-snapshot-permissions.html">Sharing Snapshots</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn reset_snapshot_attribute(&mut self,arg: &ResetSnapshotAttributeRequest,) -> Result<ResetSnapshotAttributeResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "ResetSnapshotAttribute", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Removes one or more egress rules from a security group for EC2-VPC. The values that you specify in the revoke request (for example, ports) must match the existing rule's values for the rule to be revoked.</p> <p>Each rule consists of the protocol and the CIDR range or source security group. For the TCP and UDP protocols, you must also specify the destination port or range of ports. For the ICMP protocol, you must also specify the ICMP type and code.</p> <p>Rule changes are propagated to instances within the security group as quickly as possible. However, a small delay might occur.</p>
pub fn revoke_security_group_egress(&mut self,arg: &RevokeSecurityGroupEgressRequest,) -> Result<RevokeSecurityGroupEgressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RevokeSecurityGroupEgress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Removes one or more ingress rules from a security group. The values that you specify in the revoke request (for example, ports) must match the existing rule's values for the rule to be removed.</p> <p>Each rule consists of the protocol and the CIDR range or source security group. For the TCP and UDP protocols, you must also specify the destination port or range of ports. For the ICMP protocol, you must also specify the ICMP type and code.</p> <p>Rule changes are propagated to instances within the security group as quickly as possible. However, a small delay might occur.</p>
pub fn revoke_security_group_ingress(&mut self,arg: &RevokeSecurityGroupIngressRequest,) -> Result<RevokeSecurityGroupIngressResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RevokeSecurityGroupIngress", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Launches the specified number of instances using an AMI for which you have permissions.</p> <p>When you launch an instance, it enters the <code>pending</code> state. After the instance is ready for you, it enters the <code>running</code> state. To check the state of your instance, call <a>DescribeInstances</a>.</p> <p>If you don't specify a security group when launching an instance, Amazon EC2 uses the default security group. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html">Security Groups</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>Linux instances have access to the public key of the key pair at boot. You can use this key to provide secure access to the instance. Amazon EC2 public images use this feature to provide secure access without passwords. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html">Key Pairs</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>You can provide optional user data when launching an instance. For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AESDG-chapter-instancedata.html">Instance Metadata</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>If any of the AMIs have a product code attached for which the user has not subscribed, <code>RunInstances</code> fails.</p> <p>T2 instance types can only be launched into a VPC. If you do not have a default VPC, or if you do not specify a subnet ID in the request, <code>RunInstances</code> fails.</p> <p>For more information about troubleshooting, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_InstanceStraightToTerminated.html">What To Do If An Instance Immediately Terminates</a>, and <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html">Troubleshooting Connecting to Your Instance</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn run_instances(&mut self,arg: &RunInstancesRequest,) -> Result<Reservation, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "RunInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Starts an Amazon EBS-backed AMI that you've previously stopped.</p> <p>Instances that use Amazon EBS volumes as their root devices can be quickly stopped and started. When an instance is stopped, the compute resources are released and you are not billed for hourly instance usage. However, your root partition Amazon EBS volume remains, continues to persist your data, and you are charged for Amazon EBS volume usage. You can restart your instance at any time. Each time you transition an instance from stopped to started, Amazon EC2 charges a full instance hour, even if transitions happen multiple times within a single hour.</p> <p>Before stopping an instance, make sure it is in a state from which it can be restarted. Stopping an instance does not preserve data stored in RAM.</p> <p>Performing this operation on an instance that uses an instance store as its root device returns an error.</p> <p>For more information, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Stop_Start.html">Stopping Instances</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn start_instances(&mut self,arg: &StartInstancesRequest,) -> Result<StartInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "StartInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Stops an Amazon EBS-backed instance. Each time you transition an instance from stopped to started, Amazon EC2 charges a full instance hour, even if transitions happen multiple times within a single hour.</p> <p>You can't start or stop Spot Instances.</p> <p>Instances that use Amazon EBS volumes as their root devices can be quickly stopped and started. When an instance is stopped, the compute resources are released and you are not billed for hourly instance usage. However, your root partition Amazon EBS volume remains, continues to persist your data, and you are charged for Amazon EBS volume usage. You can restart your instance at any time.</p> <p>Before stopping an instance, make sure it is in a state from which it can be restarted. Stopping an instance does not preserve data stored in RAM.</p> <p>Performing this operation on an instance that uses an instance store as its root device returns an error.</p> <p>You can stop, start, and terminate EBS-backed instances. You can only terminate instance store-backed instances. What happens to an instance differs if you stop it or terminate it. For example, when you stop an instance, the root device and any other devices attached to the instance persist. When you terminate an instance, the root device and any other devices attached during the instance launch are automatically deleted. For more information about the differences between stopping and terminating instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html">Instance Lifecycle</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>For more information about troubleshooting, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesStopping.html">Troubleshooting Stopping Your Instance</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn stop_instances(&mut self,arg: &StopInstancesRequest,) -> Result<StopInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "StopInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Shuts down one or more instances. This operation is idempotent; if you terminate an instance more than once, each call succeeds.</p> <p>Terminated instances remain visible after termination (for approximately one hour).</p> <p>By default, Amazon EC2 deletes all Amazon EBS volumes that were attached when the instance launched. Volumes attached after instance launch continue running.</p> <p>You can stop, start, and terminate EBS-backed instances. You can only terminate instance store-backed instances. What happens to an instance differs if you stop it or terminate it. For example, when you stop an instance, the root device and any other devices attached to the instance persist. When you terminate an instance, the root device and any other devices attached during the instance launch are automatically deleted. For more information about the differences between stopping and terminating instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html">Instance Lifecycle</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p> <p>For more information about troubleshooting, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesShuttingDown.html">Troubleshooting Terminating Your Instance</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn terminate_instances(&mut self,arg: &TerminateInstancesRequest,) -> Result<TerminateInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "TerminateInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Unassigns one or more secondary private IP addresses from a network interface.</p>
pub fn unassign_private_ip_addresses(&mut self,arg: &UnassignPrivateIpAddressesRequest,) -> Result<UnassignPrivateIpAddressesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "UnassignPrivateIpAddresses", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
/// <p>Disables monitoring for a running instance. For more information about monitoring instances, see <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-cloudwatch.html">Monitoring Your Instances and Volumes</a> in the <i>Amazon Elastic Compute Cloud User Guide for Linux</i>.</p>
pub fn unmonitor_instances(&mut self,arg: &UnmonitorInstancesRequest,) -> Result<UnmonitorInstancesResponse, Error> {
    
 let resp = try!(self.request(Method::Post, "/", "UnmonitorInstances", arg));
 let r = try!(de::from_str(&resp));
 Ok(r)
}
}
