



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteKeyPairRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="KeyName", rename_serialize="KeyName")]
    pub key_name: String,
}


