
use ::date_time::*;
use ::status_name::*;
use ::status_type::*;


/// <p>Describes the instance status.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InstanceStatusDetails {

    #[serde(rename_deserialize="impairedSince", rename_serialize="ImpairedSince")]
    pub impaired_since: Option<DateTime>,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: Option<StatusName>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<StatusType>,
}


