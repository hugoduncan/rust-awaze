
use ::attachment_status::*;
use ::date_time::*;


/// <p>Describes a network interface attachment.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkInterfaceAttachment {

    #[serde(rename_deserialize="attachTime", rename_serialize="AttachTime")]
    pub attach_time: Option<DateTime>,
    #[serde(rename_deserialize="attachmentId", rename_serialize="AttachmentId")]
    pub attachment_id: Option<String>,
    #[serde(rename_deserialize="deleteOnTermination", rename_serialize="DeleteOnTermination")]
    pub delete_on_termination: Option<bool>,
    #[serde(rename_deserialize="deviceIndex", rename_serialize="DeviceIndex")]
    pub device_index: Option<i32>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="instanceOwnerId", rename_serialize="InstanceOwnerId")]
    pub instance_owner_id: Option<String>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<AttachmentStatus>,
}


