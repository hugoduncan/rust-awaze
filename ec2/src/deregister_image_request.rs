



#[derive(Debug, Serialize, Deserialize)]
pub struct DeregisterImageRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_id: String,
}


