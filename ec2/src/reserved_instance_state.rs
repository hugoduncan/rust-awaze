



#[derive(Debug, Clone)]
pub enum ReservedInstanceState {
   PaymentPending,Active,PaymentFailed,Retired,
}

string_enum!{ ReservedInstanceState,
    ReservedInstanceState::PaymentPending => "payment-pending" => ReservedInstanceState::PaymentPending,
    ReservedInstanceState::Active => "active" => ReservedInstanceState::Active,
    ReservedInstanceState::PaymentFailed => "payment-failed" => ReservedInstanceState::PaymentFailed,
    ReservedInstanceState::Retired => "retired" => ReservedInstanceState::Retired

}