


/// <p>Describes VPN connection options.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpnConnectionOptionsSpecification {

    #[serde(rename_deserialize="staticRoutesOnly", rename_serialize="StaticRoutesOnly")]
    pub static_routes_only: Option<bool>,
}


