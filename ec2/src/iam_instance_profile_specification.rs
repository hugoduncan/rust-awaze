


/// <p>Describes an IAM instance profile.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct IamInstanceProfileSpecification {

    #[serde(rename_deserialize="arn", rename_serialize="Arn")]
    pub arn: Option<String>,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: Option<String>,
}


