



#[derive(Debug, Serialize, Deserialize)]
pub struct ReleaseAddressRequest {

    #[serde(rename_deserialize="AllocationId", rename_serialize="AllocationId")]
    pub allocation_id: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="PublicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
}


