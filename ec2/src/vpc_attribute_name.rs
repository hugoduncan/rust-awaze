



#[derive(Debug, Clone)]
pub enum VpcAttributeName {
   EnableDnsSupport,EnableDnsHostnames,
}

string_enum!{ VpcAttributeName,
    VpcAttributeName::EnableDnsSupport => "enableDnsSupport" => VpcAttributeName::EnableDnsSupport,
    VpcAttributeName::EnableDnsHostnames => "enableDnsHostnames" => VpcAttributeName::EnableDnsHostnames

}