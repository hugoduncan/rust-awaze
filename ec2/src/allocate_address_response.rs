
use ::domain_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AllocateAddressResponse {

    #[serde(rename_deserialize="allocationId", rename_serialize="AllocationId")]
    pub allocation_id: Option<String>,
    #[serde(rename_deserialize="domain", rename_serialize="Domain")]
    pub domain: Option<DomainType>,
    #[serde(rename_deserialize="publicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


