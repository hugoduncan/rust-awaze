
use ::propagating_vgw_list::*;
use ::route_list::*;
use ::route_table_association_list::*;
use ::tag_list::*;


/// <p>Describes a route table.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct RouteTable {

    #[serde(rename_deserialize="associationSet", rename_serialize="AssociationSet")]
    pub associations: Option<RouteTableAssociationList>,
    #[serde(rename_deserialize="propagatingVgwSet", rename_serialize="PropagatingVgwSet")]
    pub propagating_vgws: Option<PropagatingVgwList>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: Option<String>,
    #[serde(rename_deserialize="routeSet", rename_serialize="RouteSet")]
    pub routes: Option<RouteList>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


