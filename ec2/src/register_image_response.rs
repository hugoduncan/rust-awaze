



#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterImageResponse {

    #[serde(rename_deserialize="imageId", rename_serialize="ImageId")]
    pub image_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


