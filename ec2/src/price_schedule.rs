
use ::currency_code_values::*;


/// <p>Describes the price for a Reserved Instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PriceSchedule {

    #[serde(rename_deserialize="active", rename_serialize="Active")]
    pub active: Option<bool>,
    #[serde(rename_deserialize="currencyCode", rename_serialize="CurrencyCode")]
    pub currency_code: Option<CurrencyCodeValues>,
    #[serde(rename_deserialize="price", rename_serialize="Price")]
    pub price: Option<f64>,
    #[serde(rename_deserialize="term", rename_serialize="Term")]
    pub term: Option<i64>,
}


