
use ::instance_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct TerminateInstancesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_ids: InstanceIdStringList,
}


