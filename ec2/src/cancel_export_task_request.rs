



#[derive(Debug, Serialize, Deserialize)]
pub struct CancelExportTaskRequest {

    #[serde(rename_deserialize="exportTaskId", rename_serialize="ExportTaskId")]
    pub export_task_id: String,
}


