
use awaze_core::ItemVec;
use ::route_table::*;



pub type RouteTableList = ItemVec<RouteTable>;
