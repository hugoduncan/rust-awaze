
use awaze_core::ItemVec;
use ::launch_permission::*;



pub type LaunchPermissionList = ItemVec<LaunchPermission>;
