



#[derive(Debug, Clone)]
pub enum InstanceAttributeName {
   InstanceType,Kernel,Ramdisk,UserData,DisableApiTermination,InstanceInitiatedShutdownBehavior,RootDeviceName,BlockDeviceMapping,ProductCodes,SourceDestCheck,GroupSet,EbsOptimized,SriovNetSupport,
}

string_enum!{ InstanceAttributeName,
    InstanceAttributeName::InstanceType => "instanceType" => InstanceAttributeName::InstanceType,
    InstanceAttributeName::Kernel => "kernel" => InstanceAttributeName::Kernel,
    InstanceAttributeName::Ramdisk => "ramdisk" => InstanceAttributeName::Ramdisk,
    InstanceAttributeName::UserData => "userData" => InstanceAttributeName::UserData,
    InstanceAttributeName::DisableApiTermination => "disableApiTermination" => InstanceAttributeName::DisableApiTermination,
    InstanceAttributeName::InstanceInitiatedShutdownBehavior => "instanceInitiatedShutdownBehavior" => InstanceAttributeName::InstanceInitiatedShutdownBehavior,
    InstanceAttributeName::RootDeviceName => "rootDeviceName" => InstanceAttributeName::RootDeviceName,
    InstanceAttributeName::BlockDeviceMapping => "blockDeviceMapping" => InstanceAttributeName::BlockDeviceMapping,
    InstanceAttributeName::ProductCodes => "productCodes" => InstanceAttributeName::ProductCodes,
    InstanceAttributeName::SourceDestCheck => "sourceDestCheck" => InstanceAttributeName::SourceDestCheck,
    InstanceAttributeName::GroupSet => "groupSet" => InstanceAttributeName::GroupSet,
    InstanceAttributeName::EbsOptimized => "ebsOptimized" => InstanceAttributeName::EbsOptimized,
    InstanceAttributeName::SriovNetSupport => "sriovNetSupport" => InstanceAttributeName::SriovNetSupport

}