
use awaze_core::ItemVec;
use ::instance_state_change::*;



pub type InstanceStateChangeList = ItemVec<InstanceStateChange>;
