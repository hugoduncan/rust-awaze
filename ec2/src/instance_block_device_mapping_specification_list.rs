
use awaze_core::ItemVec;
use ::instance_block_device_mapping_specification::*;



pub type InstanceBlockDeviceMappingSpecificationList = ItemVec<InstanceBlockDeviceMappingSpecification>;
