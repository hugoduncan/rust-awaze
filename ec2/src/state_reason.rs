


/// <p>Describes a state change.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct StateReason {

    #[serde(rename_deserialize="code", rename_serialize="Code")]
    pub code: Option<String>,
    #[serde(rename_deserialize="message", rename_serialize="Message")]
    pub message: Option<String>,
}


