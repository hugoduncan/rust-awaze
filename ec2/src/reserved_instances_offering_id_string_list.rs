
use awaze_core::ItemVec;



pub type ReservedInstancesOfferingIdStringList = ItemVec<String>;
