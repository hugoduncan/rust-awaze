
use awaze_core::ItemVec;
use ::instance_network_interface_specification::*;



pub type InstanceNetworkInterfaceSpecificationList = ItemVec<InstanceNetworkInterfaceSpecification>;
