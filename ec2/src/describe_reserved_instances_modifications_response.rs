
use ::reserved_instances_modification_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesModificationsResponse {

    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
    #[serde(rename_deserialize="reservedInstancesModificationsSet", rename_serialize="ReservedInstancesModificationsSet")]
    pub reserved_instances_modifications: Option<ReservedInstancesModificationList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


