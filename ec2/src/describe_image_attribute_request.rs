
use ::image_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeImageAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: ImageAttributeName,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ImageId", rename_serialize="ImageId")]
    pub image_id: String,
}


