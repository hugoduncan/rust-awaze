
use ::icmp_type_code::*;
use ::port_range::*;
use ::rule_action::*;


/// <p>Describes an entry in a network ACL.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkAclEntry {

    #[serde(rename_deserialize="cidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: Option<String>,
    #[serde(rename_deserialize="egress", rename_serialize="Egress")]
    pub egress: Option<bool>,
    #[serde(rename_deserialize="icmpTypeCode", rename_serialize="IcmpTypeCode")]
    pub icmp_type_code: Option<IcmpTypeCode>,
    #[serde(rename_deserialize="portRange", rename_serialize="PortRange")]
    pub port_range: Option<PortRange>,
    #[serde(rename_deserialize="protocol", rename_serialize="Protocol")]
    pub protocol: Option<String>,
    #[serde(rename_deserialize="ruleAction", rename_serialize="RuleAction")]
    pub rule_action: Option<RuleAction>,
    #[serde(rename_deserialize="ruleNumber", rename_serialize="RuleNumber")]
    pub rule_number: Option<i32>,
}


