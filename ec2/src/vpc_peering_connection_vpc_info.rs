


/// <p>Describes a VPC in a VPC peering connection.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpcPeeringConnectionVpcInfo {

    #[serde(rename_deserialize="cidrBlock", rename_serialize="CidrBlock")]
    pub cidr_block: Option<String>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: Option<String>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


