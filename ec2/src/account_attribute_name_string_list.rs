
use awaze_core::ItemVec;
use ::account_attribute_name::*;



pub type AccountAttributeNameStringList = ItemVec<AccountAttributeName>;
