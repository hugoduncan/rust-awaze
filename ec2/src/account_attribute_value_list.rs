
use awaze_core::ItemVec;
use ::account_attribute_value::*;



pub type AccountAttributeValueList = ItemVec<AccountAttributeValue>;
