
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeTagsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="maxResults", rename_serialize="MaxResults")]
    pub max_results: Option<i32>,
    #[serde(rename_deserialize="nextToken", rename_serialize="NextToken")]
    pub next_token: Option<String>,
}


