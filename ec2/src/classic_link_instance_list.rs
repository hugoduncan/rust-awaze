
use awaze_core::ItemVec;
use ::classic_link_instance::*;



pub type ClassicLinkInstanceList = ItemVec<ClassicLinkInstance>;
