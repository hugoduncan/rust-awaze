
use awaze_core::ItemVec;
use ::bundle_task::*;



pub type BundleTaskList = ItemVec<BundleTask>;
