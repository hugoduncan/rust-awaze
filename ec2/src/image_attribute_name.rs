



#[derive(Debug, Clone)]
pub enum ImageAttributeName {
   Description,Kernel,Ramdisk,LaunchPermission,ProductCodes,BlockDeviceMapping,
}

string_enum!{ ImageAttributeName,
    ImageAttributeName::Description => "description" => ImageAttributeName::Description,
    ImageAttributeName::Kernel => "kernel" => ImageAttributeName::Kernel,
    ImageAttributeName::Ramdisk => "ramdisk" => ImageAttributeName::Ramdisk,
    ImageAttributeName::LaunchPermission => "launchPermission" => ImageAttributeName::LaunchPermission,
    ImageAttributeName::ProductCodes => "productCodes" => ImageAttributeName::ProductCodes,
    ImageAttributeName::BlockDeviceMapping => "blockDeviceMapping" => ImageAttributeName::BlockDeviceMapping

}