
use awaze_core::ItemVec;
use ::user_id_group_pair::*;



pub type UserIdGroupPairList = ItemVec<UserIdGroupPair>;
