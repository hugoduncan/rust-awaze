
use ::instance_monitoring_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct UnmonitorInstancesResponse {

    #[serde(rename_deserialize="instancesSet", rename_serialize="InstancesSet")]
    pub instance_monitorings: Option<InstanceMonitoringList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


