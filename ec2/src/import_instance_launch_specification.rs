
use ::architecture_values::*;
use ::instance_type::*;
use ::placement::*;
use ::security_group_id_string_list::*;
use ::security_group_string_list::*;
use ::shutdown_behavior::*;
use ::user_data::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportInstanceLaunchSpecification {

    #[serde(rename_deserialize="additionalInfo", rename_serialize="AdditionalInfo")]
    pub additional_info: Option<String>,
    #[serde(rename_deserialize="architecture", rename_serialize="Architecture")]
    pub architecture: Option<ArchitectureValues>,
    #[serde(rename_deserialize="GroupId", rename_serialize="GroupId")]
    pub group_ids: Option<SecurityGroupIdStringList>,
    #[serde(rename_deserialize="GroupName", rename_serialize="GroupName")]
    pub group_names: Option<SecurityGroupStringList>,
    #[serde(rename_deserialize="instanceInitiatedShutdownBehavior", rename_serialize="InstanceInitiatedShutdownBehavior")]
    pub instance_initiated_shutdown_behavior: Option<ShutdownBehavior>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="monitoring", rename_serialize="Monitoring")]
    pub monitoring: Option<bool>,
    #[serde(rename_deserialize="placement", rename_serialize="Placement")]
    pub placement: Option<Placement>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
    #[serde(rename_deserialize="userData", rename_serialize="UserData")]
    pub user_data: Option<UserData>,
}


