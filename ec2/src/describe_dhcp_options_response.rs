
use ::dhcp_options_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeDhcpOptionsResponse {

    #[serde(rename_deserialize="dhcpOptionsSet", rename_serialize="DhcpOptionsSet")]
    pub dhcp_options: Option<DhcpOptionsList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


