



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteVpnGatewayRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_id: String,
}


