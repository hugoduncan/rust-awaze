
use ::instance_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct StartInstancesRequest {

    #[serde(rename_deserialize="additionalInfo", rename_serialize="AdditionalInfo")]
    pub additional_info: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="InstanceId", rename_serialize="InstanceId")]
    pub instance_ids: InstanceIdStringList,
}


