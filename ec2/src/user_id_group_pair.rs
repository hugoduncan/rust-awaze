


/// <p>Describes a security group and AWS account ID pair. </p>
#[derive(Debug, Serialize, Deserialize)]
pub struct UserIdGroupPair {

    #[serde(rename_deserialize="groupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
    #[serde(rename_deserialize="userId", rename_serialize="UserId")]
    pub user_id: Option<String>,
}


