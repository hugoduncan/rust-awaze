
use ::filter_list::*;
use ::offering_type_values::*;
use ::reserved_instances_id_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeReservedInstancesRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="offeringType", rename_serialize="OfferingType")]
    pub offering_type: Option<OfferingTypeValues>,
    #[serde(rename_deserialize="ReservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_ids: Option<ReservedInstancesIdStringList>,
}


