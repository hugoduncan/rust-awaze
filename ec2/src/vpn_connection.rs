
use ::gateway_type::*;
use ::tag_list::*;
use ::vgw_telemetry_list::*;
use ::vpn_connection_options::*;
use ::vpn_state::*;
use ::vpn_static_route_list::*;


/// <p>Describes a VPN connection.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpnConnection {

    #[serde(rename_deserialize="customerGatewayConfiguration", rename_serialize="CustomerGatewayConfiguration")]
    pub customer_gateway_configuration: Option<String>,
    #[serde(rename_deserialize="customerGatewayId", rename_serialize="CustomerGatewayId")]
    pub customer_gateway_id: Option<String>,
    #[serde(rename_deserialize="options", rename_serialize="Options")]
    pub options: Option<VpnConnectionOptions>,
    #[serde(rename_deserialize="routes", rename_serialize="Routes")]
    pub routes: Option<VpnStaticRouteList>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<VpnState>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub gateway_type: Option<GatewayType>,
    #[serde(rename_deserialize="vgwTelemetry", rename_serialize="VgwTelemetry")]
    pub vgw_telemetry: Option<VgwTelemetryList>,
    #[serde(rename_deserialize="vpnConnectionId", rename_serialize="VpnConnectionId")]
    pub vpn_connection_id: Option<String>,
    #[serde(rename_deserialize="vpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_id: Option<String>,
}


