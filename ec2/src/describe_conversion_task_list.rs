
use awaze_core::ItemVec;
use ::conversion_task::*;



pub type DescribeConversionTaskList = ItemVec<ConversionTask>;
