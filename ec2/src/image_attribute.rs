
use ::attribute_value::*;
use ::block_device_mapping_list::*;
use ::launch_permission_list::*;
use ::product_code_list::*;


/// <p>Describes an image attribute.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ImageAttribute {

    #[serde(rename_deserialize="blockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingList>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<AttributeValue>,
    #[serde(rename_deserialize="imageId", rename_serialize="ImageId")]
    pub image_id: Option<String>,
    #[serde(rename_deserialize="kernel", rename_serialize="Kernel")]
    pub kernel_id: Option<AttributeValue>,
    #[serde(rename_deserialize="launchPermission", rename_serialize="LaunchPermission")]
    pub launch_permissions: Option<LaunchPermissionList>,
    #[serde(rename_deserialize="productCodes", rename_serialize="ProductCodes")]
    pub product_codes: Option<ProductCodeList>,
    #[serde(rename_deserialize="ramdisk", rename_serialize="Ramdisk")]
    pub ramdisk_id: Option<AttributeValue>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<AttributeValue>,
}


