
use ::customer_gateway_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeCustomerGatewaysResponse {

    #[serde(rename_deserialize="customerGatewaySet", rename_serialize="CustomerGatewaySet")]
    pub customer_gateways: Option<CustomerGatewayList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


