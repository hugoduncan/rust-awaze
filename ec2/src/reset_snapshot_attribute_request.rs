
use ::snapshot_attribute_name::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ResetSnapshotAttributeRequest {

    #[serde(rename_deserialize="Attribute", rename_serialize="Attribute")]
    pub attribute: SnapshotAttributeName,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="SnapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: String,
}


