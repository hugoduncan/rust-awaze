



#[derive(Debug, Clone)]
pub enum VirtualizationType {
   Hvm,Paravirtual,
}

string_enum!{ VirtualizationType,
    VirtualizationType::Hvm => "hvm" => VirtualizationType::Hvm,
    VirtualizationType::Paravirtual => "paravirtual" => VirtualizationType::Paravirtual

}