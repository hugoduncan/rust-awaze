
use ::volume_status_details_list::*;
use ::volume_status_info_status::*;


/// <p>Describes the status of a volume.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VolumeStatusInfo {

    #[serde(rename_deserialize="details", rename_serialize="Details")]
    pub details: Option<VolumeStatusDetailsList>,
    #[serde(rename_deserialize="status", rename_serialize="Status")]
    pub status: Option<VolumeStatusInfoStatus>,
}


