



#[derive(Debug, Clone)]
pub enum CancelSpotInstanceRequestState {
   Active,Open,Closed,Cancelled,Completed,
}

string_enum!{ CancelSpotInstanceRequestState,
    CancelSpotInstanceRequestState::Active => "active" => CancelSpotInstanceRequestState::Active,
    CancelSpotInstanceRequestState::Open => "open" => CancelSpotInstanceRequestState::Open,
    CancelSpotInstanceRequestState::Closed => "closed" => CancelSpotInstanceRequestState::Closed,
    CancelSpotInstanceRequestState::Cancelled => "cancelled" => CancelSpotInstanceRequestState::Cancelled,
    CancelSpotInstanceRequestState::Completed => "completed" => CancelSpotInstanceRequestState::Completed

}