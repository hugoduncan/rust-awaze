



#[derive(Debug, Clone)]
pub enum ListingState {
   Available,Sold,Cancelled,Pending,
}

string_enum!{ ListingState,
    ListingState::Available => "available" => ListingState::Available,
    ListingState::Sold => "sold" => ListingState::Sold,
    ListingState::Cancelled => "cancelled" => ListingState::Cancelled,
    ListingState::Pending => "pending" => ListingState::Pending

}