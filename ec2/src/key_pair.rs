


/// <p>Describes a key pair.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct KeyPair {

    #[serde(rename_deserialize="keyFingerprint", rename_serialize="KeyFingerprint")]
    pub key_fingerprint: Option<String>,
    #[serde(rename_deserialize="keyMaterial", rename_serialize="KeyMaterial")]
    pub key_material: Option<String>,
    #[serde(rename_deserialize="keyName", rename_serialize="KeyName")]
    pub key_name: Option<String>,
}


