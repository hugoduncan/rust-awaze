
use awaze_core::ItemVec;
use ::network_acl::*;



pub type NetworkAclList = ItemVec<NetworkAcl>;
