
use ::placement_group_state::*;
use ::placement_strategy::*;


/// <p>Describes a placement group.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PlacementGroup {

    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<PlacementGroupState>,
    #[serde(rename_deserialize="strategy", rename_serialize="Strategy")]
    pub strategy: Option<PlacementStrategy>,
}


