
use ::group_identifier_list::*;
use ::instance_list::*;


/// <p>Describes a reservation.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Reservation {

    #[serde(rename_deserialize="groupSet", rename_serialize="GroupSet")]
    pub groups: Option<GroupIdentifierList>,
    #[serde(rename_deserialize="instancesSet", rename_serialize="InstancesSet")]
    pub instances: Option<InstanceList>,
    #[serde(rename_deserialize="ownerId", rename_serialize="OwnerId")]
    pub owner_id: String,
    #[serde(rename_deserialize="requesterId", rename_serialize="RequesterId")]
    pub requester_id: Option<String>,
    #[serde(rename_deserialize="reservationId", rename_serialize="ReservationId")]
    pub reservation_id: String,
}


