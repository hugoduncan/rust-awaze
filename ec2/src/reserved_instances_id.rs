


/// <p>Describes the ID of a Reserved Instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesId {

    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
}


