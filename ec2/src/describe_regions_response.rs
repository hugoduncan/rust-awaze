
use ::region_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeRegionsResponse {

    #[serde(rename_deserialize="regionInfo", rename_serialize="RegionInfo")]
    pub regions: Option<RegionList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


