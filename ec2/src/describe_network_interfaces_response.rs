
use ::network_interface_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkInterfacesResponse {

    #[serde(rename_deserialize="networkInterfaceSet", rename_serialize="NetworkInterfaceSet")]
    pub network_interfaces: Option<NetworkInterfaceList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


