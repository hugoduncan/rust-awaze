
use ::attribute_boolean_value::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyVpcAttributeRequest {

    #[serde(rename_deserialize="EnableDnsHostnames", rename_serialize="EnableDnsHostnames")]
    pub enable_dns_hostnames: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="EnableDnsSupport", rename_serialize="EnableDnsSupport")]
    pub enable_dns_support: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: String,
}


