
use ::dhcp_options_id_string_list::*;
use ::filter_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeDhcpOptionsRequest {

    #[serde(rename_deserialize="DhcpOptionsId", rename_serialize="DhcpOptionsId")]
    pub dhcp_options_ids: Option<DhcpOptionsIdStringList>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
}


