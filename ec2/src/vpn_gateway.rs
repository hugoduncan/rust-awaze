
use ::gateway_type::*;
use ::tag_list::*;
use ::vpc_attachment_list::*;
use ::vpn_state::*;


/// <p>Describes a virtual private gateway.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpnGateway {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<VpnState>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="type", rename_serialize="Type")]
    pub gateway_type: Option<GatewayType>,
    #[serde(rename_deserialize="attachments", rename_serialize="Attachments")]
    pub vpc_attachments: Option<VpcAttachmentList>,
    #[serde(rename_deserialize="vpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_id: Option<String>,
}


