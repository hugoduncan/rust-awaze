
use ::vpn_connection_options_specification::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVpnConnectionRequest {

    #[serde(rename_deserialize="CustomerGatewayId", rename_serialize="CustomerGatewayId")]
    pub customer_gateway_id: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="options", rename_serialize="Options")]
    pub options: Option<VpnConnectionOptionsSpecification>,
    #[serde(rename_deserialize="ConnectionType", rename_serialize="ConnectionType")]
    pub connection_type: Option<String>,
    #[serde(rename_deserialize="VpnGatewayId", rename_serialize="VpnGatewayId")]
    pub vpn_gateway_id: String,
}


