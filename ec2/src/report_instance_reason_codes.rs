



#[derive(Debug, Clone)]
pub enum ReportInstanceReasonCodes {
   InstanceStuckInState,Unresponsive,NotAcceptingCredentials,PasswordNotAvailable,PerformanceNetwork,PerformanceInstanceStore,PerformanceEbsVolume,PerformanceOther,Other,
}

string_enum!{ ReportInstanceReasonCodes,
    ReportInstanceReasonCodes::InstanceStuckInState => "instance-stuck-in-state" => ReportInstanceReasonCodes::InstanceStuckInState,
    ReportInstanceReasonCodes::Unresponsive => "unresponsive" => ReportInstanceReasonCodes::Unresponsive,
    ReportInstanceReasonCodes::NotAcceptingCredentials => "not-accepting-credentials" => ReportInstanceReasonCodes::NotAcceptingCredentials,
    ReportInstanceReasonCodes::PasswordNotAvailable => "password-not-available" => ReportInstanceReasonCodes::PasswordNotAvailable,
    ReportInstanceReasonCodes::PerformanceNetwork => "performance-network" => ReportInstanceReasonCodes::PerformanceNetwork,
    ReportInstanceReasonCodes::PerformanceInstanceStore => "performance-instance-store" => ReportInstanceReasonCodes::PerformanceInstanceStore,
    ReportInstanceReasonCodes::PerformanceEbsVolume => "performance-ebs-volume" => ReportInstanceReasonCodes::PerformanceEbsVolume,
    ReportInstanceReasonCodes::PerformanceOther => "performance-other" => ReportInstanceReasonCodes::PerformanceOther,
    ReportInstanceReasonCodes::Other => "other" => ReportInstanceReasonCodes::Other

}