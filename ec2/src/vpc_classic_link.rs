
use ::tag_list::*;


/// <p>Describes whether a VPC is enabled for ClassicLink.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct VpcClassicLink {

    #[serde(rename_deserialize="classicLinkEnabled", rename_serialize="ClassicLinkEnabled")]
    pub classic_link_enabled: Option<bool>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


