
use ::availability_zone_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeAvailabilityZonesResponse {

    #[serde(rename_deserialize="availabilityZoneInfo", rename_serialize="AvailabilityZoneInfo")]
    pub availability_zones: Option<AvailabilityZoneList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


