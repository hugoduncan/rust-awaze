
use awaze_core::ItemVec;



pub type RegionNameStringList = ItemVec<String>;
