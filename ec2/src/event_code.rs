



#[derive(Debug, Clone)]
pub enum EventCode {
   InstanceReboot,SystemReboot,SystemMaintenance,InstanceRetirement,InstanceStop,
}

string_enum!{ EventCode,
    EventCode::InstanceReboot => "instance-reboot" => EventCode::InstanceReboot,
    EventCode::SystemReboot => "system-reboot" => EventCode::SystemReboot,
    EventCode::SystemMaintenance => "system-maintenance" => EventCode::SystemMaintenance,
    EventCode::InstanceRetirement => "instance-retirement" => EventCode::InstanceRetirement,
    EventCode::InstanceStop => "instance-stop" => EventCode::InstanceStop

}