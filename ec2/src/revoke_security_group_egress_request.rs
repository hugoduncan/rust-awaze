
use ::ip_permission_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct RevokeSecurityGroupEgressRequest {

    #[serde(rename_deserialize="cidrIp", rename_serialize="CidrIp")]
    pub cidr_ip: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="fromPort", rename_serialize="FromPort")]
    pub from_port: Option<i32>,
    #[serde(rename_deserialize="groupId", rename_serialize="GroupId")]
    pub group_id: String,
    #[serde(rename_deserialize="ipPermissions", rename_serialize="IpPermissions")]
    pub ip_permissions: Option<IpPermissionList>,
    #[serde(rename_deserialize="ipProtocol", rename_serialize="IpProtocol")]
    pub ip_protocol: Option<String>,
    #[serde(rename_deserialize="sourceSecurityGroupName", rename_serialize="SourceSecurityGroupName")]
    pub source_security_group_name: Option<String>,
    #[serde(rename_deserialize="sourceSecurityGroupOwnerId", rename_serialize="SourceSecurityGroupOwnerId")]
    pub source_security_group_owner_id: Option<String>,
    #[serde(rename_deserialize="toPort", rename_serialize="ToPort")]
    pub to_port: Option<i32>,
}


