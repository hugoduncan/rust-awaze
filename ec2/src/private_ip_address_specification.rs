


/// <p>Describes a secondary private IP address for a network interface.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PrivateIpAddressSpecification {

    #[serde(rename_deserialize="primary", rename_serialize="Primary")]
    pub primary: Option<bool>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: String,
}


