
use awaze_core::ItemVec;
use ::instance_status::*;



pub type InstanceStatusList = ItemVec<InstanceStatus>;
