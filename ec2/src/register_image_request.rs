
use ::architecture_values::*;
use ::block_device_mapping_request_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterImageRequest {

    #[serde(rename_deserialize="architecture", rename_serialize="Architecture")]
    pub architecture: Option<ArchitectureValues>,
    #[serde(rename_deserialize="BlockDeviceMapping", rename_serialize="BlockDeviceMapping")]
    pub block_device_mappings: Option<BlockDeviceMappingRequestList>,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="ImageLocation", rename_serialize="ImageLocation")]
    pub image_location: Option<String>,
    #[serde(rename_deserialize="kernelId", rename_serialize="KernelId")]
    pub kernel_id: Option<String>,
    #[serde(rename_deserialize="name", rename_serialize="Name")]
    pub name: String,
    #[serde(rename_deserialize="ramdiskId", rename_serialize="RamdiskId")]
    pub ramdisk_id: Option<String>,
    #[serde(rename_deserialize="rootDeviceName", rename_serialize="RootDeviceName")]
    pub root_device_name: Option<String>,
    #[serde(rename_deserialize="sriovNetSupport", rename_serialize="SriovNetSupport")]
    pub sriov_net_support: Option<String>,
    #[serde(rename_deserialize="virtualizationType", rename_serialize="VirtualizationType")]
    pub virtualization_type: Option<String>,
}


