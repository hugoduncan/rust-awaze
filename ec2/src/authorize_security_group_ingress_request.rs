
use ::ip_permission_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct AuthorizeSecurityGroupIngressRequest {

    #[serde(rename_deserialize="CidrIp", rename_serialize="CidrIp")]
    pub cidr_ip: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="FromPort", rename_serialize="FromPort")]
    pub from_port: Option<i32>,
    #[serde(rename_deserialize="GroupId", rename_serialize="GroupId")]
    pub group_id: Option<String>,
    #[serde(rename_deserialize="GroupName", rename_serialize="GroupName")]
    pub group_name: Option<String>,
    #[serde(rename_deserialize="IpPermissions", rename_serialize="IpPermissions")]
    pub ip_permissions: Option<IpPermissionList>,
    #[serde(rename_deserialize="IpProtocol", rename_serialize="IpProtocol")]
    pub ip_protocol: Option<String>,
    #[serde(rename_deserialize="SourceSecurityGroupName", rename_serialize="SourceSecurityGroupName")]
    pub source_security_group_name: Option<String>,
    #[serde(rename_deserialize="SourceSecurityGroupOwnerId", rename_serialize="SourceSecurityGroupOwnerId")]
    pub source_security_group_owner_id: Option<String>,
    #[serde(rename_deserialize="ToPort", rename_serialize="ToPort")]
    pub to_port: Option<i32>,
}


