



#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteRouteRequest {

    #[serde(rename_deserialize="destinationCidrBlock", rename_serialize="DestinationCidrBlock")]
    pub destination_cidr_block: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="routeTableId", rename_serialize="RouteTableId")]
    pub route_table_id: String,
}


