
use ::currency_code_values::*;
use ::date_time::*;
use ::instance_type::*;
use ::offering_type_values::*;
use ::recurring_charges_list::*;
use ::reserved_instance_state::*;
use ::ri_product_description::*;
use ::tag_list::*;
use ::tenancy::*;


/// <p>Describes a Reserved Instance.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstances {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: Option<String>,
    #[serde(rename_deserialize="currencyCode", rename_serialize="CurrencyCode")]
    pub currency_code: Option<CurrencyCodeValues>,
    #[serde(rename_deserialize="duration", rename_serialize="Duration")]
    pub duration: Option<i64>,
    #[serde(rename_deserialize="end", rename_serialize="End")]
    pub end: Option<DateTime>,
    #[serde(rename_deserialize="fixedPrice", rename_serialize="FixedPrice")]
    pub fixed_price: Option<f32>,
    #[serde(rename_deserialize="instanceCount", rename_serialize="InstanceCount")]
    pub instance_count: Option<i32>,
    #[serde(rename_deserialize="instanceTenancy", rename_serialize="InstanceTenancy")]
    pub instance_tenancy: Option<Tenancy>,
    #[serde(rename_deserialize="instanceType", rename_serialize="InstanceType")]
    pub instance_type: Option<InstanceType>,
    #[serde(rename_deserialize="offeringType", rename_serialize="OfferingType")]
    pub offering_type: Option<OfferingTypeValues>,
    #[serde(rename_deserialize="productDescription", rename_serialize="ProductDescription")]
    pub product_description: Option<RIProductDescription>,
    #[serde(rename_deserialize="recurringCharges", rename_serialize="RecurringCharges")]
    pub recurring_charges: Option<RecurringChargesList>,
    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
    #[serde(rename_deserialize="start", rename_serialize="Start")]
    pub start: Option<DateTime>,
    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<ReservedInstanceState>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
    #[serde(rename_deserialize="usagePrice", rename_serialize="UsagePrice")]
    pub usage_price: Option<f32>,
}


