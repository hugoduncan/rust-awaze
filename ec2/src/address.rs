
use ::domain_type::*;


/// <p>Describes an Elastic IP address.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct Address {

    #[serde(rename_deserialize="allocationId", rename_serialize="AllocationId")]
    pub allocation_id: Option<String>,
    #[serde(rename_deserialize="associationId", rename_serialize="AssociationId")]
    pub association_id: Option<String>,
    #[serde(rename_deserialize="domain", rename_serialize="Domain")]
    pub domain: Option<DomainType>,
    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: Option<String>,
    #[serde(rename_deserialize="networkInterfaceOwnerId", rename_serialize="NetworkInterfaceOwnerId")]
    pub network_interface_owner_id: Option<String>,
    #[serde(rename_deserialize="privateIpAddress", rename_serialize="PrivateIpAddress")]
    pub private_ip_address: Option<String>,
    #[serde(rename_deserialize="publicIp", rename_serialize="PublicIp")]
    pub public_ip: Option<String>,
}


