



#[derive(Debug, Serialize, Deserialize)]
pub struct AttachNetworkInterfaceResponse {

    #[serde(rename_deserialize="attachmentId", rename_serialize="AttachmentId")]
    pub attachment_id: Option<String>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


