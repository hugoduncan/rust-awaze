
use ::disk_image_detail::*;
use ::volume_detail::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ImportVolumeRequest {

    #[serde(rename_deserialize="availabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: String,
    #[serde(rename_deserialize="description", rename_serialize="Description")]
    pub description: Option<String>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="image", rename_serialize="Image")]
    pub image: DiskImageDetail,
    #[serde(rename_deserialize="volume", rename_serialize="Volume")]
    pub volume: VolumeDetail,
}


