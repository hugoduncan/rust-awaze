
use awaze_core::ItemVec;
use ::network_interface_private_ip_address::*;



pub type NetworkInterfacePrivateIpAddressList = ItemVec<NetworkInterfacePrivateIpAddress>;
