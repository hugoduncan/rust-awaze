
use ::volume_type::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct CreateVolumeRequest {

    #[serde(rename_deserialize="AvailabilityZone", rename_serialize="AvailabilityZone")]
    pub availability_zone: String,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="encrypted", rename_serialize="Encrypted")]
    pub encrypted: Option<bool>,
    #[serde(rename_deserialize="Iops", rename_serialize="Iops")]
    pub iops: Option<i32>,
    #[serde(rename_deserialize="KmsKeyId", rename_serialize="KmsKeyId")]
    pub kms_key_id: Option<String>,
    #[serde(rename_deserialize="Size", rename_serialize="Size")]
    pub size: Option<i32>,
    #[serde(rename_deserialize="SnapshotId", rename_serialize="SnapshotId")]
    pub snapshot_id: Option<String>,
    #[serde(rename_deserialize="VolumeType", rename_serialize="VolumeType")]
    pub volume_type: Option<VolumeType>,
}


