
use ::reserved_instances_configuration::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedInstancesModificationResponse {

    #[serde(rename_deserialize="reservedInstancesId", rename_serialize="ReservedInstancesId")]
    pub reserved_instances_id: Option<String>,
    #[serde(rename_deserialize="targetConfiguration", rename_serialize="TargetConfiguration")]
    pub target_configuration: Option<ReservedInstancesConfiguration>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


