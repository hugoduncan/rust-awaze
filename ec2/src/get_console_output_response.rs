
use ::date_time::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct GetConsoleOutputResponse {

    #[serde(rename_deserialize="instanceId", rename_serialize="InstanceId")]
    pub instance_id: Option<String>,
    #[serde(rename_deserialize="output", rename_serialize="Output")]
    pub output: Option<String>,
    #[serde(rename_deserialize="timestamp", rename_serialize="Timestamp")]
    pub timestamp: Option<DateTime>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


