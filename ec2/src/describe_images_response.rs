
use ::image_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeImagesResponse {

    #[serde(rename_deserialize="imagesSet", rename_serialize="ImagesSet")]
    pub images: Option<ImageList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


