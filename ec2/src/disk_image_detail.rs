
use ::disk_image_format::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DiskImageDetail {

    #[serde(rename_deserialize="bytes", rename_serialize="Bytes")]
    pub bytes: i64,
    #[serde(rename_deserialize="format", rename_serialize="Format")]
    pub format: DiskImageFormat,
    #[serde(rename_deserialize="importManifestUrl", rename_serialize="ImportManifestUrl")]
    pub import_manifest_url: String,
}


