
use ::filter_list::*;
use ::placement_group_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribePlacementGroupsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="groupName", rename_serialize="GroupName")]
    pub group_names: Option<PlacementGroupStringList>,
}


