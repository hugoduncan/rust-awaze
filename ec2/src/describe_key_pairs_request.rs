
use ::filter_list::*;
use ::key_name_string_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeKeyPairsRequest {

    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="Filter", rename_serialize="Filter")]
    pub filters: Option<FilterList>,
    #[serde(rename_deserialize="KeyName", rename_serialize="KeyName")]
    pub key_names: Option<KeyNameStringList>,
}


