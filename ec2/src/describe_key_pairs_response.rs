
use ::key_pair_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeKeyPairsResponse {

    #[serde(rename_deserialize="keySet", rename_serialize="KeySet")]
    pub key_pairs: Option<KeyPairList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


