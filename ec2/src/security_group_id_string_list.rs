
use awaze_core::ItemVec;



pub type SecurityGroupIdStringList = ItemVec<String>;
