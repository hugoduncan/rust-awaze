
use ::attachment_status::*;


/// <p>Describes the attachment of a VPC to an Internet gateway.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct InternetGatewayAttachment {

    #[serde(rename_deserialize="state", rename_serialize="State")]
    pub state: Option<AttachmentStatus>,
    #[serde(rename_deserialize="vpcId", rename_serialize="VpcId")]
    pub vpc_id: Option<String>,
}


