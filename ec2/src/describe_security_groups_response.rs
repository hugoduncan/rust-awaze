
use ::security_group_list::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeSecurityGroupsResponse {

    #[serde(rename_deserialize="securityGroupInfo", rename_serialize="SecurityGroupInfo")]
    pub security_groups: Option<SecurityGroupList>,
    #[serde(rename_deserialize="requestId", rename_serialize="requestId")]
    pub request_id: String,
}


