
use ::network_interface_attribute::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct DescribeNetworkInterfaceAttributeRequest {

    #[serde(rename_deserialize="attribute", rename_serialize="Attribute")]
    pub attribute: Option<NetworkInterfaceAttribute>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="networkInterfaceId", rename_serialize="NetworkInterfaceId")]
    pub network_interface_id: String,
}


