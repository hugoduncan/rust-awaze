
use awaze_core::ItemVec;



pub type RestorableByStringList = ItemVec<String>;
