


/// <p>Describes a virtual private gateway propagating route.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct PropagatingVgw {

    #[serde(rename_deserialize="gatewayId", rename_serialize="GatewayId")]
    pub gateway_id: Option<String>,
}


