
use ::resource_type::*;


/// <p>Describes a tag.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct TagDescription {

    #[serde(rename_deserialize="key", rename_serialize="Key")]
    pub key: Option<String>,
    #[serde(rename_deserialize="resourceId", rename_serialize="ResourceId")]
    pub resource_id: Option<String>,
    #[serde(rename_deserialize="resourceType", rename_serialize="ResourceType")]
    pub resource_type: Option<ResourceType>,
    #[serde(rename_deserialize="value", rename_serialize="Value")]
    pub value: Option<String>,
}


