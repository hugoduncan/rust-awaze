
use awaze_core::ItemVec;
use ::vpn_connection::*;



pub type VpnConnectionList = ItemVec<VpnConnection>;
