


/// <p>Describes an association between a network ACL and a subnet.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct NetworkAclAssociation {

    #[serde(rename_deserialize="networkAclAssociationId", rename_serialize="NetworkAclAssociationId")]
    pub network_acl_association_id: Option<String>,
    #[serde(rename_deserialize="networkAclId", rename_serialize="NetworkAclId")]
    pub network_acl_id: Option<String>,
    #[serde(rename_deserialize="subnetId", rename_serialize="SubnetId")]
    pub subnet_id: Option<String>,
}


