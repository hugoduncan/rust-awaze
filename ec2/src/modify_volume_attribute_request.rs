
use ::attribute_boolean_value::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ModifyVolumeAttributeRequest {

    #[serde(rename_deserialize="AutoEnableIO", rename_serialize="AutoEnableIO")]
    pub auto_enable_io: Option<AttributeBooleanValue>,
    #[serde(rename_deserialize="dryRun", rename_serialize="DryRun")]
    pub dry_run: Option<bool>,
    #[serde(rename_deserialize="VolumeId", rename_serialize="VolumeId")]
    pub volume_id: String,
}


