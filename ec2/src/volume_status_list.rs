
use awaze_core::ItemVec;
use ::volume_status_item::*;



pub type VolumeStatusList = ItemVec<VolumeStatusItem>;
