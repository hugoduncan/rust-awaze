
use ::container_format::*;
use ::disk_image_format::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ExportToS3Task {

    #[serde(rename_deserialize="containerFormat", rename_serialize="ContainerFormat")]
    pub container_format: Option<ContainerFormat>,
    #[serde(rename_deserialize="diskImageFormat", rename_serialize="DiskImageFormat")]
    pub disk_image_format: Option<DiskImageFormat>,
    #[serde(rename_deserialize="s3Bucket", rename_serialize="S3Bucket")]
    pub s3_bucket: Option<String>,
    #[serde(rename_deserialize="s3Key", rename_serialize="S3Key")]
    pub s3_key: Option<String>,
}


