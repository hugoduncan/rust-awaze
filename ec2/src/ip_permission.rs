
use ::ip_range_list::*;
use ::user_id_group_pair_list::*;


/// <p>Describes a security group rule.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct IpPermission {

    #[serde(rename_deserialize="fromPort", rename_serialize="FromPort")]
    pub from_port: Option<i32>,
    #[serde(rename_deserialize="ipProtocol", rename_serialize="IpProtocol")]
    pub ip_protocol: Option<String>,
    #[serde(rename_deserialize="ipRanges", rename_serialize="IpRanges")]
    pub ip_ranges: Option<IpRangeList>,
    #[serde(rename_deserialize="toPort", rename_serialize="ToPort")]
    pub to_port: Option<i32>,
    #[serde(rename_deserialize="groups", rename_serialize="Groups")]
    pub user_id_group_pairs: Option<UserIdGroupPairList>,
}


