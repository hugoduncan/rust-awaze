
use ::dhcp_configuration_list::*;
use ::tag_list::*;


/// <p>Describes a set of DHCP options.</p>
#[derive(Debug, Serialize, Deserialize)]
pub struct DhcpOptions {

    #[serde(rename_deserialize="dhcpConfigurationSet", rename_serialize="DhcpConfigurationSet")]
    pub dhcp_configurations: Option<DhcpConfigurationList>,
    #[serde(rename_deserialize="dhcpOptionsId", rename_serialize="DhcpOptionsId")]
    pub dhcp_options_id: Option<String>,
    #[serde(rename_deserialize="tagSet", rename_serialize="TagSet")]
    pub tags: Option<TagList>,
}


