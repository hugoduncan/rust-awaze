



#[derive(Debug, Clone)]
pub enum MonitoringState {
   Disabled,Enabled,Pending,
}

string_enum!{ MonitoringState,
    MonitoringState::Disabled => "disabled" => MonitoringState::Disabled,
    MonitoringState::Enabled => "enabled" => MonitoringState::Enabled,
    MonitoringState::Pending => "pending" => MonitoringState::Pending

}